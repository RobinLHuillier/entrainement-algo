import sys, os, subprocess

###### ARGS
# PATH -u -n -e
## PATH TO THE FILES: must have dirs: "input", "output", "sol"
## -u x : unique test on file number x
## -n name : specify the name of the file solution if different from "sol.py"
## -e : message of hope displayed when test fails
###### NOM FICHIER INPUT A CHANGER ICI
nomInput = ""
nomOutput = ""
######################################


print("----------------------------------------")
print("Script d'automatisation du coding battle")
print("----------------------------------------")

# récupérer les arguments
path = ""
i = 0
uniqueTest = -1
solPath = -1
mess = False
while i < len(sys.argv):
    if sys.argv[i][0] != "-":
        path = sys.argv[i]
    elif sys.argv[i][1] == "u":
        i += 1
        uniqueTest = int(sys.argv[i])
    elif sys.argv[i][1] == "n":
        i += 1
        solPath = sys.argv[i]
    elif sys.argv[i][1] == "e":
        mess = True
    i += 1


print("PATH: " + path)
#changer ici selon le type de nom de solution que tu utilises
pathSol = path + "/sol/sol.py"   
if solPath != -1:
    pathSol = path + "sol/" + solPath
pathInput = path + "/input"
pathOutput = path + "/output"
if int(os.popen("ls " + pathSol + " | wc -l").read().split('\n')[0]) != 1:
    print("Solution non trouvée, doit être dans sol/sol.py")
    exit()
else:
    print("Solution trouvée")

nbInput = int(os.popen("ls " + pathInput + " | wc -l").read().split('\n')[0])
if uniqueTest != -1:
    nbInput = 1

print("Nombre d'input à tester: " + str(nbInput))
print("----------------------------------------")

testPassed = 0
testFailed = 0
fail = ""

for numInput in range(1,nbInput+1):
    if uniqueTest != -1:
        numInput = uniqueTest

    #vérifier que output.txt n'existe pas
    suppr = True
    while int(os.popen("find . -maxdepth 1 -name output.txt | wc -l").read().split('\n')[0]) != 0:
        if suppr:
            os.popen("rm output.txt")
            suppr = False

    print("----------------\nTest d'input n°" + str(numInput) + ":")
	#changer ici selon le langage que tu utilises
    """
    if numInput < 10:
        sep = "0"
    else:
        sep = ""
        """
    sep = ""
    #####NOM DU FICHIER INPUT
    os.popen("python3 " + pathSol + " < " + pathInput + "/" + nomInput + sep + str(numInput) + ".in > output.txt").read()

    #vérifier que output.txt a bien été créé
    while int(os.popen("find . -maxdepth 1 -name output.txt | wc -l").read().split('\n')[0]) != 1:
        True

    contenu = []
    solution = []
    with open("output.txt") as output:
        contenu = output.read().split('\n')

    #####NOM DU FICHIER OUTPUT
    with open(pathOutput + "/"+ nomOutput + sep + str(numInput) + ".out") as output:
        solution = output.read().split('\n')

    print("-- Votre solution: \n" + str(contenu) + "\n-- Solution attendue: \n" + str(solution))
    if(contenu == solution):
        print("-- Test réussi")
        testPassed += 1
    else:
        print("-- Test raté")
        testFailed += 1
        fail += str(numInput) + " - "

    if uniqueTest != -1:
        break

os.popen("rm output.txt")
print("-- output supprimé")

print("----------------------------------------")
if testPassed > 0:
    print("Tests réussis: " + str(testPassed) + " / " + str(nbInput) + " ( " + str(int(100*testPassed/nbInput)) + "% )")
if testFailed > 0:
    print("Tests ratés: " + str(testFailed) + " / " + str(nbInput) + " ( " + str(int(100*testFailed/nbInput)) + "% )")
    print("-- Liste: " + fail[0:-2])

    
print("----------------------------------------")
if mess and testFailed > 0:
    print("N'abandonne pas, tu vas finir par y arriver :)")
print("Au revoir")
print("----------------------------------------")