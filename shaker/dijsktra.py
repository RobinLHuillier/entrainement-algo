def dijkstra(n, src, succ):
    dist = [inf] * n
    dist[src] = 0
    prev = [None] * n
    prev[src] = src
    queue = [(0, src)]
    while queue:
        d, u = heappop(queue)
        if d > dist[u]:
            continue
        for v, w in succ(u):
            if dist[v] > d + w:
                dist[v] = d + w
                prev[v] = u
                heappush(queue, (dist[v], v))
    return dist, prev