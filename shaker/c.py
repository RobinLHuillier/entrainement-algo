#*******
#* Read input from STDIN
#* Use print to output your result to STDOUT.
#* Use sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys

lines = []
for line in sys.stdin:
  lines.append(line.rstrip('\n'))

nom = lines[0]
N = int(lines[1])
noms = lines[2].split()
M = int(lines[3])

d = {}
d[nom] = {"cost": -1, "recettes": []}

for n in noms:
    d[n] = {"cost": 1, "recettes": []}

for i in range(4,M+4):
    a,b,c = lines[i].split()
    if a not in d:
        d[a] = {"cost": -1, "recettes": []}
    d[a]["recettes"].append([b,c])


def explore(name, d):
    if name not in d:
        return 1e9
    if d[name]["cost"] != -1:
        return d[name]["cost"]
    minCost = 1e9
    for rec in d[name]["recettes"]:
        cost = explore(rec[0], d) + explore(rec[1], d)
        minCost = min(minCost, cost)
    d[name]["cost"] = minCost
    return minCost

cost = explore(nom, d)
if cost == 1e9:
    print("impossible")
else:
    print(cost)