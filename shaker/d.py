#*******
#* Read input from STDIN
#* Use print to output your result to STDOUT.
#* Use sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys

class Trie:
    def __init__(self):
        self.links = {}
        self.is_word = False
    def add(self, word):
        if word == "":
            self.is_word = True
        else:
            self.links.setdefault(word[0], Trie()).add(word[1:])
    def list(self):
        words = [letter + subw for letter, trie in self.links.items() for subw in trie.list()]
        if self.is_word:
            words.append("")
        return words

    # def i_will_win(self):
    #     for _, trie in self.links.items():
    #         if trie.adv_will_win():
    #             return False
    #     return True

    # def i_cant_win(self):
    #     if self.is_word:
    #         return False
    #     for _, trie in self.links.items():
    #         if not trie.adv_will_win():
    #             return False
    #     return True

    # def adv_will_win(self):
    #     if self.is_word:
    #         return True
    #     for _, trie in self.links.items():
    #         if trie.i_cant_win() and not trie.is_word:
    #             return True
    #     return False
    
    def win(self):
        for _, trie in self.links.items():
            for __, subtrie in trie.links.items():
                if not subtrie.win():
                    return False
        return True


lines = []
for line in sys.stdin:
  lines.append(line.rstrip('\n\r'))
 
N = int(lines[0])
words = lines[1:]

trie = Trie()

d = {}

for w in words: 
    if w in d:
        continue
    trie.add(w)
    d[w] = True

startsOk = []
# print('\n'.join([w for w in trie.list() if w[0] == 'e']))
# print(trie.list())

for letter, subTrie in trie.links.items():
    # print(letter, subTrie.can_win(), subTrie.is_word)
    if subTrie.win():
        startsOk.append(letter)

if len(startsOk) == 0:
    print("impossible")
else:
    for l in sorted(startsOk):
        print(l)