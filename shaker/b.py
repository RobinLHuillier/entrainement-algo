#*******
#* Read input from STDIN
#* Use print to output your result to STDOUT.
#* Use sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys

class Trie:
    def __init__(self):
        self.links = {}
        self.is_word = False
    def add(self, word):
        if word == "":
            self.is_word = True
        else:
            self.links.setdefault(word[0], Trie()).add(word[1:])
    def list(self):
        words = [letter + subw for letter, trie in self.links.items() for subw in trie.list()]
        if self.is_word:
            words.append("")
        return words
    
    # def can_win(self, my_turn=True):
    #     if self.is_word:
    #         return my_turn
    #     for _, t in self.links.items():
    #         print(t.links, not my_turn)
    #         if not t.can_win(not my_turn):
    #             print(t.list(), not my_turn)
    #             return True 
    #     return False 

    # def can_win(self, my_turn=True):
    #     if self.is_word:
    #         return my_turn
    #     if my_turn:
    #         for _, trie in self.links.items():
    #             if trie.can_win(not my_turn):
    #                 return False
    #     else:
    #         for _, trie in self.links.items():
    #             if trie.can_win(not my_turn):
    #                 return False
    #     return not my_turn

    # def can_win(self):
    #     if self.is_word:
    #         return True
    #     for _, trie in self.links.items():
    #         if not trie.will_win():
    #             return False
    #     return False
    
    def will_win(self):
        for _, trie in self.links.items():
            if trie.can_win():
                return False
        return True

    def can_win(self):
        if self.is_word:
            return True
        for _, trie in self.links.items():
            if trie.will_win():
                return False
        return True


lines = []
for line in sys.stdin:
  lines.append(line.rstrip('\n\r'))
 
N = int(lines[0])
words = lines[1:]

trie = Trie()

for w in words: 
    trie.add(w)

startsOk = []
# print(trie.list())

for letter, subTrie in trie.links.items():
    # print(letter, subTrie.can_win(), subTrie.is_word)
    if subTrie.will_win():
        startsOk.append(letter)

if len(startsOk) == 0:
    print("impossible")
else:
    for l in sorted(startsOk):
        print(l)