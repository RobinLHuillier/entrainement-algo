def polygon_area(pts):
    a = 0
    l = len(pts)
    for i in range(l):
        p = pts[i]
        q = pts[(i+1)%l]
        a += (p[0]-q[0])*(p[1]+q[1])
    return abs(a)/2

N = int(input())

totArea = 0
for i in range(N):
    P = int(input())
    pts = []
    for j in range(P):
        x,y = map(int, input().split())
        pts.append((x,y))
    totArea += polygon_area(pts)

print(int(totArea))