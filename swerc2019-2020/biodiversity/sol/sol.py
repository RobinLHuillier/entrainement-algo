N = int(input())
dico = {}
for i in range(N):
    spec = input().split()[0]
    if spec not in dico:
        dico[spec] = 0
    dico[spec] += 1

dicList = list(dico.items())
dicList.sort(reverse=True, key=lambda x:x[1])

if N == 1 or len(dicList) == 1:
    print(dicList[0][0])
elif dicList[0][1] == dicList[1][1] or N-dicList[0][1] > N//2:
    print("NONE")
else:
    print(dicList[0][0])