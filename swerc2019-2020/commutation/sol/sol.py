S, L, N = map(int, input().split())

friends = {}
order = []
for i in range(S):
    spec = input().split()[0]
    friends[spec] = set()
    order.append(spec)

order.sort()
ordre = {}
for i in range(S):
    ordre[order[i]] = i

for i in range(L):
    a, b = input().split()
    friends[a].add(b)
    friends[b].add(a)

l = input().split()

# print(friends)
# print(l)

# print("contracter")

l2 = []
animal = l[0]
quantity = 1
for i in range(1, len(l)):
    if l[i] == animal:
        quantity += 1
    else:
        l2.append((animal, quantity))
        quantity = 1
        animal = l[i]
l2.append((animal, quantity))

# print(l2)

# print("décaler")

i = 1
while i < len(l2):
    animal, quantity = l2[i]
    # print(animal, quantity)
    l2[i] = None
    pos = i-1
    while pos >= 0:
        # print(pos, l2)
        if l2[pos] is None:
            pos -= 1
            continue
        ani2, q2 = l2[pos]
        if ani2 == animal:
            l2[pos] = (ani2, q2+1)
            break
        if ani2 in friends[animal] and \
            ordre[ani2] > ordre[animal]:
            l2[pos+1] = (ani2, q2)
            pos -= 1
            continue
        l2[pos+1] = (animal, quantity)
        break
    if pos < 0:
        l2[0] = (animal, quantity)
    # print("end", l2)
    i += 1

first = True
for k in l2:
    if k is None:
        continue
    animal, quantity = k
    for i in range(quantity):
        if first:
            print(animal, end="")
            first = False
        else:
            print(" "+animal, end="")