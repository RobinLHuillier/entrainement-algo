import sys
import math
import heapq

def distance(x1,y1,x2,y2):
    return math.ceil(math.sqrt((x1-x2)**2 + (y1-y2)**2))

# the beast V3 dict
def dijkstra(n, src, succ, w, kmMax):
    dist = [{"mini":math.inf} for _ in range(n)]
    dist[src] = {0:0}
    seen = set()
    prev = [None for _ in range(n)]
    prev[src] = src
    queue = [(0, src)]
    while queue:
        di, u = heapq.heappop(queue)
        if (u, di) in seen: continue
        seen.add((u, di))
        for v in succ[u]:
            if v == src:
                continue
            # on reconstruit les chemins potentiels
            nco, ndi = w[u][v]
            newOne = False
            for di,co in dist[u].items():
                if di == "mini":
                    continue
                if co != math.inf:
                    co += nco
                    di += ndi
                    if di <= kmMax and (di not in dist[v] or dist[v][di] > co):
                        dist[v][di] = co
                        if co < dist[v]["mini"]:
                            dist[v]["mini"] = co
                        newOne = True
            # si nouveau on ajoute
            if newOne:
                prev[v] = u
                heapq.heappush(queue, (dist[v]["mini"], v))
    return dist, prev

# input de ses morts
xs, ys = map(int, input().split())
xd, yd = map(int, input().split())
B = int(input())
Co = int(input())
T = int(input())
CoCost = [Co] + [int(input())for _ in range(T)]
N = int(input())
# on ajoute aussi la maison et l'arrivée
n = N+2  
succ = [[]for _ in range(n)]
w = [[sys.maxsize]*n for _ in range(n)]
pos = [0]*n
for s in range(N):
    line = [int(a)for a in input().split()]
    x = line[0]
    y = line[1]
    pos[s] = (x,y)
    l = line[2]
    for i in range(l):
        j = line[3+2*i]
        m = line[4+2*i]
        # on ajoute j et s dans les voisins
        if j != s:
            if s not in succ[j]:
                succ[j].append(s)
            if j not in succ[s]:
                succ[s].append(j)
            # si le mode de transport est moins couteux on le met dans la matrice
            if w[j][s] == sys.maxsize or CoCost[m] < CoCost[w[j][s]]:
                w[j][s] = m
                w[s][j] = m
# on a la matrice, succ, pos, on ajoute maison et arrivée
succ[-2] = [i for i in range(N)]
succ[-1] = [i for i in range(N)]
for i in range(N):
    succ[i].append(n-2)
    succ[i].append(n-1)
for i in range(N):
    w[-2][i] = 0
    w[i][-2] = 0
    w[-1][i] = 0
    w[i][-1] = 0
pos[-2] = (xs,ys)
pos[-1] = (xd,yd)
# on met à jour la matrice en ajoutant la distance et en multipliant Co2 par la distance : tuple (coutCo2Total, distance)
for i in range(n):
    for j in range(i+1,n): # on fait que la diagonale
        if w[i][j] < sys.maxsize:
            x1,y1 = pos[i]
            x2,y2 = pos[j]
            d = distance(x1,y1,x2,y2)
            cout = CoCost[w[i][j]]
            total = cout*d
            w[i][j] = (total, d)
            w[j][i] = (total, d)
# on lance
dist, prev = dijkstra(n, n-2, succ, w, B)
# dist[-1].sort(key=lambda x:x[0])
sol = dist[-1]["mini"]
if sol == math.inf:
    # cas particulier: aller en bagnole du départ à l'arrivée?
    if distance(xs,ys,xd,yd) <= B:
        sol = CoCost[0] * distance(xs,ys,xd,yd)
    else:
        sol = -1
print(sol)