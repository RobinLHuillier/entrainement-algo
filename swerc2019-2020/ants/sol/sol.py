N = int(input())
dico = [True for i in range(N+1)]
for i in range(N):
    r = input().split()[0]
    if len(r) <= 7:
        r = int(r)
        if r < N+1 and dico[r]:
            dico[r] = False
for i in range(N+1):
    if dico[i]:
        print(i)
        break