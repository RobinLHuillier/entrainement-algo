# pour chaque nombre
# est-ce qu'il est dans la range (1..n)?
# si non, diviser par deux, incrementer le compteur
# si oui, stocker 1 en pos nb d'un array
# si cet array contient déjà 1, continuer à diviser
# si on atteint 0, faux

def calc(arr, n):
    perm = [0 for i in range(n)]
    for a in arr:
        placed = False
        while a > 0 and not placed:
            if a > n or perm[a-1] == 1:
                a = a//2
            elif perm[a-1] == 0:
                placed = True
                perm[a-1] = 1
        if a == 0:
            print("NO")
            return
    print("YES")
    return

t = int(input())
for i in range(t):
    n = int(input())
    arr = [int(a)for a in input().split()]
    calc(arr,n)