t = int(input())
for i in range(t):
    a,b,c = [int(f)for f in input().split()]
    # print(a,b,c)
    d = c-b
    if b-d > 0 and (b-d)/a == (b-d)//a:
        print("YES") #, d, b-d, (b-d)/a, (b-d)//a)
    else:
        d = (c-a)//2
        if d == (c-a)/2 and c-d > 0 and c-d >= b and (c-d)/b == (c-d)//b:
            print("YES")
        else:
            d = b-a
            if b+d >= c and b+d > 0 and (b+d)/c == (b+d)//c:
                print("YES")
            else:
                print("NO")