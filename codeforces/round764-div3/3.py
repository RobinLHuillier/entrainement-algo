alpha = "abcdefghijklmnopqrstuvwxyz"

def calc(string, n):
    dico = {}
    for a in alpha:
        dico[a] = 0
    for s in string:
        dico[s] += 1
    tri = []
    for key,val in dico.items():
        if val > 0:
            tri.append([key,val])
    dico = sorted(tri,key=lambda x:x[1])[::-1]
    pals = [0 for i in range(n)]
    # tenter d'en mettre un mult de 2 par pal
    impair = False
    while not impair:
        # compter les nombres de paires
        s = 0
        for d in dico:
            s += d[1]//2
        if s < n:
            impair = True
        else:
            # répartir les paires
            posDico = 0
            nbInsert = s//n
            for i in range(n):
                insert = 0
                while insert < nbInsert:
                    if dico[posDico][1] > 1:
                        dico[posDico][1] -= 2
                        pals[i] += 2
                        insert += 1
                    else:
                        posDico += 1
    restant = 0
    for i in range(len(dico)):
        restant += dico[i][1]
    mini = min(pals)
    if restant >= n and mini%2 == 0:
        mini += 1
    return mini

t = int(input())
for i in range(t):
    s,k = [int(a)for a in input().split()]
    string = input().split()[0]
    print(calc(string,k))