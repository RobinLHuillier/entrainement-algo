t = int(input())
for i in range(t):
    input()
    a = [int(b)for b in input().split()]
    avg = int(round(sum(a)/len(a)))
    mini = min(a)
    maxi = max(a)
    if mini <= avg and maxi >= avg:
        print(abs(mini-avg)+abs(maxi-avg))
    else:
        print(max(abs(mini-avg)+abs(maxi-avg)))