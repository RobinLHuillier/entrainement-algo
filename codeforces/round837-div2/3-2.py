import math

for _ in range(int(input())):
    n = int(input())
    l = list(map(int,input().split()))
    stop = False
    for i in range(n):
        if stop:
            break
        for j in range(i+1,n):
            if math.gcd(l[i],l[j]) > 1:
                stop = True
                break
    if stop:
        print("YES")
    else:
        print("NO")