t = int(input())

for _ in range(t):
    n, m = map(int, input().split())
    sol = 0
    blairer = [n for _ in range(n+1)]

    for i in range(m):
        a,b = map(int,input().split())
        if b < a:
            b,a = a,b
        blairer[a] = min(b-1, blairer[a])
        # print(a,b, blairer)
    
    mini = blairer[n]
    for i in range(n,0,-1):
        if blairer[i] > mini:
            blairer[i] = mini
        mini = blairer[i]
    
    for i in range(1,n+1):
        sol += blairer[i]-i+1
    # print(blairer)

    print(sol)