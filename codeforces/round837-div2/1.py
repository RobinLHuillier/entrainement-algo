
for _ in range(int(input())):
    n = int(input())
    l = list(map(int, input().split()))
    l.sort()
    dist = l[-1]-l[0]
    mini = l.count(l[0])
    maxi = l.count(l[-1])
    sol = mini*maxi*2
    if l[0] == l[-1]:
        sol = mini*(mini-1)
    print(sol)
