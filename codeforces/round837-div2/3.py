
facteurDict = dict()
primeList = [2]

def facteurs(n):
    global facteurDict
    global primeList
    if n in facteurDict:
        return facteurDict[n]
    n2 = n
    k = 1
    i = primeList[0]
    factors = set()
    factors.add(n)
    while i * i <= n:
        if n % i:
            if k < len(primeList):
                i = primeList[k]
                k += 1
            else:
                i += 1
        else:
            n //= i
            factors.add(i)
            if primeList[-1] < i:
                primeList.append(i)
                k += 1
    if n > 1:
        factors.add(n)
        if primeList[-1] < n:
            primeList.append(n)
    facteurDict[n2] = factors
    return factors

for _ in range(int(input())):
    n = int(input())
    l = list(map(int,input().split()))
    s = set()
    stop = False
    for i in range(n):
        f = facteurs(l[i])
        # print(f)
        if s-f != s:
            stop = True
            break
        s = s.union(f)
    if stop:
        print("YES")
    else:
        print("NO")

