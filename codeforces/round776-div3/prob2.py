def fVlad(a, x):
    return (x//a) + x%a

t = int(input())
for _ in range(t):
    l, r, a = map(int, input().split())
    res = a*(r//a)-1
    if res >= l:
        res = fVlad(a,res)
    else:
        res = 0
    print(max(res, fVlad(a,r)))