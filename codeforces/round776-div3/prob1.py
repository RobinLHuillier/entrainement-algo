t = int(input())
for _ in range(t):
    s = input().split()[0]
    c = input().split()[0]
    youCan = False
    for i in range(len(s)):
        if s[i] == c:
            if i%2 == 0 :
                youCan = True
                break
    if youCan:
        print("YES")
    else:
        print("NO")