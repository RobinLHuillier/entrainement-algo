t = int(input())
for _ in range(t):
    input()
    n, m = map(int, input().split())
    line = []
    for __ in range(m):
        x, w = map(int, input().split())
        line.append((x,w))
    lineSort = line[:]
    lineSort.sort(key=lambda x:x[1])
    positions = line[:]
    positions.sort(key=lambda x:x[0])
    num_seg = 2*n
    to_elim = m - num_seg
    to_elim = lineSort[m-to_elim:]

    # print("\nNouveau")
    # print(line)
    # print(lineSort)
    # print("pos",positions)
    # print(to_elim)
    # print("Calcul")
    
    start = 0
    end = m-1
    to_out = []
    total = 0
    while start < end:
        # print(start, end)
        while start < n and positions[start] in to_elim:
            to_elim.remove(positions[start])
            # print("elim start",start,positions[start], to_elim)
            start += 1
        while end >= 0 and positions[end] in to_elim:
            to_elim.remove(positions[end])
            # print("elim end",end, positions[end], to_elim)
            end -= 1
        if start >= end:
            break
        p1 = line.index(positions[start])+1
        p2 = line.index(positions[end])+1
        # print(p1,p2,start,end, positions[start], positions[end])
        to_out.append((p1,p2))
        total += line[p1-1][1] + line[p2-1][1]
        start += 1
        end -= 1


    # print("Sortie")
    print(total)
    for t in to_out:
        print(t[0],t[1])
    print()