T = int(input())
for _ in range(T):
    nJob, nOpt = map(int, input().split())
    deadLines = [int(a) for a in input().split()]
    options = []
    for i in range(nOpt):
        e, t, p = map(int, input().split())
        options.append((e,t,p))
    print("NEW")
    print(nJob, nOpt)
    print(deadLines)
    print(options)