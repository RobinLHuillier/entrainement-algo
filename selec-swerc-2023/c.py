n = int(input())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

dp_a = [[0]*(n+1) for _ in range(n+1)]
dp_b = [[0]*(n+1) for _ in range(n+1)]

for i in range(1, n+1):
    for j in range(1, n+1):
        # dp_a[i][j] = max(dp_a[i-1][j-1],
        #                  dp_b[i-1][j-1] + a[i-1],
        #                  dp_a[i-1][j-1] + a[i-1])
        # dp_b[i][j] = max(dp_b[i][j-1],
        #                  dp_a[i][j-1] + b[i-1],
                        #  dp_b[i-1][j-1] + b[i-1])
        dp_a[i][j] = max(dp_a[i-1][j], dp_b[i-1][j] + a[i-1])
        dp_b[i][j] = max(dp_b[i][j-1], dp_a[i][j-1] + b[j-1])

print("a")
for aa in dp_a:
    print(aa)
print("b")
for bb in dp_b:
    print(bb)

print(max(max(dp_a[n]), max(dp_b[n])))
