# https://www.geeksforgeeks.org/maximum-sum-such-that-no-two-elements-are-adjacent/


def rec(nums, idx):
    if idx >= len(nums):
        return 0
    return max(nums[idx] + rec(nums, idx + 2), rec(nums, idx + 1))
 
def findMaxSum(arr, N):
    return rec(arr, 0)

n = int(input())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

a = [0] + a + [0]
b = [0] + b + [0]

arr = list(zip(a, b))
arr = [x for tup in arr for x in tup]

N = len(arr)

print(findMaxSum(arr, N))
