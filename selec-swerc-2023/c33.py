n = int(input())
a = list(map(int, input().split()))
b = list(map(int, input().split()))

dp_a = [0]*n
dp_b = [0]*n

dp_a[0] = a[0]
dp_b[0] = b[0]

for i in range(1, n):
    dp_a[i] = a[i] + max(dp_b[i-1], dp_b[i-2] if i >= 2 else 0)
    dp_b[i] = b[i] + max(dp_a[i-1], dp_a[i-2] if i >= 2 else 0)

max_height = max(dp_a[n-1], dp_b[n-1])

print(max(dp_a[n-1], dp_b[n-1]))