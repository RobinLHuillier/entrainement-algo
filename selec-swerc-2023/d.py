# merci copilot pour le tarjan

def tarjan(graph):
    index = 0
    stack = []
    lowlinks = {}
    index_for_node = {}
    strongly_connected_components = []

    def strongconnect(node):
        nonlocal index
        index_for_node[node] = index
        lowlinks[node] = index
        index += 1
        stack.append(node)

        for neighbor in graph[node]:
            if neighbor not in index_for_node:
                strongconnect(neighbor)
                lowlinks[node] = min(lowlinks[node], lowlinks[neighbor])
            elif neighbor in stack:
                lowlinks[node] = min(lowlinks[node], index_for_node[neighbor])

        if lowlinks[node] == index_for_node[node]:
            component = []
            while True:
                neighbor = stack.pop()
                component.append(neighbor)
                if neighbor == node:
                    break
            strongly_connected_components.append(component)

    for node in graph:
        if node not in index_for_node:
            strongconnect(node)

    return strongly_connected_components

def build(edges):
    graph = {i:[]for i in range(1, len(edges) + 1)}
    for i, edge in enumerate(edges, 1):
        graph[i].append(edge)
    return graph

def reduce(cfc, graph):
    reduced = {i: [] for i in range(len(cfc))}
    cfcDict = {}
    for index, component in enumerate(cfc):
        for node in component:
            cfcDict[node] = index

    for i, component in enumerate(cfc):
        for node in component:
            for neighbor in graph[node]:
                if cfcDict[neighbor] != i and cfcDict[neighbor] not in reduced[i]:
                    reduced[i].append(cfcDict[neighbor])
    
    return reduced

def findEntryExit(graph):
    entries = set(graph.keys())
    exits = set()

    for node, neighbors in graph.items():
        for neighbor in neighbors:
            entries.discard(neighbor)
        if not neighbors:
            exits.add(node)

    return list(entries), list(exits)

# copilot again
def dfs(graph, start, end):
    visited = set()
    stack = [start]

    while stack:
        node = stack.pop()
        if node == end:
            return True
        if node not in visited:
            visited.add(node)
            stack.extend(graph[node])

    return False

# on ajoute du dfs et on prie que la complexité n'explose pas trop
def minEdges(entries, exits, graph):
    edges = []
    # maxiLinks = max(len(entries), len(exits))
    entries2 = entries.copy()

    for ex in exits:
        added = False
        for en in entries2:
            if not dfs(graph, ex, en):
                edges.append((ex, en))
                entries2.remove(en)
                added = True
                break

        if not added and entries2:
            edges.append((ex, entries2[0]))
            entries2.pop(0)

    # for i in range(maxiLinks):
    #     ex = exits[i % len(exits)]
    #     en = entries[i % len(entries)]
    #     edges.append((ex, en))

    return edges

n = int(input())
edges = list(map(int, input().split()))
graph = build(edges)
cfc = tarjan(graph)

# reduce graph
reduced = reduce(cfc, graph)
# find entry and exit points
entries, exits = findEntryExit(reduced)
# find min edges to add
minEdgesAdd = minEdges(entries, exits, graph)
# convert to real edges
realEdges = [(cfc[edge[0]][0], cfc[edge[1]][0]) for edge in minEdgesAdd]

# print("Composantes fortement connexes :", cfc)
# print("Graphe réduit :", reduced)
# print("Points d'entrée :", entries)
# print("Points de sortie :", exits)
# print("Arêtes à ajouter :", minEdgesAdd)
# print("Arêtes à ajouter (réelles) :", realEdges)

if len(realEdges) == 1 and realEdges[0][0] == realEdges[0][1]:
    print(0) # tout est fortement connexe
else:
    print(len(realEdges))
    for edge in realEdges:
        print(*edge)  # ça c'est dingue, merci copilot aussi