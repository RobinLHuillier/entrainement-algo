t = int(input())

for _ in range(t):
    n = int(input())
    rooms = []
    for i in range(n):
        d, s = map(int, input().split())
        rooms.append((d, s))
    rooms.sort(key=lambda x: x[0])
    maxi_reach = rooms[0][0] + (rooms[0][1]-1)//2
    for d,s in rooms:
        if d > maxi_reach:
            break
        new_reach = d + (s-1)//2
        if new_reach < maxi_reach:
            maxi_reach = new_reach
    print(maxi_reach)
