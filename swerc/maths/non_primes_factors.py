from time import time
from collections import defaultdict

t = time()

def factor(n):
    factors = defaultdict(lambda: 0)
    while n%2 == 0:
        factors[2] += 1
        n //= 2
    i = 3
    while i*i <= n:
        if n%i == 0:
            n //= i
            factors[i] += 1
        else:
            i += 2
    if n > 1:
        factors[n] += 1
    return factors

def NPF2(i):
    fact = factor(i)
    prod = 1
    for k, v in fact.items():
        prod *= (v + 1)
    prod -= len(fact)
    return prod

for _ in range(int(input())):
    print(NPF2(int(input())))



# utilisation de factors de la plaquette
# en naïf :
# -> diviser le nombre d'origine par tous les facteurs premiers
# -> mettre le résultat dans un stack et un set
# -> tant que le stack n'est pas vide recommencer

# en intelligent mais sais pas pourquoi
# https://www.geeksforgeeks.org/count-of-the-non-prime-divisors-of-a-given-number/
# -> même calcul des factors 
# -> juste une série de mult pour compter
# -> time limit exceeded

# en intelligent et en c++
# -> même version, time limit exceeded + test (5/9) (même résultat)

# calcul préliminaire de tous les premiers pour accélérer la recherche de facteurs ?