from math import gcd
from math import ceil
from math import floor

def bezout(a, b):
    px, py = 1, 0
    x, y = 0, 1
    while b != 0:
        a, (q, b) = b, divmod(a, b)
        px, x = x, px - q * x
        py, y = y, py - q * y
    return a, px, py

a,b,c = [int(d) for d in input().split()]

def pasBrute(a,b,c):
    if c%a == 0 or c%b == 0:
        print("Yes")
    elif c%gcd(a,b) != 0: # pas multiple du pgcd ça marche pas
        print("No")
    else:
        pgcd,px,py = bezout(a,b)
        Kbase = c // gcd( gcd(a,c), gcd(b,c) )
        if (px*py < 0) : # même signe c'est bon
            x = px*c // pgcd
            y = py*c // pgcd
            if py < 0:
                k = Kbase * ceil(-y*a/(c*Kbase))
                if x - ((k*b)/c) < 0:
                    print("No")
                else:
                    print("Yes")
            else:
                k = Kbase * ceil(-x*b/(c*Kbase))
                if y - ((k*a)/c) < 0:
                    print("No")
                else:
                    print("Yes")
        else:
            print("Yes")

def Brute(a,b,c):
    for i in range(0,c+1,a):
        if (c-i)%b == 0:
            print("Yes")
            exit(0)
    print("No")


# https://algorithmist.com/wiki/UVa_10090_-_Marbles

def gcde(a, b, x, y):
    if b == 0 :
        x = 1
        y = 0
        return a, x, y
    gcd, x, y = gcde(b, a%b, x, y)
    x = y
    y = x - (a/b)*y
    return gcd, x, y

def copie(a,b,c):
    n = c
    n1 = a
    n2 = b
    pgcd, x, y = gcde(n1, n2, 0, 0)
    if n1 > c and n2 > c:
        print("No")
        exit(0)
    if n % pgcd != 0 :
        print("No")
        exit(0)
    else :
        t1 = ceil(-x*n/n2)
        t2 = floor(y*n/n1)
    if t2 < t1 :
        print("No")
    else :
        print("Yes")

copie(a,b,c)


# https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bachet-B%C3%A9zout



"""
6 11 60


 8 12




# Kbase = c / pgcd( pgcd(a,c), pgcd(b,c) ) = 60/1
# k = Kbase*x
# Kbase*x * a/c >= -y
# Kbase*x >= -y*a/c
# Kbase*x >= ceil(-y*(a/c))
# x >= ceil(-y*a/(c*Kbase))
# k == Kbase*x



pgcd(6,11) = 1
x y t.q x*6 + y*11 = pgcd(6,11)
        2*6 + -1*11 = 1
c = 60
res = c // pgcd(6,11) = 60
nouveau couple solution : 
60*(x y) = (2x60 -1x60)
a*(x - k*b/c) + b*(y + k*a/c) = c
6*(2*60 - k*11/60) + 11*(-1*60 + k*6/60) = 60
k*a/c + y >= 0
k*a/c >= -y
k >= -y*c/a

pgcd(a,c)
a = 2*3*5 = 30
c = 3*7 = 21
k = pgcd(a,c) = 3
ppcm(a,c) = a*c / pgcd(a,c)
a/d = a/pgcd(a,d)  /  d/pgcd(a,d)
30/21 = 30/3 / 21/3 = 10/7
k=7

k = Kbase * x
Kbase = pgcd(a,c) = 6
+ k * a/c = (Kbase * x) * a/c
a * (k/c) * x >= -y
a * x >= -y
x >= -y/a

k = pgcd(a,c)
x = ceil(-y/a)

x0 - pgcd(a,c)*ceil(-y/a)*b/c = 

Kbase = c / pgcd( pgcd(a,c), pgcd(b,c) ) = 60/1
k = Kbase*x
Kbase*x * a/c >= -y
k == ceil(-y*(a/c))

x0 - k*b/d ?>=? 0



3*a/c = 3*30/21 = 30/7

"""