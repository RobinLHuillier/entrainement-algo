# générer des graphes connexes

import random

Nodes = {}
nb_nodes = 0
for i in range(100,random.randint(100,100000)):
    if nb_nodes == 0 or random.random() < 0.01:  # ajouter un nouveau noeud
        Nodes[nb_nodes] = []
        if nb_nodes != 0: # l'apparier à un noeud
            other_node = random.randint(0,nb_nodes)
            Nodes[other_node].append(nb_nodes)
        nb_nodes += 1
    else : # apparier deux noeuds qui ne l'ont pas encore été
        node1 = random.randint(0,nb_nodes-1)
        node2 = random.randint(0,nb_nodes-1)
        if node1 != node2 and node2 not in Nodes[node1]:
            Nodes[node1].append(node2)

Graph = [[0]*nb_nodes for _ in range(nb_nodes)]
for node, voisin in Nodes.items():
    for v in voisin:
        if Graph[node][v] == 0:
            rand = random.randint(0,100)
            Graph[node][v] = rand

for i in range(len(Graph)):
    for j in range(len(Graph[i])):
        print(Graph[i][j], end="-")
    print()

print(nb_nodes)