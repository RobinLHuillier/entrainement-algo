for i in range(int(input())):
    pgcd, lcm = map(int,input().split())
    if lcm%pgcd:
        print(-1)
    else:
        print(pgcd, lcm)