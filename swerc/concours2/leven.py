alpha = "@abcdefghijklmnopqrstuvwxyz"

def leven(x,y):
    n = len(x)
    m = len(y)
    A = [[i+j for j in range(m+1)]for i in range(n+1)]
    for i in range(m):
        A[0][i+1] = alpha.index(y[i]) + A[0][i]
    for i in range(n):
        A[i+1][0] = alpha.index(x[i]) + A[i][0]
    for i in range(n):
        for j in range(m):
            A[i+1][j+1] = min(A[i][j+1] + alpha.index(x[i]) , A[i+1][j] + alpha.index(y[j]), A[i][j] + int(x[i] != y[j]) * (abs(alpha.index(y[j])-alpha.index(x[i]))))
    return A[n][m]

x = input()
y = input()