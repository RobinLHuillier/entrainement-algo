class UnionFind:
    def __init__(self,n):
        self.up = list(range(n))
        self.rank = [0]*n
    def find(self, x):
        if self.up[x] == x: return x
        else:
            self.up[x] = self.find(self.up[x])
            return self.up[x]
    def unite(self, x, y):
        rep_x = self.find(x)
        rep_y = self.find(y)
        if rep_x == rep_y: return False
        if self.rank[rep_x] == self.rank[rep_y]:
            self.rank[rep_x] += 1
            self.up[rep_y] = rep_x
        elif self.rank[rep_x] > self.rank[rep_y]:
            self.up[rep_y] = rep_x
        else:
            self.up[rep_x] = rep_y
        return True

def kruskal(n, edges):
    comp = UnionFind(n)
    edges.sort()
    saved = 0
    for p, u, v in edges:
        if not comp.unite(u,v):
            saved += p
    return saved


try:
    while True:
        m,n = [int(a) for a in raw_input().split()]
        edges = []
        for _ in range(n):
            x,y,p = [int(a) for a in raw_input().split()]
            edges.append((p,x,y))
        raw_input()
        res = kruskal(m, edges)
        print res
except EOFError:
    pass