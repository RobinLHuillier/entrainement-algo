from math import inf
def bellman_ford(n, src, arcs):
    dist = [inf]*n
    dist[src] = src
    prev = [None] * n
    prev[src] = src
    for i in range(n-1):
        relaxed = False
        for(w,u,v) in arcs:
            alt = dist[u] + w
            if dist[v] > alt:
                dist[v] = alt
                prev[v] = u
                relaxed = True
        if not relaxed: 
            break
    for w,u,v in arcs:
        if dist[v] > dist[u] + w:
            print(dist[v], dist[u], v, u, w)
            return None
    return dist, prev

n,m = [int(f) for f in input().split()]
# print(n,m)
arcs = []
for i in range(m):
    a,b,c = [int(f) for f in input().split()]
    a -= 1
    b -= 1
    arcs.append((c, a, b))

res = bellman_ford(n,1,arcs)
if res is None:
    print("negative infinity")
elif res[0][-1] == inf:
    print("unreachable")
else:
    print(res[0][-1])