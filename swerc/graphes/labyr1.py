from itertools import combinations

def parcours(tree):
    for c in tree.child:
        print(c.parent == tree)
        parcours(c)
    if len(tree.child) == 0:
        tree.b = 0
        tree.t = 0
        return    
    tu = [tr.t for tr in tree.child]
    for tr in tree.child:
        print(tr.t)
    # print(tu, tree.child)
    bu = max([tr.b for tr in tree.child])
    it = combinations(tree.child,2)
    for i in it:
        print(i)

class Tree:
    def __init__(self, parent=None, children=None):
        self.parent = parent
        self.child = children
        if children is None:
            self.child = []
        self.x = -1
        self.y = -1
        self.b = None
        self.t = None
    def addChild(self, child):
        self.child.append(child)
    def addCoor(self, x, y):
        self.x = x
        self.y = y

def maxProf(tree):
    if len(tree.child) == 0:
        return 0
    m = 1
    for c in tree.child:
        m = max(m, 1+maxProf(c))
    return m

t = int(input())
for i in range(t):
    c,r = [int(f)for f in input().split()]
    laby = []
    for row in range(r):
        laby.append(input())
    labyMem = [[0]*c for _ in range(r)]
    # print(labyMem, laby)
    # chercher une première position
    pos = None
    for i in range(c):
        if pos is not None:
            break
        for j in range(r):
            if laby[i][j] == ".":
                pos = (j,i)
                break
    # créer l'arbre
    tree = Tree()
    tree.addCoor(pos[0],pos[1])
    labyMem[pos[1]][pos[0]] = 1
    root = tree
    stack = [tree]
    while len(stack) > 0:
        actTree = stack.pop(0)
        x1,y1 = [actTree.x,actTree.y]
        for x2,y2 in [(-1,0),(0,-1),(1,0),(0,1)]:
            if x1+x2 >= 0 and x1+x2 < c and y1+y2 >= 0 and y1+y2 < r:
                if labyMem[y1+y2][x1+x2] == 0 and laby[y1+y2][x1+x2] == ".":
                    labyMem[y1+y2][x1+x2] = 1
                    tempTree = Tree()
                    tempTree.addCoor(x1+x2,y1+y2)
                    actTree.addChild(tempTree)
                    stack.append(tempTree)
       
    # parcours
    parcours(root)