from itertools import combinations
class Tree:
    def __init__(self, x, y, parent=None):
        self.parent = parent
        self.x = x
        self.y = y
        self.t = None
        self.b = None
        self.children = []
# fais les calculs de t et b
def calcul(tree):
    tu = max([tr.t for tr in tree.children])
    bu = max([tr.b for tr in tree.children])
    bv = 1 + bu
    maxi = 0
    for c1, c2 in combinations(tree.children, 2):
        temp = c1.b + c2.b + 2
        if temp > maxi:
            maxi = temp
    tree.b = bv
    tree.t = max(bv,tu,maxi)
# parcourir l'arbre formé, sans récursion
def parcoursAPlat(tree):
    if len(tree.children) == 0:
        tree.t = 0
        return
    tree = tree.children[0]
    while tree.parent is not None:
        if len(tree.children) == 0:
            tree.t = 0
            tree.b = 0
            tree = tree.parent
        else:
            allComplete = True
            for c in tree.children:
                if c.t is None:
                    tree = c
                    allComplete = False
                    break
            if allComplete:
                calcul(tree)
                tree = tree.parent
    calcul(tree)
t = int(input())
for i in range(t):
    c,r = [int(f)for f in input().split()]
    laby = []
    for row in range(r):
        laby.append(input().split()[0])
    # trouver le premier point
    pos = None
    for y in range(r):
        x = laby[y].find(".")
        if x != -1:
            pos = (x,y)
            break
    if pos is not None:   # ##### ARRETER ICI SINON
        root = Tree(pos[0],pos[1])
        stack = [root]
        while len(stack) > 0:
            cur = stack.pop(0)
            for a,b in [(-1,0),(0,-1),(1,0),(0,1)]:
                x = cur.x + a
                y = cur.y + b
                if x >= 0 and x < c and y >= 0 and y < r:
                    if laby[y][x] == "." and (cur.parent is None or cur.parent.x != x or cur.parent.y != y):
                        nTree = Tree(x,y,cur)
                        cur.children.append(nTree)
                        stack.append(nTree)
        parcoursAPlat(root)
        res = root.t
    else:
        res = 0
		
		
print("Maximum rope length is " + str(root.t) + ".")
