# code golf

for _ in range(int(input())):
    input()
    liste = [int(a)%3 for a in input().split()]
    div = [liste.count(k) for k in range(3)]
    diff = min(div[1], div[2])
    print(div[0]+diff + (div[1]-diff)//3 + (div[2]-diff)//3)