def tarjan(G, n):
    num = 0
    sccs = []
    pile = []
    is_in_pile = [False] * 2*n
    nums = [None] * 2*n
    lowlinks = [None] * 2*n

    for a in range(n):
        if nums[a] == None:
            rec = []
            rec.append(a)

            while rec:
                s = rec[-1]

                if not is_in_pile[s]:
                    num += 1
                    nums[s] = num
                    lowlinks[s] = num
                    pile.append(s)
                    is_in_pile[s] = True

                cont = True
                for v in G[s]:
                    if nums[v] == None:
                        rec.append(v)
                        cont = False
                        break

                    if nums[v] != -1:
                        lowlinks[s] = min(lowlinks[s], lowlinks[v])
                
                if cont:
                    rec.pop()
                    if lowlinks[s] == nums[s]:
                        v = -1
                        scc = []
                        while v != s:
                            v = pile.pop()
                            scc.append(v)
                            nums[v] = -1
                    
                        sccs.append(scc)
    
    return sccs