def subCountX(lyb, lx, n):
    somme = 0
    for i in range(n-1):
        somme += lx[i] * lyb[i+1]
    return somme

def subCountY(lxb, ly, n):
    somme = 0
    for i in range(n-1,0,-1):
        somme += ly[i] * lxb[i-1]
    return somme

n, k = map(int, input().split())
s = input().split()[0]
t = input().split()[0]

x = t[0]
y = t[1]

lx = [0]*n
ly = [0]*n
# positionner les x et y
for i in range(n):
    if s[i] == x:
        lx[i] = 1
    if s[i] == y:
        ly[i] = 1
# préparer la somme
lxb = [0]*n
lyb = [0]*n
lxb[0] = lx[0]
lyb[n-1] = ly[n-1]
for i in range(1,n):
    lxb[i] = lxb[i-1] + lx[i]
    lyb[n-1-i] = lyb[n-i] + ly[n-1-i]

first = 0
last = n-1
subseq = subCountX(lyb,lx,n)

if x == y:
    diff = 0
    for i in range(n):
        if s[i] != x:
            diff += 1
    ajout = 0
    if diff >= k:
        ajout += k
    else:
        ajout += diff
    somme = 0
    for i in range(1,ajout+s.count(x)):
        somme += i
    print(somme)
    exit()

while k > 0:
    while lx[first] == 1 and first < n-1:
        first += 1
    while ly[last] == 1 and last > 0:
        last -= 1
    if last < first:
        break
    # copiessss
    cpXlx = lx[::]
    cpXly = ly[::]
    cpXlxb = lxb[::]
    cpXlyb = lyb[::]
    cpYlx = lx[::]
    cpYly = ly[::]
    cpYlxb = lxb[::]
    cpYlyb = lyb[::]
    subX = 0
    subY = 0
    # changement en x
    if s[first] == y:
        cpXly[first] = 0
        for i in range(first,0,-1):
            cpXlyb[i] -= 1
    cpXlx[first] = 1
    for i in range(first, n):
        cpXlxb[i] += 1
    subX = subCountX(cpXlyb,cpXlx,n)
    # changement en y
    if s[last] == x:
        cpYlx[last] = 0
        for i in range(last,n):
            cpYlxb[i] -= 1
    cpYly[last] = 1
    for i in range(last, -1, -1):
        cpYlyb[i] += 1

    subY = subCountY(cpYlxb,cpYly,n)
    if subX > subY:
        lx = cpXlx
        ly = cpXly
        lxb = cpXlxb
        lyb = cpXlyb
        subseq = subX
    else:
        lx = cpYlx
        ly = cpYly
        lxb = cpYlxb
        lyb = cpYlyb
        subseq = subY

    # print("----",k)
    # print(lx)
    # print(ly)
    # print(lxb)
    # print(lyb)
    # print(subseq)
    # print("x:", subX, "y:", subY)
    # print("first:",first,"last:",last)

    k -= 1
print(subseq)