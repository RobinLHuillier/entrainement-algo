from collections import deque
from math import inf

def find_augm_path(n, s, t, succ, caps, flow):
    queue = deque()
    queue.append(s)
    prev = [None]*n
    prev[s] = s
    val = [0]*n
    val[s] = inf
    
    while queue:
        u = queue.popleft()
        for v in succ[u]:
            res = caps[u][v] - flow[u][v]
            if res > 0 and prev[v] is None:
                val[v] = min(val[u], res)
                prev[v] = u
                if v == t: break
                queue.append(v)
    return prev, val[t]

def edmonds_karp(n, s, t, succ, caps):
    flow = [[0]*n for i in range(n)]
    while True:
        prev, val = find_augm_path(n, s, t, succ, caps, flow)
        if not val: break
        u = t
        while prev[u] != u:
            flow[prev[u]][u] += val
            flow[u][prev[u]] -= val
            u = prev[u]
    return flow, sum(flow[s])

# input
nb_tailles = 6
src = -2
puit = -1
tailles = ["S", "M", "L", "XL", "XXL", "XXXL"]
sizes = [int(a)for a in input().split()]
nb_participants = int(input())
n = nb_tailles + nb_participants + 2   # + 2 pour source et puit
# créer le graphe
# rangé :   de 0 à 5 les tailles
#           de 6 à 5+participants: les participants
#           en -2 la source, en -1 le puit
caps = [[0]*n for _ in range(n)]
succ = [[] for _ in range(n)]
succ[src] = [a for a in range(nb_tailles)]
for a in range(nb_tailles):
    caps[src][a] = sizes[a]
for a in range(nb_participants):
    succ[a+nb_tailles].append(puit)
    # 1 seul tshirt par participant
    caps[a+nb_tailles][puit] = 1

for i in range(nb_participants):
    demande = input().split(",")
    if len(demande) == 0:
        print("NO")
        exit()
    ind = tailles.index(demande[0])
    for b in range(len(demande)):
        succ[ind+b].append(i+nb_tailles)
        caps[ind+b][i+nb_tailles] = 1

flow, somme = edmonds_karp(n, src, puit, succ, caps)
if somme != nb_participants:
    print("NO")
else:
    print("YES")
    for i in range(nb_participants):
        print(tailles[flow[nb_tailles+i].index(-1)])