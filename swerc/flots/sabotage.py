from collections import deque
from math import inf

def find_augm_path(n, s, t, succ, caps, flow):
    queue = deque()
    queue.append(s)
    prev = [None] * n
    prev[s] = s
    val = [0]*n
    val[s] = inf
    while queue:
        u = queue.popleft()
        for v in succ[u]:
            res = caps[u][v] - flow[u][v]
            if res > 0 and prev[v] is None:
                val[v] = min(val[u], res)
                prev[v] = u
                if v == t: break
                else: queue.append(v)
    return prev, val[t]
def edmond_karps(n, s, t, succ, caps):
    flow = [[0]*n for _ in range(n)]
    while True:
        prev, val = find_augm_path(n, s, t, succ, caps, flow)
        if not val: break
        u = t
        while prev[u] != u:
            flow[prev[u]][u] += val
            flow[u][prev[u]] -= val
            u = prev[u]
    return flow, sum(flow[s])

while True:
    n, m = map(int, input().split())
    if n==0 and m==0:
        break
    caps = [[0]*n for i in range(n)]
    succ = [[] for i in range(n)]
    src = 0
    puit = 1
    arcs = []
    for _ in range(m):
        c, d, cost = map(int, input().split())
        c -= 1
        d -= 1
        arcs.append((c,d,cost))
        caps[c][d] = cost
        caps[d][c] = cost
        succ[c].append(d)
        succ[d].append(c)
    for i in range(n):
        caps[i][0] = 0 # no arcs from i to src
        if 0 in succ[i]:
            succ[i].remove(0)
        caps[1][i] = 0 # no arcs from well to i
        succ[1] = []
    flow, sumFlow = edmond_karps(n, src, puit, succ, caps)
    print("flow", flow)
    print("succ",succ)
    print("caps",caps)

    reachable_vertices = set([src])
    to_explore = [src]
    seen = set()
    
    while to_explore:
        v = to_explore.pop()
        if v not in seen:
            seen.add(v)
            print(list(seen), v, succ[v], list(reachable_vertices))
            for n in succ[v]:
                print(v, n, caps[v][n], flow[v][n])
                if n not in seen and caps[v][n] > flow[v][n]:
                    reachable_vertices.add(n)
                    to_explore.append(n)
    
    print("reach", reachable_vertices)

    for v in reachable_vertices:
        for n in succ[v]:
            if n not in reachable_vertices:
               print(v+1, n+1)
    
    print("")