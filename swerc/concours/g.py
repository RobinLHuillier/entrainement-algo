n = int(input())
s = 0
for i in range(n):
    k = sum(map(int, input().split()))
    if k >= 2:
        s += 1
print(s)