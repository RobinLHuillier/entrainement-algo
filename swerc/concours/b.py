from math import gcd

t = int(input())
for _ in range(t):
    n = int(input())
    p = list(map(int, input().split()))

    m = -1

    for i in range(len(p)):
        d = abs(p[i]-(i+1))
        if d != 0:
            if m == -1:
                m = d
            else:
                m = gcd(m,d)
                if m == 1:
                    break
    
    print(m)
