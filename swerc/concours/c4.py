t = int(input())
for _ in range(t):
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    b = sorted(a)
    c = b[::]
    s = sum(b)
    f = s
    h = []

    b = [(b[i],0,0)for i in range(len(b))]

    for i in range(k):
        if 2*i != len(b)-1:
            s = s - (c[2*i] + c[2*i+1])
            b[2*i] = (c[i], s, 0)

    p = 2*(k-1)
    curs = len(b)
    s = 0
    h.append(b[p][1])
    for i in range(k-1):
        p -= 2
        curs -= 1
        s += c[curs]
        h.append(b[p][1]-s)
        b[p] = (b[p][0], b[p][1]-s, 0)

    s = f
    p = len(b)
    for i in range(k):
        p -= 1
        s -= c[p]
        b[p] = (c[p], b[p][1], s)

    s = 0
    curs = -2
    p = len(b)-k
    h.append(b[p][2])
    for i in range(k-1):
        p += 1
        curs += 2
        s += c[curs] + (c[curs+1] if curs+1 < len(b) else 0)
        h.append(b[p][2]-s)
        b[p] = (c[p], b[p][1], b[p][2]-s)


    # print(b)
    # print(h)

    print(max(h))