t = int(input())
for _ in range(t):
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    b = sorted(a)
    c = b[::]
    e = sum(b)

    for i in range(len(b)):
        if i%2 == 0:
            s = b[i]
            if i != len(b)-1:
                s += b[i+1]
            b[i] = (b[i], s)
        else:
            b[i] = (b[i], 0)

    start = 0
    end = len(b)-1
    

    visit = [(e, start, end, 0)]
    maxi = 0

    while visit:
        s, start, end, p = visit.pop()
        if k == p:
            maxi = max(maxi, s)
            continue
        if s < maxi:
            continue
        visit.append((s-b[start][1], start+2, end, p+1))
        visit.append((s-b[end][0], start, end-1, p+1))

    print(maxi)

    # print(n,k,a)