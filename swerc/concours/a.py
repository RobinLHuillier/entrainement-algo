t = int(input())
for _ in range(t):
    n = int(input())
    a = list(map(int, input().split()))

    noImp = True
    maxImp = 0
    maxP = 0
    minImp = 1e11
    minP = 1e11
    for i in a:
        if i%2 != 0:
            noImp = False
            maxImp = max(maxImp, i)
            minImp = min(minImp, i)
        else:
            maxP = max(maxP, i)
            minP = min(minP, i)

    # print(noImp, maxImp, maxP)
    if noImp :
        print("YES")
    elif minP > minImp:
        print("YES")
    else:
        print("NO")