t = int(input())
for _ in range(t):
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    b = sorted(a)
    c = b[::]

    for i in range(len(b)):
        if i%2 == 0:
            s = b[i]
            if i != len(b)-1:
                s += b[i+1]
            b[i] = (b[i], s)
        else:
            b[i] = (b[i], 0)

    start = 0
    end = len(b)-1

    # print(b)

    for i in range(k):
        # print(b[start], b[end], start, end)
        if b[start][1] < b[end][0]:
            start += 2
        else:
            end -= 1

    # print(start, end)

    print(sum(c[start:end+1]))

    # print(n,k,a)