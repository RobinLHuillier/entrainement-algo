import copy
import math
# affichage
def printTile(tile):
    for t in tile:
        print(t)
def printLineTile(line,nTile,nTable):
    l = []
    for li in line:
        if li is not None:
            l.append(tiles[li[0]][li[1]][1])
    for i in range(nTable):
        for j in range(nTile):
            for k in range(nTable):
                # print(i*nTable+k, nTile, end=" ")
                print(l[i*nTable+k][j], end = " ")
            print("")
        print("")
# manipulation
def flip(tile):
    flipVert = tile[::-1]
    flipHoriz = [f[::-1]for f in tile]
    return flipVert, flipHoriz
def rotate(tile):
    newTile = [""for i in range(len(tile[0]))]
    for i in range(len(tile[0])):
        for j in range(len(tile[0])):
            newTile[len(tile[0])-i-1] += tile[j][i]
    return newTile
def genAllTiles(tile):
    allTiles = []
    curTile = tile
    for i in range(4):
        curTile = rotate(curTile)
        allTiles.append(sugarMyTile(curTile))
        f = flip(curTile)
        allTiles.append(sugarMyTile(f[0]))
    return allTiles
def sugarMyTile(tile):
    borders = []
    borders.append(tile[0])
    right = ""
    for i in range(len(tile)):
        right += tile[i][-1]
    borders.append(right)
    borders.append(tile[-1])
    left = ""
    for i in range(len(tile)):
        left += tile[i][0]
    borders.append(left)
    return [borders, tile]
def genFourBorders(table,pos,n):  # top right down left
    borders = [None]*4
    y = pos//n
    x = pos%n
    if y > 0:
        borders[0] = table[(y-1)*n+x]
    if y < n-1:
        borders[2] = table[(y+1)*n+x]
    if x > 0:
        borders[3] = table[y*n+x-1]
    if x < n-1:
        borders[1] = table[y*n+x+1]
    return borders
def conflict(tileNum, tileI, borderTiles, n):
    for i in range(4):
        if borderTiles[i] is not None:
            if tiles[tileNum][tileI][0][i] != tiles[borderTiles[i][0]][borderTiles[i][1]][0][(i+2)%4]:
                return True
    return False
def checkPossibilities(num, borderTiles, n):
    numsOK = []
    for i in range(8):
        if not conflict(num, i, borderTiles, n):
            numsOK.append(i)
    return numsOK
def eraseBordersAndGlueBack(table, nTable, nTile):
    image = [""for _ in range(nTable*(nTile-2))]
    for i in range(nTable):
        for j in range(nTile-2):
            for k in range(nTable):
                image[(nTile-2)*i+j] += tiles[table[nTable*i+k][0]][table[nTable*i+k][1]][1][j+1][1:-1]
    return image
def checkForSeaMonster(tile,x,y):
    seaMonster = [
        ".#...#.###...#.##.O#",
        "O.##.OO#.#.OO.##.OOO",
        "#O.#O#.O##O..O.#O##.", ]
    for i in range(len(seaMonster)):
        for j in range(len(seaMonster[0])):
            if seaMonster[i][j]=="O" and tile[y+i][x+j] != "#":
                return False
    for i in range(len(seaMonster)):
        for j in range(len(seaMonster[0])):
            if seaMonster[i][j]=="O":
                tile[y+i] = tile[y+i][:x+j] + "O" + tile[y+i][x+j+1:]
    return True
def parseSeaMonster(tile):
    width = 20
    height = 3
    found = 0
    for i in range(len(tile)-height):
        for j in range(len(tile[0])-width):
            if checkForSeaMonster(tile,j,i):
                found += 1
    return found
# start
def findOnlyTwoBorders():  # return array of (tileNum, orientation) shares two borders right-down
    candidates = []
    for t1 in tiles.keys():
        for orient in range(len(tiles[t1])):
            t1Borders = tiles[t1][orient][0]
            borders = [0]*4
            for t2 in tiles.keys():
                if t2 != t1:
                    for orient2 in range(len(tiles[t2])):
                        t2Borders = tiles[t2][orient2][0]
                        for i in range(4):
                            if t1Borders[i] == t2Borders[(i+2)%4]:
                                borders[i] = 1
            if borders == [0,1,1,0]:
                candidates.append((t1,orient))
    return candidates
# the beast
def backtrack(table,remains,pos,nTile,nTable):
    if(pos >= len(table)):
        return table
    ########### choper le coin haut gauche en premier !
    if pos == 0:
        candidates = findOnlyTwoBorders()
        for c in candidates:
            t = table[::]
            t[0] = c
            rem = remains[::]
            rem.remove(c[0])
            b = backtrack(t,rem,pos+1,nTile,nTable)
            if b is not None:
                return b
    ############# on divise le temps par 2-5
    for r in remains:
        borderTiles = genFourBorders(table,pos,nTable)
        numsOK = checkPossibilities(r,borderTiles, nTile)
        if len(numsOK) != 0:
            for num in numsOK:
                t = table[::]
                t[pos] = (r,num)
                rem = remains[::]
                rem.remove(r)
                b = backtrack(t,rem,pos+1,nTile,nTable)
                if b is not None:
                    return b
    return None
# Q1
def multiplyCorners(table, nTable):
    return table[0][0] * table[nTable-1][0] * table[nTable*(nTable-1)][0] * table[-1][0]
# input in tiles
inp = [f.split('\n')[0]for f in open("advent2020-20.in")]
tiles = {}
tile = []
num = -1
k = 0
for i in inp:
    if len(i) == 0:
        tiles[num] = genAllTiles(tile)
        tile = []
    elif i[0] == "T":
        num = int(i[5:9])
    else:
        k = len(i)
        tile.append(i)
# vars
nTile = k
remains = list(tiles.keys())
nTable = int(math.sqrt(len(remains)))
table = [None for i in range(len(remains))]
# backtrack
b = backtrack(table, remains, 0, nTile, nTable)
corners = multiplyCorners(b, nTable)
print("Q1 ----------")
print(corners)
print("Q2 ----------")
img = eraseBordersAndGlueBack(b,nTable,nTile)
print(len(img), "x", len(img[0]))
allImg = [f[1] for f in genAllTiles(img)]
print(len(allImg), "x", len(allImg[4][0]), "x", len(allImg[4][0]))
for a in range(len(allImg)):
    monsters = parseSeaMonster(allImg[a])
    print("image",a,":",monsters,"sea monsters")
    if monsters > 0:
        for i in allImg[a]:
            print(i)
        cpt = 0
        for i in range(len(allImg[a])):
            cpt += allImg[a][i].count("#")
        # cpt -= monsters*15
        print("total:",cpt)

########################################################################################
# pour améliorer:   -> prégénérer toutes les tables et toutes les rotations
#                   -> conserver aussi les 4 string-bords pour chaque rotation
# tiles est un dict qui contient tout ça:
# tiles[2349]: tile numéro 2349
# tiles[2349][4]: tile numéro 2349, orientation 4
# tiles[2349][4][1]: la tile complète sous forme d'array
# tiles[2349][4][0]: array contenant les string des bords: 0:top/1:right/2:down/3:left