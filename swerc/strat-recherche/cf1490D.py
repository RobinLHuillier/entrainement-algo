class Tree:
    def __init__(self, arr, parent=None):
        self.val = max(arr)
        self.parent = parent
        left = self.leftArr(arr)
        if left == None:
            self.childL = left
        else:
            self.childL = Tree(left, self)
        right = self.rightArr(arr)
        if right == None:
            self.childR = right
        else:
            self.childR = Tree(right, self)
    def leftArr(self, arr):
        if len(arr) == 0:
            return None
        pos = arr.index(self.val)
        if pos > 0:
            return arr[:pos]
        else:
            return None
    def rightArr(self, arr):
        if len(arr) == 0:
            return None
        pos = arr.index(self.val)
        if pos < len(arr)-1:
            return arr[pos+1:]
        else:
            return None
    def prof(self):
        pos = self
        pro = 0
        while pos.parent is not None:
            pos = pos.parent
            pro += 1
        return pro

def parcours(tree):
    dico = {}
    stack = [tree]
    while len(stack) > 0:
        pos = stack.pop(0)
        if pos is not None:
            dico[pos.val] = pos.prof()
            stack.append(pos.childL)
            stack.append(pos.childR)
    return dico

t = int(input())
for i in range(t):
    input()
    arr = [int(a) for a in input().split()]
    tree = Tree(arr)
    dico = parcours(tree)
    print(" ".join([str(dico[a])for a in arr]))