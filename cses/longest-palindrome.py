s = input()
t = "^#" + "#".join(s) + "#$"
c = 1
d = 1
r = len(t)
p = [0] * r
i = 2
while i < r -1:
    h = 2*c - i
    p[i] = max(0, min(d-i, p[h]))
    while t[i+1+p[i]] == t[i-1-p[i]]:
        p[i] += 1
    k = i + p[i]
    if k > d:
        c = i
        d = k
    i += 1
k = max(p)
i = p.index(k)
print(s[(i-k)//2:(i+k)//2])