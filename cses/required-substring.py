n = int(input())
w = input().split()[0]
k = len(w)
if k > n:
    print(0)
elif k == n:
    print(1)
else:
    diff = n-k
    tot = 0
    for j in range(diff+1):
        c = 1
        for i in range(diff):
            val = 25
            if j > i:
                val = 26
            c = (c*val)%1000000007
        tot += c
        print(tot)
    print(c)