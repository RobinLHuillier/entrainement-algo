def left_turn(a, b, c):
    return (((a[0]-c[0])*(b[1]-c[1]) - (a[1]-c[1])*(b[0]-c[0])) >= 0)
def andrew(S):
    S.sort()
    top, bot = [], []
    for p in S:
        while len(top) >= 2 and not left_turn(p, top[-1], top[-2]):
            top.pop()
        top.append(p)
        while len(bot) >= 2 and not left_turn(bot[-2], bot[-1], p):
            bot.pop()
        bot.append(p)
    return bot[:-1] + top[:0:-1]

n = int(input())
S = []
for i in range(n):
    a,b = map(int, input().split())
    S.append((a,b))

res = andrew(S)
print(len(res))
for r in res:
    print(r[0],r[1])