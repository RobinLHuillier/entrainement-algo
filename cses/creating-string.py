from itertools import permutations

s = list(input())
s.sort()
a = set(permutations(s))
print(len(a))
for i in a:
    print("".join(i))