n, x = map(int, input().split())
vals = list(map(int, input().split()))
dico = {}
for i in range(len(vals)):
    v = vals[i]
    d = x-v
    if d in dico:
        print(dico[d]+1, i+1)
        exit(0)
    dico[v] = i

print("IMPOSSIBLE")