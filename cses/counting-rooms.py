n,m = map(int, input().split())
grid = [[0]*m for i in range(n)]
for i in range(n):
    line = input()
    for j in range(m):
        if line[j] == "#":
            grid[i][j] = -1

def findSpot(grid,mini=0):
    for i in range(mini,len(grid)):
        if 0 in grid[i]:
            return (i,grid[i].index(0))
    return None

def remplir(grid,i,j,w,h):
    to_visit = [(i,j)]
    grid[i][j] = 1
    while to_visit:
        i,j = to_visit.pop()
        for c,r in [(-1,0),(1,0),(0,1),(0,-1)]:
            if i+r >= 0 and i+r < h and j+c >= 0 and j+c < w:
                if grid[i+r][j+c] == 0:
                    grid[i+r][j+c] = 1
                    to_visit.append((i+r,j+c))

res = findSpot(grid)
rooms = 0
while res is not None:
    i,j = res
    remplir(grid,i,j,m,n)
    rooms += 1
    res = findSpot(grid,i)
    # for g in grid:
    #     for i in g:
    #         if i == -1:
    #             print(end="#")
    #         elif i == 0:
    #             print(end=".")
    #         else:
    #             print(end="x")
    #     print()
print(rooms)