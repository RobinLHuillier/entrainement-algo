import sys
sys.setrecursionlimit(20001)

# Python3 program to implement
# the above approach
maxN = 200001
 
# Adjacency List to store the graph
adj = [[] for i in range(maxN)]
  
# Stores the height of each node
height = [0 for i in range(maxN)]
  
# Stores the maximum distance of a
# node from its ancestors
dist = [0 for i in range(maxN)]
  
# Function to add edge between
# two vertices
def addEdge(u, v):
 
    # Insert edge from u to v
    adj[u].append(v)
  
    # Insert edge from v to u
    adj[v].append(u)
 
# Function to calculate height of
# each Node
def dfs1(cur, par):
 
    # Iterate in the adjacency
    # list of the current node
    for u in adj[cur]:
        if (u != par):
  
            # Dfs for child node
            dfs1(u, cur)
  
            # Calculate height of nodes
            height[cur] = max(height[cur],
                              height[u])
  
    # Increase height
    height[cur] += 1
 
# Function to calculate the maximum
# distance of a node from its ancestor
def dfs2(cur, par):
 
    max1 = 0
    max2 = 0
  
    # Iterate in the adjacency
    # list of the current node
    for u in adj[cur]:
        if (u != par):
  
            # Find two children
            # with maximum heights
            if (height[u] >= max1):
                max2 = max1
                max1 = height[u]
             
            elif (height[u] > max2):
                max2 = height[u]
  
    sum = 0
     
    for u in adj[cur]:
        if (u != par):
  
            # Calculate the maximum distance
            # with ancestor for every node
            sum = (max2 if (max1 == height[u]) else max1)
  
            if (max1 == height[u]):
                dist[u] = 1 + max(1 + max2, dist[cur])
            else:
                dist[u] = 1 + max(1 + max1, dist[cur])
  
            # Calculating for children
            dfs2(u, cur)
         
# Driver Code
if __name__=="__main__":
 
    n = int(input())

    for i in range(n-1):
        a,b = map(int, input().split())
        addEdge(a,b)
  
    # Calculate height of
    # nodes of the tree
    dfs1(1, 0)
  
    # Calculate the maximum
    # distance with ancestors
    dfs2(1, 0)
     
    # Print the maximum of the two
    # distances from each node
    for i in range(1, n + 1):
        print(max(dist[i],
                height[i]) - 1, end = ' ')
  
# This code is contributed by rutvik_56