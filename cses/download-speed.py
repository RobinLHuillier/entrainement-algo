e = input
n,m = map(int, e().split())
N = n+1
w = range(N)
x = [[0]*N for i in w]
s = 1
t = n
y = [[] for i in w]
for i in range(m):
    a,b,c = map(int, e().split())
    y[a]+=[b]
    x[a][b] += c
    if a != s and b != t:
        y[b]+=[a]
        
f = [[0] * N for i in w]
while 1:
    q = []
    q+=[s]
    p = [-1] * N
    p[s] = s
    z = [0] * N
    z[s] = 1e20
    while q:
        u = q.pop()
        for v in y[u]:
            r = x[u][v] - f[u][v]
            if r > 0 and p[v]==-1:
                z[v] = min(z[u], r)
                p[v] = u
                if v == t: break
                q+=[v]
    z = z[t]
    if not z: break
    u = t
    while p[u] != u:
        f[p[u]][u] += z
        f[u][p[u]] -= z
        u = p[u]
print(sum(f[s]))