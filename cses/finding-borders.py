def max_border(w):
    n = len(w)
    f = [0]*n
    k = 0
    g = set()
    last = 0
    for i in range(1, n):
        while w[k] != w[i] and k > 0:
            k = f[k-1]
        if w[k] == w[i]:
            k += 1
        f[i] = k
        if k > 0 and k < last:
            g.add(last)
        last = k
    g.add(last)
    return f,g

f,g = max_border(input().split()[0])
g = list(g)
g.sort()
print(" ".join(map(str,g)))