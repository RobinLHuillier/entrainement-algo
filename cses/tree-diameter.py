n = int(input())
succ = [[] for i in range(n)]
for i in range(n-1):
    a,b = map(int, input().split())
    a -= 1
    b -= 1
    succ[a].append(b)
    succ[b].append(a)

def dfs(m, x):
    depths = [-1 for _ in range(len(m))]
    depths[x] = 0
    stack = [x]

    while len(stack):
        x = stack.pop()
        for v in m[x]:
            if depths[v] == -1:
                depths[v] = depths[x] + 1
                stack.append(v)
    
    m = max(depths)
    return (m, depths.index(m))
    
print(dfs(succ,dfs(succ, 0)[1])[0])