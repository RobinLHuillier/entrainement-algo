n = int(input())
prec = list(map(int, input().split()))
succ = [[] for i in range(n+1)]
for i in range(len(prec)):
    succ[prec[i]].append(i+2)

from collections import deque
from math import inf

def bfs(graph, start=0):
    to_visit = deque()
    dist = [inf] * len(graph)
    prev = [None] * len(graph)
    dist[start] = 0
    to_visit.appendleft(start)
    while to_visit:
        node = to_visit.pop()
        for neigh in graph[node]:
            if dist[neigh] == inf:
                dist[neigh] = dist[node] +1
                prev[neigh] = node
                to_visit.appendleft(neigh)
    return dist, prev

dist, prev = bfs(succ,1)
distInd = [(i,dist[i])for i in range(1,n+1)]
distInd.sort(key=lambda x:x[1])

tot = [0]*(n+1)
for i in range(n-1,-1,-1):
    ind, d = distInd[i]
    if len(succ[ind]) == 0:
        tot[ind] = 0
        continue
    s = 0
    for j in succ[ind]:
        s += 1+ tot[j]
    tot[ind] = s

print(" ".join(map(str,tot[1:])))

"""
prec = [0,0] + list(map(int, input().split()))
sub = [0]*(n+1)
for i in range(2,n+1):
    e = prec[i]
    while e != 1:
        sub[e] += 1
        e = prec[e]
    sub[1] += 1
print(" ".join(map(str,sub[1:])))
"""