t = int(input())
for i in range(t):
    a,b = map(int,input().split())
    sol = "NO"
    if a <= 2*b and b <= 2*a:
        if a%3 == 0 and b%3 == 0 and a+b > 1:
            sol = "YES"
        elif (a+b)%3 == 0:
            sol = "YES"
        elif a+b == 0:
            sol = "YES"
    print(sol)