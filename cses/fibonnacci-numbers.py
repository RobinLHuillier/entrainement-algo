n = int(input())
a = 0
b = 1
if n < 1:
    print(n)
    exit(0)
for i in range(n-1):
    c = (a+b)%1000000007
    a = b
    b = c
print(b)
