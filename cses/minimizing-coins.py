

def iter(x,vals):
    dico = {}
    dico[0] = 10e6 +1
    to_see = [(x,0)]
    while to_see:
        x,num = to_see.pop()
        if x not in dico or dico[x] > num:
            dico[x] = num
        else:
            continue
        if x == 0:
            continue
        for v in vals:
            if v <= x:
                to_see.append((x-v,num+1))
    return dico[0]

def rec(x, vals):
    global dico
    if x == 0:
        return 0
    if x in dico:
        return dico[x]
    mini = 10e6 +1
    if vals[-1] > x:
        return mini
    for v in vals:
        if v <= x:
            val = x-v
            if val in dico:
                mini = min(mini, 1+dico[val])
            else:
                res = rec(val,vals)
                dico[val] = res
                mini = min(mini, 1+res)
    return mini

n, x = map(int, input().split())
vals = list(map(int, input().split()))
vals.sort(reverse=True)
num = 0
i = 0
res = iter(x,vals)
if res == 10e6+1:
    print(-1)
else:
    print(res)