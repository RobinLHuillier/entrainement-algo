n,m,k = map(int, input().split())
appli = list(map(int, input().split()))
size = list(map(int, input().split()))
appli.sort()
size.sort()

attr = 0
a = 0
s = 0
while s < m and a < n:
    desir = appli[a]
    appart = size[s]
    if abs(desir-appart) <= k:
        attr += 1
        a += 1
        s += 1
        continue
    if desir > appart:
        s += 1
        continue
    a += 1

print(attr)