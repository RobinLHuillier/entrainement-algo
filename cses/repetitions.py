s = input()
repet = 1
maxRepet = 0
for i in range(len(s)-1):
    if s[i] == s[i+1]:
        repet += 1
    else:
        maxRepet = max(maxRepet, repet)
        repet = 1
maxRepet = max(maxRepet, repet)
print(maxRepet)