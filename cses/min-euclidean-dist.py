from math import hypot
from random import shuffle
def dist(p, q): # Distance euclidienne
    return hypot(p[0] - q[0], p[1] - q[1])
def dist2(p,q):
    return (p[0]-q[0])*(p[0]-q[0]) + (p[1]-q[1])*(p[1]-q[1])
def cell(point, size):
    x, y = point
    return (int(x // size), int(y // size))
def improve(S, d):
    G = {}
    for p in S:
        a, b = cell(p, d / 2)
        for a1 in range(a - 2, a + 3):
            for b1 in range(b - 2, b + 3):
                if (a1, b1) in G:
                    q = G[a1, b1]
                    pq = dist(p, q)
                    if pq < d:
                        return pq, p, q
        G[a, b] = p
    return None
def closest_points(S):
    shuffle(S)
    assert len(S) >= 2
    p, q = S[0], S[1]
    d = dist(p, q)
    while d > 0:
        r = improve(S, d)
        if r:
            d, p, q = r
        else:
            break
    return p, q

n = int(input())
S = []
for i in range(n):
    a,b = map(int, input().split())
    S.append((a,b))

a,b = closest_points(S)

print(dist2(a,b))