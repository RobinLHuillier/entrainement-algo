t = int(input())
for i in range(t):
    row, col = map(int,input().split())
    start = 0
    if col > row:
        if col%2 == 1:
            start = col*col
            print(start - row +1)
        else:
            start = (col-1)*(col-1) +1
            print(start + row -1)
    else:
        if row%2 == 0:
            start = row*row
            print(start - col +1)
        else:
            start = (row-1)*(row-1) +1
            print(start + col -1)