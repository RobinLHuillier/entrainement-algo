from collections import deque
from math import inf
def find_augm_path(n, s, t, succ, caps, flow):
    queue = deque()
    queue.append(s)
    prev = [None] * n
    prev[s] = s
    val = [0] * n
    val[s] = inf
    while queue:
        u = queue.popleft()
        for v in succ[u]:
            res = caps[u][v] - flow[u][v]
            if res > 0 and prev[v] is None:
                val[v] = min(val[u], res)
                prev[v] = u
                if v == t: break
                else: queue.append(v)
    return prev, val[t]
def edmonds_karp(n, s, t, succ, caps):
    flow = [[0] * n for i in range(n)]
    while True:
        prev, val = find_augm_path(n, s, t, succ, caps, flow)
        if not val: break
        u = t
        while prev[u] != u:
            flow[prev[u]][u] += val
            flow[u][prev[u]] -= val
            u = prev[u]
    return flow, sum(flow[s])

n,m = map(int, input().split())
N = n+1
caps = [[0]*N for i in range(N)]
s = 1
t = n
succ = [[] for i in range(N)]
for i in range(m):
    a,b = map(int, input().split())
    if (a == s and b == t) or (a == t and b == s):
        succ[a].append(b)
        caps[a][b] = 1
        succ[b].append(a)
        caps[b][a] = 1
    else:
        if b != s and a != t:
            succ[a].append(b)
            caps[a][b] = 1
        if a != s and b != t:
            succ[b].append(a)
            caps[b][a] = 1
flow, maxFlow = edmonds_karp(N,s,t,succ,caps)
print(maxFlow)

reachable_vertices = set([s])
to_explore = [s]
seen = set()

while to_explore:
    v = to_explore.pop()
    if v not in seen:
        seen.add(v)
        for n in succ[v]:
            if n not in seen and caps[v][n] > flow[v][n]:
                reachable_vertices.add(n)
                to_explore.append(n)
                
for v in reachable_vertices:
    for n in succ[v]:
        if n not in reachable_vertices:
            print(v, n)
