n = int(input())
if n < 0 or n == 2 or n == 3:
    print("NO SOLUTION")
elif n == 1:
    print(1)
elif n == 4:
    print(2,4,1,3)
else:
    base = [2,4,1,3]
    left = 0
    right = 0
    if n%2 == 1:
        left = n
        right = n-1
    else:
        left = n-1
        right = n
    for i in range(left,4,-2):
        print(i,end=" ")
    for b in base:
        print(b,end=" ")
    for i in range(6,right+1,2):
        print(i,end=" ")