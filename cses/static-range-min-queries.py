from math import inf

class RangeMinQuery:
    def __init__(self, t, INF = inf):
        self.INF = INF
        self.N = 1
        while self.N < len(t):
            self.N *= 2
        self.s = [self.INF] * (2*self.N)
        for i in range(len(t)):
            self.s[self.N+i] = t[i]
        for p in range(self.N-1,0,-1):
            self.s[p] = min(self.s[2*p],self.s[2*p+1])
    def __getitem__(self, i):
        return self.s[self.N+i]
    def __setitem__(self, i, v):
        p = self.N+i
        self.s[p] = v
        p //=2
        while p > 0:
            self.s[p] = min(self.s[2*p],self.s[2*p+1])
            p //= 2
    def range_min(self, i, k):
        return self._range_min(1,0,self.N,i,k)
    def _range_min(self, p, start, span, i, k):
        if start+span <= i or k <= start:
            return self.INF
        if i <= start and start + span <= k:
            return self.s[p]
        left = self._range_min(2*p, start, span//2, i, k)
        right = self._range_min(2*p+1, start + span//2, span//2, i, k)
        return min(left,right)

n,q = map(int, input().split())
arr = list(map(int,input().split()))
rangeMin = RangeMinQuery(arr)
for i in range(q):
    a,b = map(int, input().split())
    if a == b:
        print(arr[a-1])
    else:
        print(rangeMin.range_min(a-1,b))