n = input()
alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
dico = {}
for a in alpha:
    dico[a] = 0
for a in n:
    dico[a] += 1

dico2 = {}
imp = 0
lImp = ""
for k,v in dico.items():
    if v > 0:
        dico2[k] = v
    if v%2 == 1:
        imp += 1
        lImp = k
dico = dico2

if imp > 1:
    print("NO SOLUTION")
    exit(0)

debut = ""
milieu = ""
if imp > 0:
    milieu = lImp*dico[lImp]
fin = ""
for k,v in dico.items():
    if k == lImp:
        continue
    al = k*(v//2)
    debut += al
    fin = al + fin
print(debut+milieu+fin)