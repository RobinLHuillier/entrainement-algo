import heapq
from math import inf
def dijkstra(n, src, succ):
    dist = [inf] * n
    dist[src] = 0
    prev = [None] * n
    prev[src] = src
    queue = [(0, src)]
    while queue:
        u = heapq.heappop(queue)[1]
        for v, w in succ[u]:
            alt = dist[u] + w
            if dist[v] > alt:
                dist[v] = alt
                prev[v] = u
                heapq.heappush(queue, (dist[v], v))
    return dist, prev


def toposort(n, succ):
    indeg = [0] * n
    for u in range(n):
        for v in succ[u]:
            indeg[v] += 1
    queue = [u for u in range(n) if indeg[u] == 0]
    order = []
    while queue:
        u = queue.pop()
        order.append(u)
        for v in succ[u]:
            indeg[v] -= 1
            if indeg[v] == 0:
                queue.append(v)
    if len(order) < n: return None
    return order

n,m = map(int, input().split())
succ = [[] for i in range(n+1)]
s = [[] for i in range(n+1)]
for i in range(m):
    a,b,c = map(int, input().split())
    succ[a].append((b,c))
    s[a].append(b)
order = toposort(n+1, s)
succ2 = [[] for i in range(n+1)]
print(order)
dist, prev = dijkstra(n+1,1,succ)
print(" ".join(map(str,dist[1:])))