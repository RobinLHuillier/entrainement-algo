def mapSum(arr):
    a = []
    s = 0
    for i in range(len(arr)):
        s += arr[i]
        a.append(s)
    return a

n,q = map(int,input().split())
arr = list(map(int,input().split()))
sumMap = mapSum(arr)
for i in range(q):
    a,b = map(int,input().split())
    print(sumMap[b-1]-sumMap[a-1]+arr[a-1])