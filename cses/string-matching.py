n = input()
m = input()

P = 72057594037927931 # < 2^{56}
D = 128
def roll_hash(old, out, inp, last):
    val = (old - out * last + D * P) % P
    val = (val * D) % P
    return (val + inp) % P
def matches(s, t, i, j, k):
    for d in range(k):
        if s[i + d] != t[j + d]:
            return False
    return True
def rabin_karp_factor(s, t, k):
    last_pos = pow(D, k - 1) % P
    pos = {}
    assert k > 0
    if len(s) < k or len(t) < k:
        return None
    h_t = 0

    for j in range(k):
        h_t = (D * h_t + ord(t[j])) % P
    for j in range(len(t) - k + 1):
        if h_t in pos:
            pos[h_t].append(j)
        else:
            pos[h_t] = [j]
        if j < len(t) - k:
            h_t = roll_hash(h_t, ord(t[j]), ord(t[j+k]), last_pos)
    
    h_s = 0
    match = []
    for i in range(k):
        h_s = (D * h_s + ord(s[i])) % P
    for i in range(len(s) - k + 1):
        if h_s in pos:
            for j in pos[h_s]:
                if matches(s, t, i, j, k):
                    match.append((i, j))
        if i < len(s) - k:
            h_s = roll_hash(h_s, ord(s[i]), ord(s[i+k]), last_pos)
    return match

print(len(rabin_karp_factor(n,m,len(m))))
















"""
k = len(m)
s = 0
if k > len(n):
    print(0)
    exit(0)
for i in range(len(n)-k+1):
    if n[i:i+k] == m:
        s += 1
print(s)
"""