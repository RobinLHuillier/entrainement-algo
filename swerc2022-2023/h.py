t = int(input())
for _ in range(t):
    n = int(input())
    a = [int(c) for c in input().split()]
    b = [int(c) for c in input().split()]
    
    x = len(a)-1
    y = len(b)-1

    while x >= 0 and y >= 0:
        if a[x] == b[y]:
            y -= 1
        else:
            x -= 1
        
    print(y+1)