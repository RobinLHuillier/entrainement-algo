def fullLine(coul, n):
    for c in range(1, len(coul)):
        # print(c, coul[c], len(coul[c]), list(coul[c]))
        if len(coul[c]) == n:
            return c
    return None

def leastDeg(tab, c):
    minline = 0
    minnum = 1e8
    for i in range(len(tab)):
        cpt = tab[i].count(c)
        if cpt < minnum:
            minnum = cpt
            minline = i
    return minline, tab[minline].index(c)

t = int(input())
for _ in range(t):
    n, m = map(int, input().split())
    tab = [[0]*n for i in range(n)]
    coul = [set(), set()]
    routes = []
    for i in range(m):
        v,w = map(int, input().split())
        v -= 1
        w -= 1
        tab[v][w] = 1
        tab[w][v] = 1
        coul[1].add(v)
        coul[1].add(w)
        routes.append((v,w))
    
    next = fullLine(coul, n)

    while next:
        # print(next)
        # for t in tab:
        #     print(t)
            
        v,w = leastDeg(tab, next)
        # print(v, w)
        notAttributed = True

        for c in range(1, len(coul)):
            if c == next:
                continue
            copy = set(coul[c])
            copy.add(v)
            copy.add(w)
            # for i in range(len(coul)):
            #     print(i, list(coul[i]))
            if len(copy) == n:
                continue
            coul[c].add(v)
            coul[c].add(w)
            tab[v][w] = c
            tab[w][v] = c
            notAttributed = False
        if notAttributed:
            coul.append({v, w})
            tab[v][w] = len(coul)-1
            tab[w][v] = len(coul)-1
        
        for k in [v, w]:
            if tab[k].count(next) == 0:
                coul[next].remove(k)

        # for i in range(len(coul)):
        #     print(i, list(coul[i]))

        next = fullLine(coul, n)

    # for t in tab:
    #     print(t)

    print(len(coul)-1)
    print(' '.join(map(str, [tab[v][w] for (v,w) in routes])))