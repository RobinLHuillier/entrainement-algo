t = int(input())
for _ in range(t):
    n = int(input())
    times = [int(a) for a in input().split()]
    walk = 0
    prec = 0
    for t in range(len(times)):
        dist = times[t]-prec
        walk += dist//120
        prec = times[t]
    dist = 1440 - prec
    walk += dist//120
    if walk >= 2:
        print("YES")
    else:
        print("NO")