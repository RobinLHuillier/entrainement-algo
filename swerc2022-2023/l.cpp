#include <bits/stdc++.h>

using namespace std;
using ULL = unsigned long long;
using P = pair<ULL,ULL>;

P plusAndMinuses(const string &s) {
    P pam { 0, 0 };
    for (char c : s) {
        pam.first += (c == '+');
        pam.second += (c == '-');
    }
    return pam;
}

void solve(ULL n, P pam, ULL e, ULL f) {
    ULL a = pam.first, b = pam.second;
    if (a == b) {
        cout << "YES\n";
        return;
    }

    ULL lcm = std::lcm(e,f);
    
    double x = (double)lcm / e;
    double y = (double)lcm / f;

    double ans1 = ((a*y)-(b*x))/(y-x);
    double ans2 = ((a*x)-(b*y))/(x-y);

    // cout << ans1 << " " << "\n";
    // cout << ans2 << " " << "\n";

    if ((floor(ans1) == ceil(ans1) and floor(ans1) < min(a,b) and floor(ans1) >= 0)
        or (floor(ans2) == ceil(ans2) and floor(ans2) < min(a,b) and floor(ans2) >= 0)
    
    ) {
        cout << "YES\n";
        return;
    }

    cout << "NO\n";
}

int main() {
    ULL n; cin >> n;
    string s; cin >> s;
    P pam = plusAndMinuses(s);

    ULL q; cin >> q;
    for (ULL i = 0; i < q; ++i) {
        ULL a, b; cin >> a >> b;
        solve(n, pam, a, b);
    }

    return 0;
}