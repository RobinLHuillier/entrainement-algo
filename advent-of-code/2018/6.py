pts = [list(map(int,a.split(',')))for a in[a.strip()for a in open("6").readlines()]]
size = 400
grid = [[0]*size for _ in range(size)]

def manDist(a, b):
    x1, y1 = a
    x2, y2 = b
    return abs(x1-x2)+abs(y1-y2)

for i in range(size):
    for j in range(size):
        mini = 1e8
        l = 0
        eq = False
        for k in range(len(pts)):
            pt = pts[k]
            d = manDist((i,j), pt)
            if d == mini:
                eq = True
            if d < mini:
                l = k+1
                mini = d
                eq = False
        if not eq:
            grid[j][i] = l

# for g in grid:
#     print(''.join(map(str,g)))

def infinite(grid, size):
    inf = []
    for i in range(size):
        inf.append(grid[0][i])
        inf.append(grid[i][0])
        inf.append(grid[size-1][i])
        inf.append(grid[i][size-1])
    # print(list(set(inf)))
    for i in range(size):
        for j in range(size):
            if grid[j][i] in inf and grid[j][i] != 0:
                grid[j][i] = -1

def maxCount(grid, size):
    c = dict()
    for i in range(size):
        for j in range(size):
            if grid[j][i] > 0:
                if grid[j][i] not in c:
                    c[grid[j][i]] = 1
                else:
                    c[grid[j][i]] += 1
    maxi = 0
    key = -1
    for k in c.keys():
        if c[k] > maxi:
            key = k
            maxi = c[k]
    return key

def countNum(grid, size, num):
    c = 0
    for i in range(size):
        for j in range(size):
            if grid[j][i] == num:
                c += 1
    return c

infinite(grid, size)

# for g in grid:
#     print(''.join(map(str,g)))
# print("prout")

print("Part 1: ", countNum(grid, size, maxCount(grid, size)))

# for g in grid:
#     print(''.join(map(str,g)))

# print()
# print()
# print()
# print()
# print()

def allDist(pt1, pts):
    return sum(manDist(pt1,p)for p in pts)

def makeSafe(grid, size, pts):
    for i in range(size):
        for j in range(size):
            if grid[j][i] != -1:
                # print(j,i)
                allD = allDist((i,j), pts)
                # print(allD)
                if allD < 10000:
                    grid[j][i] = 1
                else:
                    grid[j][i] = -1
    c = 0
    for i in range(size):
        for j in range(size):
            if grid[j][i] >= 1:
                c += 1
    return c

# print(allDist((5,2),pts))
# print(pts)
# i,j = (2,5)
# a,b = (3,4)
# print(manDist((i,j),(a,b)),abs(i-a)+abs(j-b))
# print([manDist((2,5),p)for p in pts])

# for i in range(size):
#     for j in range(size):
#         if grid[j][i] == -1:
#             grid[j][i] = '.'
# for g in grid:
#     print(''.join(map(str,g)))
# for i in range(size):
#     for j in range(size):
#         if grid[j][i] == '.':
#             grid[j][i] = -1

print("Part 2: ", makeSafe(grid, size, pts))

# for i in range(size):
#     for j in range(size):
#         if grid[j][i] == -1:
#             grid[j][i] = '.'
# for g in grid:
#     print(' '.join(map(str,g)))
# for i in range(size):
#     for j in range(size):
#         if grid[j][i] == '.':
#             grid[j][i] = -1