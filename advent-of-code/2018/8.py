class Node:
    def __init__(self):
        self.child = []
        self.metadata = []

def readEntry(entry, pos, node):
    nb_childs = entry[pos]
    nb_metadata = entry[pos+1]
    pos += 2
    for i in range(nb_childs):
        child = Node()
        node.child.append(child)
        pos = readEntry(entry, pos, child)
    for i in range(nb_metadata):
        node.metadata.append(entry[pos+i])
    return pos + nb_metadata

def DFSum(node):
    som = sum(node.metadata)
    for c in node.child:
        som += DFSum(c)
    return som

def DFCheck(node):
    if len(node.child) == 0:
        return sum(node.metadata)
    som = 0
    for m in node.metadata:
        if m-1 < len(node.child) and m > 0:
            som += DFCheck(node.child[m-1])
    return som

entry = list(map(int, input().strip('\r\n').split()))
root = Node()
readEntry(entry, 0, root)

print("Part 1:", DFSum(root))
print("Part 2:", DFCheck(root))