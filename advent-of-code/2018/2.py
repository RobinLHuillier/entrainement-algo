def checksum(boxes):
    double = 0
    triple = 0
    for b in boxes:
        d = False
        t = False
        for l in b:
            if b.count(l) == 2:
                d = True
            if b.count(l) == 3:
                t = True
            if d and t:
                break
        if d:
            double += 1
        if t:
            triple += 1
    return double * triple

def compare(boxes):
    n = len(boxes)
    # print(n,"boxes to compare.")
    for i in range(n):
        # print(i,"st box")
        b1 = boxes[i]
        for j in range(n):
            if i != j:
                b2 = boxes[j]
                diff = 0
                for k in range(len(b1)):
                    if b1[k] != b2[k]:
                        diff+=1
                    if diff > 1:
                        break
                if diff == 1:
                    return (i,j)

def substract(b1, b2):
    for i in range(len(b1)):
        if b1[i] != b2[i]:
            return b1[:i]+b1[i+1:]

boxes = [a.split()[0] for a in open("2.in")]
print(checksum(boxes))
b1, b2 = compare(boxes)
print(substract(boxes[b1],boxes[b2]))