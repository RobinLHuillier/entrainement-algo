def isItAPair(a,b):
    return a != b and a.lower() == b.lower()

def removePairs(polymer):
    poly = polymer[::]
    i = 0
    while i < len(poly)-1:
        if isItAPair(poly[i],poly[i+1]):
            del poly[i]
            del poly[i]
            i = max(0, i-1)
        else:
            i += 1
    return poly

def removeLetter(polymer, letter):
    i = 0
    poly = polymer[::]
    while i < len(poly):
        if poly[i] == letter.lower() or poly[i] == letter.upper():
            del poly[i]
        else:
            i += 1
    return poly

ALPHA = "abcdefghijklmnopqrstuvwxyz"
polymer = list(input().strip('\n\r'))

minLen = float('inf')
for a in ALPHA:
    print("----\nRemoving letter",a,"/",a.upper())
    poly = removeLetter(polymer, a)
    print("Letter removed, reacting pairs")
    poly = removePairs(poly)
    if len(poly) < minLen:
        minLen = len(poly)
    print("Pairs reacted, length obtained", len(poly), "(best so far",minLen,")")