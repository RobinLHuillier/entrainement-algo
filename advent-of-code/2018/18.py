def readInput(path):
    lines = open(path).read().splitlines()
    grid = []
    for l in lines:
        grid.append([*l])
    return grid

def printGrid(grid):
    for g in grid:
        print(''.join(g))

def countXAround(grid, X, i, j):
    c = 0
    for k in (-1,0,1):
        for l in (-1,0,1):
            if k==0 and l==0:
                continue
            if k+i >= 0 and l+j >= 0 and k+i < len(grid) and l+j < len(grid[0]):
                if grid[k+i][l+j] == X:
                    c += 1
    return c

def round(grid):
    ngrid = [['.'for i in range(len(grid[0]))]for j in range(len(grid))]
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == '.':
                ngrid[i][j] = '|' if countXAround(grid, '|', i, j) >= 3 else '.'
            elif grid[i][j] == '|':
                ngrid[i][j] = '#' if countXAround(grid, '#', i, j) >= 3 else '|'
            else:
                ngrid[i][j] = '#' if countXAround(grid, '#', i, j) >= 1 and countXAround(grid, '|', i, j) >= 1 else '.'
    return ngrid

def countX(grid, X):
    c = 0
    for i in grid:
        c += i.count(X)
    return c

def hashGrid(grid):
    return ''.join([''.join(g)for g in grid])

grid = readInput("18")
for i in range(10):
    grid = round(grid)

print("Part1: ", countX(grid, '#')*countX(grid, '|'))

hashes = []
grid = readInput("18")
repet = 0
start = 0

for i in range(10000):
    grid = round(grid)
    hashG = hashGrid(grid)
    if hashG in hashes:
        # print("found ! ", hashes.index(hashG))
        repet = i
        start = hashes.index(hashG)
        break    
    hashes.append(hashG)

period = repet - start

time = 1000000000 - start
time = (time-1)%period
hashG = hashes[time+start]

print("Part2: ", countX(hashG, '|')*countX(hashG, '#'))
print("Part2: ", countX(hashes[9], '|')*countX(hashes[9], '#'))