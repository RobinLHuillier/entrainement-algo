def readInput(file):
    lines = []
    with open(file) as f:
        lines = f.read().splitlines()
    
    rules = {}
    for line in lines:
        if "initial" in line:
            initialState = line.split(':')[1].strip()
        elif "=>" in line:
            init, res = line.split('=>')
            rules[init.strip()] = res.strip()

    return initialState, rules

def nextState(stringStart, state, rules):
    newState = '..'
    for i in range(2, len(state)-2):
        current = state[i-2:i+3]
        if current in rules.keys():
            newState += rules[current]
        else: 
            newState += '.'
    newState += '..'
    if '#' in newState[2:5]:
        newState = '...' + newState
        stringStart -= 3
    if '#' in newState[-5:-2]:
        newState += '...'
    return (stringStart, newState)

def displayState(stringStart, state, i, j):
    if i < stringStart:
        i = stringStart
    if j > len(state) + i -1:
        j = len(state) + i -1
    return state[i-stringStart:j-stringStart]

def sumPlantContaining(stringStart, state):
    s = 0
    for i in range(len(state)):
        if state[i] == '#':
            s += i+stringStart
    return s

num_states = 20
initialState, rules = readInput('12.in')

stringStart = -5
state = "....." + initialState  + "....."

print(0, displayState(stringStart, state, -3, 35))
for i in range(num_states):
    stringStart, state = nextState(stringStart, state, rules)
    print(i+1, displayState(stringStart, state, -3, 35))

print(sumPlantContaining(stringStart, state))