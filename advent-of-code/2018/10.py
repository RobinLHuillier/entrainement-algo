import os

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

def manDist(u, v):
    return abs(u[0]-v[0]) + abs(u[1]-v[1])

def readCoor(line):
    trash, point = line.split('<')
    points, trash = point.split('>')
    x, y = map(int,points.split(','))
    return x,y

def readLine(line):
    pos, vel = line.split('v')
    x,y = readCoor(pos)
    a,b = readCoor(vel)
    return (x,y,a,b)

def minmax(pts, index):
    mini = min(pts, key=lambda x:x[index])
    maxi = max(pts, key=lambda x:x[index])
    return mini[index], maxi[index]

def showPts(pts, maxDist):
    minX, maxX = minmax(pts, 0)
    minY, maxY = minmax(pts, 1)
    lenX = maxX-minX+1
    lenY = maxY-minY+1
    if lenX > maxDist or lenY > maxDist:
        # print("Distance trop grande (",lenX, "-", lenY,")")
        return False
    mat = [["." for i in range(lenX)] for j in range(lenY)]
    for p in pts:
        x,y,a,b = p
        mat[y-minY][x-minX] = "#"
    for l in mat:
        print("".join(l))
    return True

def iterPts(pts):
    pts2 = []
    for p in pts:
        x,y,a,b = p
        pts2.append((x+a,y+b,a,b))
    return pts2

pts = []
while True:
    line = input().strip('\r\n')
    if len(line) == 0:
        break
    x,y,a,b = readLine(line)
    pts.append((x,y,a,b))

found = False
foundAndLost = False
sec = 0
while not foundAndLost:
    res = showPts(pts, 100)
    if res:
        found = True
        print("above, at", sec, "seconds\n")
    elif found:
        foundAndLost = True
    pts = iterPts(pts)
    sec += 1