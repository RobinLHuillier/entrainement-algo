def readInput(path):
    lines = open(path).read().splitlines()
    bots = []
    for l in lines:
        pos, radius = l.split('r=')
        radius = int(radius)
        pos = tuple(map(int, pos.split('<')[1].split('>')[0].split(',')))
        bots.append([pos, radius, 0])
    return bots

def manDist(p1, p2):
    assert len(p1) == len(p2)
    return sum([abs(p1[i]-p2[i])for i in range(len(p1))])

def calculateInRange(bots):
    for i in range(len(bots)):
        s = 0
        b1, radius, _ = bots[i]
        for j in range (len(bots)):
            b2, _, _ = bots[j]
            if manDist(b1, b2) <= radius:
                s += 1
        bots[i][2] = s

def biggestRange(bots):
    maxRange = 0
    maxBot = []
    for bot in bots:
        _, _, radius = bot
        if radius > maxRange:
            maxRange = radius
            maxBot = [bot]
        elif radius == maxRange:
            maxBot.append(bot)
    return maxBot

bots = readInput('23.in')
calculateInRange(bots)
# Part 1
maxRange = biggestRange(bots)
if len(maxRange) > 1:
    for b in maxRange:
        print(b)
else:
    print(f"Part 1 --- range max: {maxRange[0][1]}, nanobots in radius : {maxRange[0][2]}")
