cl = [a.split('\n')[0] for a in open("3.in")]
claims = []
for c in cl:
    n, suite = c.split('@')
    num = int(n.split('#')[1])
    pos, dim = suite.split(':')
    x, y = pos.split(',')
    w, h = dim.split('x')
    claims.append({"num":num,"x":int(x),"y":int(y),"w":int(w),"h":int(h)})
fabric = [[0]*1500 for _ in range(1500)]

for c in claims:
    x = c["x"]
    y = c["y"]
    num = c["num"]
    w = c["w"]
    h = c["h"]
    for a in range(w):
        for b in range(h):
            if fabric[y+b][x+a] != 0:
                fabric[y+b][x+a] = "X"
            else:
                fabric[y+b][x+a] = num

cpt = 0
for i in range(len(fabric)):
    for j in range(len(fabric[i])):
        if fabric[i][j] == "X":
            cpt += 1
print(cpt)

for c in claims:
    x = c["x"]
    y = c["y"]
    num = c["num"]
    w = c["w"]
    h = c["h"]
    noOverlap = True
    for a in range(w):
        for b in range(h):
            if fabric[y+b][x+a] == "X":
                noOverlap = False
                break
        if not noOverlap:
            break
    if noOverlap:
        print(num)