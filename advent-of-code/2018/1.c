#include <stdio.h>
#include <stdlib.h>

int main() {
   char ch;
   char file_name[25] = "1.in";
   char content[5000];
   int frequencies[200000];
   int i = 0;
   for(i=0; i<200000; i++) {
      frequencies[i] = 0;
   }
   i=0;
   int maxi;
   FILE *fp;
   fp = fopen("1.in", "r"); // read mode
   if (fp == NULL)
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
   printf("The contents of %s file are:\n", file_name);

   while((ch = fgetc(fp)) != EOF) {
      content[i] = ch;
      i++;
   }
   content[i] = '\0';
   maxi = i;
   printf("%s\n\n", content);
   fclose(fp);

   i = 0;
   int j = 0;
   int current = 0;
   int length = 0;
   int reading = 0;
   int minus = 0;
   while(i < maxi) {
      if(content[i] == '+') {
         minus = 0;
      } else {
         minus = 1;
      }
      i++;
      j = i-1;
      length = 0;
      while(content[i] != '\n') {
         length++;
         i++;
      }
      length--;
      length = -length;
      //printf("\nlength:%i",length);
      reading = 0;
      while(length < 0) {
         //printf("\nreading: %i %i %i %i",length,i,(int)(content[i-1+length]),(int)('0'));
         reading = 10*reading + (int)(content[i-1+length]) - (int)('0');
         length++;
      }
      if(minus == 1) {
         current = current - reading;
      } else {
         current = current + reading;
      }
      printf("\ncurrent: %i, reading: %i",current,reading);
      i++;
   }
   printf("\nCurrent frequency: %i\n", current);


   i = 0;
   int cpt = 0;
   j = 0;
   current = 0;
   length = 0;
   reading = 0;
   minus = 0;
   int found = 0;
   while(found == 0) {
      i = 0;
      j = 0;
      while(i < maxi && found == 0) {
         if(content[i] == '+') {
            minus = 0;
         } else {
            minus = 1;
         }
         i++;
         j = i-1;
         length = 0;
         while(content[i] != '\n') {
            length++;
            i++;
         }
         length--;
         length = -length;
         //printf("\nlength:%i",length);
         reading = 0;
         while(length < 0) {
            //printf("\nreading: %i %i %i %i",length,i,(int)(content[i-1+length]),(int)('0'));
            reading = 10*reading + (int)(content[i-1+length]) - (int)('0');
            length++;
         }
         if(minus == 1) {
            current = current - reading;
         } else {
            current = current + reading;
         }
         //printf("\ncurrent: %i, reading: %i",current,reading);
         if(cpt != 0) {
            for(j=0; j<cpt; j++) {
               if(frequencies[j] == current) {
                  found = 1;
                  printf("TROUVEEEEE %i",current);
                  break;
               }
            }
         }
         //printf("occurrence %i", cpt);
         cpt++;
         frequencies[cpt] = current;
         i++;
      }
   }
   
   printf("\nCurrent frequency: %i\n", current);

   return 0;
}
