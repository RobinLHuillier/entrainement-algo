def power(x,y,serial):
    rack = x+10
    p = rack * y
    p += serial
    p *= rack
    sp = str(p)
    if len(sp) < 3:
        p = -5
    else:
        p = int(sp[-3]) -5
    return p

SERIAL = 3628

N = 301
g = [[0]*N for i in range(N)]
for i in range(1,N):
    for j in range(1,N):
        g[i][j] = power(j,i,SERIAL)

som = 0
sx = -1
sy = -1
for i in range(1,N-3):
    for j in range(1,N-3):
        s = 0
        for u in range(3):
            for v in range(3):
                s += g[i+u][j+v]
        if s > som:
            som = s
            sx = j
            sy = i

print("Part 1:", som, sx, sy)

som = 0
sx = -1
sy = -1
sq = -1
for i in range(1,N-1):
    print(i, som, sx, sy, sq)
    for j in range(1,N-1):
        for square in range(min(N-i,N-j)):
            s = 0
            for u in range(square):
                for v in range(square):
                    s += g[i+u][j+v]
            if s > som:
                som = s
                sx = j
                sy = i
                sq = square

print("Part 2:", som, sx, sy, sq)