g = [[] for i in range(26)]
parents = [[] for i in range(26)]
inIt = [False]*26
done = [False]*26
ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

while True:
    line = input().strip('\r\n')
    if len(line) == 0:
        break
    father = line[5]
    child = line[36]
    father = ALPHA.index(father)
    child = ALPHA.index(child)
    parents[child].append(father)
    g[father].append(child)
    inIt[child] = True
    inIt[father] = True

# find starters
start = []
for i in range(26):
    if inIt[i] and parents[i] == []:
        start.append(i)
        
start.sort()
while start:
    act = start.pop(0)
    done[act] = True
    print(ALPHA[act],end="")
    # remove act from all parents requirements
    for i in range(26):
        if act in parents[i]:
            parents[i].remove(act)
    # search null array
    for i in range(26):
        if inIt[i] and not done[i] and i not in start and parents[i] == []:
            start.append(i)
    start.sort()

print()