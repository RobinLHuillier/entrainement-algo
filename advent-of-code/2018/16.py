def opcode(op, a, b, c, reg):
    try:
        if op == 0: #addr
            reg[c] = reg[a]+reg[b]
        elif op == 1: #addi
            reg[c] = reg[a] + b
        elif op == 2: #mulr
            reg[c] = reg[a]*reg[b]
        elif op == 3: #muli
            reg[c] = reg[a] * b
        elif op == 4: #banr
            reg[c] = reg[a]&reg[b]
        elif op == 5: #bani
            reg[c] = reg[a]&b
        elif op == 6: #borr
            reg[c] = reg[a]|reg[b]
        elif op == 7: #bori
            reg[c] = reg[a]|b
        elif op == 8: #setr
            reg[c] = reg[a]
        elif op == 9: #seti
            reg[c] = a
        elif op == 10: #gtir
            reg[c] = 1 if a > reg[b] else 0
        elif op == 11: #gtri
            reg[c] = 1 if reg[a] > b else 0
        elif op == 12: #gtrr
            reg[c] = 1 if reg[a] > reg[b] else 0
        elif op == 13: #eqir
            reg[c] = 1 if a == reg[b] else 0
        elif op == 14: #eqri
            reg[c] = 1 if reg[a] == b else 0
        elif op == 15: #eqrr
            reg[c] = 1 if reg[a] == reg[b] else 0
        return True
    except:
        return False

def regEqual(regA, regB):
    for i in range(len(regA)):
        if regA[i] != regB[i]:
            return False
    return True

def tryOpCode(before, inst, after):
    possible = []
    for i in range(16):
        cpReg = before[::]
        if opcode(i, *inst[1:], cpReg) and regEqual(cpReg, after):
            possible.append(i)
    return possible

def getTests():
    lines = open("16").read().splitlines()
    tests = []

    for i in range(0,len(lines), 4):
        if len(lines[i]) == 0:
            break
        before = eval(lines[i].split(':')[1])
        after = eval(lines[i+2].split(':')[1])
        inst = list(map(int, lines[i+1].split()))
        tests.append((before, inst, after))

    return tests

tests = getTests()
possible = [set([i for i in range(16)])for _ in range(16)]

cpt = 0

for t in tests:
    before, inst, after = t
    p = tryOpCode(before, inst, after)
    if len(p) >= 3:
        cpt += 1
    op = inst[0]
    possible[op] = possible[op].intersection(set(p))

print("Part1: ", cpt)

unique = set()

while len(unique) < 16:
    for i in range(len(possible)):
        if len(possible[i]) == 1:
            unique.add(list(possible[i])[0])
    for i in range(len(possible)):
        if len(possible[i]) > 1:
            for a in unique:
                possible[i].discard(a)

possible = [list(i)[0] for i in possible]

tests = open("16-2").read().splitlines()

reg = [0,0,0,0]

for t in tests:
    op, a, b, c = map(int, t.split())
    opcode(possible[op], a, b, c, reg)

print("Part2: ", reg[0])