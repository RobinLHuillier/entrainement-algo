mini = 307237
maxi = 769058
s = 0
for num in range(mini,maxi+1):
    numStr = str(num)
    # two adjacent
    adj = False
    for i in range(5):
        if numStr[i] == numStr[i+1] and (i == 0 or numStr[i-1] != numStr[i]) and (i > 3 or numStr[i+2] != numStr[i+1]):
            adj = True
            break
    if not adj:
        continue
    # never decrease
    dec = False
    # numStr = "311111"
    for i in range(5):
        if int(numStr[i]) > int(numStr[i+1]):
            dec = True
            break
    if not dec:
        print(numStr)
        s += 1
print(s)