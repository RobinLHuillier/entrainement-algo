modules = [int(f.split('\n')[0])for f in open("1.in")]
s = 0
for m in modules:
    ns = m
    while ns > 0:
        nns = (ns//3)-2
        ns = nns
        if ns > 0:
            s += ns
print(s)