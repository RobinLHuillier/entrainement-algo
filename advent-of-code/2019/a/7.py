from intcode import Intcode
from itertools import permutations

perm = permutations([0, 1, 2, 3, 4])
program = Intcode()
program.importProg("7.in")

maxi = 0
for p in list(perm):
    out = 0
    for i in range(5):
        program.init()
        print([p[i],out])
        program.input([p[i], out])
        program.run()
        out = program.out
    if out > maxi:
        maxi = out
print(maxi)