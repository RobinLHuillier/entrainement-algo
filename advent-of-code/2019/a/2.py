from intcode import Intcode


prog = [int(a)for a in[f.split('\n')[0]for f in open("2.in")][0].split(',')]
mem = prog[:]
# def execute(prog,noun,verb):
#     pos = 0
#     prog[1] = noun
#     prog[2] = verb
#     while pos < len(prog) and prog[pos] != 99:
#         pos1 = prog[pos+1]
#         pos2 = prog[pos+2]
#         pos3 = prog[pos+3]
#         if prog[pos] == 1:
#             val = prog[pos1] + prog[pos2]
#         else:
#             val = prog[pos1] * prog[pos2]
#         prog[pos3] = val
#         pos += 4
#     return prog[0]
# print("Q1:",execute(mem[:],12,2))
program = Intcode(prog)
program.init()
program.modify(1,12)
program.modify(2,2)
program.run()
print("Q1:",program.read(0))
out = 19690720
for noun in range(100):
    for verb in range(100):
        program.init()
        program.modify(1,noun)
        program.modify(2,verb)
        program.run()
        if program.read(0) == out:
            print("Q2:", 100*noun + verb)
        # if execute(mem[:],noun,verb) == out:
            # print("Q2:", 100*noun + verb)

##### INTCODE
# 99 -> halt
# [1,2] -> 3 parameters a,b,c
#           read mem[a] [1]+/[2]x mem[b], put in mem[c]

# Q1 : 3765464
# Q2 : 7610