grid = input()
w = 25
h = 6
layers = len(grid)//(w*h)

mini = float('inf')
lay = -1
for i in range(layers):
    som = 0
    for j in range(w*h):
        if grid[w*h*i + j] == "0":
            som += 1
    if som < mini:
        mini = som
        lay = i

uns = 0
deux = 0
for i in range(w*h):
    if grid[w*h*lay + i] == "1":
        uns += 1
    elif grid[w*h*lay + i] == "2":
        deux += 1

print("Part 1:", uns*deux)
print("Part 2:")

realG = ""
for i in range(w*h):
    lay = 0
    while lay < layers and grid[i + w*h*lay] == "2":
        lay += 1
    if grid[i + w*h*lay] == "1":
        realG += "#"
    else:
        realG += "."

for i in range(h):
    print(realG[i*w:(i+1)*w])