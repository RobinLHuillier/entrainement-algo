sizeX, sizeY = (50,50)

t = [[0]*sizeX for i in range(sizeY)]

wire1 = input().split(',')
wire2 = input().split(',')

def applyToPos(pos, dir):
    d = dir[0]
    n = int(dir[1:])
    x,y = pos
    dx, dy = (0,0)
    if d == "U":
        dy = -n
    if d == "D":
        dy = n
    if d == "R":
        dx = -n
    if d == "L":
        dx = n
    return (x+dx, y+dy)

def changeWire(pos1, pos2, t):
    x1,y1 = pos1
    x2,y2 = pos2
    for i in range(0,x2-x1+(x2-x1>0)-(x2-x1<0),1*(x2-x1==0)+(x2-x1)//(abs(x2-x1)+1*(x2-x1==0))):
        t[x1+i+1][y1] += 1
    for j in range(0,y2-y1+(y2-y1>0)-(y2-y1<0),1*(y2-y1==0)+(y2-y1)//(abs(y2-y1)+1*(y2-y1==0))):
        t[x1][y1+j+1] += 1

def getAllInter(t):
    inter = []
    for i in range(sizeX):
        for j in range(sizeY):
            if t[i][j] > 1:
                inter.append((i,j))
    return inter

def distToStart(pos):
    x,y = pos


pos = (sizeX//2,sizeY//2)
for w in wire1:
    x,y = applyToPos(pos, w)
    changeWire((pos), (x,y), t)
    pos = (x,y)

pos = (sizeX//2,sizeY//2)
for w in wire2:
    x,y = applyToPos(pos, w)
    changeWire((pos), (x,y), t)
    pos = (x,y)


inter = getAllInter(t)

print(inter)

for i in t:
    print(''.join([str(j)for j in i]))