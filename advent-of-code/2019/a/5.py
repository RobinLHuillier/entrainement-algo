from intcode import Intcode

prog = [int(a)for a in[f.split('\n')[0]for f in open("5.in")][0].split(',')]
program = Intcode(prog)
program.init()
program.input(5)
program.run()

#15486302