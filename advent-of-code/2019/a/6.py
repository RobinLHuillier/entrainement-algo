lines = [a.split(")")for a in[f.split('\n')[0]for f in open("6.in")]]
orbits = {}
planets = set()
for l in lines:
    a,b = l
    if a not in orbits:
        orbits[a] = None
    orbits[b] = a
    planets.add(a)
    planets.add(b)
s = 0
for p in planets:
    x = p
    while(orbits[x] is not None):
        s += 1
        x = orbits[x]
print(s)

def listOrbitsToCom(start):
    x = start
    path = []
    while(orbits[x] is not None):
        x = orbits[x]
        path.append(x)
    return path

pathY = listOrbitsToCom("YOU")[:-1]
pathS = listOrbitsToCom("SAN")[:-1]
sY = 0
sS = 0
found = False
for i in range(len(pathY)):
    if found:
        break
    for j in range(len(pathS)):
        if pathY[i] == pathS[j]:
            sY = i
            sS = j
            found = True
            break
print(pathY,pathS)
print(pathY[sY],pathS[sS],sY,sS,sY+sS)