class Intcode:
    def __init__(self, prog=[]):
        self.mem = prog[:]
        self.prog = prog[:]
        self.inp = None
        self.inpIndex = 0
        self.out = None
    # init, getters, setters
    def init(self):
        self.prog = self.mem[:]
        self.inp = None
        self.out = None
    def modify(self,pos,value):
        self.prog[pos] = value
    def input(self,val):
        if isinstance(val, list):
            self.inp = val[:]
        else:
            self.inp = [val]
        self.inpIndex = 0
    def read(self,pos):
        return self.prog[pos]
    def importProg(self,file):
        self.prog = [int(a)for a in[f.split('\n')[0]for f in open(file)][0].split(',')]
        self.mem = self.prog[:]
    # instructions
    def add(self,pos,m1,m2,m3):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        val = n1+n2
        self.prog[self.prog[pos+3]] = val
    def mul(self,pos,m1,m2,m3):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        val = n1 * n2
        self.prog[self.prog[pos+3]] = val
    def inpt(self,pos,m1):
        self.prog[self.prog[pos+1]] = self.inp[self.inpIndex]
        self.inpIndex += 1
    def outpt(self,pos,m1):
        if m1 == "0":
            self.out = self.prog[self.prog[pos+1]]
        else:
            self.out = self.prog[pos+1]
    def jumpTrue(self,pos,m1,m2):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        if n1 != 0:
            return n2
        return -1
    def jumpFalse(self,pos,m1,m2):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        if n1 == 0:
            return n2
        return -1
    def lessThan(self,pos,m1,m2,m3):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        if n1 < n2:
            val = 1
        else:
            val = 0
        self.prog[self.prog[pos+3]] = val
    def equals(self,pos,m1,m2,m3):
        if m1 == "0":
            n1 = self.prog[self.prog[pos+1]]
        else:
            n1 = self.prog[pos+1]
        if m2 == "0":
            n2 = self.prog[self.prog[pos+2]]
        else:
            n2 = self.prog[pos+2]
        if n1 == n2:
            val = 1
        else:
            val = 0
        self.prog[self.prog[pos+3]] = val
    # opcodes
    def readOpCode(self,pos):
        opcode = str(self.prog[pos])
        opmode = opcode[:-2]
        opcode = int(opcode[-2:])
        if opcode == 99:  # halt
            return -1 
        elif opcode == 1:   # addition of pos+1 and pos+2, value in pos+3
            opmode = "0"*(3-len(opmode))+opmode
            self.add(pos,opmode[2],opmode[1],opmode[0])
            return pos+4
        elif opcode == 2:  # mult of pos+1 and pos+2, value in pos+3
            opmode = "0"*(3-len(opmode))+opmode
            self.mul(pos,opmode[2],opmode[1],opmode[0])
            return pos+4
        elif opcode == 3:   # takes an input
            opmode = "0"*(1-len(opmode))+opmode
            self.inpt(pos,opmode[0])
            return pos+2
        elif opcode == 4:   # output something
            opmode = "0"*(1-len(opmode))+opmode
            self.outpt(pos,opmode[0])
            return pos+2
        elif opcode == 5:   # jump if true
            opmode = "0"*(2-len(opmode))+opmode
            ret = self.jumpTrue(pos,opmode[1],opmode[0])
            if ret == -1:
                return pos+3
            return ret
        elif opcode == 6:   # jump if true
            opmode = "0"*(2-len(opmode))+opmode
            ret = self.jumpFalse(pos,opmode[1],opmode[0])
            if ret == -1:
                return pos+3
            return ret
        elif opcode == 7:   # less than
            opmode = "0"*(3-len(opmode))+opmode
            self.lessThan(pos,opmode[2],opmode[1],opmode[0])
            return pos+4
        elif opcode == 8:   # equals
            opmode = "0"*(3-len(opmode))+opmode
            self.equals(pos,opmode[2],opmode[1],opmode[0])
            return pos+4
        return pos
    # run
    def run(self, pos=0):
        # if pos == 0:
        #     print("----------------------")
        #     print("--- IntCode reader ---")
        #     print("----------------------")
        #     print("-- In: ", self.inp)
        newPos = self.readOpCode(pos)
        if self.out is not None:
            print("-- Output: ",self.out)
            # self.out = None
        if newPos == -1 or newPos >= len(self.prog):
            # print("-------- END ---------")
            return
        self.run(newPos)