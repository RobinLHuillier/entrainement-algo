from intcode import Intcode
import itertools

def resetAllComputers(computers):
    for comp in computers:
        comp.reinit()

def runAllAmps(computers, sequence):
    out = 0
    resetAllComputers(computers)
    for i in range(len(sequence)):
        computers[i].setInput([sequence[i], out])
        computers[i].run()
        out = computers[i].getOutput()
    return out

def runRepeat(computers, sequence):
    out = 0
    resetAllComputers(computers)
    for i in range(5):
        computers[i].setInput(sequence[i])
        computers[i].run()
    i = 0
    while True:
        computers[i].setInput(out)
        computers[i].run()
        out = computers[i].getOutput()
        if i == 4 and computers[i].halt:
            return out
        i += 1
        if i == 5:
            i = 0


computers = [Intcode() for _ in range(5)]
for computer in computers:
    computer.loadProg('7.in')
    computer.setVerbose(False)

# permutations = list(itertools.permutations([0,1,2,3,4]))
# maxi = 0
# for p in permutations:
#     res = runAllAmps(computers, p)
#     maxi = max(res, maxi)
#     print(p, res, maxi)
# print("Part 1:", maxi)

permutations = list(itertools.permutations([5,6,7,8,9]))
maxi = 0
for p in permutations:
    res = runRepeat(computers, p)
    maxi = max(res, maxi)
    print(p, res, maxi)
print("Part 2:", maxi)