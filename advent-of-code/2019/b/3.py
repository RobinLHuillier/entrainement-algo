inst = [a.strip('\n').split(',') for a in open('3.in').readlines()]
separate = lambda x: (x[0], int(x[1:]))
getDirection = lambda x: [(0,-1), (1,0), (0,1), (-1,0)]["URDL".index(x)]
getAllIntersection = lambda g: [a for a in [(i,j) if g[i][j] == 9 else None for i in range(20000) for j in range(13000)] if a is not None]
manhattanDistance = lambda a,b: abs(a[0]-b[0])+abs(a[1]-b[1])
center = (4500, 4000)

def followInst(inst, grid, num):
    pos = center
    for i in range(len(inst)):
        print(i/len(inst), inst[i], pos)
        d, n = separate(inst[i])
        direction = getDirection(d)
        for k in range(n):
            pos = (pos[0]+direction[0], pos[1]+direction[1])
            val = grid[pos[0]][pos[1]]
            if val != 0 and val != num:
                grid[pos[0]][pos[1]] = 9
            else:
                grid[pos[0]][pos[1]] = num

def closestIntersection(grid):
    intersections = getAllIntersection(grid)
    saveAllIntersections(intersections)
    print("got all intersections")
    mini = manhattanDistance(intersections[0], center)
    for i in intersections[1:]:
        mini = min(mini, manhattanDistance(i, center))
    return mini

def saveAllIntersections(intersections):
    with open('3.out', 'w') as f:
        for intersection in intersections:
            f.write(','.join(map(str, intersection)) + '\n')

def readAllIntersections():
    return [tuple(map(int, a.split(','))) for a in [a.strip('\n') for a in open('3.out').readlines()]]

def retraceStep(intersection, instructions):
    steps = 0
    for instruction in instructions:
        pos = center
        s = 0
        for inst in instruction:
            d, n = separate(inst)
            direction = getDirection(d)
            for k in range(n):
                pos = (pos[0]+direction[0], pos[1]+direction[1])
                s += 1
                if pos[0] == intersection[0] and pos[1] == intersection[1]:
                    break
            if pos[0] == intersection[0] and pos[1] == intersection[1]:
                steps += s
                break
            
        #     print(s, pos, intersection, inst, direction)
        # print("-------")
        # print(s, pos, intersection)
    return steps

def retraceAllSteps(intersections, instructions):
    mini = 1e9
    for intersection in intersections:
        steps = retraceStep(intersection, instructions)
        print(f'{intersection} : {steps}')
        mini = min(mini, steps)
    return mini

# grid = [[0]*13000 for i in range(20000)]

# print(" --- ligne 1")
# followInst(inst[0], grid, 1)
# print(" --- ligne 2")
# followInst(inst[1], grid, 2)
# print("Part 1:", closestIntersection(grid))

intersections = readAllIntersections()
print("Part 2:", retraceAllSteps(intersections, inst))