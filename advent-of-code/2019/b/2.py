from intcode import Intcode

computer = Intcode()
computer.loadProg('2.in')

computer.prog[1] = 12
computer.prog[2] = 2
computer.run()
print("Part 1:", computer.prog[0])

print("Part 2:")

i = 76
for j in range(100):
    computer.reinit()
    computer.prog[1] = i
    computer.prog[2] = j
    computer.run()
    if computer.prog[0] == 19690720:
        print(f'1 - {i} / 2 - {j} :  {computer.prog[0]}')
        print(100*i + j)
        exit()