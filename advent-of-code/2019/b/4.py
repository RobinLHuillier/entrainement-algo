def increasing(num):
    n = list(map(int,list(str(num))))
    for i in range(1, len(n)):
        if n[i] < n[i-1]:
            return False
    return True

def double(num):
    n = str(num)
    for i in range(len(n)-1):
        if n[i] == n[i+1]:
            return True
    return False

def strongDouble(num):
    n = 'a' + str(num) + 'a'
    for i in range(len(n)-2):
        if n[i] == n[i+1] and n[i] != n[i-1] and n[i] != n[i+2]:
            return True
    return False

s = 0
q = 0
for i in range(307237, 769059):
    if increasing(i) and double(i):
        s += 1
        if strongDouble(i):
            q += 1
    if i%10000 == 0:
        print(i, s, q)
    
print("--- Part 1: ", s)
print("--- Part 2: ", q)