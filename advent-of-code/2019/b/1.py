mass = [int(f.split('\n')[0])for f in open("1.in")]

print("Part 1 :", sum([-2+i//3 for i in mass]))

calculated = {}

def cost(m):
    r = -2+m//3
    if r < 0:
        return 0
    return r

s = 0

for m in mass:
    while m > 0:
        m = cost(m)
        s += m

print("Part 2 :", s)
