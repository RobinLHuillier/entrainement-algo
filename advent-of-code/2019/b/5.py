from intcode import Intcode

computer = Intcode()
computer.loadProg('5.in')
computer.setVerbose(True)
computer.setInput(1)
computer.run()
print("Part 1:", computer.getOutput())

computer.reinit()
computer.setInput(5)
computer.run()
print("Part 2:", computer.getOutput())