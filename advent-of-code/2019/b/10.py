import os
import time
from copy import deepcopy
from math import gcd

def makeGrid(file):
    g = [list('.'+a.strip('\n')+'.') for a in open(file).readlines()]
    return [list('.'*len(g[0]))] + g + [list('.'*len(g[0]))] 
def printGrid(grid):
    for g in grid:
        print(''.join(g))
def highlightGrid(grid, position):
    g = deepcopy(grid)
    g[position[0]][position[1]] = 'O'
    printGrid(g)
def clear():
    os.system('cls' if os.name == 'nt' else 'clear')
def getOffset(start, end):
    return (end[0]-start[0], end[1]-start[1])
def addTuple(a, b):
    return tuple(x+y for x,y in zip(a,b))
def getSign(a):
    return 1 if abs(a) == a else -1
def getUnit(offset):
    a, b = offset
    g = gcd(a,b)
    return (a//g, b//g)
    # sa = getSign(a)
    # sb = getSign(b)
    # # if (a, b) == (-6, 1):
    # #     print(a//b, a/b, b//a, b/a)
    # if a == 0:
    #     return (0, sb)
    # if b == 0:
    #     return (sa, 0)
    # if a//b == a/b and abs(a/b) >= 1:
    #     return (sa*abs(a//b), sb)
    # if b//a == b/a and abs(b/a) >= 1:
    #     return (sa, sb*abs(b//a))
    # return (a, b)
def rayTrace(grid, start, end):
    if start == end:
        return False
    offset = getOffset(start, end)
    unit = getUnit(offset)
    position = start
    # print("---", position, end, unit, offset)
    newSeen = False
    blocked = False
    while position != end:
        position = addTuple(position, unit)
        # print(position, grid[position[0]][position[1]])
        if grid[position[0]][position[1]] == "#":
            if blocked:
                grid[position[0]][position[1]] = "X"
            else:
                grid[position[0]][position[1]] = "V"
                blocked = True
                newSeen = True
        elif grid[position[0]][position[1]] in "VX":
            blocked = True
    return newSeen
def getFrontier(grid):
    return  [(0, i)for i in range(len(grid))] + \
            [(len(grid[0])-1, i)for i in range(len(grid))] + \
            [(i, 0)for i in range(1,len(grid[0])-1)] + \
            [(i, len(grid)-1)for i in range(1,len(grid[0])-1)]
def getAllSats(grid):
    return [a for a in [(y,x) if grid[y][x] == "#" else None for y in range(len(grid)) for x in range(len(grid[0]))] if a is not None]
def countRayTrace(grid, start, allSats):
    s = 0
    for sat in allSats:
        if rayTrace(grid, start, sat):
            # print("-------", start, sat)
            s += 1
    return s
def completeRayTrace(grid):
    allSats = getAllSats(grid)
    best = 0
    pos = (0,0)
    for y in range(len(grid)-2):
        for x in range(len(grid[0])-2):
            g = deepcopy(grid)
            a, b = x+1, y+1
            if g[b][a] != '#':
                continue
            s = countRayTrace(g, (b, a), allSats)
            # clear()
            highlightGrid(g, (b, a))
            print(f'({a-1},{b-1}) : {s}')
            print()
            if s > best:
                best = s
                pos = (a-1,b-1)
    return (best, pos)

grid = makeGrid('10.in')
best, pos = completeRayTrace(grid)

print("Part 1: ", best, pos)

# allSats = getAllSats(grid)
# g = grid
# a, b = 1,1
# s = countRayTrace(g, (b, a), allSats)
# # clear()
# highlightGrid(g, (b, a))
# print(f'({b},{a}) : {s}')