from collections import defaultdict
from copy import deepcopy

class Intcode:
    
    ####################
    ####    INIT   #####
    ####################
    def __init__(self):
        self.prog = defaultdict(int)
        self.mem = defaultdict(int)
        self.position = 0
        self.numberOfModes = 3
        self.inp = []
        self.out = None
        self.verbose = False
        self.halt = False
        self.paused = False
        self.relativeBase = 0

    def loadProg(self, file):
        prog = list(map(int, open(file).readlines()[0].split(',')))
        for i, value in enumerate(prog):
            self.mem[i] = value
        self.prog = deepcopy(self.mem)

    def reinit(self):
        self.position = 0
        self.prog = deepcopy(self.mem)
        self.inp = []
        self.out = None
        self.halt = False
        self.paused = False
        self.relativeBase = 0

    def setVerbose(self, verbose):
        self.verbose = verbose

    ####################
    ####    RUN    #####
    ####################
    def stop(self):
        self.halt = True
        if self.verbose:
            print("--- STOPPING")

    def run(self):
        if self.paused and self.verbose:
            print("--- RESUMING")
        self.paused = False
        while not self.paused and not self.halt:
            self.execute()

    def execute(self):
        mode = [0]*(self.numberOfModes+1)
        code = self.prog[self.position]
        if code > 100:
            code = str(code)
            mode = code[:-2]
            code = code[-2:]
            mode = "0"*(self.numberOfModes-len(mode))+mode+"0"
            mode = list(map(int,list(mode[::-1])))
        getattr(self, f'opcode_{int(code)}')(mode)

    def advanceAndContinue(self, offset):
        self.position += offset

    def jump(self, position):
        self.position = position

    def setInput(self, inp):
        if isinstance(inp, list):
            self.inp += inp[:]
        else:
            self.inp += [inp]
    def getInput(self):
        if len(self.inp) > 0:
            return self.inp.pop(0)
        return None
    def setOutput(self, out):
        self.out = out
        if self.verbose:
            print("--- output : ", out)
    def getOutput(self):
        return self.out

    ####################
    ####    I/O    #####
    ####################
    # mode = 0 : pointer
    # mode = 1 : immediate
    # mode = 2 : relative
    def readAtPosition(self, offset, mode):
        value = self.prog[self.position + offset]
        if mode[offset] == 0:
            return self.prog[value]
        if mode[offset] == 1:
            return value
        if mode[offset] == 2:
            return self.prog[value + self.relativeBase]

    def writeAtPosition(self, offset, value, mode):
        pos = self.prog[self.position + offset]
        if mode[offset] == 0:
            self.prog[pos] = value
            return
        if mode[offset] == 1:
            print("--- ERROR : WRITE IN IMMEDIATE MODE")
            return
        if mode[offset] == 2:
            self.prog[pos + self.relativeBase] = value
            return

    ####################
    ####  OPCODES  #####
    ####################
    # add 1 and 2 to 3
    def opcode_1(self, mode):
        self.writeAtPosition(3, self.readAtPosition(1, mode) + self.readAtPosition(2, mode), mode)
        self.advanceAndContinue(4)
    # mul 1 and 2 to 3
    def opcode_2(self, mode):
        self.writeAtPosition(3, self.readAtPosition(1, mode) * self.readAtPosition(2, mode), mode)
        self.advanceAndContinue(4)
    # write input to 1
    def opcode_3(self, mode):
        inp = self.getInput()
        if inp is None:
            self.paused = True
            if self.verbose:
                print("--- PAUSING")
            return
        self.writeAtPosition(1, inp, mode)
        self.advanceAndContinue(2)
    # output from 1
    def opcode_4(self, mode):
        self.setOutput(self.readAtPosition(1, mode))
        self.advanceAndContinue(2)
    # jump if 1 true (non zero) to 2
    def opcode_5(self, mode):
        if self.readAtPosition(1, mode) != 0:
            self.jump(self.readAtPosition(2, mode))
        else:
            self.advanceAndContinue(3)
    # jump if 1 false (zero) to 2
    def opcode_6(self, mode):
        if self.readAtPosition(1, mode) == 0:
            self.jump(self.readAtPosition(2, mode))
        else:
            self.advanceAndContinue(3)
    # less-than store "1" in 3 if 1 < 2
    def opcode_7(self, mode):
        res = 0
        if self.readAtPosition(1, mode) < self.readAtPosition(2, mode):
            res = 1
        self.writeAtPosition(3, res, mode)
        self.advanceAndContinue(4)
    # equals store "1" in 3 if 1 == 2
    def opcode_8(self, mode):
        res = 0
        if self.readAtPosition(1, mode) == self.readAtPosition(2, mode):
            res = 1
        self.writeAtPosition(3, res, mode)
        self.advanceAndContinue(4)
    # adjust relative base by offset 1
    def opcode_9(self, mode):
        self.relativeBase += self.readAtPosition(1, mode)
        self.advanceAndContinue(2)
    # stop
    def opcode_99(self, mode):
        self.stop()

