def insertion(l, e, idx):
    return l[:idx] + [e] + l[idx:]

def insertInList(l, e):
    if len(l) == 0:
        return [e]
    for i in range(len(l)):
        if compare(e, l[i]) == 1:
            return insertion(l, e, i)
    return l + [e]

def compare(e1, e2):
    # print("Compare",e1,"vs",e2)
    if isinstance(e1, int) and isinstance(e2, int):
        if e1 == e2:
            return 2 # continue
        if e1 < e2:
            return 1 # reussi
        return 0 # rate
    if isinstance(e1, int):
        return compare([e1],e2)
    if isinstance(e2, int):
        return compare(e1,[e2])
    if len(e1) > 0 and len(e2) > 0:
        res = compare(e1[0],e2[0])
        if res < 2:
            return res
        return compare(e1[1:],e2[1:])
    if len(e1) == 0 and len(e2) == 0:
        return 2
    if len(e1) == 0:
        return 1
    return 0

# lines = open("13").read().splitlines()

# part1 = 0

# for i in range(0,len(lines),3):
#     print("Paire", (i//3)+1)
#     l1 = eval(lines[i])
#     l2 = eval(lines[i+1])
#     print(l1)
#     print(l2)
#     res = compare(l1,l2)
#     if res == 1:
#         print("right order")
#         part1 += (i//3)+1
#     else:
#         print("wrong order")

#     print()


# print("Part 1:", part1)


lines = open("13").read().splitlines()

while '' in lines:
    lines.remove('')

order = []

for l in lines:
    order = insertInList(order, eval(l))
for l in ([[2]],[[6]]):
    order = insertInList(order, l)

a = order.index([[2]])+1
b = order.index([[6]])+1

for o in order:
    print(o) 

print("Part 2:",a*b)
