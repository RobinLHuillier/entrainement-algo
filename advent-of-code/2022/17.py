rocks = [
    [
        "####"
    ],
    [
        ".#.",
        "###",
        ".#."
    ],
    [
        "..#",
        "..#",
        "###"
    ],
    [
        "#",
        "#",
        "#",
        "#"
    ],
    [
        "##",
        "##"
    ]
]

movement = input()

def printGrid(grid, hauteurMax):
    for i in range(hauteurMax-5, min(len(grid), hauteurMax+15)):
        print(''.join(grid[i]))

def rewriteChar(s, pos, char):
    return s[:pos] + char + s[pos+1:]

def addToGrid(grid, rock, i, j):
    for k in range(len(rock)):
        for l in range(len(rock[0])):
            if rock[k][l] == "#":
                grid[i+k][j+l] = rock[k][l]

def tryMove(grid, rock, i, j):
    for k in range(len(rock)):
        for l in range(len(rock[0])):
            if grid[i+k][j+l] != '.' and rock[k][l] == '#':
                return False
    return True

numberOfRocks = 2022
grid = [[k for k in"|.......|"]for i in range(4*numberOfRocks)] # 4*2022 devrait suffire dans le pire cas part 1 
grid[-1] = "---------"
hauteurMax = 4*numberOfRocks-1

currentMovement = 0

for r in range(numberOfRocks):
    rock = rocks[r%5]
    i = hauteurMax-3-len(rock)
    j = 3
    while True:
        move = movement[currentMovement]
        currentMovement = (currentMovement+1)%len(movement)
        if move == '>'and tryMove(grid, rock, i, j+1):
            j += 1
        if move == '<' and tryMove(grid, rock, i, j-1):
            j -= 1

        if tryMove(grid, rock, i+1, j):
            i += 1
        else:
            break
    if i < hauteurMax:
        hauteurMax = i
    addToGrid(grid, rock, i, j)
    # printGrid(grid, hauteurMax)
    # print()

# printGrid(grid, hauteurMax)
print("Part 1 :" , len(grid)-1-hauteurMax)

# part 2 :
# trouver à un moment une ligne remplie