def mix(file, pos):
    for i in range(len(file)):
        idx = pos.index(i)
        newPos = (file[idx]+idx)%(len(file)-1)
        save = file[idx]
        savePos = pos[idx]
        file = file[:idx] + file[idx+1:]
        pos = pos[:idx] + pos[idx+1:]
        file = file[:newPos] + [save] + file[newPos:]
        pos = pos[:newPos] + [savePos] + pos[newPos:]
    return file, pos

file = list(map(int,open("20").read().splitlines()))
pos = [i for i in range(len(file))]
file, _ = mix(file, pos)
zero = file.index(0)
grove = file[(zero+1000)%len(file)] + file[(zero+2000)%len(file)] + file[(zero+3000)%len(file)]
print("Part 1:", grove == 2622)

file = list(map(int,open("20").read().splitlines()))
pos = [i for i in range(len(file))]
for i in range(len(file)):
    file[i] *= 811589153
for _ in range(10):
    file, pos = mix(file,pos)
zero = file.index(0)
grove = file[(zero+1000)%len(file)] + file[(zero+2000)%len(file)] + file[(zero+3000)%len(file)]
print("Part 2:", grove)