def snafu(num):
    res = 0
    for i in range(len(num)):
        n = num[i]
        if n == "-":
            n = -1
        elif n == "=":
            n = -2
        else:
            n = int(n)
        res += n*(5**(len(num)-i-1))
    return res

c = 0
for s in open("25").read().splitlines():
    c += snafu(s)

print(c)

# snafu me senpai

# # i = 0
# # while 5**i < c:
# #     i += 1
# # print(5**i - c, i)
# s = "2" + "2"*19

# res = snafu(s)
# for i in range(len(s)-1,-1,-1):
#     p = len(s)-i-1
#     diff = abs(res - c)
#     print(s, res-c, diff)
#     lsave = "2"
#     for l in "-=012":
#         ns = s[:i] + l + s[i+1:]
#         sn = snafu(ns)
#         # if i == 1:
#         #     print(l, sn, abs(sn-c))
#         if abs(sn - c) < diff:
#             diff = abs(sn - c)
#             lsave = l
#     s = s[:i] + lsave + s[i+1:]
#     res = snafu(s)
#     # break

# randomizer:
import random

def randomSnafu(length):
    s = ""
    for i in range(length):
        s += random.choice("=-012")
    return s

def generateFromScratch(goal):
    r = []
    for i in range(1000):
        for l in range(15,25):
            s = randomSnafu(l)
            v = snafu(s)
            d = abs(v - goal)
            r.append((d,v,s))
    r.sort(key=lambda x:x[0])
    return r[0]

def changeOneLetter(s):
    pos = random.randint(0,len(s))
    l = random.choice("=-012")
    return s[:pos] + l + s[pos+1:]

def changeSomeLetters(s):
    ns = s
    for i in range(random.randint(1,6)):
        ns = changeOneLetter(ns)
    return ns

def generateFromPrev(goal, d,v,s):
    r = [(d,v,s)]
    for i in range(10000):
        ns = changeSomeLetters(s)
        v = snafu(ns)
        d = abs(v - goal)
        r.append((d,v,s))
    r.sort(key=lambda x:x[0])
    return r[0]

def everChanging(goal, seed, quantity=1e25):
    bs = seed
    bv = snafu(bs)
    bd = abs(bv - goal)
    for i in range(quantity):
        s = changeSomeLetters(bs)
        v = snafu(s)
        d = abs(v - goal)
        if d < bd:
            print("Better: ", i, d, v, s, goal)
            bs = s
            bv = v
            bd = d
        if d == 0:
            break
    return bs,bv,bd

d,v,s = generateFromScratch(c)
print(d, v, s)
# for i in range(100):
#     print("itér", i)
#     d,v,s = generateFromPrev(c, d,v,s)
#     print(d,v,s)
everChanging(c, s, 1000000)