def readInput(path):
    lines = open(path).read().splitlines()
    w = len(lines[0])
    h = len(lines)
    grid = [['.']*w*5 for _ in range(h*5)]
    for i in range(h):
        for j in range(w):
            grid[2*h+i][2*w+j] = lines[i][j]
    order = ['north', 'south', 'west', 'east']
    return grid, order

def getMinMax(grid):
    minI, maxI, minJ, maxJ = len(grid),0,len(grid[0]),0
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == '#':
                minI = min(i, minI)
                maxI = max(i, maxI)
                minJ = min(j, minJ)
                maxJ = max(j, maxJ)
    return minI, maxI, minJ, maxJ

def printSmallestGrid(grid):
    minI, maxI, minJ, maxJ = getMinMax(grid)
    for i in range(minI, maxI+1):
        print(''.join(grid[i][minJ:maxJ+1]))

def getAllElves(grid):
    elves = []
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == '#':
                elves.append((i,j))
    return elves

def allEmptyAround(grid, elf):
    i,j = elf
    c = 0
    for k in range(i-1,i+2):
        for l in range(j-1,j+2):
            if grid[k][l] == '#':
                c+=1
                if c == 2:
                    return False
    return True

def checkDirection(grid, elf, direction):
    i,j = elf
    if direction == 'north':
        for k in range(j-1,j+2):
            if grid[i-1][k] == '#':
                return False
        return True
    if direction == 'south':
        for k in range(j-1,j+2):
            if grid[i+1][k] == '#':
                return False
        return True
    if direction == 'east':
        for k in range(i-1,i+2):
            if grid[k][j+1] == '#':
                return False
        return True
    if direction == 'west':
        for k in range(i-1,i+2):
            if grid[k][j-1] == '#':
                return False
        return True

def getMoveProposition(elf, direction):
    i,j = elf
    if direction == 'north':
        return (i-1,j)
    if direction == 'south':
        return (i+1,j)
    if direction == 'west':
        return (i,j-1)
    if direction == 'east':
        return (i,j+1)

def getAllPropositions(grid, order):
    elves = getAllElves(grid)
    propositions = []
    for e in elves:
        if allEmptyAround(grid, e):
            propositions.append(None)
        else:
            dirPossible = False
            for o in range(len(order)):
                if checkDirection(grid, e, order[o]):
                    propositions.append(getMoveProposition(e, order[o]))
                    dirPossible = True
                    break
            if not dirPossible:
                propositions.append(None)
    return elves, propositions

def reducePropositions(propositions):
    for i in range(len(propositions)):
        prop = propositions[i]
        if prop is None:
            continue
        toDel = False
        for j in range(i+1,len(propositions)):
            if i == j:
                continue
            if prop == propositions[j]:
                toDel = True
                propositions[j] = None
        if toDel:
            propositions[i] = None

def moveAllElves(grid, elves, propositions):
    for i in range(len(elves)):
        elf = elves[i]
        if propositions[i] is None:
            continue
        grid[elf[0]][elf[1]] = '.'
        grid[propositions[i][0]][propositions[i][1]] = '#'

def wrapOrder(order):
    o = order.pop(0)
    order.append(o)

def noProposition(propositions):
    for p in propositions:
        if p is not None:
            return False
    return True

def round(grid, order):
    elves, propositions = getAllPropositions(grid, order)
    reducePropositions(propositions)
    moveAllElves(grid, elves, propositions)
    wrapOrder(order)
    stop = noProposition(propositions)
    return grid, order, stop

def quantityOfEmptyGround(grid):
    minI, maxI, minJ, maxJ = getMinMax(grid)
    c = 0
    for i in range(minI,maxI+1):
        for j in range(minJ,maxJ+1):
            if grid[i][j] == '.':
                c += 1
    return c

def makeXRounds(grid, order, X, verbose=False):
    for _ in range(X):
        grid, order, stop = round(grid, order)
        if verbose:
            print("Round", _+1)
            printSmallestGrid(grid)
    return grid, order

def makeRoundsWhileMoving(grid, order, verbose=False):
    stop = False
    roundCpt = 0
    while not stop:
        grid, order, stop = round(grid, order)
        roundCpt += 1
        if verbose:
            print("Round", roundCpt)
            printSmallestGrid(grid)
    return grid, order, roundCpt

grid, order = readInput("23")
grid, order = makeXRounds(grid, order, 10)
print("Part 1: ", quantityOfEmptyGround(grid))

grid, order = readInput("23")
grid, order, roundCpt = makeRoundsWhileMoving(grid, order)
print("Part 2: ", roundCpt)