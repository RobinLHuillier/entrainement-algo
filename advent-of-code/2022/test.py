def range_custom(n):
  i = 0
  while i < n:
    yield i
    i += 1

print(range_custom(4))
print(range_custom(4))
print(range_custom(4))