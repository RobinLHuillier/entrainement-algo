import heapq

lines = open("16").read().splitlines()

system = dict()

def bfsFromXtoY(tree, x, y):
    queue = [(x, [x])]
    best = 1e9
    while queue:
        pos, path = queue.pop(0)
        for v in tree[pos][1]:
            if v not in path:
                if v == y:
                    best = min(best, len(path))
                    break
                queue.append((v, path+[v]))
    return best

def getNonOpened(opened, keys):
    r = []
    for k in keys:
        if k not in opened:
            r.append(k)
    return r

for l in lines:
    l = l.split()
    name = l[1]
    flow = int(l[4].split('=')[1][:-1])
    valves = [i.split(',')[0] for i in l[9:]]
    opened = False
    system[name] = (flow, valves, dict())

for s in system.keys():
    print(s)
    for s2 in system.keys():
        if s == s2:
            continue
        res = bfsFromXtoY(system, s, s2)
        system[s][2][s2] = res

for s in system.keys():
    print(s, system[s])

queue = [(0, 'AA', [], 0)]
best = (0, 'AA', [], 0)
n = len(system.keys())
cpt = 0

while queue:
    pressure, pos, opened, minute = heapq.heappop(queue)
    pressure *= -1
    cpt += 1
    if cpt%1000 == 0:
        print(len(queue), best[0], pressure, pos, ' '.join(opened), minute)
    if minute >= 30 or len(opened) == n:
        if pressure > best[0]:
            best = (pressure, pos, opened, minute)
        continue

    # ajouter ouverture si pas ouvert
    if pos not in opened:
        if system[pos][0] == 0:
            opened += [pos]
        else:
            valvePressure = (29-minute)*system[pos][0]
            heapq.heappush(queue, (-(pressure+valvePressure), pos, opened+[pos], minute+1))
            continue

    # pour chaque valve ajouter déplacement vers une non ouverte
    for v in getNonOpened(opened, system.keys()):
        if v != pos and minute+system[pos][2][v] < 30:
            heapq.heappush(queue, (-pressure, v, opened, minute+system[pos][2][v]))


# greedy:
# trouver le meilleur

# print(best)
        # p,i,j = heapq.heappop(q)
                        # heapq.heappush(q, (p+1,di,dj))