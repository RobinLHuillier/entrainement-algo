def manDist(x1,y1,x2,y2):
    return abs(x1-x2)+(abs(y1-y2))

lines = open("15").read().splitlines()

sensors = [] # (xs,ys,manDist,xb,yb)
xmin = 1e20
xmax = 0
maxDist = 0

for l in lines:
    sens, beac = l.split(':')
    sens = sens.split()
    xs = int(sens[2][2:-1])
    ys = int(sens[3][2:])
    beac = beac.split()
    xb = int(beac[4][2:-1])
    yb = int(beac[5][2:])
    dist = manDist(xs,ys,xb,yb)
    sensors.append((xs,ys,dist,xb,yb))
    xmin = min(xmin, xs)
    xmax = max(xmax, xs)
    maxDist = max(maxDist, dist)

for s in sensors:
    print(s)

print(xmin, xmax, maxDist)

# c = 0
# y = 10
# for x in range(xmin-maxDist, xmax+maxDist+1):
#     # if x%1000 == 0:
#     #     print(x, xmax)
#     for s in sensors:
#         xs,ys,dist,_,_ = s
#         if manDist(xs,ys,x,y) <= dist:
#             c += 1
#             break
#     for s in sensors:
#         _,_,_,xb,yb = s
#         if xb == x and yb == y:
#             c -= 1
#             break

# print("Part 1: ", c)

# total = 4000001
# # for x in range(20):
# for x in range(4000001):
#     # for y in range(20):
#     for y in range(4000001):
#         if y%100000:
#             print(x/total, y/total)
#         found = True
#         for s in sensors:
#             xs,ys,dist,_,_ = s
#             if manDist(xs,ys,x,y) <= dist:
#                 c += 1
#                 found = False
#                 break
#         if found:
#             print("TROU TROUVE", x, y, x*4000000+y)
#             exit()

maxi = 4000000

def overlap(t, a):
    return t[0] <= a and t[1] >= a

def union(l, a, b):
    ta = ()
    tb = ()
    for t in l:
        if overlap(t, a):
            ta = t
        elif overlap(t, b):
            tb = t
    if ta == tb and ta == ():
        l.append((a,b))
    elif ta == () or tb == ():
        if ta == ():
            l.remove(tb)
            l.append((min(a,tb[0]),tb[1]))
        else:
            l.remove(ta)
            l.append((ta[0],max(b,ta[1])))
    else:
        l.remove(ta)
        l.remove(tb)
        l.append((ta[0],tb[1]))
    while True:
        stop = False
        for t in l:
            if stop:
                break
            for t2 in l:
                if t == t2:
                    continue
                if t[0] > t2[1]:
                    t,t2 = t2,t
                if t[1] == t2[0]-1:
                    l.remove(t)
                    l.remove(t2)
                    l.append((t[0],t2[1]))
                    stop = True
                    break
                if t[0] <= t2[0] and t[1] >= t2[1]:
                    l.remove(t2)
                    stop = True
                    break
        if not stop:
            break

# def trou(l, mini, maxi):
#     if l[0][0] > mini:
#         return l[0][0] -1
#     if len(l) == 1:
#         if l[0][1] < maxi:
#             return l[0][1] +1
#     l.sort(key=lambda x:x[0])
#     for i in range(len(l)-1):
#         a,b = l[i]
#         c,d = l[i+1]
#         if b+1 < c

from tqdm import tqdm

for y in tqdm(range(maxi)):
    # if y%1000 == 0:
    #     print(y, y/maxi)
    impos = []
    for s in sensors:
        xs,ys,dist,_,_ = s
        # print()
        # print(f"abs(x-{xs}) + abs(y-{ys}) > {dist}")
        # print(f"abs(x-{xs}) > {dist - abs(y-ys)}")
        diff = dist - abs(y-ys)
        if diff < 0:
            # print("aucun changement")
            continue
        minX = max(0, abs(xs) - diff)
        maxX = min(maxi, abs(xs) + diff)
        # print(f"x < {minX} ou x > {maxX}")
        union(impos, minX, maxX)
        # print(impos)
    if len(impos) > 1:
        x = impos[0][1]+1
        print(f"TROU x={x} y={y}")
        print(f"part2 : {x*4000000+y}")
        exit()

"""
Trouver x/y tels que:
0 <= x <= 4000000
0 <= y <= 4000000
abs(x-2765643) + abs(y-3042538) > 770044

"""


"""
Trouver x/y tels que:
0 <= x <= 20
0 <= y <= 20
abs(x-2) + abs(y-18) > 7
abs(x-9) + abs(y-16) > 1
abs(x-13) + abs(y-2) > 3
abs(x-12) + abs(y-14) > 4
abs(x-10) + abs(y-20) > 4
abs(x-14) + abs(y-17) > 5
abs(x-8) + abs(y-7) > 9
abs(x-2) + abs(y-0) > 10
abs(x-0) + abs(y-11) > 3
abs(x-20) + abs(y-14) > 8
abs(x-17) + abs(y-20) > 6
abs(x-16) + abs(y-7) > 5
abs(x-14) + abs(y-3) > 1
abs(x-20) + abs(y-1) > 7

0 <= x <= 20 && 0 <= y <= 20 && abs(x-2) + abs(y-18) > 7  && abs(x-9) + abs(y-16) > 1 && abs(x-13) + abs(y-2) > 3 && abs(x-12) + abs(y-14) > 4 && abs(x-8) + abs(y-7) > 9 && abs(x-2) + abs(y-0) > 10 && abs(x-0) + abs(y-11) > 3 && abs(x-20) + abs(y-14) > 8 && abs(x-17) + abs(y-20) > 6 && abs(x-16) + abs(y-7) > 5 && abs(x-14) + abs(y-3) > 1 && abs(x-20) + abs(y-1) > 7
"""