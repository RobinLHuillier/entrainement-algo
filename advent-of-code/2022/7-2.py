class Node:
    def __init__(self, name, size, dir, parent=None):
        self.name = name
        self.size = size
        self.dir = dir
        self.children = []
        self.parent = parent
    def getChild(self, name):
        return [n for n in self.children if n.name==name][0]
    def totalSize(self):
        return sum(c.totalSize() for c in self.children) + self.size
    def part1(self):
        return (self.totalSize() if self.dir and self.totalSize() <= 100000 else 0) + sum(c.part1()for c in self.children)
    def part2(self, needToDelete):
        return min(
            self.totalSize() if self.dir and self.totalSize() >= needToDelete else 1e25,
            min([c.part2(needToDelete)for c in self.children],default=1e25))
    def print(self):
        string = ["File : " + self.name + " (" + str(self.size) + ")" + "\n"]
        if self.dir:
            string = ["Dir " + self.name + " (" + str(self.totalSize()) + ")" + "\n"]
            for c in self.children:
                for r in c.print():
                    string.append('--' + r)
        return string

[((currentNode.children.append(Node(args[1], 0, True, currentNode))) if args[0]=='dir' else (
    currentNode.children.append(Node(args[1], int(args[0]), False, currentNode)))) if args[0] != '$' else (
        '' if args[1]!='cd' else(
            firstDir:=Node(args[2], 0, True), currentNode:=firstDir) if args[2]=='/' else (
                currentNode:=currentNode.parent) if args[2]=='..' else (
                    currentNode:=currentNode.getChild(args[2])))
                        for args in [e.split()for e in [a.rstrip() for a in open("7-2").readlines()]]]

print(''.join(firstDir.print()))
print("Part 1:", firstDir.part1() == 1084134)
print("Part 2:", firstDir.part2(30000000-(70000000-firstDir.totalSize())) == 6183184)

