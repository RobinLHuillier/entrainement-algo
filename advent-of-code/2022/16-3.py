import heapq

lines = open("16").read().splitlines()

system = dict()

def getNonOpened(opened, keys, tree):
    r = []
    for k in keys:
        if k not in opened:
            if tree[k][0] > 0:
                r.append(k)
    return r

for l in lines:
    l = l.split()
    name = l[1]
    flow = int(l[4].split('=')[1][:-1])
    valves = [i.split(',')[0] for i in l[9:]]
    opened = False
    system[name] = (flow, valves, dict())

def floydWarshall(g):
    keys = list(g.keys())
    w = [[1e9]*len(keys)for _ in range(len(keys))]
    for i in range(len(keys)):
        key = keys[i]
        for s in g[key][1]:
            w[i][keys.index(s)] = 1
            w[keys.index(s)][i] = 1
    for k in range(len(keys)):
        for i in range(len(keys)):
            for j in range(len(keys)):
                w[i][j] = min(w[i][j], w[i][k]+w[k][j])
    for i in range(len(keys)):
        g[keys[i]] = (g[keys[i]][0], g[keys[i]][1], dict())
        for j in range(len(keys)):
            if i == j:
                continue
            g[keys[i]][2][keys[j]] = w[i][j]

floydWarshall(system)

queue = [(0, 'AA', [], [], 0)]
best = (0, 'AA', [], 0)
n = len(system.keys())
cpt = 0

bestByMinute = dict()
for i in range(5,31):
    bestByMinute[i] = 0

while queue:
    pressure, pos, opened, path, minute = heapq.heappop(queue)
    pressure *= -1
    if minute > 5:
        if bestByMinute[minute] < pressure:
            bestByMinute[minute] = pressure
            for k in range(minute+1, 31):
                bestByMinute[k] = max(pressure, bestByMinute[k])
        elif bestByMinute[minute]-500 > pressure:
            continue
    # cpt += 1
    # if cpt%1000 == 0:
    #     print(len(queue), best[0], pressure, pos, ' '.join(opened), minute)
    if pressure > best[0]:
        best = (pressure, pos, opened, path, minute)
    if minute >= 30 or len(opened) == n:
        continue

    # ajouter ouverture si pas ouvert
    if pos not in opened:
        if system[pos][0] == 0:
            opened += [pos]
        else:
            valvePressure = (29-minute)*system[pos][0]
            heapq.heappush(queue, (-(pressure+valvePressure), pos, opened+[pos], path, minute+1))
            continue

    # pour chaque valve ajouter déplacement vers une non ouverte
    for v in getNonOpened(opened, system.keys(), system):
        if v != pos and minute+system[pos][2][v] < 30:
            heapq.heappush(queue, (-pressure, v, opened, path+[(v,system[pos][2][v])], minute+system[pos][2][v]))

print(best)
print("Part 1 : ", best[0])
# keys = list(bestByMinute.keys())
# keys.sort()
# for m in keys:
    # print(m, bestByMinute[m])


queue = [(0, 'AA', 'AA', 0, 0, [], 0)]
best = (0, 0, 'AA', [], 0)
n = len(system.keys())
cpt = 0

bestByMinute = dict()
for i in range(1,27):
    bestByMinute[i] = 0

while queue:
    pressure, pos1, pos2, lockedUntil1, lockedUntil2, opened, minute = heapq.heappop(queue)
    pressure *= -1
    if minute > 26:
        continue
    if minute > 7:
        if bestByMinute[minute] < pressure:
            bestByMinute[minute] = pressure
            for k in range(minute+1, 27):
                bestByMinute[k] = max(pressure, bestByMinute[k])
        elif bestByMinute[minute]-(400-10*minute) > pressure:
            continue
    # cpt += 1
    # if cpt%1000 == 0:
    #     print(len(queue), best[0], pressure, pos1, pos2, lockedUntil1,lockedUntil2, [' '.join(opened)], minute)
    if pressure > best[0]:
        best = (pressure, pos1, pos2, opened, minute)
    if len(opened) == n:
        continue

    # un est lock
    if lockedUntil1 > minute or lockedUntil2 > minute:
        # on interverti pour n'en gérer qu'un
        if lockedUntil1 > minute:
            lockedUntil1, lockedUntil2 = lockedUntil2, lockedUntil1
            pos1, pos2 = pos2, pos1
        if pos1 not in opened and system[pos1][0] == 0:
            opened += [pos1]

        # l'autre ouvre
        if pos1 not in opened:
            valvePressure = (25-minute)*system[pos1][0]
            heapq.heappush(queue, (-(pressure+valvePressure), pos1, pos2, minute+1, lockedUntil2, opened+[pos1], minute+1))

        pushed = False
        for v in getNonOpened(opened, system.keys(), system):
            if v != pos1 and v != pos2:
                pushed = True
                heapq.heappush(queue, (-pressure, v, pos2, minute+system[pos1][2][v], lockedUntil2, opened, min(minute+system[pos1][2][v], lockedUntil2)))
        # l'autre se déplace
        if not pushed:
            heapq.heappush(queue, (-pressure, pos1, pos2, 100, lockedUntil2, opened, lockedUntil2))

    # aucun n'est lock
    if lockedUntil1 <= minute and lockedUntil2 <= minute:
        if pos1 not in opened and system[pos1][0] == 0:
            opened += [pos1]
        if pos2 not in opened and system[pos2][0] == 0:
            opened += [pos2]
        
        if pos1 not in opened:
            # j'ouvre et il se déplace
            valvePressure = (25-minute)*system[pos1][0]
            for v in getNonOpened(opened, system.keys(), system):
                if v != pos1 and v != pos2:
                    heapq.heappush(queue, (-(pressure+valvePressure), pos1, v, minute+1, minute+system[pos2][2][v], opened+[pos1], minute+1))

            # on ouvre tous les deux
            if pos2 not in opened:
                valvePressure += (25-minute)*system[pos2][0]
                heapq.heappush(queue, (-(pressure+valvePressure), pos1, pos2, minute+1, minute+1, opened+[pos1,pos2], minute+1))

        elif pos2 not in opened:
            # il ouvre et je me déplace
            valvePressure = (25-minute)*system[pos2][0]
            for v in getNonOpened(opened, system.keys(), system):
                if v != pos1 and v != pos2:
                    heapq.heappush(queue, (-(pressure+valvePressure), v, pos2, minute+system[pos1][2][v], minute+1, opened+[pos2], minute+1))

        # on se déplace tous les deux
        nonOpened = getNonOpened(opened, system.keys(), system)
        if len(nonOpened) == 1:
            # un seul se déplace
            v = nonOpened[0]
            if v == pos1 or v == pos2:
                continue
            heapq.heappush(queue, (-pressure, v, pos2, minute+system[pos1][2][v], minute, opened,  minute+system[pos1][2][v]))
            heapq.heappush(queue, (-pressure, pos1, v, minute, minute+system[pos2][2][v], opened, minute+system[pos2][2][v]))
        else:
            for i in range(len(nonOpened)-1):
                for j in range(i+1,len(nonOpened)):
                    v1 = nonOpened[i]
                    v2 = nonOpened[j]
                    if v1 == v2 or v1 == pos1 or v2 == pos1 or v1 == pos2 or v2 == pos2:
                        continue
                    heapq.heappush(queue, (-pressure, v1, v2, minute+system[pos1][2][v1], minute+system[pos2][2][v2], opened, minute+min(system[pos1][2][v1], system[pos2][2][v2])))

print(best)
print("Part 2 : ", best[0])
# keys = list(bestByMinute.keys())
# keys.sort()
# for m in keys:
    # print(m, bestByMinute[m])
    