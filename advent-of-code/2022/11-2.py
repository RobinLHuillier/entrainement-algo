class Worry:
    def __init__(self, num):
        self.num = num
        self.div = set()
    def addition(self, add):
        self.num += add
    def multiplication(self, mul):
        self.num = (self.num*mul)%7757
        # self.div.add(mul)
    def divisible(self, div):
        return self.num%div==0
        # if self.num%div==0:
        #     self.num //= div
        #     self.div.add(div)
        # return div in self.div

class Monkey:
    def __init__(self, num, items, op, div, ttrue, tfalse):
        self.num = num
        self.items = items
        self.op = op # (arg1, op, arg2) avec arg1/arg2 pouvant etre old
        self.div = div
        self.ttrue = ttrue
        self.tfalse = tfalse
        self.inspected = 0
    def addItem(self, item):
        self.items.append(item)
    def getItem(self):
        return self.items.pop(0)
    def nbItems(self):
        return len(self.items)
    def getValue(self, arg, old):
        if arg == "old":
            return old
        return int(arg)
    def operation(self,old):
        arg1, op, arg2 = self.op
        arg1 = self.getValue(arg1, old)
        arg2 = self.getValue(arg2, old)
        if op == "+":
            return arg1+arg2
        if op == "*":
            return arg1*arg2
        print("GROS PROBLEME D'OPERATION")
    def print(self):
        print("Monkey", self.num)
        print("--Starting items:", self.items)
        print("--Operation: new = "+self.op[0]+" "+self.op[1]+" "+self.op[2])
        print("--Test: divisible by",self.div)
        print("----If true: throw to",self.ttrue)
        print("----If false: throw to",self.tfalse)

def round(monkeys, part2=False, verbose=False):
    for m in monkeys:
        if verbose:print("Monkey",m.num)
        m.inspected += m.nbItems()
        for i in range(m.nbItems()):
            item = m.getItem()
            if verbose:print("--Inspects an item wlvl", item)
            wlvl = m.operation(item)
            # print(wlvl)
            if verbose:print("----New wlvl", wlvl)
            if not part2: 
                wlvl //= 3
                if verbose:print("----New wlvl", wlvl)
            div = m.div
            if verbose:print("----",list(wlvl.div))
            divisible = (wlvl%div==0)
            thrownTo = -1
            # wlvl = (wlvl%154619619375835662983970021289)
            if (divisible):
                if verbose:print("----Wlvl divisible by",div)
                thrownTo = m.ttrue
            else:
                if verbose:print("----Wlvl not divisible by",div)
                thrownTo = m.tfalse
            if verbose:print("----Wlvl",wlvl.num,"thrown to", thrownTo)
            monkeys[thrownTo].addItem(wlvl)


def printM(monkeys):
    for m in monkeys:
        m.print()
        print()

def printR(monkeys):
    for m in monkeys:
        print("Monkey",m.num,":",m.items)

def printI(monkeys):
    for m in monkeys:
        print("Monkey",m.num,"inspected items",m.inspected,"times")

def monkeyBusiness(monkeys):
    inspected = [m.inspected for m in monkeys]
    inspected.sort()
    return inspected[-1]*inspected[-2]

def readInput(path):
    monkeys = []
    lines = [a.strip()for a in [open(path).read().splitlines()][0]]
    for i in range(0,len(lines),7):
        numMonkey = int(lines[i].split()[1][:-1])
        starting = list(map(int,''.join(lines[i+1].split()[2:]).split(',')))
        # starting = [Worry(a)for a in starting]
        operation = lines[i+2].split()[3:]
        test = int(lines[i+3].split()[3])
        ttrue = int(lines[i+4].split()[5])
        tfalse = int(lines[i+5].split()[5])
        monkeys.append(Monkey(numMonkey, starting, operation, test, ttrue, tfalse))
    return monkeys

path = "11-2"

# monkeys = readInput(path)

# for i in range(20):
#     round(monkeys)
# print("Part 1:",monkeyBusiness(monkeys))

monkeys = readInput(path)

for i in range(10000):
    round(monkeys,part2=True)
    # print(i)
    if i+1 in (1,20,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000):
        print("--After round",i+1,"--")
        printI(monkeys)
        print()

print("Part 2:",monkeyBusiness(monkeys))