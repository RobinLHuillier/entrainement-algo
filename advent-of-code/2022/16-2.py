import heapq

lines = open("16-2").read().splitlines()

system = dict()

def bfsFromXtoY(tree, x, y):
    queue = [(x, [x])]
    best = 1e9
    while queue:
        pos, path = queue.pop(0)
        for v in tree[pos][1]:
            if v not in path:
                if v == y:
                    best = min(best, len(path))
                    break
                queue.append((v, path+[v]))
    return best

def getNonOpened(opened, keys):
    r = []
    for k in keys:
        if k not in opened:
            r.append(k)
    return r

for l in lines:
    l = l.split()
    name = l[1]
    flow = int(l[4].split('=')[1][:-1])
    valves = [i.split(',')[0] for i in l[9:]]
    opened = False
    system[name] = (flow, valves, dict())

for s in system.keys():
    print(s)
    for s2 in system.keys():
        if s == s2:
            continue
        res = bfsFromXtoY(system, s, s2)
        system[s][2][s2] = res

for s in system.keys():
    print(s, system[s])

def bestRatio(tree, x, cut, depth, sumP, minP, minutes):
    explore = []
    for v in tree[x][1]:
        if v in cut:
            continue
        explore.append(v)
    print()
    pressure = tree[x][0]
    minutes += 1
    minP += sumP
    depth += 1
    if pressure > 0:
        minP += sumP
        sumP += pressure
        minutes += 1    
    # print(x, cut, explore, sumP, minP, minutes, pressure)
    if explore == []:
        finish = (sumP, minP, minutes)
        cycle = (sumP, minP+(depth*sumP), minutes+depth)
        print("leaf", x, cut+[x], finish, cycle)
        return (finish, cycle, cut+[x])
    res = []
    for e in explore:
        f, c, cutted = bestRatio(tree, e, cut+[x], 0, 0, 0, 0)
        # print(e, f, c)
        res.append((f,c,cutted))
    for e in zip(explore, res):
        print(e[0], e[1])
    # print(list(zip(explore, res)))
    # print("res", res)
    # heuristique de choix privilégié ? 
    res.sort(reverse=True,key=lambda y:y[0][0])
    # verifier cycle, retirer les machins qui cyclent:
    while True:
        print("---")
        again = False
        for i in range(len(res)-1):
            if again:
                break
            path1 = res[i]
            for j in range(i+1,len(res)):
                path2 = res[j]
                if path1 == path2:
                    continue
                different = False
                # print(path1, path2)
                for p in path1[2]:
                    if p not in path2[2]:
                        # print(p)
                        different = True
                        break
                if not different:
                    print(path1, path2)
                    f1, c1, p = path1
                    f2, c2, _ = path2
                    f = f1
                    c = c2
                    path = p
                    if f1[2] > f2[2]:
                        f1, f2 = f2, f1
                        c1, c2 = c2, c1
                    minDiffF = f2[1] - f1[1]
                    minDiffC = c2[1] - c1[1]
                    sumPF1 = f1[0]*minDiffF + f1[1]
                    if sumPF1 < f2[1]:
                        f = f2
                    sumPC1 = c1[0]*minDiffC + c1[1]
                    if sumPC1 < c2[1]:
                        c = c2
                    print(i,j,f,c,p)
                    res.pop(j)
                    res.pop(i)
                    res.append((f,c,p))
                    again = True
                    break
        print("---")
        if not again:
            break

    for i in range(len(res)-1):
        f,c,cutted = res[i]
        minP += (c[2]*sumP) + c[1]
        sumP += c[0]
        minutes += c[2]
        for cc in cutted:
            if cc not in cut:
                cut += [cc]
    f,c,cutted = res[-1]
    print(sumP, minP, minutes, c[2])
    finish = (sumP + f[0], minP + (f[2]*sumP) + f[1], minutes + f[2])
    cycle = (sumP + c[0], minP + (c[2]*sumP) + c[1] + c[0], minutes + c[2])
    for cc in cutted:
        if cc not in cut:
            cut += [cc]
    print("node", x, cut, finish, cycle)
    return (finish, cycle, cut)

f,c,cut = bestRatio(system, 'AA', [], 0, 0, 0, -1)


exit()

queue = [(0, 'AA', [], 0)]
best = (0, 'AA', [], 0)
n = len(system.keys())
cpt = 0

while queue:
    pressure, pos, opened, minute = heapq.heappop(queue)
    pressure *= -1
    cpt += 1
    print(len(queue), best[0], pressure, pos, ' '.join(opened), minute)
    if minute >= 30 or len(opened) == n:
        if pressure > best[0]:
            best = (pressure, pos, opened, minute)
        continue

    # ajouter ouverture si pas ouvert
    if pos not in opened:
        if system[pos][0] == 0:
            opened += [pos]
        else:
            valvePressure = (29-minute)*system[pos][0]
            heapq.heappush(queue, (-(pressure+valvePressure), pos, opened+[pos], minute+1))
            continue

    # pour chaque valve ajouter déplacement vers une non ouverte
    for v in getNonOpened(opened, system.keys()):
        if v != pos and minute+system[pos][2][v] < 30:
            heapq.heappush(queue, (-pressure, v, opened, minute+system[pos][2][v]))


# greedy:
# trouver le meilleur

# print(best)
        # p,i,j = heapq.heappop(q)
                        # heapq.heappush(q, (p+1,di,dj))