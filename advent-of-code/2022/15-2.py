def manDist(x1,y1,x2,y2):
    return abs(x1-x2)+(abs(y1-y2))

lines = open("15-2").read().splitlines()

sensors = [] # (xs,ys,manDist,xb,yb)
xmin = 1e20
xmax = 0
maxDist = 0

for l in lines:
    sens, beac = l.split(':')
    sens = sens.split()
    xs = int(sens[2][2:-1])
    ys = int(sens[3][2:])
    beac = beac.split()
    xb = int(beac[4][2:-1])
    yb = int(beac[5][2:])
    dist = manDist(xs,ys,xb,yb)
    sensors.append((xs,ys,dist,xb,yb))
    xmin = min(xmin, xs)
    xmax = max(xmax, xs)
    maxDist = max(maxDist, dist)

maxiXY = 20

from sympy import *



x,y = symbols('x y')
# expr1 = reduce_inequalities(x >= 0, [])
expr1 = reduce_abs_inequalities([(x-20, '<='), (x, '>=')], x)
expr2 = reduce_abs_inequalities([(y-20, '<='), (y, '>=')], y)
expr3 = reduce_inequalities(sqrt(x-2)**2 + sqrt(y-18)**2 > 7, [])
print(expr2 & expr1)
# print(solve([expr1, expr2]))

# eq1 = Eq((abs()))


"""
Trouver x/y tels que:
0 <= x <= 20
0 <= y <= 20
abs(x-2) + abs(y-18) > 7
abs(x-9) + abs(y-16) > 1
abs(x-13) + abs(y-2) > 3
abs(x-12) + abs(y-14) > 4
abs(x-10) + abs(y-20) > 4
abs(x-14) + abs(y-17) > 5
abs(x-8) + abs(y-7) > 9
abs(x-2) + abs(y-0) > 10
abs(x-0) + abs(y-11) > 3
abs(x-20) + abs(y-14) > 8
abs(x-17) + abs(y-20) > 6
abs(x-16) + abs(y-7) > 5
abs(x-14) + abs(y-3) > 1
abs(x-20) + abs(y-1) > 7


abs(x-2) + abs(y-18) > 7, abs(x-9) + abs(y-16) > 1

Trouver x/y tels que:
0 <= x <= 4000000
0 <= y <= 4000000
abs(x-2765643) + abs(y-3042538) > 770044
abs(x-2765643) - 770044 > - abs(y-3042538)
[abs(x-2765643) - 770044]/[- abs(y-3042538)] > 1


"""