from tqdm import tqdm

rocks = [
    [
        "####"
    ],
    [
        ".#.",
        "###",
        ".#."
    ],
    [
        "..#",
        "..#",
        "###"
    ],
    [
        "#",
        "#",
        "#",
        "#"
    ],
    [
        "##",
        "##"
    ]
]

movement = input()

def printGrid(grid, hauteurMax):
    for i in range(hauteurMax-5, min(len(grid), hauteurMax+15)):
        print(''.join(grid[i]))

def rewriteChar(s, pos, char):
    return s[:pos] + char + s[pos+1:]

def addToGrid(grid, rock, i, j):
    for k in range(len(rock)):
        for l in range(len(rock[0])):
            if rock[k][l] == "#":
                grid[i+k][j+l] = rock[k][l]

def tryMove(grid, rock, i, j):
    for k in range(len(rock)):
        for l in range(len(rock[0])):
            if grid[i+k][j+l] != '.' and rock[k][l] == '#':
                return False
    return True

def scanGridForCompleteLine(grid, hauteurMax):
    for i in range(hauteurMax, len(grid)):
        if grid[i].count('#') == 6 and grid[i-1].count('#') == 6:
            print(i)

numberOfRocks = 150000
grid = [[k for k in"|.......|"]for i in range(4*numberOfRocks)] # 4*2022 devrait suffire dans le pire cas part 1 
grid[-1] = "---------"
hauteurMax = 4*numberOfRocks-1

currentMovement = 0

for r in range(numberOfRocks):
    rock = rocks[r%5]
    i = hauteurMax-3-len(rock)
    j = 3
    while True:
        move = movement[currentMovement]
        currentMovement = (currentMovement+1)%len(movement)
        if move == '>'and tryMove(grid, rock, i, j+1):
            j += 1
        if move == '<' and tryMove(grid, rock, i, j-1):
            j -= 1

        if tryMove(grid, rock, i+1, j):
            i += 1
        else:
            break
    if i < hauteurMax:
        hauteurMax = i
    # print(len(grid)-hauteurMax-1080)
    # if (len(grid)-hauteurMax-1080)%2778 == 0:
    #     print("---" , r, len(grid)-1-hauteurMax)
    # if r == 691 + 319:
    #     print("////", r, len(grid)-1-hauteurMax)
    # if (len(grid)-hauteurMax-1025)%53 == 0:
    #     print("---" , r, len(grid)-1-hauteurMax)
    # if r == 42+8:
    #     print("////", r, len(grid)-1-hauteurMax)
    addToGrid(grid, rock, i, j)
    # scanGridForCompleteLine(grid, hauteurMax)
    # printGrid(grid, hauteurMax)
    # print()

# printGrid(grid, hauteurMax)
# print("Part 1 :" , len(grid)-1-hauteurMax, hauteurMax)

def chercherPattern(grid, hauteurMax):
    lines = dict()
    print("Creation dictionnaire")
    for i in range(hauteurMax-2, len(grid)-1):
        s = ''.join(grid[i])
        if s not in lines:
            lines[s] = set()
        lines[s].add(i)
    for i in range(len(grid)-2, hauteurMax-1000, -1):
        candidates = lines[''.join(grid[i])]
        for j in range(1,1000):
            newCandidates = set()
            for c in candidates:
                newCandidates.add(c-1)
            nextCandidates = lines[''.join(grid[i-j])]
            candidates = newCandidates.intersection(nextCandidates)
        if len(candidates) > 1:
            print(len(candidates))
            can = [k+1000 for k in list(candidates)]
            can.sort()
            print(sorted([len(grid)-i-1 for i in can]))
            # print([can[i+1]-can[i]for i in range(len(can)-1)])
            return sorted([len(grid)-i-1 for i in can])[0]


hauteurStart = chercherPattern(grid, hauteurMax)
print(hauteurStart)
print(hauteurMax, len(grid))

bigRock = 1000000000000
rockStart = 691
hauteurStart = 1079
moduloRock = 1745
moduloHauteur = 2778
depasseRock = (bigRock-rockStart)%moduloRock
depasseHauteur = 1587 - 1079
nombreHauteur = hauteurStart + ((bigRock-rockStart)//moduloRock)*moduloHauteur + depasseHauteur -1 

print(nombreHauteur == 1591977077342)

# grid = [[k for k in"|.......|"]for i in range(4*numberOfRocks)] # 4*2022 devrait suffire dans le pire cas part 1 
# grid[-1] = "---------"
# hauteurMax = 4*numberOfRocks-1

# currentMovement = 0

# for r in range(numberOfRocks):
#     rock = rocks[r%5]
#     i = hauteurMax-3-len(rock)
#     j = 3
#     while True:
#         move = movement[currentMovement]
#         currentMovement = (currentMovement+1)%len(movement)
#         if move == '>'and tryMove(grid, rock, i, j+1):
#             j += 1
#         if move == '<' and tryMove(grid, rock, i, j-1):
#             j -= 1

#         if tryMove(grid, rock, i+1, j):
#             i += 1
#         else:
#             break
#     if i < hauteurMax:
#         hauteurMax = i
#     if hauteurMax == hauteurStart:
#         print(hauteurMax, r)
#     # print(len(grid)-hauteurMax-1080)
#     # if (len(grid)-hauteurMax-1080)%2778 == 0:
#     #     print("---" , r, len(grid)-1-hauteurMax)
#     # if r == 691 + 319:
#     #     print("////", r, len(grid)-1-hauteurMax)
#     # if (len(grid)-hauteurMax-1025)%53 == 0:
#     #     print("---" , r, len(grid)-1-hauteurMax)
#     # if r == 42+8:
#     #     print("////", r, len(grid)-1-hauteurMax)
#     addToGrid(grid, rock, i, j)



# part 2 :
# trouver à un moment une ligne remplie

# testé :
# 1591977077343

# a tester:
# 1591977077342