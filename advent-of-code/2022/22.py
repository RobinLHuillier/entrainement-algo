def readInput(path):
    lines = open(path).read().splitlines()
    lens = [len(i)for i in lines[:-1]]
    grid = [[-1]*(max(lens)+2)for i in range(len(lines))]
    password = lines[-1]
    for i in range(len(lines)-2):
        line = lines[i]
        for j in range(len(line)):
            if line[j] == '.':
                grid[i+1][j+1] = 0
            elif line[j] == '#':
                grid[i+1][j+1] = 1
    return (grid, password)

def startPosition(grid):
    i = 1
    for j in range(len(grid[1])):
        if grid[i][j] == 0:
            return (i,j)

def getNextElement(password, idx):
    if password[idx] in 'RL':
        return password[idx], idx+1
    i = idx+1
    while i < len(password) and password[i] not in 'RL':
        i += 1
    return int(password[idx:i]), i

def changeFacing(nextElem, facing):
    if nextElem == 'R':
        facing += 1
    if nextElem == 'L':
        facing -= 1
    if facing == 4:
        facing = 0
    if facing == -1:
        facing = 3
    return facing

def wrapAroundPart1(grid, i, j, di, dj):
    x,y = i,j
    while grid[x][y] != -1:
        x -= di
        y -= dj
    x += di
    y += dj
    if grid[x][y] == 1:
        return i,j
    return x,y

def move(grid, i, j, facing, nextElem, mask, size):
    for _ in range(nextElem):
        di, dj = [(0,1),(1,0),(0,-1),(-1,0)][facing]
        if grid[i+di][j+dj] == 1:
            return i,j,facing
        if grid[i+di][j+dj] == 0:
            i += di
            j += dj
            continue
        if grid[i+di][j+dj] == -1:
            if size == 0: # part 1
                i,j = wrapAroundPart1(grid, i, j, di, dj)
            else:  # part 2
                i,j,facing = wrapAroundPart2(grid, i, j, facing, size, mask)
    return i,j,facing

def scorePart1(i,j,facing):
    return 1000*i + 4*j + facing

def solvePart1(grid, password, mask=[], size=0):
    i,j = startPosition(grid)
    facing = 0
    idxPassword = 0
    while True:
        nextElem, idxPassword = getNextElement(password, idxPassword)
        if str(nextElem) in "RL":
            facing = changeFacing(nextElem, facing)
        else:
            i,j,facing = move(grid, i, j, facing, nextElem, mask, size)
        # print(nextElem, i,j,facing)
        if idxPassword == len(password):
            return (i,j,facing)

def mapGrid(grid, mask, size, h, w):
    squareGrid = [[-1]*len(grid[0])for i in range(len(grid))]
    for i in range(h):
        for j in range(w):
            for di in range(size):
                for dj in range(size):
                    squareGrid[1+i*size+di][1+j*size+dj] = mask[i][j]
    return squareGrid

# print("[EX] Part 1:", scorePart1(*solvePart1(*readInput("22-2"))))
# print("[REAL] Part 1:", scorePart1(*solvePart1(*readInput("22"))))

grid, password = readInput("22")
mask = [
    (-1,5,3),
    (-1,1,-1),
    (4,2,-1),
    (6,-1,-1)
]
squareGrid = mapGrid(grid, mask, 50, 4, 3)

def mySquare(i,j,mask,size):
    return mask[(i-1)//size][(j-1)//size]

def sortirFacing(square, facing):
    return [(3,2,4,5),(3,6,4,1),(2,1,5,6),(2,6,5,1),(3,1,4,6),(2,3,5,4)][square-1][facing]

def getLocalIJ(gi, gj, size):
    return (gi-1)%size, (gj-1)%size

def getGlobalIJ(li, lj, size, mask, square):
    for i in range(len(mask)):
        for j in range(len(mask[0])):
            if mask[i][j] == square:
                return 1+size*i+li, 1+size*j+lj

def transformFromSquareToSquare(i,j,size,squareSrc,squareDest):
    if squareSrc == 1:
        if squareDest == 3:
            return j, i, 3
        if squareDest == 4:
            return j, i, 1
    elif squareSrc == 2:
        if squareDest == 3:
            return size-i-1, j, 2
        if squareDest == 6:
            return j, i, 2
    elif squareSrc == 3:
        if squareDest == 1:
            return j, i, 2
        if squareDest == 2:
            return size-i-1, j, 2
        if squareDest == 6:
            return size-i-1, j, 3
    elif squareSrc == 4:
        if squareDest == 1:
            return j, i, 0
        if squareDest == 5:
            return size-i-1, j, 0
    elif squareSrc == 5:
        if squareDest == 4:
            return size-i-1, j, 0
        if squareDest == 6:
            return j, i, 0
    elif squareSrc == 6:
        if squareDest == 2:
            return j, i, 3
        if squareDest == 3:
            return size-i-1, j, 1
        if squareDest == 5:
            return j, i, 1
    print("DEMANDE IMPOSSIBLE", squareSrc, squareDest)

def wrapAroundPart2(grid, i, j, facing, size, mask):
    li, lj = getLocalIJ(i,j,size)
    squareSrc = mySquare(i,j,mask,size)
    squareDest = sortirFacing(squareSrc, facing)
    print(squareSrc, squareDest, i, j, facing, li, lj)
    li, lj, nfacing = transformFromSquareToSquare(li, lj, size, squareSrc, squareDest)
    ni, nj = getGlobalIJ(li, lj, size, mask, squareDest)
    print("wrap:", squareSrc, squareDest, ni, nj, nfacing, li, lj)
    if grid[ni][nj] == 1:
        print("Conflit")
        print(grid[ni])
        return i,j,facing
    return ni, nj, nfacing


print("[REAL] Part 2:", scorePart1(*solvePart1(*readInput("22"), mask, 50)))


### faire des tests par rapport à la map, commencer à tel endroit et voir si on apparait bien là où il faut


grid, password = readInput("22")
## test 3 vers 2 :
print(wrapAroundPart2(grid, 1, 150, 0, 50, mask) == (150,100,2))
## test 3 vers 6 :
print(wrapAroundPart2(grid, 1, 150, 3, 50, mask) == (200, 50, 3))
## 2 vers 3 
print(wrapAroundPart2(grid, 150, 100, 0, 50, mask) == (1,150,2))
## 3 vers 6 
print(wrapAroundPart2(grid, 1, 150, 3, 50, mask) == (200,50,3))
## 6 vers 3
print(wrapAroundPart2(grid, 200,50,1, 50, mask) == (1,150,1))


# 179009 too high

# 117330 too low