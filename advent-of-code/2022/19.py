import heapq

def readInput(path):
    blueprints = []
    for l in open(path).read().splitlines():
        ore, clay, obsidian, geode, _ = l.split('.')
        ore = ore.split()
        ore = [int(ore[-2]),0,0,0]
        clay = clay.split()
        clay = [int(clay[-2]),0,0,0]
        obsidian = obsidian.split()
        obsidian = [int(obsidian[-5]), int(obsidian[-2]),0,0]
        geode = geode.split()
        geode = [int(geode[-5]), 0,int(geode[-2]),0]
        blueprints.append((ore, clay, obsidian, geode))
    return blueprints

def canBuy(res, cost):
    for i in range(len(res)):
        if res[i] < cost[i]:
            return False
    return True

def buy(res, cost):
    for i in range(len(cost)):
        res[i] -= cost[i]

def incrRes(res, robots):
    for i in range(len(robots)):
        res[i] += robots[i]

def backtrack(bp, minutes, res, robots):
    incrRes(res, robots)
    if minutes == 0:
        return res[3]
    # print(minutes, res, robots)
    bestGeo = backtrack(bp, minutes-1, res[::], robots[::])
    for i in range(len(bp)):
        if canBuy(res, bp[i]):
            rob2 = robots[::]
            rob2[i] += 1
            res2 = res[::]
            buy(res2, bp[i])
            bestGeo = max(bestGeo, backtrack(bp, minutes-1, res2, rob2))
    return bestGeo

def btUntilFirstI(bp, minutes, res, robots, ceiling, quantity=1, bestMinute=0):
    if robots[ceiling] == quantity:
        return (minutes, res, robots)
    if minutes == bestMinute:
        return (minutes, res, robots)
    incrRes(res, robots)
    bestCombo = btUntilFirstI(bp, minutes-1, res[::], robots[::], ceiling, quantity, bestMinute)
    for i in range(ceiling+1):
        if canBuy(res, bp[i]):
            rob2 = robots[::]
            rob2[i] += 1
            res2 = res[::]
            buy(res2, bp[i])
            combo = btUntilFirstI(bp, minutes-1, res2, rob2, ceiling, quantity, bestCombo[0])
            if combo[0] > bestCombo[0]:
                bestCombo = combo
    return bestCombo

def btQueue(bp, minutes, res, robots):
    queue = [(0, minutes, res, robots)]
    bestGeo = ([0,0,0,0],[0,0,0,0])
    while queue:
        _, minutes, res, robots = heapq.heappop(queue)
        incrRes(res, robots)
        print(len(queue), bestGeo, minutes, res, robots)
        if minutes == 24:
            if res[3] > bestGeo[0][3]:
                bestGeo = (res, robots)
                if res[3] == 22:
                    exit(0)
            continue
        heapq.heappush(queue, (-robots[3], minutes+1, res[::], robots[::]))
        for i in range(len(bp)):
            if canBuy(res, bp[i]):
                rob2 = robots[::]
                rob2[i] += 1
                res2 = res[::]
                buy(res2, bp[i])
                heapq.heappush(queue, (-rob2[3], minutes+1, res2, rob2))

def bestOpenedGeodeBy(bp, minutes):
    return backtrack(bp, minutes, [0,0,0,0], [1,0,0,0])

def timePerX(bp, res, robot, X):
    time = 0
    cost = bp[X]
    for i in range(len(res)):
        if cost[i] > 0:
            time = max(time, cost[i]/(robot[i]if robot[i]>0 else 0.0001))
    return time

def timeUntilX(bp, res, robot, X):
    time = 0
    cost = bp[X]
    for i in range(len(res)):
        if cost[i] > 0:
            if res[i] < cost[i]:
                time = max(time, (cost[i]-res[i])/(robot[i]if robot[i]>0 else 0.0001))
    # print("time until", res, robot, cost, time, cost[2]-res[2], robot[2], (cost[2]-res[2])/(robot[2]if robot[2]>0 else 0.0001))
    if int(time) < time:
        return int(time)+1
    return int(time)    

def btQueueInt(bp, minutes, res, robots):
    queue = [(0, minutes, res, robots)]
    bestGeo = ([0,0,0,0],[0,0,0,0])
    heuristique = [[30,30,0]for i in range(30)]
    cpt = 0
    alreadySeen = set()
    while queue:
        _, minutes, res, robots = heapq.heappop(queue)
        cpt += 1
        if cpt%10000 == 0:
            print(cpt, len(queue), bestGeo, minutes, heuristique[minutes], res, robots)
        key = keyMe(minutes, res, robots)
        if key not in alreadySeen:
            alreadySeen.add(key)
        else:
            continue
        if minutes == 24:
            incrRes(res, robots)
            if res[3] > bestGeo[0][3]:
                bestGeo = (res, robots)
            continue
        timePerRG = timePerX(bp, res, robots, 3)
        timeUntilRG = timeUntilX(bp, res, robots, 3)
        quantityRG = robots[3]
        if minutes > 15:
            if timePerRG > heuristique[minutes][0] and timeUntilRG > heuristique[minutes][1] and quantityRG < heuristique[minutes][2]:
                continue
            if timePerRG < heuristique[minutes][0]:
                heuristique[minutes][0] = timePerRG
            if timeUntilRG < heuristique[minutes][1]:
                heuristique[minutes][1] = timeUntilRG
            if quantityRG > heuristique[minutes][2]:
                heuristique[minutes][2] = quantityRG
        res2 = res[::]
        rob2 = robots[::]
        incrRes(res2, rob2)
        heapq.heappush(queue, (timePerRG, minutes+1, res2, rob2))
        for i in range(len(bp)):
            if canBuy(res, bp[i]):
                rob2 = robots[::]
                res2 = res[::]
                buy(res2, bp[i])
                incrRes(res2, rob2)
                rob2[i] += 1
                heapq.heappush(queue, (timePerRG, minutes+1, res2, rob2))
        
    for i in range(10, 24):
        print(i, heuristique[i])
    return bestGeo

def keyMe(minutes, res, robots):
    return (minutes, *res, *robots)

def btWithMap(bp, minutes, res, robots):
    queue = [(0, minutes, res, robots)]
    bestGeo = ([0,0,0,0],[0,0,0,0])
    alreadySeen = set()
    while queue:
        _, minutes, res, robots = heapq.heappop(queue)
        print(len(queue), bestGeo, minutes, res, robots)
        key = keyMe(minutes, res, robots)
        if key not in alreadySeen:
            alreadySeen.add(key)
        else:
            continue
        if minutes == 24:
            incrRes(res, robots)
            if res[3] > bestGeo[0][3]:
                bestGeo = (res, robots)
            continue
        res2 = res[::]
        rob2 = robots[::]
        incrRes(res2, rob2)
        heapq.heappush(queue, (-robots[3], minutes+1, res2, rob2))
        for i in range(len(bp)):
            if canBuy(res, bp[i]):
                rob2 = robots[::]
                res2 = res[::]
                buy(res2, bp[i])
                incrRes(res2, rob2)
                rob2[i] += 1
                heapq.heappush(queue, (-rob2[3], minutes+1, res2, rob2))

blueprints = readInput("19")
best = []
for b in blueprints:
    print(b)
    bestGeo = btQueueInt(b, 1, [0,0,0,0], [1,0,0,0])
    best.append(bestGeo)
    # print(btWithMap(b, 1, [0,0,0,0], [1,0,0,0]))
    # firstClay = btUntilFirstClay(b, 24, [0,0,0,0], [1,0,0,0])
    # print(firstClay)
    # firstObsi = btUntilFirstObsidian(b, firstClay[0], firstClay[1], firstClay[2])
    # print(firstObsi)
    # firstClay = btUntilFirstI(b, 24, [0,0,0,0], [1,0,0,0], 1)
    # print("first clay:",firstClay)
    # firstObsi = btUntilFirstI(b, firstClay[0], firstClay[1], firstClay[2], 2)
    # print("first obsi:",firstObsi)
    # firstGeo = btUntilFirstI(b, firstObsi[0], firstObsi[1], firstObsi[2], 3)
    # print("first geo:",firstGeo)
    # sndGeo = btUntilFirstI(b, firstClay[0], firstClay[1], firstClay[2], 3, 2)
    # print("second geo:", sndGeo)

print("finished")

somme = 0
for i in range(len(best)):
    geodes = best[i][0][3]
    somme += geodes*(i+1)
    print(i+1, geodes, geodes*(i+1), somme)