trees = [a.strip()for a in open("8-2").readlines()]
n = len(trees)
# grid pour enregistrer la visibilité de chaque arbre
grid = [[0]*n for _ in range(n)]

# fonction qui calcule le score scénique pour un arbre en (i,j)
def scenicScore(trees, n, i, j):
    facteur = 1
    pasSortir = lambda i,j,x,y,m,n: x*m+i>=0 and x*m+i<n and y*m+j>=0 and y*m+j<n
    for direction in ((-1,0),(1,0),(0,-1),(0,1)):
        facteurLocal = 1
        x,y = direction
        while pasSortir(i,j,x,y,facteurLocal,n) and trees[x*facteurLocal+i][y*facteurLocal+j] < trees[i][j]:
            facteurLocal += 1
        # -1 parce qu'on démarre à 1, + pasSortir pour vérifier si le dernier arbre est pas en dehors
        facteur *= (facteurLocal - 1 + pasSortir(i,j,x,y,facteurLocal,n))
    return facteur

for i in range(n):
    plusGrand = [-1,-1,-1,-1]
    for j in range(n):
        for direction in range(4):
            x,y = [(i,j),(i,n-j-1),(j,i),(n-j-1,i)][direction]
            if int(trees[x][y]) > plusGrand[direction]:
                plusGrand[direction] = int(trees[x][y])
                grid[x][y] = 1

visible,meilleurArbre = 0,0
for i in range(n):
    for j in range(n):
        meilleurArbre = max(meilleurArbre, scenicScore(trees, n, i, j))
        if grid[i][j] > 0 :
            visible += 1

print("Part 1: ", visible==1832)
print("Part 2: ", meilleurArbre==157320)