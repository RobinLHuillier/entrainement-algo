prog = open("10-2").read().splitlines()

c = 1
val = [1 for _ in range(500)]
for p in prog:
    if p == 'noop':
        c += 1
    elif p != 'noop':
        num = int(p.split()[1])
        for j in range(c+2,500):
            val[j] += num
        c += 2

f = sum(val[i]*i for i in range(20,221,40))

s = ''.join("█" if abs(val[i+1]-i%40)<=1 else "░" for i in range(241))

k = "\n".join(s[i*40:i*40+40] for i in range(6))

print("Part 1: ", f)
print("Part 2: \n" + k)

# 12980