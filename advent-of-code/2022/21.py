lines = open("21").read().splitlines()

# un monkey : monkeys[name] = [number|None, calcul, [monkeys that reference it]]
# calcul: (monkey op monkey)
monkeys = dict()

queue = []

for l in lines:
    sp = l.split()
    monkey = sp[0][:-1]
    if monkey not in monkeys:
        monkeys[monkey] = [None, (), []]
    if len(sp) == 2:
        monkeys[monkey][0] = int(sp[1])
    else:
        mon1, op, mon2 = sp[1:]
        monkeys[monkey][1] = (mon1, op, mon2)
        if mon1 not in monkeys:
            monkeys[mon1] = [None, (), []]
        if mon2 not in monkeys:
            monkeys[mon2] = [None, (), []]
        monkeys[mon1][2].append(monkey)
        monkeys[mon2][2].append(monkey)
        queue.append(monkey)

# for m in monkeys.keys():
#     print(m, monkeys[m])

while monkeys['root'][0] is None:
    # print(len(queue))
    queue2 = []
    while queue:
        name = queue.pop(-1)
        num, calc, ref = monkeys[name]
        if num is None:
            mon1, op, mon2 = calc
            if monkeys[mon1][0] is not None and monkeys[mon2][0] is not None:
                monkeys[name][0] = eval(str(monkeys[mon1][0])+op+str(monkeys[mon2][0]))
                queue2 += monkeys[name][2]
                break
            else:
                queue2.append(name)
    queue += queue2

print("Part 1: ", monkeys['root'][0])


lines = open("21").read().splitlines()

# un monkey : monkeys[name] = [number|None, calcul, [monkeys that reference it]]
# calcul: (monkey op monkey)
monkeys = dict()

queue = []

for l in lines:
    sp = l.split()
    monkey = sp[0][:-1]
    if monkey not in monkeys:
        monkeys[monkey] = [None, (), []]
    if len(sp) == 2:
        monkeys[monkey][0] = int(sp[1])
        if monkey == 'humn':
            monkeys[monkey][0] = None
    else:
        mon1, op, mon2 = sp[1:]
        if monkey == 'root':
            op = '=='
        monkeys[monkey][1] = (mon1, op, mon2)
        if mon1 not in monkeys:
            monkeys[mon1] = [None, (), []]
        if mon2 not in monkeys:
            monkeys[mon2] = [None, (), []]
        monkeys[mon1][2].append(monkey)
        monkeys[mon2][2].append(monkey)
        queue.append(monkey)

# for m in monkeys.keys():
#     print(m, monkeys[m])

while monkeys['root'][0] is None:
    queue2 = []
    completePass = True
    while queue:
        name = queue.pop(-1)
        num, calc, ref = monkeys[name]
        if name == 'root' or name == 'humn':
            continue
        if num is None:
            mon1, op, mon2 = calc
            if monkeys[mon1][0] is not None and monkeys[mon2][0] is not None:
                monkeys[name][0] = eval(str(monkeys[mon1][0])+op+str(monkeys[mon2][0]))
                queue2 += monkeys[name][2]
                completePass = False
                break
            else:
                queue2.append(name)
    if completePass:
        break
    queue += queue2

# for m in monkeys.keys():
#     print(m, monkeys[m])

# print complete operation

def getUnderlyingOp(monkeys, monk):
    if monkeys[monk][0] is not None:
        return str(monkeys[monk][0])
    if monkeys[monk][1] == ():
        return monk
    m1, op, m2 = monkeys[monk][1]
    return "(" + getUnderlyingOp(monkeys, m1) + " " + op + " " + getUnderlyingOp(monkeys, m2) + ")"

def humanToNum(string, num):
    i = string.index('humn')
    return string[:i] + str(num) + string[i+4:]

strRes = getUnderlyingOp(monkeys, 'root')

print(strRes)

part1 = strRes.split('==')[0][1:]
part2 = float(strRes.split('==')[1][:-1])

str0 = humanToNum(part1, 0)
print(eval(str0))

str1 = humanToNum(part1, 1)
print(eval(str1))

diff = eval(str1)-eval(str0)
diff2 = eval(humanToNum(part1, 2)) - eval(str1)
diff3 = eval(str0) - eval(humanToNum(part1, -1))
diff4 = eval(humanToNum(part1, -1)) - eval(humanToNum(part1, -2))
print(diff, diff2, diff3, diff4)

start = eval(str0)
print(part2, part2-start, diff, (part2-start)/diff)

t = int((part2-start)/diff)
str2 = humanToNum(part1, t)
# while eval(str2) < part2:
#     t += 1
#     str2 = humanToNum(part1, t)
#     print(eval(str2))
# while eval(str2) > part2:
#     t += 1
#     str2 = humanToNum(part1, t)
#     print(eval(str2), part2, part2-eval(str2))
print(eval(str2), part2, part2-eval(str2))

manque = (part2-eval(str2))/diff
while abs(manque) > 1:
    t += manque
    manque = (part2-eval(humanToNum(part1, t)))/diff
    print(part2-eval(humanToNum(part1, t)), manque)

print("Part 2:",t)