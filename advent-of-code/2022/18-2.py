lines = open("18").read().splitlines()
cubes = []

for l in lines:
    x,y,z = eval(l)
    exposed = 6
    for i in range(len(cubes)):
        a,b,c,_ = cubes[i]
        if abs(c-z)+abs(b-y)+abs(a-x) == 1:
            cubes[i][3] -= 1
            exposed -= 1
    cubes.append([x,y,z,exposed])

print("Part 1: ", sum(c[3]for c in cubes) == 3636)

NOTHING = 0
OUTSIDE = 1
CUBE = 2
size = 21

grid = [[[NOTHING for x in range(size)]for y in range(size)]for z in range(size)]

for c in cubes:
    a,b,c,_ = c
    grid[a][b][c] = CUBE

queue = [(0,0,0)]

# on propage OUTSIDE
while queue:
    a,b,c = queue.pop()
    assert grid[a][b][c] == NOTHING
    grid[a][b][c] = OUTSIDE
    for w in ((-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)):
        x,y,z = w
        if x+a<0 or y+b<0 or z+c<0 or x+a>=size or y+b>=size or z+c>=size:
            continue
        if grid[x+a][y+b][z+c]!=NOTHING:
            continue
        if (x+a,y+b,z+c) in queue:
            continue
        queue.append((x+a,y+b,z+c))

cpt = 0
for cube in cubes:
    a,b,c,_ = cube
    for w in ((-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)):
        x,y,z = w
        cpt += (grid[x+a][y+b][z+c]==OUTSIDE)
    
print("Part 2: ", cpt == 2102)