distanceManhattan = lambda a,b,x,y : abs(a-x)+abs(b-y)
signe = lambda val : 0 if val==0 else abs(val)//val

def moveHead(posHead, letter):
    x, y = posHead
    diff = ((1,0),(-1,0),(0,1),(0,-1))
    dx, dy = diff['RLUD'.index(letter)]
    return (x+dx, y+dy)

def moveFollow(posHead, posFollow):
    xH, yH = posHead
    xT, yT = posFollow
    d = distanceManhattan(*posHead,*posFollow)
    if d <= 1 or (xT != xH and yT != yH and d == 2):
        return posFollow
    return (xT + signe(xH - xT), yT + signe(yH - yT))

def resolveMove(lengthRope, moves):
    posRope = [(0,0) for _ in range(lengthRope)]
    posTailVisited = set()
    for m in moves:
        letter, num = m
        for _ in range(num):
            posRope[0] = moveHead(posRope[0], letter)
            for j in range(lengthRope-1):
                posRope[j+1] = moveFollow(posRope[j], posRope[j+1])
            posTailVisited.add(posRope[-1])
    return len(posTailVisited)

moves = [(a[0], int(a[1])) for a in [a.split() for a in open("9").read().splitlines()]]

print("Part 1: ", resolveMove(2, moves) == 6314)
print("Part 2: ", resolveMove(10, moves) == 2504)