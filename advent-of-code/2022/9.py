def distMan(posH,posT):
    xH,yH = posH
    xT,yT = posT
    return abs(xH-xT)+abs(yH-yT)

def sign(val):
    if val > 0:
        return 1
    if val < 0:
        return -1
    return 0

def moveH(posH, letter):
    x,y = posH
    if letter == 'R':
        x += 1
    if letter == 'L':
        x -= 1
    if letter == 'U':
        y += 1
    if letter == 'D':
        y -= 1
    return (x,y)

def moveT(posH, posT):
    xH,yH = posH
    xT,yT = posT
    d = distMan(posH, posT)
    if (xT == xH or yT == yH):
        if d <= 1:
            return posT
        if xT == xH:
            yT += sign(yH - yT)
        else:
            xT += sign(xH - xT)
    if (xT != xH and yT != yH):
        if d == 2:
            return posT
        yT += sign(yH - yT)
        xT += sign(xH - xT)
    return (xT,yT)

moves = open("9").read().splitlines()

posTVisited = set()
posH = (0,0)
posT = (0,0)

for m in moves:
    # print("move: ", m)
    letter, num = m.split()
    num = int(num)
    for i in range(num):
        posH = moveH(posH, letter)
        posT = moveT(posH, posT)
        posTVisited.add(posT)

print("Part 1: ", len(posTVisited) == 6314)

posTVisited = set()
posK = [(0,0)for _ in range(10)]

for m in moves:
    # print("move: ", m)
    letter, num = m.split()
    num = int(num)
    for i in range(num):
        posK[0] = moveH(posK[0], letter)
        for j in range(9):
            posK[j+1] = moveT(posK[j], posK[j+1])
        # for j in range(9):
            # print(posK[j], end=" ")
        # print()
        posTVisited.add(posK[9])

print("Part 2: ", len(posTVisited) == 2504)