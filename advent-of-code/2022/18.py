lines = open("18").read().splitlines()

cubes = []

for l in lines:
    x,y,z = eval(l)
    e = 6
    
    for i in range(len(cubes)):
        a,b,c, exposed = cubes[i]
        if ((a == x and b == y and abs(c-z) == 1) or
            (a == x and c == z and abs(b-y) == 1) or 
            (c == z and b == y and abs(a-x) == 1)):
            cubes[i][3] -= 1
            e -= 1
    cubes.append([x,y,z,e])

print("Part 1: ", sum(c[3]for c in cubes))

NOTHING = 0
OUTSIDE = 1
CUBE = 2
size = 20

grid = [[[NOTHING for x in range(size)]for y in range(size)]for z in range(size)]

for c in cubes:
    a,b,c,_ = c
    grid[a][b][c] = CUBE

queue = [(0,0,0)]

# on propage OUTSIDE
while queue:
    a,b,c = queue.pop()
    assert grid[a][b][c] == NOTHING
    grid[a][b][c] = OUTSIDE
    for w in ((-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)):
        x,y,z = w
        if x+a<0 or y+b<0 or z+c<0 or x+a>=size or y+b>=size or z+c>=size:
            continue
        if grid[x+a][y+b][z+c]!=NOTHING:
            continue
        if (x+a,y+b,z+c) in queue:
            continue
        queue.append((x+a,y+b,z+c))

cpt = 0
for a in range(size):
    for b in range(size):
        for c in range(size):
            if grid[a][b][c] == CUBE:
                for w in ((-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)):
                    x,y,z = w
                    if x+a<0 or y+b<0 or z+c<0 or x+a>=size or y+b>=size or z+c>=size or grid[x+a][y+b][z+c] == OUTSIDE:
                        cpt += 1
    
print("Part 2: ", cpt)