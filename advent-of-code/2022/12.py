import heapq
from math import inf

grid = open("12").read().splitlines()
src = (0,0)
sink = (0,0)
for i in range(len(grid)):
    for j in range(len(grid[0])):
        if grid[i][j] == "S":
            src = (i,j)
            grid[i] = grid[i][:j] + 'a' + grid[i][j+1:]
        if grid[i][j] == "E":
            sink = (i,j)
            grid[i] = grid[i][:j] + 'z' + grid[i][j+1:]

def possibleStep(c1, c2):
    return ord(c1)-ord(c2) >= -1

def dijsktra(grid, src):
    ni = len(grid)
    nj = len(grid[0])
    dist = [[inf]*nj for _ in range(ni)]
    visited = [[False]*nj for _ in range(ni)]
    dist[src[0]][src[1]] = 0
    q = [(0,*src)]
    c = 0
    while q:
        p,i,j = heapq.heappop(q)
        if visited[i][j]:
            continue
        visited[i][j] = True
        for d in ((-1,0),(1,0),(0,1),(0,-1)):
            di, dj = d
            di += i
            dj += j
            if di < 0 or di >= ni or dj < 0 or dj >= nj:
                continue
            if possibleStep(grid[i][j], grid[di][dj]) and p+1 < dist[di][dj]:
                c += 1
                dist[di][dj] = p +1
                heapq.heappush(q, (p+1,di,dj))
    return dist

mini = inf
part1 = -1
for i in range(len(grid)):
    for j in range(len(grid[0])):
        if grid[i][j] == 'a':
            src2 = (i,j)
            dist = dijsktra(grid, src2)
            dsink = dist[sink[0]][sink[1]]
            mini = min(mini, dsink)
            if src2 == src:
                part1 = dsink

print(part1, mini)
