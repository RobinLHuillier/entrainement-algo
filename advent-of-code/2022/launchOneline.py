from subprocess import Popen, PIPE
import time

nbAdvent = 12

def afficheLigne():
    print("----------------------------------")

afficheLigne()
print("Lancement de chaque advent of code")
afficheLigne()

tLines = 0
tLineMin = 1e8
tLineMax = 0
tChars = 0
tCharMin = 1e8
tCharMax = 0
tTime = 0
tTimeMin = 1e8
tTimeMax = 0
tTest = 0

for i in range(1,nbAdvent+1):
    print("Jour " + str(i) + ":")
    path = "oneline-"+str(i)+".py"
    cmd = "pypy3 " + path
    lines = len(open(path).readlines()[2:])
    chars = sum(len(i.strip())for i in open(path).readlines()[2:])
    part1 = open(path).readlines()[0].strip()[1:-1]
    part2 = open(path).readlines()[1].strip()[1:-1]
    print("Fichier " + path + ":")
    print("Nombre de lignes : " + str(lines))
    print("Nombre de caractères : " + str(chars))

    print("----\nTest : ", end="")
    begin = time.time()
    res = Popen(cmd, stdout=PIPE, shell=True)
    res = ''.join(map(chr, (res.communicate())[0])).split('\n')[0]
    spanTime = int((time.time()-begin)*1000)/1000
    res = res.split()
    while len(res) < 2:
        res.append('')
    print(res[0]==part1, res[1]==part2)
    print("----")
    print("Exécuté en " + str(spanTime) + "s")

    tTest += (res[0]==part1) + (res[1]==part2)
    tLines += lines
    tLineMin = min(tLineMin, lines)
    tLineMax = max(tLineMax, lines)
    tChars += chars
    tCharMin = min(tCharMin, chars)
    tCharMax = max(tCharMax, chars)
    tTime += spanTime
    tTimeMin = min(tTimeMin, spanTime)
    tTimeMax = max(tTimeMax, spanTime)
    afficheLigne()

print("Total lignes : " + str(tLines) + " (moyenne : " + str(tLines//nbAdvent) + "  / min : " + str(tLineMin) + " / max : " + str(tLineMax) + ")")
print("Total caractères : " + str(tChars) + " (moyenne : " + str(tChars//nbAdvent) + "  / min : " + str(tCharMin) + " / max : " + str(tCharMax) + ")")
print("Total temps : " + str(int(tTime*1000)/1000) + "s (moyenne : " + str(int(1000*tTime/nbAdvent)/1000) + "s  / min : " + str(tTimeMin) + "s / max : " + str(tTimeMax) + "s)")
print("Total tests : " + str(tTest) + "/" + str(nbAdvent*2))