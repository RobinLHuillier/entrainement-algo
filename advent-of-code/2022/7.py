class Node:
    def __init__(self, name, size, dir, parent=None):
        self.name = name
        self.size = size
        self.dir = dir
        self.children = []
        self.parent = parent
    def addChild(self, Node):
        self.children.append(Node)
    def getChild(self, name):
        for n in self.children:
            if n.name == name:
                return n
    def goUp(self):
        return self.parent
    def totalSize(self):
        s = self.size
        for c in self.children:
            s += c.totalSize()
        return s
    def print(self):
        string = ["File : " + self.name + " (" + str(self.size) + ")" + "\n"]
        if self.dir:
            string = ["Dir " + self.name + " (" + str(self.totalSize()) + ")" + "\n"]
            for c in self.children:
                rep = c.print()
                for r in rep:
                    string.append('--' + r)
        return string
    def part1(self):
        c = 0
        if self.dir and self.totalSize() <= 100000:
            c = self.totalSize()
        for e in self.children:
            c += e.part1()
        return c
    def part2(self, needToDelete):
        c = 1e25
        if self.dir and self.totalSize() >= needToDelete:
            c = self.totalSize()
        for e in self.children:
            c = min(c, e.part2(needToDelete))
        return c

entry = [a.rstrip() for a in open("7-2").readlines()]

firstDir = None
currentNode = None

for e in entry:
    args = e.split()
    if args[0] == '$':
        if args[1] == 'cd': 
            name = args[2]
            if name == '/':
                firstDir = Node(name, 0, True)
                currentNode = firstDir
            elif name == '..':
                currentNode = currentNode.goUp()
            else:
                currentNode = currentNode.getChild(name)
    else:
        if args[0] == 'dir':
            currentNode.addChild(Node(args[1], 0, True, currentNode))
        else:
            currentNode.addChild(Node(args[1], int(args[0]), False, currentNode))


print(''.join(firstDir.print()))
print("Part 1:", firstDir.part1())
print("Part 2:", firstDir.part2(30000000-(70000000-firstDir.totalSize())))

