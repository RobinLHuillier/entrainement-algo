from constraints import *

problem = constraint.Problem()

problem.addVariable('x', range(2))
problem.addVariable('y', [1])

def our_constraint(x, y):
    if 2*x + 4*y == 6:
        return True

problem.addConstraint(our_constraint, ['x','y'])

solutions = problem.getSolutions()

for solution in solutions:
    print(solution)