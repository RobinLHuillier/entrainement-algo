wall = '█'
void = ' '
sand = 'o'

def inputToGrid(path):
    size = 1000
    grid = [[' 'for a in range(size)]for _ in range(size)]
    lines = open(path).read().splitlines()
    for l in lines:
        dirs = [list(map(int,k.split(',')))for k in l.split(' -> ')]
        x,y = dirs[0]
        for d in dirs[1:]:
            x2,y2 = d
            if x2 < x:
                x,x2 = x2,x
            if y2 < y:
                y,y2 = y2,y
            if x2 != x:
                for dx in range(x,x2+1):
                    grid[y][dx] = '█'
            if y2 != y:
                for dy in range(y,y2+1):
                    grid[dy][x] = '█'
            x,y = d
    return grid

def relevantInGrid(grid):
    xmin = 999
    xmax = 0
    ymin = 999
    ymax = 0
    for i in range(1000):
        for j in range(1000):
            if grid[i][j] == '█':
                xmin = min(xmin, j)
                xmax = max(xmax, j)
                ymin = min(ymin, i)
                ymax = max(ymax, i)
    return (xmin, xmax, ymin, ymax)

def addFloor(grid):
    _,_,_,ymax = relevantInGrid(grid)
    ymax += 2
    for i in range(1000):
        grid[ymax][i] = "-"

def printGrid(grid):
    print(chr(27) + "[2J")
    xmin,xmax,ymin,ymax = relevantInGrid(grid)
    for i in range(ymin-10,ymax+10):
        print(''.join([grid[i][j]for j in range(xmin-10,xmax+10)]))
        
def grainFall(sandStart, grid, ymax):
    ymax += 10
    x,y = sandStart
    if grid[y][x] == 'o':
        return False
    while y < ymax:
        if grid[y+1][x] == ' ':
            y += 1
        elif grid[y+1][x-1] == ' ':
            y += 1
            x -= 1
        elif grid[y+1][x+1] == ' ':
            y += 1
            x += 1
        else:
            break
    if y == ymax:
        return False
    grid[y][x] = 'o'
    return True


sandStart = (500,0)       
grid = inputToGrid("14")
addFloor(grid)
sand = 0
_,_,_,ymax = relevantInGrid(grid)
while True:
    if sand%100 == 0:
        print(sand)
    if grainFall(sandStart, grid, ymax):
        sand += 1
    else:
        break
printGrid(grid)
# print("Part 1:", sand)
print("Part 2:", sand)
