class Worry:
    def __init__(self, num):
        self.num = num
        self.primes = (2,3,5,7,11,13,17,19,23)
        self.div = [[i,0]for i in self.primes]
        self.decomposition()
    def decomposition(self):
        a = self.num
        for i in range(len(self.primes)):
            p = self.primes[i]
            self.div[i][1] = a%p
    def addition(self, add):
        for i in range(len(self.div)):
            p, a = self.div[i]
            self.div[i][1] = (a+add)%p
    def multiplication(self, mul):
        for i in range(len(self.div)):
            p, a = self.div[i]
            self.div[i][1] = (a*mul)%p
    def square(self):
        for i in range(len(self.div)):
            self.div[i][1] = (self.div[i][1]*self.div[i][1])%self.div[i][0]
    def divisible(self, div):
        return self.div[self.primes.index(div)][1] == 0
    def print(self):
        print(self.num, self.div)

class Monkey:
    def __init__(self, num, items, op, div, ttrue, tfalse):
        self.num = num
        self.items = items
        self.op = op # (arg1, op, arg2) avec arg1/arg2 pouvant etre old
        self.div = div
        self.ttrue = ttrue
        self.tfalse = tfalse
        self.inspected = 0
    def addItem(self, item):
        self.items.append(item)
    def getItem(self):
        return self.items.pop(0)
    def nbItems(self):
        return len(self.items)
    # def operation(self,old, trueItem):
    def operation(self,old):
        arg1, op, arg2 = self.op
        # print("---")
        # print(self.op)
        # old.print()
        # oldTrue = trueItem
        # save = old.num
        # old.recomposition()
        # oldRecomp = old.num
        # old.num = save
        if op == '+':
            if arg2 == 'old':
                old.addition(old.num)
                # trueItem += trueItem
            else:
                old.addition(int(arg2))
                # trueItem += int(arg2)
        elif arg2 != 'old':
            old.multiplication(int(arg2))
            # trueItem *= int(arg2)
        else:
            # old.print()
            old.square()
            # old.print()
            # trueItem *= trueItem
        # old.print()
        # save = old.num
        # old.recomposition()
        # recomp = old.num
        # old.num = save
        # if trueItem != recomp:
        #     print(oldTrue)
        #     print(trueItem)
        #     print(oldRecomp)
        #     print(recomp)
        # return (old, trueItem)
        return old
    def print(self):
        print("Monkey", self.num)
        print("--Starting items:", self.items)
        print("--Operation: new = "+self.op[0]+" "+self.op[1]+" "+self.op[2])
        print("--Test: divisible by",self.div)
        print("----If true: throw to",self.ttrue)
        print("----If false: throw to",self.tfalse)

def round(monkeys, part2=False, verbose=False):
    for m in monkeys:
        if verbose:print("Monkey",m.num)
        m.inspected += m.nbItems()
        for i in range(m.nbItems()):
            # item, trueItem = m.getItem()
            item = m.getItem()
            if verbose:print("--Inspects an item wlvl", item.num)
            # wlvl, trueItem = m.operation(item, trueItem)
            wlvl = m.operation(item)
            if verbose:print("----New wlvl", wlvl.num)
            if not part2: 
                wlvl //= 3
                if verbose:print("----New wlvl", wlvl.num)
            div = m.div
            if verbose:print("----",list(wlvl.div))
            divisible = wlvl.divisible(div)
            thrownTo = -1
            if (divisible):
                if verbose:print("----Wlvl divisible by",div)
                thrownTo = m.ttrue
            else:
                if verbose:print("----Wlvl not divisible by",div)
                thrownTo = m.tfalse
            if verbose:print("----Wlvl",wlvl.num,"thrown to", thrownTo)
            # monkeys[thrownTo].addItem((wlvl,trueItem))
            monkeys[thrownTo].addItem(wlvl)


def printM(monkeys):
    for m in monkeys:
        m.print()
        print()

def printR(monkeys):
    for m in monkeys:
        print("Monkey",m.num,":",m.items)

def printI(monkeys):
    for m in monkeys:
        print("Monkey",m.num,"inspected items",m.inspected,"times")

def monkeyBusiness(monkeys):
    inspected = [m.inspected for m in monkeys]
    inspected.sort()
    return inspected[-1]*inspected[-2]

def readInput(path):
    monkeys = []
    lines = [a.strip()for a in [open(path).read().splitlines()][0]]
    for i in range(0,len(lines),7):
        numMonkey = int(lines[i].split()[1][:-1])
        starting = list(map(int,''.join(lines[i+1].split()[2:]).split(',')))
        starting = [Worry(a)for a in starting]
        # starting = [(Worry(a),a)for a in starting]
        operation = lines[i+2].split()[3:]
        test = int(lines[i+3].split()[3])
        ttrue = int(lines[i+4].split()[5])
        tfalse = int(lines[i+5].split()[5])
        monkeys.append(Monkey(numMonkey, starting, operation, test, ttrue, tfalse))
    return monkeys

path = "11"

# monkeys = readInput(path)

# for i in range(20):
#     round(monkeys)
# print("Part 1:",monkeyBusiness(monkeys))

monkeys = readInput(path)

for i in range(10000):
    round(monkeys,part2=True)
    # print(i)
    if i+1 in (1,20,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000):
        print("--After round",i+1,"--")
        printI(monkeys)
        print()

print("Part 2:",monkeyBusiness(monkeys))