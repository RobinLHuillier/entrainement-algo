prog = open("10-2").read().splitlines()

# print(prog)

reg = 1

c = 1
val = [1 for j in range(500)]
for p in prog:
    # print(c, p)
    if p == 'noop':
        c += 1
    elif p != 'noop':
        num = int(p.split()[1])
        for j in range(c+2,500):
            val[j] += num
        c += 2

f = 0
for i in range(20,221,40):
    f += val[i]*i
    # print(i, val[i], i*val[i], s)


s = ""
for i in range(241):
    # print(i, val[i])
    if abs(val[i+1]-i%40) <= 1:
        s += "#"
    else:
        s += '.'

k = ""
for i in range(6):
    k += s[i*40:i*40+40] + "e"


print("Part 1: ", f == 12980)
print("Part 2: ", k == "###..###....##.#....####.#..#.#....###..e#..#.#..#....#.#....#....#..#.#....#..#.e###..#..#....#.#....###..#..#.#....#..#.e#..#.###.....#.#....#....#..#.#....###..e#..#.#.#..#..#.#....#....#..#.#....#....e###..#..#..##..####.#.....##..####.#....e")

# 12980