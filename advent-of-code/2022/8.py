trees = [a.strip()for a in open("8-2").readlines()]
grid = [[False]*len(trees[0])for i in range(len(trees))]

n = len(trees)

for i in range(n):
    t1 = -1
    t2 = -1
    t3 = -1
    t4 = -1
    for j in range(n):
        if int(trees[i][j]) > t1:
            t1 = int(trees[i][j])
            grid[i][j] = True
        if int(trees[i][n-j-1]) > t2:
            t2 = int(trees[i][n-j-1])
            grid[i][n-j-1] = True
        if int(trees[j][i]) > t3:
            t3 = int(trees[j][i])
            grid[j][i] = True
        if int(trees[n-j-1][i]) > t4:
            t4 = int(trees[n-j-1][i])
            grid[n-j-1][i] = True

v = 0
for i in range(n):
    for j in range(n):
        if grid[i][j]:
            v += 1

# for g in grid:
    # print(''.join(['1'if a else '0'for a in g]))

print("Part 1: ", v)

def scenicDist(trees, n, i, j):
    a = 1
    while i-a>=0 and trees[i-a][j] < trees[i][j]:
        a += 1
    if i-a >= 0:
        a += 1
    b = 1
    while i+b<n and trees[i+b][j] < trees[i][j]:
        b += 1
    if i+b<n:
        b += 1
    c = 1
    while j-c>=0 and trees[i][j-c] < trees[i][j]:
        c += 1
    if j-c>=0:
        c += 1
    d = 1
    while j+d<n and trees[i][j+d] < trees[i][j]:
        d += 1
    if j+d<n:
        d += 1
    a-=1
    b-=1
    c-=1
    d-=1
    # print(trees[i][j],a,b,c,d)
    return a*b*c*d

c = 0
for i in range(n):
    for j in range(n):
        c = max(c, scenicDist(trees, n, i, j))

print("Part 2: ", c)
