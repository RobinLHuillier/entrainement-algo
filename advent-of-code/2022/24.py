import heapq

def readInput(path):
    lines = open(path).read().splitlines()
    grid = [[[]for i in range(len(lines[0]))]for j in range(len(lines))]
    for i in range(len(lines)):
        for j in range(len(lines[i])):
            if lines[i][j] == '#':
                grid[i][j] = '#'
            elif lines[i][j] in '><^v':
                idx = '><^v'.index(lines[i][j])
                direction = [(0,1),(0,-1),(-1,0),(1,0)][idx]
                grid[i][j].append(direction)
    src = (0,1)
    dest = (len(lines)-1, len(lines[0])-2)
    return grid, src, dest

def moveBlizzard(grid):
    g = [[[]for i in range(len(grid[0]))]for j in range(len(grid))]
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == '#':
                g[i][j] = '#'
            elif len(grid[i][j]) > 0:
                for bliz in grid[i][j]:
                    di, dj = bliz
                    ni = i + di
                    nj = j + dj
                    if ni == len(grid)-1:
                        ni = 1
                    if ni == 0:
                        ni = len(grid)-2
                    if nj == len(grid[0])-1:
                        nj = 1
                    if nj == 0:
                        nj = len(grid[0])-2
                    g[ni][nj].append(bliz)
    return g

def printGrid(grid, pos):
    for i in range(len(grid)):
        g = grid[i]
        for j in range(len(g)):
            e = g[j]
            if i == pos[0] and j == pos[1]:
                print("E",end="")
            elif e == '#':
                print(e,end="")
            elif len(e) == 0:
                print('.',end="")
            elif len(e) == 1:
                print('><^v'[[(0,1),(0,-1),(-1,0),(1,0)].index(e[0])], end="")
            else:
                print(len(e),end="")
        print()

def manDist(x1,y1,x2,y2):
    return abs(x1-x2)+abs(y1-y2)

def generateAllBlizForXMinutes(grid, X):
    megaBliz = [grid]
    for _ in range(X):
        ng = moveBlizzard(grid)
        megaBliz.append(ng)
        grid = ng
    return megaBliz

def addOneBlizToMegaBliz(megaBlizz):
    bliz = megaBlizz[-1]
    ng = moveBlizzard(bliz)
    megaBlizz.append(ng)

def backtrack(grid, src, dest, verbose=False):
    queue = [(manDist(*src, *dest), 0, src)]
    bestMinute = 1e9
    megaBlizz = generateAllBlizForXMinutes(grid, 20)
    maxMinute = 20
    cpt = 0
    bestForMinute = [1e9 for i in range(10000)]
    posVisited = dict()
    while queue:        
        dist, minute, src = heapq.heappop(queue)
        cpt += 1
        if verbose and cpt % 1000 == 0:
            print(cpt, bestMinute, len(queue))
        if dist == 0:
            bestMinute = min(bestMinute, minute)
            continue
        if minute > bestMinute:
            continue
        if bestForMinute[minute] + 30 < dist:
            continue
        bestForMinute[minute] = min(bestForMinute[minute], dist)
        if (minute, src) in posVisited:
            continue
        posVisited[(minute, src)] = True
        if minute >= maxMinute:
            addOneBlizToMegaBliz(megaBlizz)
            maxMinute += 1
        i,j = src
        minute += 1
        g = megaBlizz[minute]
        for (di,dj) in [(0,0),(-1,0),(1,0),(0,-1),(0,1)]:
            if i+di < 0 or i+di >= len(grid):
                continue
            if g[i+di][j+dj] == '#':
                continue
            if len(g[i+di][j+dj]) > 0:
                continue
            nsrc = (i+di,j+dj)
            heapq.heappush(queue, (manDist(*nsrc,*dest),minute,nsrc))
    return bestMinute, megaBlizz[bestMinute]

def part2(path):
    grid, src, dest = readInput(path)
    bestMin1, grid = backtrack(grid, src, dest)
    bestMin2, grid = backtrack(grid, dest, src)
    bestMin3, grid = backtrack(grid, src, dest)
    return bestMin1+bestMin2+bestMin3

print("[EX] Part 1: ", backtrack(*readInput("24-2"))[0])
print("[REAL] Part 1: ", backtrack(*readInput("24"))[0])

print("[EX] Part 2: ", part2("24-2"))
print("[REAL] Part 2: ", part2("24"))
