def round(monkeys, m, part2=False, verbose=False):
    for j in range(len(monkeys)):
        inspected, items, operation, test, ttrue, tfalse = monkeys[j]
        monkeys[j][0] += len(items)
        for i in range(len(items)):
            w = items[i]
            op, arg2 = operation
            w = ((w*2)%m if arg2 == 'old' else (w+int(arg2))%m) if op == '+' else ((w*int(arg2))%m if arg2 != 'old' else (w**2)%m)
            w //= 1+2*(not part2)
            monkeys[ttrue if w%test == 0 else tfalse][1].append(w)
        monkeys[j][1] = []


def monkeyBusiness(monkeys):
    inspected = sorted([m[0] for m in monkeys])
    return inspected[-1]*inspected[-2]

def readInput():
    monkeys = []
    mult = 1
    lines = [a.strip()for a in [open("11").read().splitlines()][0]]
    for i in range(0,len(lines),7):
        numMonkey = int(lines[i].split()[1][:-1])
        starting = list(map(int,''.join(lines[i+1].split()[2:]).split(',')))
        operation = lines[i+2].split()[4:]
        test = int(lines[i+3].split()[3])
        mult *= test
        ttrue = int(lines[i+4].split()[5])
        tfalse = int(lines[i+5].split()[5])
        monkeys.append([0, starting, operation, test, ttrue, tfalse])
    return (monkeys, mult)

monkeys, mult = readInput()

for i in range(20):
    round(monkeys, mult)
print("Part 1:",monkeyBusiness(monkeys)==50830)

monkeys, mult = readInput()

for i in range(10000):
    round(monkeys, mult, part2=True)

print("Part 2:",monkeyBusiness(monkeys)==14399640002)