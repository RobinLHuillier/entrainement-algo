boardPass = [f.split('\n')[0] for f in open("5.in")]

def dichotomy(str, rangeD):
    a,b = rangeD
    if a == b:
        return a
    da = (a+b)//2
    db = da + 1
    if str[0] in "FL":
        return dichotomy(str[1:], (a,da))
    else:
        return dichotomy(str[1:], (db,b))

def seatId(code):
    row = dichotomy(code[:-3],(0,127))
    col = dichotomy(code[-3:],(0,7))
    return 8*row + col

ids = []
for b in boardPass:
    ids.append(seatId(b))

for i in range(min(ids)+1,max(ids)):
    if i not in ids:
        print(i)