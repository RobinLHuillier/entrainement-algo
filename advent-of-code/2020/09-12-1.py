xmas = [int(f.split('\n')[0])for f in open("9.in")]
preambleLength = 25
preamble = [xmas[i]for i in range(preambleLength)]

def findContiguousInPrevious(start,current,curSum,numSum):
    if curSum == numSum:
        return (current+1,start)
    curSum += xmas[current]
    if curSum > numSum:
        return findContiguousInPrevious(start-1,start-1,0,numSum)
    return findContiguousInPrevious(start,current-1,curSum,numSum)

i = preambleLength
while i < len(xmas):
    numSum = xmas[i]
    found = False
    for a in range(preambleLength-1):
        for b in range(a+1,preambleLength):
            if a != b and preamble[a]+preamble[b] == numSum:
                found = True
                break
        if found:
            break
    if not found:
        print("nombre qui n'est pas la somme de deux des nombres du préambule:",numSum)
        res = findContiguousInPrevious(i-1,i-1,0,numSum)
        print(min(xmas[res[0]:res[1]+1])+max(xmas[res[0]:res[1]+1]))
        break
    preamble.pop(0)
    preamble.append(numSum)
    i += 1