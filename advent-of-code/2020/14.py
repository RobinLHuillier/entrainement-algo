lines = [f.split('\n')[0]for f in open("14.in")]
dico = {}
mask = ""
cpt = 0
for l in lines:
    if l[1] == "a": #mask
        mask = l[7:]
    else:
        end = l.index("]")
        mem = int(l[4:end])
        value = int(l[end+4:])
        binVal = bin(value)[2:]
        binVal = "0"*(36-len(binVal)) + binVal
        for i in range(len(mask)):
            if mask[i] != "X":
                binVal = binVal[:i] + mask[i] + binVal[i+1:]
        value = int(binVal,2)
        dico[mem] = value
    cpt += 1
    # print(cpt,"/",len(lines))
s = 0
for k in dico.keys():
    s += dico[k]
print("somme:",s)
print("-----------partie 2-----------")
dico = {}
mask = ""
cpt = 0
for l in lines:
    if l[1] == "a": #mask
        mask = l[7:]
    else:
        end = l.index("]")
        mem = int(l[4:end])
        value = int(l[end+4:])
        binMem = bin(mem)[2:]
        binMem = "0"*(36-len(binMem)) + binMem
        for i in range(len(mask)):
            if mask[i] in "1X":
                binMem = binMem[:i] + mask[i] + binMem[i+1:]
        memList = [binMem]
        pos = 0
        while pos < 36:
            for i in range(len(memList)):
                if memList[i][pos] == "X":
                    memList[i] = memList[i][:pos] + "1" + memList[i][pos+1:]
                    memList.append(memList[i][:pos] + "0" + memList[i][pos+1:])
            pos += 1
        for m in memList:
            mem = int(m,2)
            dico[mem] = value
    cpt += 1
    print(cpt,"/",len(lines))
s = 0
for k in dico.keys():
    s += dico[k]
print("somme:",s)