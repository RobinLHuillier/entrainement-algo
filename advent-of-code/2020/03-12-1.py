treeMap = [f.split('\n')[0]for f in open("3.in")]

def slope(right, down, nbTrees=0, pos=(0,0)):
    x,y = pos
    li = len(treeMap)
    col = len(treeMap[0])
    if y >= li:
        return nbTrees
    y += down
    x += right
    x = x%col
    if y < li:
        return slope(right, down, nbTrees+(treeMap[y][x]=="#"), (x,y))
    return slope(right, down, nbTrees, (x,y))

slopes = [(1,1),(3,1),(5,1),(7,1),(1,2)]
nums = []
for s in slopes:
    nums.append(slope(s[0],s[1]))
print(nums)
tot = 1
for n in nums:
    tot *= n
print(tot)