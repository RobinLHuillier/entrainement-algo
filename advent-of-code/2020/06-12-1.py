lines = [f.split('\n')[0] for f in open("6.in")]
groups = []
currGroup = []
for l in lines:
    if len(l) == 0 and len(currGroup) > 0:
        groups.append(currGroup)
        currGroup = []
    else:
        currGroup.append(l)
alphabet = "abcdefghijklmnopqrstuvwxyz"

tot = 0
for g in groups:
    yes = [0]*26
    for p in g:
        for c in p:
            yes[alphabet.index(c)] += 1
    for y in yes:
        if y == len(g):
            tot += 1
print(tot)