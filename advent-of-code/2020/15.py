nums = list(map(int,[f.split('\n')[0]for f in open("15.in")][0].split(',')))
print(nums)

spoken = dict()
last = 0

for turn in range(30000000):
    if turn < len(nums):
        spoken[nums[turn]] = (turn, -1)
        last = nums[turn]
    else:
        t1, t2 = spoken[last]
        if t2 == -1: # first time spoken
            if 0 in spoken:
                z1, z2 = spoken[0]
                spoken[0] = (turn, z1)
            else:
                spoken[0] = (turn, -1)
            last = 0
        else:
            last = t1 - t2
            if last not in spoken:
                spoken[last] = (turn, -1)
            else:
                z1, z2 = spoken[last]
                spoken[last] = (turn, z1)
    if turn%100000 == 0:
        print(turn+1, ":", last, spoken[last])
    
print(turn+1, ":", last, spoken[last])
