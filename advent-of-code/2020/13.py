import math

infos = [f.split('\n')[0]for f in open("13.in")]
early = int(infos[0])
bus = []
for i in infos[1].split(','):
    if i != "x":
        bus.append(int(i))
print(early, bus)
earliest = []
for b in bus:
    earliest.append((b, b - (early%b)))
earliest.sort(key = lambda x:x[1])
print(earliest)
print(earliest[0][0]*earliest[0][1])
print("-------------Q2------------")
bus = []
t = 0
for i in infos[1].split(','):
    if i != "x":
        bus.append((int(i),t))
    t += 1
print(bus)
res = ""
for b in bus:
    num, off = b
    print("bus num",num,"off",off)
    res += "(x+"+str(off)+")%"+str(num)+"==0 ;"
    # print(off, num, off%num)
print(res)
maxi = (0,0)
for i in range(len(bus)):
    if bus[i][0] > maxi[0]:
        maxi = bus[i]
print(maxi)
# found = False
# i = 0
# preums = True
# while not found:
#     ok = True
#     for b in bus:
#         num, off = b
#         if (i+off)%num != 0:
#             ok = False
#             break
#     if ok:
#         found = True
#         print("res",i)
#     i += maxi[0]
#     if preums:
#         i -= maxi[1]
#         preums = False

print(bus)
bus2 = []
for b in bus:
    num, off = b
    bus2.append((num,(num-off%num)%num))
print(bus2)

while len(bus2) > 1:
    print("----------nouveau couple---------")
    m1, r1 = bus2.pop(0)
    m2, r2 = bus2.pop(0)
    # ppcm (a,b) = a*b / pgcd(a,b)
    # pgcd (a,b) : math.gcd(a,b)
    ppcm = int(m1*m2/math.gcd(m1,m2))
    r3 = (r2-r1)%m2
    r = 0
    for a in range(m2):
        # print((a*m1)%m2)
        if (a*m1)%m2 == r3:
            r = m1*a + r1
            print(a,r,r%m2,r3)
            break
    r = r%ppcm
    print(r1,"%",m1,"---",r2,"%",m2)
    print(r,ppcm,r%m1==r1,r%m2==r2,r%m1,r%m2)
    bus2.append((ppcm,r))

# for a in range(37):
#     print(41*a%37 == 35, a)
# a = 41*18
# print(a%41, a%37, a%1517)
# a = 7*379
# print(a%23,a%379,(a+41)%379,(a+41)%23)

#\left(x+0\right)\mod \:41=0\:,\:\left(x+35\right)\mod \:37=0\:,\:\left(x+41\right)\mod \:379=0,\:\left(x+49\right)\mod \:23=0\:,\:\left(x+54\right)\mod \:13=0\:,\:\left(x+58\right)\mod \:17=0\:,\:\left(x+70\right)\mod \:29=0\:,\:\left(x+72\right)\mod \:557=0\:,\:\left(x+91\right)\mod \:19=0

##########################
# thm des restes chinois #
##########################
#x,12,x,x,x,x,x,x,x,x,45,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,50
