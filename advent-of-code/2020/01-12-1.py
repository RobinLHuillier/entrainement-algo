num = [int(f) for f in [f.split('\n')[0] for f in open("1.in")]]

for i in range(len(num)-2):
    for j in range(len(num)-1):
        for k in range(len(num)):
            if i!=j and j!=k and num[i] + num[j] + num[k] == 2020:
                print(num[i]*num[j]*num[k])