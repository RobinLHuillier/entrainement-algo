jolters = [0] + [int(f.split('\n')[0])for f in open("10.in")]
jolters.sort()
jolters += [max(jolters)+3]
diff = [0,0,0,0]
for i in range(len(jolters)-1):
    difference = jolters[i+1] - jolters[i]
    diff[difference] += 1
print("partie 1:")
print("1-diff:",diff[1],"    3-diff:",diff[3],"    mult:",diff[1]*diff[3])
print("partie 2:")
compte = [0]*len(jolters)
compte[0] = 1
#print(jolters)
for i in range(len(jolters)-1):
    #print(compte, i, jolters[i])
    for j in range(i+1,i+4):
        if j < len(jolters) and jolters[j] - jolters[i] <= 3:
            compte[j] += compte[i]
print("possibilités:", compte[-1])