lines = [f.split('\n')[0] for f in open("7.in")]
bags = {}
for l in lines:
    i = 0
    while l[i:i+4] != "bags":
        i += 1
    color = l[:i-1]
    if color not in bags:
        bags[color] = {}
    i += 13
    if l[i] != "n": # no other bags
        bagsContained = l[i:].split(', ')
        for b in bagsContained:
            num = int(b[0])
            i = 0
            while b[i:i+3] != "bag":
                i += 1
            containColor = b[2:i-1]
            if containColor not in bags[color]:
                bags[color][containColor] = num
            else:
                bags[color][containColor] += num

# partie 1:
containsShiny = []

def searchShiny(bagColor):
    if bagColor in containsShiny:
        return True
    colorSearched = set()
    colorToSearch = [bagColor]
    while len(colorToSearch) > 0:
        color = colorToSearch.pop()
        colorSearched.add(color)
        for c in bags[color].keys():
            if c == "shiny gold" or c in containsShiny:
                return True
            if c not in colorSearched and c not in colorToSearch:
                colorToSearch.append(c)
    return False

for b in bags.keys():
    if b != "shiny gold":
        if searchShiny(b):
            containsShiny.append(b)

print("partie 1", len(containsShiny))

#partie 2
def recContains(color):
    if bags[color] == {}:
        return 0
    tot = 0
    for c in bags[color].keys():
        tot += bags[color][c] * recContains(c) + bags[color][c]
    return tot

print("partie 2", recContains("shiny gold"))