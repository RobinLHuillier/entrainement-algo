def readRule(rule):
    name, content = rule.split(':')
    rules = content.split('|')
    name = name.strip()
    rulesContent = []
    for r in rules:
        content = r.split()
        rulesContent.append([c.strip('"') for c in content])
    return (name, rulesContent)

def readInput(file):
    lines = []
    with open(file) as f:
        lines = f.read().splitlines()
    rules = []
    text = []

    for l in lines:
        if len(l) == 0:
            continue
        if l[0] in '0123456789':
            rules.append(readRule(l))
        else:
            text.append(l)
    
    return rules, text

def listRulesToDict(rules):
    dict = {}
    for r in rules:
        dict[r[0]] = r[1]
    return dict

def testTextToRules(rules, text, rule='0'):
    if len(text) == 0:
        return '' # end of text
    if rule not in rules.keys():
        if text[0] == rule:
            return text[1:]
        return -1 # error
    consideredRules = rules[rule]
    print(f"Remaining text {text} // has to test rule {rule}: {consideredRules}")
    restText = text
    for ruleToTest in consideredRules:
        print(f"ruleToTest: {ruleToTest}")
        restText = text
        for partRule in ruleToTest:
            print(f"partRule: {partRule}")
            if len(restText) == 0:
                break  # no more text to consider, rule too long
            restText = testTextToRules(rules, restText, partRule)
            print(f"rest text: {restText}")
            if restText == -1:
                break  # found an error
        print("end part rule loop", restText)
        if restText == '':
            return '' # end of text, rule worked
    if len(consideredRules) == 1:
        return restText # no branch, no need s
    return -1 # no rule worked

rules, text = readInput('19.in')
rules = listRulesToDict(rules)

print(rules)

for t in text:
    print(f"Test: {t}")

    print(testTextToRules(rules, t))

    break