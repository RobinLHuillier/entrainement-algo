def readInput(file):
    lines = []
    with open(file) as f:
        lines = f.read().splitlines()
    return [list(map(int, line.split(','))) for line in lines]

def manDist(p1, p2):
    return sum([abs(p1[i]-p2[i])for i in range(4)])

def isInConstellation(constellation, point):
    for constPoint in constellation:
        if manDist(constPoint, point) <= 3:
            return True
    return False

def initConstellations(points):
    return [[point]for point in points]

def mergeConstellations(const1, const2):
    return [*const1, *const2]

def thisConstellationsShouldMerge(const1, const2):
    for point2 in const2:
        if isInConstellation(const1, point2):
            return True
    return False

def searchMergeable(constellations):
    for i in range(len(constellations)-1):
        for j in range(i+1, len(constellations)):
            if thisConstellationsShouldMerge(constellations[i], constellations[j]):
                # print(f"should merge: {constellations[i]} and {constellations[j]}")
                return (i,j)
    return None

def mergeAndReturn(constellations, i, j):
    newConstellations = [mergeConstellations(constellations[i], constellations[j])]
    for k in range(len(constellations)):
        if k == i or k == j:
            continue
        newConstellations.append(constellations[k])
    return newConstellations
    

def resolvePart1(constellations):
    res = searchMergeable(constellations)
    while res is not None:
        i, j = res
        constellations = mergeAndReturn(constellations, i, j)
        # print(i, j, constellations)
        print(f"Merge effectué, nombre de constellations: {len(constellations)}")
        res = searchMergeable(constellations)
    return constellations

points = readInput('25.in')
constellations = initConstellations(points)
newConstellations = resolvePart1(constellations)
print(f"Part 1 : {len(newConstellations)}")

# for i in range(1, len(points)):
#     print(f"manDist entre {points[i-1]} et {points[i]}: {manDist(points[i-1], points[i])}")
