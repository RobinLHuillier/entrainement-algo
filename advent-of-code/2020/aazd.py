a = [1,2,3]
print(a)
b = a

def rec(e):
    if len(e) >= 8:
        return
    rec(e + [5])
    rec(e)

rec(b)
print(b)