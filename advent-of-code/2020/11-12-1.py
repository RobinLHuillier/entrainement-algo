lines = [f.split('\n')[0]for f in open("11.in")]
mat = []
start = [0]*(len(lines[0])+2)
mat.append(start)
for l in lines:
    line = []
    for c in l:
        if c == ".":
            line.append(0)
        else:
            line.append(1)
    line = [0] + line + [0]
    mat.append(line)
mat.append(start)
matStart = mat

def affiche(mat):
    for m in mat:
        string = ""
        for c in m:
            if c == 0:
                string += "."
            elif c == 1:
                string += "L"
            else:
                string += "#"
        print(string)

def countVoisins(mat, i, j):
    tot = 0
    for a in [-1,0,1]:
        for b in [-1,0,1]:
            if a != 0 or b != 0:
                tot += (mat[i+a][j+b]==2)
    return tot

def step(mat):
    newMat = [[0]*len(mat[0]) for _ in range(len(mat))]
    for i in range(1,len(mat)-1):
        for j in range(1,len(mat[0])-1):
            if mat[i][j] > 0:
                num = countVoisins(mat,i,j)
                if num == 0:
                    newMat[i][j] = 2
                elif num >= 4:
                    newMat[i][j] = 1
                else:
                    newMat[i][j] = mat[i][j]
    return newMat

def countSeats(mat):
    tot = 0
    for m in mat:
        for c in m:
            tot += (c==2)
    return tot

newMat = step(mat)
while newMat != mat:
    mat = newMat
    newMat = step(mat)
print("partie 1:")
print(countSeats(newMat))
print("partie 2:")

def calculateVois(mat,i,j):
    vois = []
    #gauche
    a = i-1
    while a >= 0 and mat[a][j] == 0:
        a -= 1
    if a >= 0:
        vois.append((a,j))
    #droite
    a = i+1
    while a < len(mat) and mat[a][j] == 0:
        a += 1
    if a < len(mat):
        vois.append((a,j))
    #haut
    b = j-1
    while b >= 0 and mat[i][b] == 0:
        b -= 1
    if b >= 0:
        vois.append((i,b))
    #bas
    b = j+1
    while b < len(mat[0]) and mat[i][b] == 0:
        b += 1
    if b < len(mat[0]):
        vois.append((i,b))
    #haut gauche
    a = i-1
    b = j-1
    while a >= 0 and b >= 0 and mat[a][b] == 0:
        a -= 1
        b -= 1
    if a >= 0 and b >= 0:
        vois.append((a,b))
    #haut droite
    a = i-1
    b = j+1
    while a >= 0 and b < len(mat[0]) and mat[a][b] == 0:
        a -= 1
        b += 1
    if a >= 0 and b < len(mat[0]):
        vois.append((a,b))
    #bas gauche
    a = i+1
    b = j-1
    while a < len(mat) and b >= 0 and mat[a][b] == 0:
        a += 1
        b -= 1
    if a < len(mat) and b >= 0:
        vois.append((a,b))
    #bas droite
    a = i+1
    b = j+1
    while a < len(mat) and b < len(mat[0]) and mat[a][b] == 0:
        a += 1
        b += 1
    if a < len(mat) and b < len(mat[0]):
        vois.append((a,b))
    return vois

mat = matStart
matVois = [[0]*len(mat[0]) for _ in range(len(mat))]
for i in range(len(mat)):
    for j in range(len(mat[0])):
        if mat[i][j] == 0:
            matVois[i][j] = []
        else:
            matVois[i][j] = calculateVois(mat,i,j)

def countVoisins2(mat, matVois, i, j):
    tot = 0
    for v in matVois[i][j]:
        a, b = v
        tot += (mat[a][b]==2)
    return tot

def step2(mat, matVois):
    newMat = [[0]*len(mat[0]) for _ in range(len(mat))]
    for i in range(1,len(mat)-1):
        for j in range(1,len(mat[0])-1):
            if mat[i][j] > 0:
                num = countVoisins2(mat, matVois, i, j)
                if num == 0:
                    newMat[i][j] = 2
                elif num >= 5:
                    newMat[i][j] = 1
                else:
                    newMat[i][j] = mat[i][j]
    return newMat

newMat = step2(mat, matVois)
while newMat != mat:
    mat = newMat
    newMat = step2(mat, matVois)
print(countSeats(newMat))