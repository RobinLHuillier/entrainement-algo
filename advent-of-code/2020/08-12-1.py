from copy import deepcopy

lines = [(ins, int(nb)) for ins,nb in [f.split('\n')[0].split() for f in open("8.in")]]

def executeProg(prog):
    executed = [0]*len(prog)
    accumulator = 0
    i = 0
    while i < len(prog) and executed[i] == 0:
        ins,nb = prog[i]
        executed[i] = 1
        if ins == "acc":
            accumulator += nb
            i += 1
        elif ins == "nop":
            i += 1
        elif ins == "jmp":
            i += nb
    return (accumulator, i==len(prog), executed)

print("PARTIE 1")
res = executeProg(lines)
print(res[0])

print("PARTIE 2")
toModify = res[2][:]
for i in range(len(toModify)):
    if toModify[i] == 1 and lines[i][0] != "acc":
        modifProg = deepcopy(lines)
        if lines[i][0] == "jmp":
            modifProg[i] = ("nop", modifProg[i][1])
        else:
            modifProg[i] = ("jmp", modifProg[i][1])
        print("modification: ligne",i,lines[i],"->",modifProg[i])
        res = executeProg(modifProg)
        if res[1]:
            print("trouvé!",res[0])
            break