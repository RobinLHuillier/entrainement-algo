x,y = (0,0)
facing = 0
rotate = "ENWS"
advance = [(1,0),(0,1),(-1,0),(0,-1)]
orders = [f.split('\n')[0]for f in open("12.in")]
for o in orders:
    action = o[0]
    num = int(o[1:])
    print("order:", action, num)
    if action == "F":
        action = rotate[facing]
    if action in rotate:
        dir = advance[rotate.index(action)]
        x += dir[0]*num
        y += dir[1]*num
    if action in "LR":
        rot = num//90
        if action == "R":
            rot = -rot
        facing = (facing+rot)%4
    print("new pos: ",x,y)
print("Q1 -----------")
print("manhattan:",abs(x)+abs(y))
print("Q2 -----------")
x,y = (0,0)
xW,yW = (10,1)
facing = 0
rotate = "ENWS"
advance = [(1,0),(0,1),(-1,0),(0,-1)]
rotMap = [()]
orders = [f.split('\n')[0]for f in open("12.in")]
for o in orders:
    action = o[0]
    num = int(o[1:])
    print("order:", action, num)
    if action == "F":
        x += num*xW
        y += num*yW
    if action in rotate:
        dir = advance[rotate.index(action)]
        xW += dir[0]*num
        yW += dir[1]*num
    if action in "LR":
        rot = num//90
        while rot != 0:
            if action == "L":
                xW,yW = (-yW,xW)
            else:
                xW,yW = (yW,-xW)
            rot -= 1
    print("new pos: ",x,y)
    print("waypoint:", xW,yW)
print("manhattan:",abs(x)+abs(y))