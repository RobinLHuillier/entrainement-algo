line = [f.split('\n')[0] for f in open("4.in")]
passports = []
currPass = []
for l in line:
    if len(l)==0 and len(currPass)>0:
        passports.append(currPass)
        currPass = []
    else:
        data = l.split()
        currPass += data

def verifyValidity(passport):
    fields = ['byr','iyr','eyr','hgt','hcl','ecl','pid','cid']
    for p in passport:
        field, data = p.split(':')
        ok = False
        if field == "byr":
            if data.isnumeric():
                year = int(data)
                if year >= 1920 and year <= 2002:
                    ok = True
        if field == "iyr":
            if data.isnumeric():
                year = int(data)
                if year >= 2010 and year <= 2020:
                    ok = True
        if field == "eyr":
            if data.isnumeric():
                year = int(data)
                if year >= 2020 and year <= 2030:
                    ok = True
        if field == "hgt":
            metric = data[-2:]
            length = data[:-2]
            if length.isnumeric():
                length = int(length)
                if metric == "cm" and length >= 150 and length <= 193:
                    ok = True
                if metric == "in" and length >= 59 and length <= 76:
                    ok = True
        if field == "hcl":
            if data[0] == "#" and len(data) == 7:
                ok = True
                for i in range(1,7):
                    if data[i] not in "0123456789abcdef":
                        ok = False
        if field == "ecl":
            if len(data) == 3 and data in ["amb","blu","brn","gry","grn","hzl","oth"]:
                ok = True
        if field == "pid":
            if len(data) == 9 and data.isnumeric():
                ok = True
        if ok and field in fields:
            fields.remove(field)
    if len(fields) == 0 or fields == ['cid']:
        return True
    return False

tot = 0
for p in passports:
    tot += verifyValidity(p)
print(tot)