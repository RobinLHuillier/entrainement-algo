startEx1 = "..^^."
startEx2 = ".^^.^.^^^^"
startInput = ".^^^.^.^^^.^.......^^.^^^^.^^^^..^^^^^.^.^^^..^^.^.^^..^.^..^^...^.^^.^^^...^^.^.^^^..^^^^.....^...."

def isATrap(tile):
    return tile in ("^^.",".^^","^..","..^")

def generateRow(row):
    nrow = ""
    for i in range(len(row)):
        consider = row[i-1:i+2]
        if i == 0:
            consider = "." + row[i:i+2]
        if i == len(row)-1:
            consider += "."
        nrow += "^" if isATrap(consider) else "."
    return nrow

def generateXRow(startTile, X):
    grid = [startTile]
    row = startTile
    for i in range(X-1):
        row = generateRow(row)
        grid.append(row)
    return grid

def generateXRow2(startTile, X):
    row = startTile
    c = row.count(".")
    for i in range(X-1):
        if i%1000 == 0:
            print(i, c)
        row = generateRow(row)
        c += row.count(".")
    return c

def printGrid(grid):
    for g in grid:
        print(g)

def countSafe(grid):
    c = 0
    for g in grid:
        c += g.count(".")
    return c

print("EX1 ")
grid = generateXRow(startEx1, 3)
printGrid(grid)

print("\nEX2")
grid = generateXRow(startEx2, 10)
printGrid(grid)
print(countSafe(grid))

print("\nREEL1")
grid = generateXRow(startInput, 40)
printGrid(grid)
print("part1 : " , countSafe(grid))
print("part1 : " , generateXRow2(startInput, 40))

print("\nREEL 2")
count = generateXRow2(startInput, 400000)
print("part2: ", count)