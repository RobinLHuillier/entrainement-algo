def process(a):
    b = a[::-1]
    b2 = ""
    for i in b:
        b2 += "1" if i == "0" else "0"
    return a + "0" + b2

def dragonCurve(start, length):
    while len(start) < length:
        print(len(start)/length, "%")
        start = process(start)
    return start[:length]

def checksum(string):
    print(len(string))
    newsum = ""
    for i in range(0, len(string), 2):
        newsum += "1" if string[i] == string[i+1] else "0"
    if len(newsum)%2 == 1:
        return newsum
    return checksum(newsum)

start = "10011111011011001"
length = 35651584

curve = dragonCurve(start, length)
print("Dragon curve finished")
check = checksum(curve)
print(check)