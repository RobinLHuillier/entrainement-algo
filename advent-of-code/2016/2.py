pad = [[1,2,3], [4,5,6], [7,8,9]]

dir = []
while True:
    i = input().strip(' \n\r')
    if len(i) < 1:
        break
    dir.append(i)

def followDirection(dir, pad, x, y):
    for d in dir:
        if d == 'U':
            y -= 1
        if d == 'D':
            y += 1
        if d == 'L':
            x -= 1
        if d == 'R':
            x += 1
        if y > 2:
            y = 2
        if y < 0:
            y = 0
        if x > 2:
            x = 2
        if x < 0:
            x = 0
    return (pad[y][x], x, y)

code = ""
x = 1
y = 1
for d in dir:
    num, x, y = followDirection(d, pad, x, y)
    code += str(num)

print('PART 1:', code)

pad2 = [[-1,-1,1,-1,-1], [-1,2,3,4,-1], [5,6,7,8,9], [-1,'A','B','C',-1], [-1,-1,'D',-1,-1]]

def followDirection2(dir, pad, x, y):
    for d in dir:
        sx = x
        sy = y
        if d == 'U':
            y -= 1
        if d == 'D':
            y += 1
        if d == 'L':
            x -= 1
        if d == 'R':
            x += 1
        if y > 4 or y < 0 or x > 4 or x < 0 or pad[y][x] == -1:
            x = sx
            y = sy
    return (pad[y][x], x, y)

code = ""
x = 0
y = 2
for d in dir:
    num, x, y = followDirection2(d, pad2, x, y)
    code += str(num)

print('PART 2:', code)