def somme(a,b,c):
    l = [a,b,c]
    l.sort()
    return l[0]+l[1] > l[2]

possible = 0
triangles = []

while True:
    i = input().strip(' \n\r')
    if len(i) < 1:
        break
    a, b, c = [int(j) for j in i.split()]
    triangles.append((a,b,c))
    if somme(a,b,c):
        possible += 1

print('PART 1:', possible)

possible = 0
for i in range(0,len(triangles),3):
    for j in range(3):
        if somme(triangles[i][j], triangles[i+1][j], triangles[i+2][j]):
            possible += 1

print('PART 2:', possible)