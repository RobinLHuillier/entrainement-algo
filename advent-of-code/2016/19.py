size = 3014387

class Node:
    def __init__(self, num):
        self.num = num
        self.next = None
        self.prec = None

def getMid(node):
    nprec = node.prec
    nnext = node.next
    if nprec == nnext:
        return nprec
    print("while")
    while nprec != nnext and nprec.prec != nnext:
        # print(nprec.num, nnext.num)
        # if (abs(nprec.num - nnext.num) == 1):
            # break
        nprec = nprec.prec
        nnext = nnext.next
    return nnext

circle = Node(1)
current = circle
for i in range(2,size+1):
    node = Node(i)
    current.next = node
    node.prec = current
    current = node

current.next = circle
circle.prec = current

current = circle
csize = size
nmid = getMid(current)

print("mid:", nmid.num)
print(current.num)
# exit()

while True:
    if nmid == current:
        print("Part 2: ", nmid.num)
        break
    snmid = nmid.next
    nmid.prec.next = nmid.next
    nmid.next.prec = nmid.prec
    nmid.next = None
    nmid.prec = None
    current = current.next
    csize -= 1
    if csize%2 == 0:
        snmid = snmid.next
    nmid = snmid
    if csize%10000 == 0:
        print(csize)

