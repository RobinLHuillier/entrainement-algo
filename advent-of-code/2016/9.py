def getMarker(inp, i):
    j = i+1
    while inp[j] != "x":
        j += 1
    span = int(inp[i+1:j])
    i = j+1
    while inp[i] != ")":
        i += 1
    mult = int(inp[j+1:i])
    return span, mult, i

def uncompress(inp):
    sortie = ""
    i = 0
    uncompressed = False
    while i < len(inp):
        if inp[i] == "(":
            uncompressed = True
            span, mult, end = getMarker(inp, i)
            sortie += mult*inp[end+1:end+1+span]
            i = end+span
        else:
            sortie += inp[i]
        i += 1
    return sortie, uncompressed

def containsMarker(inp):
    return "(" in inp

def getNextMarker(inp):
    return inp.index("(")

def uncompress2(inp):
    if not containsMarker(inp):
        return len(inp)
    i = getNextMarker(inp)
    span, mult, end = getMarker(inp, i)
    toTest = inp[end+1:end+1+span]
    toFinish = inp[end+1+span:]
    print(len(toFinish))
    return mult*uncompress2(toTest) + uncompress2(toFinish)

inp = input()

print(uncompress2(inp))

#### PART 1
# uncompressed = True
# while uncompressed:
#     inp, uncompressed = uncompress(inp)
#     print("uncompress done, len", len(inp))

# print(inp)
# print(len(inp))