import hashlib

door = "abbhdwsy"
idx = "0"
mdp = [""]*8
nb = 0

while nb < 8:
    md5 = "aaaaaaaaaa"
    while md5[0:5] != "00000":
        mp = bytes(door+idx, 'utf-8')
        md5 = hashlib.md5(mp).hexdigest()
        idx = str(int(idx)+1)
    print(md5, int(idx)-1, md5[5], md5[6])
    if (md5[5] in "01234567" and mdp[int(md5[5])]==""):
        mdp[int(md5[5])] = md5[6]
        print("CHOSEN")
        nb += 1

print(''.join(mdp))
