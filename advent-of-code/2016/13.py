def isItAWall(x, y, favorite):
    num = x*x + 3*x + 2*x*y + y + y*y + favorite
    binary = bin(num)[2:]
    ones = binary.count("1")
    return ones%2 == 1

def resetGrid(w, h, baseValue=0):
    return [[baseValue for i in range(w)]for j in range(h)]

def constructGrid(w, h, favorite, val=1, baseValue=0):
    grid = resetGrid(w, h, baseValue)
    for i in range(w):
        for j in range(h):
            if isItAWall(i, j, favorite):
                grid[j][i] = val
    return grid

def printGrid(grid, w, h):
    for j in range(h):
        for i in range(w):
            val = '.'
            if grid[j][i] == 1:
                val = '#'
            elif grid[j][i] == 2:
                val = 'O'
            print(val, end="")
        print()

def printGrid2(grid, w, h):
    for j in range(h):
        for i in range(w):
            val = '. '
            if grid[j][i] == -1:
                val = '# '
            elif grid[j][i] > 0 and grid[j][i] < 99:
                val = str(grid[j][i])
                if len(val) < 2:
                    val += " "
            print(val, end="")
        print()

def addVisited(grid, node):
    grid[node[0]][node[1]] = True

def isVisited(grid, node):
    return grid[node[0]][node[1]]

def getNeighbors(grid, node, w, h):
    i = node[0]
    j = node[1]
    neighbors = []
    if i > 0 and grid[i-1][j] == 0:
        neighbors.append((i-1,j))
    if i < w-1 and grid[i+1][j] == 0:
        neighbors.append((i+1,j))
    if j > 0 and grid[i][j-1] == 0:
        neighbors.append((i,j-1))
    if j < h-1 and grid[i][j+1] == 0:
        neighbors.append((i,j+1))
    return neighbors

def getNeighbors2(grid, node, w, h):
    i = node[0]
    j = node[1]
    neighbors = []
    if i > 0 and grid[i-1][j] > -1:
        neighbors.append((i-1,j))
    if i < w-1 and grid[i+1][j] > -1:
        neighbors.append((i+1,j))
    if j > 0 and grid[i][j-1] > -1:
        neighbors.append((i,j-1))
    if j < h-1 and grid[i][j+1] > -1:
        neighbors.append((i,j+1))
    return neighbors

def bfs(s, e, grid, w, h):
    queue = [(s, [])]
    visited = resetGrid(w, h, False)

    while len(queue) > 0:
        node, path = queue.pop(0)
        path.append(node)
        addVisited(visited, node)

        if node == e:
            return path

        adjNodes = getNeighbors(grid, node, w, h)
        for item in adjNodes:
            if not isVisited(visited, item):
                queue.append((item, path[:]))

    return None

def addPathToGrid(path, grid):
    for node in path:
        y, x = node
        grid[y][x] = 2

def markDistToGrid(grid, w, h, start, dist=0):
    if dist == 51:
        return
    grid[start[0]][start[1]] = dist
    neighbors = getNeighbors2(grid, start, w, h)
    print(neighbors)
    for n in neighbors:
        y, x = n
        if grid[y][x] > dist+1:
            markDistToGrid(grid, w, h, (y,x), dist+1) 

def countGrid(grid, w, h):
    count = 0
    for i in range(w):
        for j in range(h):
            if grid[j][i] > -1 and grid[j][i] < 99:
                count += 1
    return count

favorite = 1362
w = 200
h = 200
wShow = 50
hShow = 50
start = (1,1)
end = (39,31)

print("PART 1")
grid = constructGrid(w,h,favorite)
path = bfs(start, end, grid, w, h)
addPathToGrid(path, grid)
printGrid(grid, wShow, hShow)
print(len(path)-1)

print("\nPART 2")
grid = constructGrid(w,h,favorite,-1,99)
markDistToGrid(grid, w, h, start)
printGrid2(grid, wShow, hShow)
print(countGrid(grid, w, h))