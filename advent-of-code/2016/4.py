alpha = 'abcdefghijklmnopqrstuvwxyz'
num = '0123456789'

roomCodes = []

while True:
    i = input().strip(' \n\r')
    if len(i) < 1:
        break
    code, checksum = i.split('[')
    checksum = checksum.split(']')[0]
    l = code.split('-')
    name = ""
    roomNumber = -1
    for j in l:
        if j[0] in alpha:
            name += j
        else:
            roomNumber = int(j)
    roomCodes.append((name, roomNumber, checksum))

def countLettersInOrder(name):
    letters = []
    for a in alpha:
        letters.append((a, name.count(a)))
    letters = sorted(letters, reverse=True, key=lambda l: l[1])
    return letters

def concatLetters(letters):
    c = ""
    for i in range(5):
        c += letters[i][0]
    return c

def checkIfReal(name, roomNumber, checksum):
    if len(checksum) != 5:
        return False
    letters = countLettersInOrder(name)
    sum = concatLetters(letters)
    return checksum==sum

sumId = 0
for r in roomCodes:
    if checkIfReal(r[0],r[1],r[2]):
        sumId += r[1]

print('PART 1:', sumId)

print('---------------\nPART 2:\n---------------')

for r in roomCodes:
    name, roomNumber, checksum = r
    realName = ""
    if checkIfReal(name, roomNumber, checksum):
        for n in name:
            if n == '-':
                realName += ' '
            else:
                realName += alpha[(alpha.index(n)+roomNumber)%26]
        print(realName, roomNumber)
        if "north" in realName:
            print("------------suspect")