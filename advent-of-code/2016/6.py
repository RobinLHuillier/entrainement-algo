alpha = 'abcdefghijklmnopqrstuvwxyz'

codes = ['','','','','','','','','','']

while True:
    i = input().strip(' \n\r')
    if len(i) < 1:
        break
    for j in range(len(i)):
        codes[j] += i[j]

def mostCommon(h):
    maxi = 0
    l = ''
    for a in alpha:
        if h.count(a) > maxi:
            maxi = h.count(a)
            l = a
    return l

def leastCommon(h):
    mini = 10000
    l = ''
    for a in alpha:
        if h.count(a) < mini and h.count(a) != 0:
            mini = h.count(a)
            l = a
    return l

print('PART 1: ', end="")
for c in codes:
    if len(c) > 0:
        print(mostCommon(c), end="")
print()

print('PART 2: ', end="")
for c in codes:
    if len(c) > 0:
        print(leastCommon(c), end="")
print()