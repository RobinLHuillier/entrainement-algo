lines = open("22").read().splitlines()

files = []

for l in lines[2:]:
    name, size, used, avail, use = l.split()
    size = int(size[:-1])
    used = int(used[:-1])
    avail = int(avail[:-1])
    use = int(use[:-1])
    files.append((name,size,used,avail,use))

pairs = 0

for i in range(len(files)):
    for j in range(len(files)):
        if i == j:
            continue
        _, _, usedA, availA, _ = files[i]
        _, _, usedB, availB, _ = files[j]
        if usedA == 0:
            continue
        if usedA <= availB:
            pairs += 1

print("part1: ", pairs)