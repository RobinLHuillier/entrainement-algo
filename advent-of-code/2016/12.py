prog = []
while True:
    inp = input()
    if len(inp) == 0:
        break
    prog.append(inp.split())

reg = {
    "a": 0,
    "b": 0,
    "c": 1,
    "d": 0,
}

def execCode(code, reg):
    com = code[0]
    if com == "inc":
        reg[code[1]] += 1
    elif com == "dec":
        reg[code[1]] -= 1
    elif com == "cpy":
        val = 0
        if code[1] in "abcd":
            val = reg[code[1]]
        else:
            val = int(code[1])
        reg[code[2]] = val
    elif com == "jnz":
        if code[1] == "0" or (code[1] in "abcd" and reg[code[1]] == 0):
            return 1
        val = 0
        if code[2] in "abcd":
            val = reg[code[2]]
        else:
            val = int(code[2])
        return val
    else:
        print("PROBLEM WHILE READING CODE")
    return 1

def execProg(prog, reg):
    i = 0
    while i < len(prog):
        i += execCode(prog[i], reg)
        if i < 0:
            print("PROBLEM INDEX CODE < 0")

execProg(prog, reg)
print(reg["a"])