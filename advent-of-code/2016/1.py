east = 0
north = 0
direction = 0
visited = []

dir = input().strip(' \n\r').split(', ')
for d in dir:
    num = int(d[1:])
    if d[0] == 'R':
        direction += 1
    else:
        direction -= 1
    direction = direction%4
    while num > 0:
        if direction == 0:
            north += 1
        if direction == 1:
            east += 1
        if direction == 2:
            north -= 1
        if direction == 3:
            east -= 1
        num -= 1
        if ((east, north) in visited):
            print('PART 2:', abs(north)+abs(east))
            quit()
        visited.append((east, north))
    print(d, direction, num, east, north)

print('PART 1:', abs(north)+abs(east))