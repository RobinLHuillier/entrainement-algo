def printScreen(screen):
    for i in range(6):
        print(''.join(screen[i]))

def rect(a, b, screen):
    for i in range(b):
        for j in range(a):
            screen[i][j] = "#"

def rotateCol(x, l, screen):
    for i in range(l):
        s = screen[5][x]
        for j in range(5, 0, -1):
            screen[j][x] = screen[j-1][x]
        screen[0][x] = s

def rotateRow(y, l, screen):
    for i in range(l):
        s = screen[y][49]
        for j in range(49,0,-1):
            screen[y][j] = screen[y][j-1]
        screen[y][0] = s

screen = [["."]*50 for i in range(6)]

while True:
    i = input()
    if len(i) == 0:
        break
    w = i.split()
    if w[0] == "rect":
        a,b = w[1].split('x')
        rect(int(a), int(b), screen)
    else:
        b = int(w[4])
        a = int(w[2].split('=')[1])
        if w[1] == "column":
            rotateCol(a,b,screen)
        else:
            rotateRow(a,b,screen)

printScreen(screen)

c = 0
for i in range(6):
    for j in range(50):
        if screen[i][j] == "#":
            c += 1

print("Part 1: ", c)
