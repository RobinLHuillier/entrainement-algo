def rotate(direction, s):
    if direction == "left":
        return s[1:] + s[0]
    else:
        return s[-1] + s[:-1]

def rotateX(direction, s, X):
    for _ in range(X):
        s = rotate(direction, s)
    return s

def modify(puzzle, inst):
    order = inst.split()
    if order[0] == "swap":
        if order[1] == "position":
            a = int(order[2])
            b = int(order[5])
            puzzle[a], puzzle[b] = puzzle[b], puzzle[a]
        elif order[1] == "letter":
            a = puzzle.index(order[2])
            b = puzzle.index(order[5])
            puzzle[a], puzzle[b] = puzzle[b], puzzle[a]
        else:
            print("order not recognized", order)
    elif order[0] == "rotate":
        if order[1] == "based":
            x = puzzle.index(order[6])+1
            if x >= 5:
                x += 1
            puzzle = [*rotateX("right", ''.join(puzzle), x)]
        else:
            direction = order[1]
            x = int(order[2])
            puzzle = [*rotateX(direction, ''.join(puzzle), x)]
    elif order[0] == "reverse":
        a = int(order[2])
        b = int(order[4])
        for i in range(1+(b-a)//2):
            puzzle[a+i], puzzle[b-i] = puzzle[b-i], puzzle[a+i]
    elif order[0] == "move":
        a = int(order[2])
        b = int(order[5])
        letter = puzzle[a]
        puzzle = puzzle[:a] + puzzle[a+1:]
        puzzle = puzzle[:b] + [letter] + puzzle[b:]
    else:
        print("order not recognized", order)
    return puzzle

def applyInst(puzzle, allInst):
    for inst in allInst:
        puzzle = modify(puzzle, inst)
    return puzzle

puzzle = [*'abcdefgh']
allInst = open("23").read().splitlines()
puzzle = applyInst(puzzle, allInst)

print("part1: ", ''.join(puzzle))

toFind = "fbgdceah"

from itertools import permutations
from tqdm import tqdm
permut = list(permutations("abcdefgh", 8))

for i in tqdm(range(len(permut))):
    puzzle = applyInst([*permut[i]], allInst)
    if ''.join(puzzle) == toFind:
        print("part2: ", ''.join(permut[i]))
        break
