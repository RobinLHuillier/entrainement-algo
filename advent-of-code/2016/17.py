import hashlib

sHash = "ihgpwlah"

def atEnd(pos):
    return pos[0] == 3 and pos[1] == 3

def createHash(h, path):
    return hashlib.md5(bytes(h+path, 'utf-8')).hexdigest()

def getAllDirPossibleFromHashAndPos(h, pos):
    x,y = pos
    dir = ""
    for i in range(4):
        if h[i] in "bcdef":
            dir += "UDLR"[i]
    rdir = []
    for d in dir:
        if d == "U" and y > 0:
            rdir.append(("U", (x,y-1)))
            continue
        if d == "D" and y < 3:
            rdir.append(("D", (x,y+1)))
            continue
        if d == "L" and x > 0:
            rdir.append(("L", (x-1,y)))
            continue
        if d == "R" and x < 3:
            rdir.append(("R", (x+1,y)))
            continue
    return rdir

def maxPath(p, p2):
    if p is None:
        return p2
    if p2 is None or len(p) > len(p2):
        return p
    return p2

def backtrack(sH, pos=(0,0), path=""):
    x,y = pos
    if x == 3 and y == 3:
        return path
    h = createHash(sH, path)
    # print(h)
    p = None
    allDir = getAllDirPossibleFromHashAndPos(h, pos)
    # print(allDir)
    for a in allDir:
        p = maxPath(p, backtrack(sH, a[1], path+a[0]))
    return p

print(len(backtrack("qljzarfv")))