# bot : (val1, val2, low, high)

def readInput():
    inst = []
    while True:
        i = input()
        if len(i) == 0:
            break
        inst.append(i)
    return inst

def createBot():
    return [None, None, None, None]

def createListBot(size):
    return [createBot()for i in range(size)]

def readInstGive(inst):
    i = inst.split()
    bot = int(i[1])
    kw1 = i[5]
    fst = int(i[6])
    kw2 = i[10]
    snd = int(i[11])
    return (bot, (kw1, fst), (kw2, snd))

def readInstGet(inst):
    i = inst.split()
    val = int(i[1])
    bot = int(i[5])
    return (bot, val)

def addVal(bots, bot, val):
    if bots[bot][0] is None:
        bots[bot][0] = val
    else:
        bots[bot][1] = val

def updateInstBot(inst, bots, outputs):
    if inst[0:3] == "bot":
        bot, low, high = readInstGive(inst)
        bots[bot][2] = low
        bots[bot][3] = high
    else:
        bot, val = readInstGet(inst)
        addVal(bots, bot, val)

def hasTwoValues(bot):
    return bot[0] is not None and bot[1] is not None

def getAllBotsWith2Values(bots):
    nBots = []
    for i in range(len(bots)):
        b = bots[i]
        if hasTwoValues(b):
            nBots.append((i,b))
    return nBots

def writeAllInst(bots, outputs):
    for i in inst:
        updateInstBot(i, bots, outputs)

def sortVals(bot):
    i, j = (bot[0], bot[1])
    if i > j:
        return (j, i)
    return (i, j)

def execBot(bot, bots, outputs):
    low, high = sortVals(bot)
    part1 = False
    if low == 17 and high == 61:
        part1 = True
    for i in (2,3):
        val = low
        if i == 3:
            val = high
        dest, num = bot[i]
        if dest == "bot":
            addVal(bots, num, val)
        else:
            outputs[num] = val
    bot[0] = None
    bot[1] = None
    return part1

def execAllInst(bots, outputs):
    b = getAllBotsWith2Values(bots)
    while len(b) > 0:
        # print(b)
        num, bot = b[0]
        if execBot(bot, bots, outputs):
            print("Part 1 : ", num)
        b = getAllBotsWith2Values(bots)


inst = readInput()
bots = createListBot(400)
outputs = [0]*400
writeAllInst(bots, outputs)

execAllInst(bots, outputs)

print("Part 2: ", outputs[0]*outputs[1]*outputs[2])