blocked = sorted([list(map(int,a.split('-')))for a in[a.strip('\n')for a in open("20").readlines()]],key=lambda a:a[0])

firstIp = 0
i = 0
unblocked = []

while i < len(blocked):
    a,b = blocked[i]
    if a <= firstIp:
        firstIp = b+1
        print("Block", blocked[i])
        print("new first:", firstIp)
        i += 1
        while i < len(blocked) and blocked[i][1] < firstIp:
            print("Skip", blocked[i])
            i += 1
    else:
        maxIp = blocked[i][0]-1
        unblocked.append((firstIp, maxIp))
        print("Unblocked: ", unblocked[-1])
        firstIp = blocked[i][0]

c = 0
for u in unblocked:
    a,b = u
    c += b-a+1
print("Part 2:", c)