import hashlib

salt = "ahsbgdzn"

def generateHash(salt, idx):
    mp = bytes(salt + str(idx), 'utf-8')
    m = hashlib.md5(mp).hexdigest()
    for i in range(2016):
        mp = bytes(m, 'utf-8')
        m = hashlib.md5(mp).hexdigest()
    return m

def analyseHash(hash):
    triple = []
    quint = []
    for i in range(len(hash)-2):
        if hash[i] == hash[i+1] and hash[i+1] == hash[i+2]:
            h = hash[i:i+3]
            if h not in triple:
                triple.append(h)
            if i+4 < len(hash) and hash[i+2] == hash[i+3] and hash[i+3] == hash[i+4]:
                h = hash[i:i+5]
                if h not in quint:
                    quint.append(h)
    return (triple, quint)

def nextTriple(hashes, idx):
    while idx < len(hashes) and hashes[idx][1] == []:
        idx += 1
    return idx
    
def quintInThousand(hashes, idx, triple):
    i = idx
    quint = triple + triple[0]*2
    while i < idx+1000:
        if quint in hashes[i][2]:
            print("FOUND: ", idx-1, i)
            return True
        i += 1
    return False

hashes = []
for i in range(30000):
    if i%1000 == 0:
        print(i)
    h = generateHash(salt, i)
    triple, quint = analyseHash(h)
    hashes.append((h, triple, quint))

keys = 0
i = -1

while keys < 64:
    if i%1000 == 0:
        print(i, keys)
    i = nextTriple(hashes, i+1)
    t = hashes[i][1][0]
    if quintInThousand(hashes, i+1, t):
        keys += 1
        print("KEY", keys)

print(i)