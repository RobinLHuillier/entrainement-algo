def insideOutside(s):
    inside = []
    outside = []
    start = 0
    end = 0
    while end < len(s):
        if s[end] == '[':
            outside.append(s[start:end])
            start = end+1
            end = start
        elif s[end] == ']':
            inside.append(s[start:end])
            start = end+1
            end = start
        else:
            end += 1
    if start < len(s):
        outside.append(s[start:end])
    return (inside, outside)

def isABBA(s):
    return s[1] != s[0] and s[0] == s[3] and s[1] == s[2]

def isABA(s):
    return s[0] != s[1] and s[0] == s[2]

def createBAB(aba):
    return aba[1] + aba[0] + aba[1]

def containsBAB(inside, bab):
    for st in inside:
        while len(st) >= 3:
            t = st[0:3]
            if t == bab:
                return True
            st = st[1:]
    return False

def supportTLS(s):
    inside, outside = insideOutside(s)
    for st in inside:
        while len(st) >= 4:
            t = st[0:4]
            if isABBA(t):
                return False
            st = st[1:]
    for st in outside:
        while len(st) >= 4:
            t = st[0:4]
            if isABBA(t):
                return True
            st = st[1:]
    return False

def supportSSL(s):
    inside, outside = insideOutside(s)
    for st in outside:
        while len(st) >= 3:
            t = st[0:3]
            if isABA(t):
                r = createBAB(t)
                if containsBAB(inside, r):
                    return True
            st = st[1:]
    return False

countTLS = 0
countSSL = 0
while True:
    i = input()
    if len(i) == 0:
        break
    if supportTLS(i):
        countTLS += 1
    if supportSSL(i):
        countSSL += 1

print("Part 1: ", countTLS)
print("Part 2: ", countSSL)