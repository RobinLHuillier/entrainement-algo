def readInput(path):
    lines = open(path).read().splitlines()
    games = []
    for l in lines:
        sets = []
        rawSets = l.split(':')[1].split(';')
        for s in rawSets:
            dico = {}
            pull = s.split(',')
            for p in pull:
                num, color = p.strip().split()
                dico[color] = int(num)
            sets.append(dico)
        games.append(sets)
    return games

def idPossible(games, maximums):
    ids = []
    for i in range(len(games)):
        game = games[i]
        impossible = False
        for s in game:
            if impossible:
                break
            for color in s.keys():
                if s[color] > maximums[color]:
                    impossible = True
                    break
        if not impossible:
            ids.append(i+1)
    return ids

def powerGames(games):
    power = 0
    for game in games:
        mini = {'red': 0, 'green': 0, 'blue': 0}
        for s in game:
            for color in s.keys():
                mini[color] = max(mini[color], s[color])
        gamePower = 1
        for color in mini.keys():
            gamePower *= mini[color]
        power += gamePower
    return power

games = readInput('2')
ids = idPossible(games, {'red': 12, 'green': 13, 'blue': 14})
print(f"Part 1: {sum(ids)}")
power = powerGames(games)
print(f"Part 2: {power}")