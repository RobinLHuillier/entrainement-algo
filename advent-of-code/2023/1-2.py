inputs = open('1').read().splitlines()

def searchNum(string, part1=True, reverse=False):
    nums=[n for n in'0123456789']+(['zero','one','two','three','four','five','six','seven','eight','nine']if part1 else [])
    minIndex = len(string)
    first = None
    if reverse:
        nums = [n[::-1]for n in nums]

    test = [(string.index(a),idx%10) for idx,a in enumerate(nums) if a in string]
    # return str(min(test)[0])

    for a in nums:
        if a not in string:
            continue
        idx = string.index(a)
        if idx < minIndex:
            minIndex = idx
            first = a
    
    
    # print(string, test, min(test)[1], str(nums.index(first)%10))
    return str(min(test)[1])

    return str(nums.index(first)%10)

print(' '.join([str(sum(int(searchNum(i,k)+searchNum(i[::-1],k,True))for i in inputs))for k in (1,0)]) == "53855 54634")

"54634"
"53855"
