print(
    (
        searchNum:=lambda string,part2,reverse: # part2 et reverse sont des booléens
            str(min(
                [(string.index(num),idx%10) for idx,num in   
                    # tableau de tuples (index, nombre), le modulo pour avoir un nombre entre 0 et 9
                    enumerate([n[::1-2*reverse] for n in
                        # ici on reverse chaque chaine de caractère si reverse est vrai: 'zero' devient 'orez'
                        [str(n)for n in range(10)]  # ['0','1','2','3','4','5','6','7','8','9']
                        + ['zero','one','two','three','four','five','six','seven','eight','nine']*part2]
                        # *part2: si c'est faux, on ajoute [] 
                    )if num in string] # string.index(num) craque son slip si num n'est pas dans string
            )[1]),

        ' '.join( # séparer les deux résultats d'un espace
            [
                str(
                    sum(
                        int(searchNum(string,part2,reverse=0) # on cherche le premier nombre
                            +searchNum(string[::-1],part2,reverse=1)) # on inverse la string pour chercher le dernier nombre
                    for string in open('1').read().splitlines()) # lire l'input, séparer les lignes
                )
                for part2 in (0,1) # part2 = 0 pour la partie 1, part2 = 1 pour la partie 2
            ]
        )

    )[1] # second element contient le résultat à afficher
)
