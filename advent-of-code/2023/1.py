inputs = open('1').read().splitlines()

# s = 0
# for i in inputs:
#     firstNum = None
#     lastNum = None
#     for a in range(len(i)):
#         if i[a] in '0123456789':
#             firstNum = i[a]
#             break
#     for a in range(len(i)):
#         if i[len(i)-1-a] in '0123456789':
#             lastNum = i[len(i)-1-a]
#             break
#     s += int(firstNum + lastNum)

# print(f"Part 1 : {s}")

nums = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
otherNums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

# inputs = ['3tgsppcfpk']

s = 0
for i in inputs:
    firstNum = None
    minIndex = len(i)
    for a in nums + otherNums:
        if a not in i:
            continue
        idx = i.index(a)
        if idx < minIndex:
            if a in nums:
                firstNum = nums.index(a)
            else:
                firstNum = a
            minIndex = idx
    # print(i, firstNum)
    lastNum = None
    maxIndex = -1
    for a in nums + otherNums:
        for b in range(len(i)):
            if a not in i[len(i)-1-b:]:
                continue
            idx = i[len(i)-1-b:].index(a) + len(i)-1-b
            # print(a, idx, i[len(i)-1-b:])
            if idx > maxIndex:
                if a in nums:
                    lastNum = nums.index(a)
                else:
                    lastNum = a
                maxIndex = idx
    print(i, firstNum, lastNum)
    s += int(str(firstNum) + str(lastNum))

print(f"Part 2: {s}")