def readInput(path):
    lines = open(path).read().splitlines()
    numbers = [] # tuples (num, (posRow, posCol))  -- pos correspond au début du num
    row = 0
    for l in lines:
        i = 0
        while i < len(l):
            if l[i] in '0123456789':
                start = i
                i += 1
                while i < len(l) and l[i] in '0123456789':
                    i += 1
                numbers.append((int(l[start:i]), (row, start)))
            else:
                i += 1
        row += 1
    return numbers, lines

def neighHasSymbol(row, col, grid):
    for r in range(row-1,row+2):
        for c in range(col-1,col+2):
            if r == row and c == col:
                continue
            if r < 0 or c < 0 or r >= len(grid) or c >= len(grid[r]):
                continue
            if grid[r][c] not in '.0123456789':
                return True
    return False

def isAdjacent(number, grid):
    num, (row, col) = number
    strNum = str(num)
    for i in range(len(strNum)):
        char = strNum[i]
        colStart = col + i
        if neighHasSymbol(row, colStart, grid):
            return True
    return False

def getAllAdjacents(numbers, grid):
    adj = []
    for n in numbers:
        if isAdjacent(n, grid):
            adj.append(n)
    return adj

def sumAdjacents(numbers):
    return sum([n[0]for n in numbers])

def getAllGears(grid):
    gears = []
    for col in range(len(grid)):
        for row in range(len(grid[col])):
            if grid[row][col] == '*':
                gears.append((row, col))
    return gears

def getAllPotentialNumbers(grid, row, col):
    potential = []
    for r in range(row-1,row+2):
        for c in range(col-1,col+2):
            if r == row and c == col:
                continue
            if r < 0 or c < 0 or r >= len(grid) or c >= len(grid[r]):
                continue
            if grid[r][c] in '0123456789':
                potential.append((r, c))
    return potential

def searchNumber(grid, row, col):
    nums = '0123456789'
    start, end = col, col
    while start-1 >= 0 and grid[row][start-1] in nums:
        start -= 1
    while end+1 < len(grid[row]) and grid[row][end+1] in nums:
        end += 1
    num = int(grid[row][start:end+1])
    return (num, (row, start))

def getNumbersFromPotential(grid, potentials):
    numbers = []
    for num in potentials:
        number = searchNumber(grid, *num)
        if number not in numbers:
            numbers.append(number)
    return numbers

def findAllGears(grid):
    gears = getAllGears(grid)
    gearRatios = []
    for gear in gears:
        potentials = getAllPotentialNumbers(grid, *gear)
        realNumbers = getNumbersFromPotential(grid, potentials)
        if len(realNumbers) == 2:
            gearRatio = realNumbers[0][0] * realNumbers[1][0]
            # print("found: ", gearRatio, realNumbers)
            gearRatios.append(gearRatio)
    return sum(gearRatios)


numbers, grid = readInput('3')
adj = getAllAdjacents(numbers, grid)
sumAdj = sumAdjacents(adj)
print(f"Part 1: {sumAdj}")


gearRatio = findAllGears(grid)
print(f"Part 2: {gearRatio}")