def comp(cond, a, b):
    return  (cond == ">" and a > b) or \
            (cond == ">=" and a >= b) or \
            (cond == "==" and a == b) or \
            (cond == "<" and a < b) or \
            (cond == "<=" and a <= b) or \
            (cond == "!=" and a != b)

reg = {}

maximax = "not"

while True:
    inp = input().split('\n')[0]
    if (len(inp) < 2):
        break
    # print(inp)
    r = inp.split()[0]
    inc = (inp.split()[1][0] == "i")
    q = int(inp.split()[2])
    rCond = inp.split()[4]
    cond = inp.split()[5]
    qCond = int(inp.split()[6])
    # print(r, inc, q, rCond, cond, qCond)
    if rCond not in reg:
        reg[rCond] = 0
    if r not in reg:
        reg[r] = 0
    if comp(cond, reg[rCond], qCond):
        if not inc:
            q *= -1
        reg[r] += q
    # print(reg)
    maxi = "not"
    for key, val in reg.items():
        if maxi == "not":
            maxi = val
        if val > maxi:
            maxi = val
    if maximax == "not":
        maximax = maxi
    if maximax < maxi:
        maximax = maxi


print(maximax)
