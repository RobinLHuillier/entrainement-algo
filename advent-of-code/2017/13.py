n = 100
guards = [(-1,0,1)for i in range(n)]
while True:
    inp = input().split('\n')[0]
    inp = inp.split('\r')[0]
    if len(inp) == 0:
        break
    pos, var = map(int,inp.split(":"))
    if var > 1:
        guards[pos] = (0,var,1)
    else:
        guards[pos] = (0,var,0)

cost = 0
pos = -1
for i in range(len(guards)):
    pos += 1
    if guards[pos][0] == 0:
        aie = pos*guards[pos][1]
        print("caught!", aie)
        cost += aie
    for j in range(len(guards)):
        posG, var, dir = guards[j]
        if var != 0:
            posG += dir
            if (dir == 1 and posG == var-1) or (dir == -1 and posG == 0):
                dir *= -1
            guards[j] = (posG,var,dir)
    # print(i, pos, guards)

print("part 1:", cost)
print("part 2:")

equ = []
for i in range(len(guards)):
    if guards[i][1] != 0:
        equ.append((i,(guards[i][1]-1)*2))
print(equ)

delay = 0
while True:
    if delay%1000 == 0:
        print(delay)
    found = True
    for i in range(len(equ)):
        pos, var = equ[i]
        if (delay+pos)%var == 0:
            # print(i, equ[i], delay, pos, var, delay+pos, (delay+pos)%var)
            delay += 1
            found = False
            break
    if found:
        print(delay)
        break