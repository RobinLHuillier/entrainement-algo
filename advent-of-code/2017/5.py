breakPoint = 40000000

jumps = []
while True:
    inp = int(input().split('\n')[0])
    if inp == breakPoint:
        break
    jumps.append(inp)

n = len(jumps)
print("len", n)

i = 0
step = 0
while i >= 0 and i < n:
    i2 = i + jumps[i]
    if jumps[i] >= 3:
        jumps[i] -= 1
    else:
        jumps[i] += 1
    i = i2
    step += 1

print("step:",step)