k = input().split()[0]

sum = 0
sec = len(k)//2
for i in range(len(k)):
    if k[i] == k[(i+sec)%len(k)]:
        sum += int(k[i])
print(sum)