passPhrases = []
valid = 0
while True:
    inp = input().split('\n')[0]
    if inp[0] == "4":
        break
    p = inp.split()
    passPhrases.append(p)
    possible = True
    for m in p:
        if p.count(m)>1:
            possible = False
            break
    if possible:
        valid += 1
        print("possible:", p)
    else:
        print("impossible:", p)

print(valid)
print("\n\nPART2")

valid = 0
for p in passPhrases:
    p2 = []
    for m in p:
        p2.append(''.join(sorted(m)))
    possible = True
    for m in p2:
        if p2.count(m)>1:
            possible = False
            break
    if possible:
        valid += 1
        print("possible:", p2)
    else:
        print("impossible:", p2)

print(valid)
