alpha = "abcdefghijklmnopqrstuvwxyz"
prog = []
while True:
    inp = input().split('\n')[0]
    inp = inp.split('\r')[0]
    if len(inp) == 0:
        break
    prog.append(inp.split())
reg1 = {}
reg2 = {}
for a in alpha:
    reg1[a] = 0
    reg2[a] = 0
reg2["p"] = 1

def exe(cmd,reg):
    if cmd[0] == "snd":
        if cmd[1] in alpha:
            a = reg[cmd[1]]
        else:
            a = int(cmd[1])
        return ("send", a)
    if cmd[0] == "set":
        if cmd[2] in alpha:
            reg[cmd[1]] = reg[cmd[2]]
        else:
            reg[cmd[1]] = int(cmd[2])
        return None
    if cmd[0] == "add":
        if cmd[2] in alpha:
            reg[cmd[1]] += reg[cmd[2]]
        else:
            reg[cmd[1]] += int(cmd[2])
        return None
    if cmd[0] == "mul":
        if cmd[2] in alpha:
            reg[cmd[1]] *= reg[cmd[2]]
        else:
            reg[cmd[1]] *= int(cmd[2])
        return None
    if cmd[0] == "mod":
        if cmd[2] in alpha:
            reg[cmd[1]] %= reg[cmd[2]]
        else:
            reg[cmd[1]] %= int(cmd[2])
        return None
    if cmd[0] == "rcv":
        return ("recover",cmd[1])
    if cmd[0] == "jgz":
        if cmd[1] in alpha:
            a = reg[cmd[1]]
        else:
            a = int(cmd[1])
        if a > 0:
            if cmd[2] in alpha:
                return cmd[2]
            return ("jump",int(cmd[2]))
        return None
    print("NOT RECOGNIZED")
    return None

pos1 = 0
pos2 = 0
send1 = []
send2 = []
rcv1 = None
rcv2 = None
prog1send = 0
prog2send = 0

print("start")

while True:
    if rcv1 is not None and len(send2) > 0:
        reg1[rcv1] = send2.pop(0)
        # print("pop1",len(send2))
        rcv1 = None
    if rcv2 is not None and len(send1) > 0:
        reg2[rcv2] = send1.pop(0)
        # print("pop2",len(send1))
        rcv2 = None

    if rcv1 is not None and rcv2 is not None: # deadlock
        print(len(send2),len(send1))
        break

    if rcv1 is None:
        res1 = exe(prog[pos1],reg1)
        pos1 += 1
        if res1 is not None:
            if res1[0] == "jump":
                pos1 = pos1 -1 + res1[1]
            elif res1[0] == "recover":
                rcv1 = res1[1]
            elif res1[0] == "send":
                prog1send += 1
                send1.append(res1[1])
                # print("1:",prog1send, len(send1))
    if rcv2 is None:
        res2 = exe(prog[pos2],reg2)
        pos2 += 1
        if res2 is not None:
            if res2[0] == "jump":
                pos2 = pos2 -1 + res2[1]
            elif res2[0] == "recover":
                rcv2 = res2[1]
            elif res2[0] == "send":
                prog2send += 1
                send2.append(res2[1])
                # print("2:",prog2send,len(send2))

print("fin:",prog1send, prog2send//2)
print(reg1)
print(reg2)
