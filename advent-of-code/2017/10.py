def cutHash(hash,pos,e):
    if pos+e < len(hash):
        return hash[pos:pos+e]
    return hash[pos:] + hash[:(pos+e)%len(hash)]

size = 256

hash = [i for i in range(size)]
# print(hash)
l = input().split('\n')[0]
l = list(map(int, l.split(',')))
# print(l)
pos = 0
skip = 0

for e in l:
    # print("---- NOUVELLE LONGUEUR")
    # print(hash)
    # print("pos:",pos,"  - length:",e)
    rev = []
    for i in range(e):
        rev.append(hash[(pos+i)%size])
    rev = rev[::-1]
    # print(len(rev))
    # print("rev:",rev)
    for i in range(e):
        hash[(pos+i)%size] = rev[i]
    # print("hash:",hash)
    pos = (pos+e+skip)%size
    skip += 1
    # print("new pos:", pos, " - skip:",skip)

print("--- deux premiers:")
print(hash[0],hash[1])
print("mult: ", hash[0]*hash[1])
print("\n--Part 2")

text = "18,1,0,161,255,137,254,252,14,95,165,33,181,168,2,188"
end = [17, 31, 73, 47, 23]
l = []
for t in text:
    l.append(ord(t))
l += end
print(l)

def round(hash,pos,skip,seq):
    for e in seq:
        rev = []
        for i in range(e):
            rev.append(hash[(pos+i)%size])
        rev = rev[::-1]
        for i in range(e):
            hash[(pos+i)%size] = rev[i]
        pos = (pos+e+skip)%size
        skip += 1
    return (hash,pos,skip,seq)

hash = [i for i in range(size)]
pos = 0
skip = 0
for i in range(64):
    hash, pos, skip, l = round(hash,pos,skip,l)

print("64 rounds ok")

def dense(hash):
    d = []
    for i in range(16):
        xor = hash[16*i]
        for j in range(1,16):
            xor = xor ^ hash[16*i+j]
        d.append(xor)
    return d

d = dense(hash)
print("dense ok", d)

def hexa(hash):
    hexS = ""
    for h in hash:
        temp = hex(h)[2:]
        if len(temp) == 1:
            hexS += "0" + temp
        else:
            hexS += temp
    return hexS

h = hexa(d)
print("hex:", h)