class Tape:
    def __init__(self, right=None, left=None):
        self.val = 0
        self.right = right
        self.left = left
    def setVal(self, val):
        self.val = val
    def getVal(self):
        return self.val
    def moveRight(self):
        if self.right is None:
            self.right = Tape(None,self)
        return self.right
    def moveLeft(self):
        if self.left is None:
            self.left = Tape(self,None)
        return self.left

def countOnes(tape):
    # go left
    print("TAPE: ",end="")
    while tape.left is not None:
        tape = tape.left
    cpt = 0
    while tape is not None:
        print(tape.getVal(),end="")
        if tape.getVal() == 1:
            cpt += 1
        tape = tape.right
    print()
    return cpt

STATE = "A"
STEPS = 25
tape = Tape()
cpt1 = 0
for _ in range(STEPS):
    if STATE == "A":
        if tape.getVal() == 0:
            tape.setVal(1)
            tape = tape.moveRight()
            STATE = "B"
        else:
            tape.setVal(0)
            tape = tape.moveLeft()
            STATE = "B"

    elif STATE == "B":
        if tape.getVal() == 0:
            tape.setVal(1)
            tape = tape.moveLeft()
            STATE = "A"
        else:
            tape = tape.moveRight()
            STATE = "A"

    # print(_,tape.getVal(),countOnes(tape),STATE)

print(countOnes(tape))
print(cpt1)