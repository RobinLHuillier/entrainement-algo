from json import JSONEncoder


def hexa(hash):
    hexS = ""
    for h in hash:
        temp = hex(h)[2:]
        if len(temp) == 1:
            hexS += "0" + temp
        else:
            hexS += temp
    return hexS
def dense(hash):
    d = []
    for i in range(16):
        xor = hash[16*i]
        for j in range(1,16):
            xor = xor ^ hash[16*i+j]
        d.append(xor)
    return d
def round(hash,pos,skip,seq,size):
    for e in seq:
        rev = []
        for i in range(e):
            rev.append(hash[(pos+i)%size])
        rev = rev[::-1]
        for i in range(e):
            hash[(pos+i)%size] = rev[i]
        pos = (pos+e+skip)%size
        skip += 1
    return (hash,pos,skip,seq)
def knotHash(text):
    size = 256
    l = []
    for t in text:
        l.append(ord(t))
    l += [17, 31, 73, 47, 23]
    hash = [i for i in range(size)]
    pos = 0
    skip = 0
    for i in range(64):
        hash, pos, skip, l = round(hash,pos,skip,l,size)
    d = dense(hash)
    h = hexa(d)
    return h

def knotToBinary(knot):
    binS = ""
    for k in knot:
        binI = bin(int("0x"+k,16))[2:]
        while len(binI) < 4:
            binI = "0" + binI
        binS += binI
    return binS


key = "hfdlxzhv"
used = 0
grid = []
for i in range(128):
    currKey = key + "-" + str(i)
    knot = knotHash(currKey)
    if len(knot) < 32:
        print("PROBLEME")
    binS = knotToBinary(knot)
    grid.append(binS)
    used += binS.count("1")

print("part 1:",used)

print("part 2:")
mat = [[0]*128 for i in range(128)]
for i in range(128):
    for j in range(128):
        if grid[i][j] == "1":
            mat[i][j] = -1

def mark(mat, i, j, r):
    if i<0 or j<0 or i>127 or j>127 or mat[i][j] != -1:
        return
    mat[i][j] = r
    mark(mat, i+1, j, r)
    mark(mat, i-1, j, r)
    mark(mat, i, j+1, r)
    mark(mat, i, j-1, r)    

region = 1
while True:
    si = -1
    sj = -1
    for i in range(128):
        if si != -1:
            break
        for j in range(128):
            if mat[i][j] == -1:
                si = i
                sj = j
                break
    if si == -1:
        print(region-1)
        break
    mark(mat, si, sj, region)
    region += 1