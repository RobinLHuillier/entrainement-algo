stream = input().split('\n')[0]

open = []
garbage = False
canceled = False

score = 0

for i in range(len(stream)):
    char = stream[i]

    if canceled: 
        canceled = False
        continue

    if garbage:
        if char == ">":
            garbage = False
            continue
        elif char == "!":
            canceled = True
            continue
        score += 1
        continue

    if char == "{":
        open.append(char)
    elif char == "}":
        # score += len(open)
        open = open[:-1]
    elif char == "<":
        garbage = True

print(score)