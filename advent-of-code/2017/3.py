size = 601
start = 300
mat = [[0]*size for i in range(size)]
row = start
col = start+1
doubleStep = 2
maxStep = 1
step = 1
rowDiff = 0
colDiff = 1
mat[start][start] = 1
for i in range(2,size*size +1):
    mat[row][col] = i
    step -= 1
    if step == 0:
        doubleStep -= 1
        if doubleStep == 0:
            doubleStep = 2
            maxStep += 1
        step = maxStep
        if colDiff == 1:
            rowDiff = -1
            colDiff = 0
        elif rowDiff == -1:
            rowDiff = 0
            colDiff = -1
        elif colDiff == -1:
            colDiff = 0
            rowDiff = 1
        elif rowDiff == 1:
            rowDiff = 0
            colDiff = 1
    row += rowDiff
    col += colDiff

num = int(input().split()[0])

row = 0
col = 0

for row in range(len(mat)):
    if num in mat[row]:
        col = mat[row].index(num)
        break

print(row, col)
print(abs(start-row)+abs(start-col))

print("PART2")

size = 11
start = 5
mat = [[0]*size for i in range(size)]
row = start
col = start+1
doubleStep = 2
maxStep = 1
step = 1
rowDiff = 0
colDiff = 1
mat[start][start] = 1
for i in range(2,size*size +1):
    sum = 0
    for x in [-1,0,1]:
        for y in [-1,0,1]:
            if row+x < size and col+y < size and row+x >= 0 and col+y >= 0:
                sum += mat[row+x][col+y]
    mat[row][col] = sum
    if sum > num:
        print(sum)
        break
    step -= 1
    if step == 0:
        doubleStep -= 1
        if doubleStep == 0:
            doubleStep = 2
            maxStep += 1
        step = maxStep
        if colDiff == 1:
            rowDiff = -1
            colDiff = 0
        elif rowDiff == -1:
            rowDiff = 0
            colDiff = -1
        elif colDiff == -1:
            colDiff = 0
            rowDiff = 1
        elif rowDiff == 1:
            rowDiff = 0
            colDiff = 1
    row += rowDiff
    col += colDiff
