def generator(start,mult,div,mask,mod):
    while True:
        start = (start*mult)%div
        if start % mod == 0:
            yield start & mask

Astart = 703
Bstart = 516
# Astart = 65
# Bstart = 8921
multA = 16807
multB = 48271
div = 2147483647

mask = 0xffff
genA = generator(Astart,multA,div,mask,4)
genB = generator(Bstart,multB,div,mask,8)
paire = 0
for i in range(5000000):
    paire += (next(genA) == next(genB))

    # precA = nextVal(precA,multA,div)
    # precB = nextVal(precB,multB,div)
    # if precA%4 == 0:
    #     binA = binString16(precA)
    #     stackA.append(binA)
    #     # if precA in cycleA:
    #     #     print("A TROUVE ------------")
    #     # cycleA.append(precA)
    #     # if len(cycleA)%1000 == 0:
    #     #     print("A",len(cycleA))
    # if precB%8 == 0:
    #     binB = binString16(precB)
    #     stackB.append(binB)
    #     # if precB in cycleB:
    #     #     print("B TROUVE")
    #     # cycleB.append(precB)
    #     # if len(cycleB)%1000 == 0:
    #     #     print("B",len(cycleB))
    # if len(stackA) > 0 and len(stackB) > 0:
    #     binA = stackA.pop(0)
    #     binB = stackB.pop(0)
    #     if binA == binB:
    #         print(pairProcessed,paires,len(stackA),len(stackB))
    #         paires += 1
    #     pairProcessed += 1
    #     if pairProcessed >= 5000000:
    #         break
    
print("part2: ", paire)