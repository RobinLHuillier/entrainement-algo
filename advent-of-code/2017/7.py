tower = {}
progs = []

while True:
    inp = input().split('\n')[0]
    if inp[0] == "4":
        break
    name = inp.split()[0]
    w = inp.split()[1]
    w = w.split('(')[1]
    w = int(w.split(')')[0])
    progs.append(name)
    if name not in tower:
        tower[name] = {"w":w,"s":[],"tot":w}
    if '>' in inp:
        supp = inp.split('>')[1]
        s = []
        for t in supp.split(','):
            s.append(t.split()[0])
        tower[name]["s"] = list(s)

# print(tower)
# print(progs)
start = ""

for i in range(len(progs)):
    prog = progs[i]
    preums = True
    for p in progs:
        if p != prog and prog in tower[p]["s"]:
            preums = False
            break
    if preums:
        print(prog, tower[prog]["w"])
        start = prog
        break

print("\nPart 2\n")

def recTot(prog):
    if len(tower[prog]["s"]) == 0:
        return tower[prog]["tot"]
    somme = 0
    for p in tower[prog]["s"]:
        somme += recTot(p)
    tower[prog]["tot"] += somme
    return tower[prog]["tot"]

recTot(start)

def standOut(progList):
    w1 = tower[progList[0]]["tot"]
    p1 = progList[0]
    w2 = -1
    p2 = ""
    for p in progList:
        if p == p1:
            continue
        if tower[p]["tot"] != w1:
            if w2 == -1:
                w2 = tower[p]["tot"]
                p2 = p
            else:
                return (w1, p1, w2)
        elif w2 != -1:
            return (w2, p2, w1)
    if w2 != -1:
        return (w2,p2,w1)
    return (-1, -1, -1)

def search(prog):
    print(prog)
    if len(tower[prog]["s"]) == 0:
        return
    w1, p1, w2 = standOut(tower[prog]["s"])
    if w1 != -1:
        diff = w2 - w1
        if search(p1) == -1:
            print("diff:", diff, "p:", p1)
            print("w:", tower[p1]["w"], "new w:", tower[p1]["w"]+diff)
    else:
        return -1

# search(start)

print(search("vfjnsd"))
# print(tower["vfjnsd"]["s"])
# print(standOut(tower["vfjnsd"]["s"]))

# print('tests')
# for p in tower["vfjnsd"]["s"]:
#     print(p, tower[p])
#     print(standOut(tower[p]["s"]))