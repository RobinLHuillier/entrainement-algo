def moveIt(p,move):
    prog = p[::]
    if move[0] == "s":   #spin
        num = int(move[1:])
        n = len(prog)
        prog = prog[n-num:] + prog[:n-num]
    elif move[0] == "x": #exchange
        a,b = map(int,move[1:].split('/'))
        if a > b:
            t = a
            a = b
            b = t
        A = prog[a]
        B = prog[b]
        prog = prog[:a] + B + prog[a+1:b] + A + prog[b+1:]
    elif move[0] == "p": #partner
        a,b = move[1:].split('/')
        a = prog.index(a)
        b = prog.index(b)
        if a > b:
            t = a
            a = b
            b = t
        A = prog[a]
        B = prog[b]
        prog = prog[:a] + B + prog[a+1:b] + A + prog[b+1:]
    return prog

nbProg = 16
alpha = "abcdefghijklmnopqrstuvwxyz"
prog = alpha[:nbProg]
moves = input().split('\n')[0]
moves = moves.split('\r')[0]
moves = moves.split(',')

save = [prog]
repet = -1
for i in range(1,1000000001):
    for m in moves:
        prog = moveIt(prog,m)
    if prog in save:
        if prog == save[0]:
            print("YES")
        print(i)
        repet = i
        break
    save.append(prog)

r = 1000000000%repet
print(r)
print(save[1000000000%len(save)])
for i in range(r):
    for m in moves:
        prog = moveIt(prog,m)

print(prog)
