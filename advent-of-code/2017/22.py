def turn(dRow, dCol, dir):
    if dRow == -1:
        if dir == "left":
            return (0,-1)
        return (0,1)
    if dRow == 1:
        if dir == "left":
            return (0,1)
        return (0,-1)
    if dCol == -1:
        if dir == "left":
            return (1,0)
        return (-1,0)
    if dCol == 1:
        if dir == "left":
            return (-1,0)
        return (1,0)

CLEAN = 0
WEAK = 1
INF = 2
FLAG = 3
mat = []
while True:
    inp = input().strip("\n\r ")
    if len(inp) == 0:
        break
    mat.append(inp)
n = len(mat)
mid = n//2
grid = set()
for i in range(n):
    for j in range(n):
        row = i-mid
        col = j-mid
        if mat[i][j]=="#":
            flg = INF
        else:
            flg = CLEAN
        grid.add((row,col,flg))

dRow = -1
dCol = 0
row = 0
col = 0
infections = 0
iterations = 10000000

for _ in range(iterations):
    flg = -1
    for f in range(4):
        if (row,col,f) in grid:
            grid.remove((row,col,f))
            flg = f
            break
    if flg == -1:
        flg = CLEAN
        
    if flg == CLEAN:
        dRow, dCol = turn(dRow,dCol,"left")
    elif flg == WEAK:
        infections += 1
    elif flg == INF:
        dRow, dCol = turn(dRow,dCol,"right")
    elif flg == FLAG:
        dRow, dCol = -dRow, -dCol

    flg = (flg+1)%4

    grid.add((row,col,flg))
    row += dRow
    col += dCol

print("infections:",infections)