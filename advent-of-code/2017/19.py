from re import search


def searchStart(path):
    for i in range(len(path[0])):
        if path[0][i] == "|":
            return i

path = []
while True:
    inp = input().split('\n')[0]
    inp = inp.split('\r')[0]
    if len(inp) == 0:
        break
    path.append(inp)


alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
i = 0
j = searchStart(path)
dI = 1      # difference (direction)
dJ = 0
pI = 0      # prec position
pJ = 0
seen = ""   # letters seen
step = 1

while True:
    step += 1

    print(i,j,dI,dJ,seen,path[i][j],path[i+dI][j+dJ],len(path),len(path[0]), step)
    if path[i+dI][j+dJ] in alpha:   # found a letter
        seen += path[i+dI][j+dJ]
        pI = i
        pJ = j
        i += dI
        j += dJ
        continue

    if path[i+dI][j+dJ] == "+":     # intersection
        pI = i
        pJ = j
        i += dI
        j += dJ
        found = False
        for dI2,dJ2 in [(-1,0),(1,0),(0,-1),(0,1)]:
            if i+dI2 != pI or j+dJ2 != pJ:    # don't go back
                if i+dI2 >= 0 and j+dJ2 >= 0 and i+dI2 < len(path) and j+dJ2 < len(path[i+dI2]) and path[i+dI2][j+dJ2] != " ":
                    dI = dI2
                    dJ = dJ2
                    found = True
                    break
        if not found :      # end of the road
            break
        continue

    pI = i
    pJ = j
    i += dI
    j += dJ

print("part 1:",seen)