def oracle(p,v,a):
    oracl = 100000000000000000000000000000000000000000000000000000000000000000
    bigS = (oracl*(oracl+1))//2
    dist = 0
    for i in range(3):
        dist += abs(p[i] + v[i] +bigS*a[i])
    return dist

distances = []
points = []
n = 0
while True:
    inp = input().strip("\n\r")
    if len(inp) == 0:
        break
    n += 1
    p,v,a,nothing = inp.split('>')
    p = p.split('<')[1]
    px,py,pz = map(int,p.split(','))
    v = v.split('<')[1]
    vx,vy,vz = map(int,v.split(','))
    a = a.split('<')[1]
    ax,ay,az = map(int,a.split(','))
    # print(px,py,pz,vx,vy,vz,ax,ay,az)
    dist = oracle([px,py,pz],[vx,vy,vz],[ax,ay,az])
    points.append(((px,py,pz),(vx,vy,vz),(ax,ay,az)))
    # print(dist)
    distances.append(dist)

mini = distances[0]
si = 0
for i in range(1,len(distances)):
    if distances[i] < mini:
        # print("mini", i)
        # print(mini)
        # print(distances[i])
        mini = distances[i]
        si = i

print("part 1:")
print(len(distances),si)
print("part 2:")

from math import sqrt

def egal(a,b):
    e = 1e-6
    return abs(a-b) < e
def collide(a,b):
    # print("NEW COLLISION")
    pa, va, aa = a
    pb, vb, ab = b
    sol = []
    for i in range(3):
        # print("I:", i)
        X = pa[i] - pb[i]
        Y = va[i] - vb[i]
        Z = aa[i] - ab[i]
        A = Z/2
        B = Y+A
        C = X
        # print("A:",A, "-B:",B, "-C:",C)
        
        if egal(A,0):   # equation du premier degre
            if egal(B,0): # equation de degré 0
                if egal(C,0):
                    sol.append([-2])  # -2 code pour "quand tu veux"
                    continue
                else:   # pas sur le même plan ne se croiseront jamais
                    return [-1]
            else:
                x = -C/B
                # print(x)
                if egal(x, int(x)) and x>=0:
                    sol.append([int(x)])
                    continue
                else:
                    return [-1]

        # equation second degre
        det = B*B - 4*A*C
        if det < 0 and not egal(det,0):  # solution complexe on tej
            return [-1]
        if egal(det,0):
            x = -B/(2*A)
            if egal(x,int(x)) and x>=0:  # entier
                sol.append([int(x)])
                continue
            return [-1]
        rac = sqrt(det)
        x = (-B+rac)/(2*A)
        y = (-B-rac)/(2*A)
        ssol = []
        if egal(x,int(x)) and x>=0:
            ssol.append(int(x))
        if egal(y,int(y)) and y>=0:
            ssol.append(int(y))
        if len(ssol) == 0:
            return [-1]
        sol.append(ssol)
    # meme solutions pour les 3 ? 
    vsol = set()
    # print(sol)
    for s in sol[0]:
        if (s in sol[1] or -2 in sol[1]) and (s in sol[2] or -2 in sol[2]):
            vsol.add(s)
    for s in sol[1]:
        if (s in sol[0] or -2 in sol[0]) and (s in sol[2] or -2 in sol[2]):
            vsol.add(s)
    for s in sol[2]:
        if (s in sol[1] or -2 in sol[1]) and (s in sol[0] or -2 in sol[0]):
            vsol.add(s)
    if len(vsol) == 0:
        return [-1]
    v2sol = set()
    for v in vsol:
        if v == -2:
            v2sol.add(0)
        else:
            v2sol.add(v)
    return list(v2sol)


collisions = [[[] for i in range(n)] for j in range(n)]
for i in range(n):
    for j in range(n):
        if j != i and collisions[i][j] == []:  # sinon on comparerait le même vecteur ou alors on l'a déjà fait
            res = collide(points[i],points[j])
            if res[0] != -1:
                print(res, i, j)
                res.sort()
                collisions[i][j] = res[::]
                collisions[j][i] = res[::]

collided = set()
while True:    # chercher toutes les collisions
    mini = 1e50
    for i in range(n):
        if i in collided:
            continue
        for j in range(n):
            if j in collided:
                continue
            if collisions[i][j] != []:
                if collisions[i][j][0] < mini:
                    mini = collisions[i][j][0]
    if mini == 1e50:
        break
    newCollide = set()
    for i in range(n):
        if i in collided:
            continue
        for j in range(n):
            if j in collided:
                continue
            if collisions[i][j] != [] and collisions[i][j][0] == mini:
                newCollide.add(i)
                newCollide.add(j)
    for c in newCollide:
        collided.add(c)

print(list(collided))
nCollide = len(list(collided))
print(n, nCollide, n-nCollide)