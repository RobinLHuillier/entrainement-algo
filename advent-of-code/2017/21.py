def rotate(l):
    n = len(l)
    sub = ["" for i in range(n)]
    for i in range(n):
        for j in range(n-1,-1,-1):
            sub[i] += l[j][i]
    return sub
def flip(l,h):
    if h == 0:
        return l
    n = len(l)
    sub = ["" for i in range(n)]
    if h == 1:   # horizontal
        for i in range(n):
            for j in range(n-1,-1,-1):
                sub[i] += l[i][j]
    else:
        for i in range(n):
            sub[i] = l[n-i-1]
    return sub
def divide(mat, div):
    n = len(mat)
    nb = n//div
    nMat = [[] for i in range(nb)]
    for i in range(0,n,div):
        for j in range(0,n,div):
            cell = []
            for k in range(div):
                cell.append(mat[i+k][j:j+div])
            # print(cell,i,j,i+div-1,j+div+div-1)
            nMat[i//div].append(cell)
    return nMat
def fusion(mat,div):
    nb = len(mat)
    n = nb*div    
    # remise à plat
    nnMat = ["" for i in range(n)]
    for i in range(nb):
        for j in range(nb):
            for k in range(div):
                nnMat[i*div+k] += mat[i][j][k]
    return nnMat
def whichDiv(mat):
    if len(mat)%2 == 0:
        return 2
    return 3
def matchRules(cell,rules):
    n = len(cell)
    for r in rules[n]:
        c = cell[::]
        for i in range(4):
            for j in range(3):
                c = flip(c,j)
                ok = True
                for j in range(n):
                    if r[0][j] != c[j]:
                        ok = False
                        break
                if ok:
                    return r[1]
                c = flip(c,j)
            c = rotate(c)
    print(cell,rules[n][0][0])
    print("NO RULE MATCHED")
    c = cell[::]
    for i in range(4):
        print("/".join(c))
        c = rotate(c)
    return None
def iterate(mat,rules):
    n = len(mat)
    div = whichDiv(mat)
    nMat = divide(mat,div)
    nnMat = [[]for i in range(n//div)]
    for i in range(len(nMat)):
        for c in nMat[i]:
            nCell = matchRules(c,rules)
            if nCell is None:
                return None
            nnMat[i].append(nCell)
    div += 1
    mat = fusion(nnMat,div)
    return mat
def multIterate(mat,rules,nb):
    for i in range(nb):
        print("iteration",i,"  - taille:",len(mat),"x",len(mat))
        mat = iterate(mat,rules)
        if mat is None:
            print("nb:",nb)
            return None
    return mat
def countOn(mat):
    if mat is None:
        return 0
    cpt = 0
    for i in range(len(mat)):
        if "#" in mat[i]:
            cpt += mat[i].count("#")
    return cpt

# inputs and rules
start = [".#.", "..#", "###"]
rules = {2:[],3:[]}
while True:
    inp = input().strip("\n\r ")
    if len(inp) == 0:
        break
    l, r = inp.split(' => ')
    l = l.split('/')
    r = r.split('/')
    rules[len(l)].append((l, r))

mat = multIterate(start,rules,18)
print(countOn(mat))
# for m in mat:
#     print(m)