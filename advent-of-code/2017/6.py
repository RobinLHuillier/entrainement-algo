def iMax(l):
    maxi = -1
    si = 0
    for i in range(len(l)):
        if l[i] > maxi:
            maxi = l[i]
            si = i
    return (si, maxi)

banks = list(map(int, input().split()))
n = len(banks)
print(banks)

seen = []
saveState = []
cycle = 0
nC = 0

while cycle < 2:
    if cycle == 0:
        if banks in seen:
            cycle += 1
            if cycle == 1:
                nC = 0
                saveState = list(banks)
        seen.append(list(banks))
    elif banks == saveState:
        break
    i, q = iMax(banks)
    banks[i] = 0
    i += 1
    while q:
        banks[i%n] += 1
        q -= 1
        i += 1
    nC += 1
    # print(banks)

print(len(seen), nC)