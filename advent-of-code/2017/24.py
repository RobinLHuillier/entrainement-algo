ports = []
while True:
    inp = input().strip("\n\r ")
    if len(inp) == 0:
        break
    a,b = map(int, inp.split("/"))
    ports.append((a,b))
# print(ports)

def available(ports,point):
    possible = []
    for p in ports:
        a,b = p
        if a == point:
            possible.append((a,b))
        elif b == point:
            possible.append((b,a))
    return possible

def copyAndDelete(ports,port):
    nPort = ports[::]
    if port in ports:
        nPort.remove(port)
        return nPort
    nPort.remove((port[1],port[0]))
    return nPort

def backtrack(portsLeft,value,length,currPort):
    possible = available(portsLeft,currPort)
    if possible == []:
        return (value,length)
    bestVal = 0
    bestLength = 0
    for p in possible:
        pLeft = copyAndDelete(portsLeft,p)
        rVal, rLength = backtrack(pLeft,value+p[0]+p[1],length+1,p[1])
        if rLength > bestLength or (rLength == bestLength and rVal > bestVal):
            bestLength = rLength
            bestVal = rVal
    return (bestVal, bestLength)

print(backtrack(ports,0,0,0))