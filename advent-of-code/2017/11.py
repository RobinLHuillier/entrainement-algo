dir = input().split('\n')[0]
dir = dir.split()[0]
dir = dir.split(',')
saveDir = dir

couples = [["n","s",""],
           ["nw","se",""],
           ["sw","ne",""],
           ["nw","se",""],
           ["ne","s","se"],
           ["se","sw","s"],
           ["s","nw","sw"],
           ["sw","n","nw"],
           ["nw","ne","n"],
           ["n","se","ne"]]

def elim(dir, couples):
    dir2 = dir[::]
    while True:
        noChange = True
        for c in couples:
            if c[0] in dir2 and c[1] in dir2:
                dir2.remove(c[0])
                dir2.remove(c[1])
                if len(c[2]) > 0:
                    dir2.append(c[2])
                # print(len(dir))
                noChange = False
        if noChange:
            break
    return len(dir2)

print("part 1:", elim(dir, couples))

print("part 2:")

# recherche dichotomique de la dir la plus intéressante
# une fois trouvée, on itère 100 au dessus et 100 en dessous au cas où

def max3(a,b,c):
    if a > b and a > c:
        return a
    if b > c:
        return b
    return c

def dicho(bas,haut,vBas,vHaut,direc,couples):
    if bas >= haut:
        return (bas, vBas)
    if vBas == 0 and vHaut == 0:
        vBas = elim(direc[:bas],couples)
        vHaut = elim(direc[:haut],couples)
    mid = (haut-bas)//2 + bas
    vMid = elim(direc[:mid],couples)
    gros = max3(vBas,vMid,vHaut)
    if gros == vBas or vBas > vHaut:
        return dicho(bas,mid,vBas,vMid,direc,couples)
    return dicho(mid,haut,vMid,vHaut,direc,couples)

best, val = dicho(1,len(saveDir),0,0,saveDir,couples)
print("recherche dicho:",best,val)
maxi = val
# for i in range(-100,100):
#     res = elim(saveDir[:best+i],couples)
#     if res > maxi:
#         maxi = res
#     print("step", best+i, "/", len(saveDir), ":", res, "(best", maxi, ")")

# on a établi une map des meilleurs val en [-100 : 100]
# maintenant on progresse dans les deux directions
# à partir de 100, on avance de step en step: 
# si le meilleur est 1259, que l'actuel est 1231, 1259-1231=28, on peut avancer de 28 et voir
i = best+100
diff = abs(elim(saveDir[:i],couples) - maxi)
i += diff
while i < len(saveDir):
    res = elim(saveDir[:i],couples)
    print("step:",i,": res",res,"( best",maxi,")")
    if res > maxi:
        maxi = res
    diff = maxi-res
    if diff == 0:
        diff = 1
    i += diff