nbProg = 2000
neighbors = [[] for i in range(nbProg)]

for i in range(nbProg):
    inp = input().split('\n')[0]
    neigh = [int(j) for j in inp.split('>')[1].split(',')]
    neighbors[i] = neigh

def dfs(neighbors,start=0):
    seen = []
    stack = [start]
    while stack:
        curr = stack.pop()
        for n in neighbors[curr]:
            if n not in seen:
                seen.append(n)
                stack.append(n)
    return seen

group = dfs(neighbors)
print("part 1:",len(group))

groups = 1

while group:

    for g in group:
        for f in neighbors[g]:
            if g in neighbors[f]:
                neighbors[f].remove(g)
        neighbors[g] = []
    
    index = -1
    for i in range(len(neighbors)):
        if len(neighbors[i]) > 0:
            index = i
            break
    if index == -1:
        break
    group = dfs(neighbors,index)
    groups += 1

print("part 2:", groups)