sheet = []
checksum = 0
div = 0
while True:
    inp = input()
    if inp[0] == 'a':
        break
    liste = list(map(int,inp.split()))
    sheet.append(liste)
    checksum += max(liste)-min(liste)
    # print(checksum, max(liste), min(liste))
    found = False
    for i in liste:
        if found:
            break
        for k in liste:
            if i == k: continue
            if i%k == 0:
                div += i//k
                found = True
                break

print("checksum:", checksum)
print("divs:",div)