weapon = [(8,4),(10,5),(25,6),(40,7),(74,8)]
# cost, damage
# 1

armor = [(0,0),(13,1),(31,2),(53,3),(75,4),(102,5)]
# cost, armor
# 0-1

ring = [(0,0,0),(0,0,0),(25,1,0),(50,2,0),(100,3,0),(20,0,1),(40,0,2),(80,0,3)]
# cost, damage, armor
# 0-2

m_hp = 100 # 100
m_dm = 0 # 0
m_arm = 0 # 0

b_hp = 109 # 109
b_dm = 8 # 8
b_arm = 2 # 2

def combat(m_stats, b_stats):
    mhp, mdm, marm = m_stats
    bhp, bdm, barm = b_stats
    while True:
        bhp -= mdm - barm
        if bhp <= 0:
            return True
        mhp -= bdm - marm
        if mhp <= 0:
            return False

mini = 700

for w in weapon:
    for a in armor:
        for i in range(len(ring)):
            for j in range(i+1,len(ring)):
                r1 = ring[i]
                r2 = ring[j]
                ddm = w[1] + r1[1] + r2[1]
                darm = a[1] + r1[2] + r2[2]
                if combat((m_hp,m_dm+ddm,m_arm+darm),(b_hp,b_dm,b_arm)):
                    cost = w[0] + a[0] + r1[0] + r2[0]
                    if cost < mini:
                        mini = cost

print("Part 1:", mini)

maxi = 0

for w in weapon:
    for a in armor:
        for i in range(len(ring)):
            for j in range(i+1,len(ring)):
                r1 = ring[i]
                r2 = ring[j]
                ddm = w[1] + r1[1] + r2[1]
                darm = a[1] + r1[2] + r2[2]
                if not combat((m_hp,m_dm+ddm,m_arm+darm),(b_hp,b_dm,b_arm)):
                    cost = w[0] + a[0] + r1[0] + r2[0]
                    if cost > maxi:
                        maxi = cost

print("Part 2:", maxi)