def readInput(path):
    lines = open(path).read().splitlines()
    return list(map(int, lines))

sac = readInput("24")
poidsMax = sum(sac)//3
permut = []
sacPossibles = [[[],[],[],0,0,0]]
for s in sac[::-1]:
    newSacs = []
    for possible in sacPossibles:
        sac1, sac2, sac3, poids1, poids2, poids3 = possible
        if poids1 + s <= poidsMax:
            newSacs.append([sac1+[s],sac2,sac3,poids1+s,poids2,poids3])
        if poids2 + s <= poidsMax:
            newSacs.append([sac1,sac2+[s],sac3,poids1,poids2+s,poids3])
        if poids3 + s <= poidsMax:
            newSacs.append([sac1,sac2,sac3+[s],poids1,poids2,poids3+s])
    sacPossibles = newSacs
    print(s, len(sacPossibles))