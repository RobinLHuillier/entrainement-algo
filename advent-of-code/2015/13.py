from math import inf

happ = {}

while True:
    i = input()
    if len(i) == 0:
        break
    name1 = i.split()[0]
    name2 = i.split()[-1]
    name1 = name1.strip()
    name2 = name2.strip(" .")
    val = int(i.split()[3])
    if i.split()[2] == "lose":
        val *= -1
    if name1 not in happ:
        happ[name1] = {}
    if name2 not in happ[name1]:
        happ[name1][name2] = val

happ["me"] = {}
for h in list(happ.keys())[::]:
    if h == "me":
        continue
    happ[h]["me"] = 0
    happ["me"][h] = 0

def getVal(happ, table, curr):
    if curr == 0:
        return 0
    n1 = table[curr]
    n2 = table[curr-1]
    v = happ[n1][n2] + happ[n2][n1]
    if curr == len(table)-1:
        n2 = table[0]
        v += happ[n1][n2] + happ[n2][n1]
    return v


def bruteforce(happ, table=[None]*len(happ.keys()), curr=0, value=0):
    if None not in table:
        return table[::], value
    
    maxi = -inf
    nTable = table[::]
    cTable = []

    for name in happ.keys():
        if name in table:
            continue
        # print(name, curr)
        nTable[curr] = name
        v = value + getVal(happ, nTable, curr)
        tab, val = bruteforce(happ, nTable, curr+1, v)
        if val > maxi:
            maxi = val
            cTable = tab[::]
    
    return cTable, maxi

table, val = bruteforce(happ)
print(table, val)