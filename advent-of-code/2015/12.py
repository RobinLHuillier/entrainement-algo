i = input().strip(' \r\n')

total = 0
j = 0
while j < len(i):
    if i[j] == "-" or i[j] in "0123456789":
        j2 = j
        while i[j2] in "-0123456789":
            j2 += 1
        # print(i[j:j2])
        total += int(i[j:j2])
        j = j2
    else:
        j += 1

print("Part 1:", total)

while "red" in i:
    u = i.index("red")
    if i[u-2] != ":":
        i = i[:u+1] + i[u+2:]
        continue
    open = 1
    while open > 0 and u >= 0:
        if i[u] == "}":
            open += 1
        elif i[u] == "{":
            open -= 1
        u -= 1
    open = u+1
    u = i.index("red")
    close = 1
    while close > 0 and u < len(i):
        if i[u] == "{":
            close += 1
        elif i[u] == "}":
            close -= 1
        u += 1
    close = u-1
    # print(len(i),open, close, i[open:close+1])
    i = i[:open] + i[close+1:]

total = 0
j = 0
while j < len(i):
    if i[j] == "-" or i[j] in "0123456789":
        j2 = j
        while i[j2] in "-0123456789":
            j2 += 1
        # print(i[j:j2])
        total += int(i[j:j2])
        j = j2
    else:
        j += 1

print("Part 2:", total)