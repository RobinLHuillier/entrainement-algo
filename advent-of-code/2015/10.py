KEY = "1113122113"

def iteration(key):
    i = 1
    prec = key[0]
    num = 1
    s = ""
    while i < len(key):
        if key[i] != prec:
            s += str(num) + prec
            prec = key[i]
            num = 1
        else:
            num += 1
        i += 1
    s += str(num) + prec
    return s

for i in range(50):
    KEY = iteration(KEY)
    print(i, len(KEY))