key = "hepxcrrq"
alpha = "abcdefghijklmnopqrstuvwxyz"

def increase(password):
    i = 7
    word = [alpha.index(l) for l in password]
    while True:
        ind = word[i]
        if ind < 25:
            word[i] += 1
            break
        word[i] = 0
        i -= 1
    return "".join([alpha[i]for i in word])

def require(password):
    if "i" in password or "o" in password or "l" in password:
        return False

    word = [alpha.index(l) for l in password]

    inc = False
    for i in range(5):
        if word[i]+1 == word[i+1] and word[i+1]+1 == word[i+2]:
            inc = True
            break
    if not inc:
        return False

    double = 0
    i = 0
    while i < 7:
        if word[i] == word[i+1]:
            double += 1
            i += 1
        i += 1
    if double < 2:
        return False

    return True
    
def next(password):
    password = increase(password)
    while not require(password):
        password = increase(password)
    return password

key = next(key)
print("Part 1:", key)
key = next(key)
print("Part 2:", key)