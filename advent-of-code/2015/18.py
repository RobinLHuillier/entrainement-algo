sizeX = 100
sizeY = 100

def readInput():
    l = [['.']*(sizeX+2) for i in range(sizeY+2)]
    for i in range(sizeY):
        d = input()
        for j in range(sizeX):
            l[i+1][j+1] = d[j]
    
    for i in (1,sizeX):
        for j in (1,sizeY):
            l[i][j]="#"

    return l

def printScreen(l):
    for i in l:
        print(''.join(i))

def countNeigh(l, i, j):
    c = 0
    for m in (-1,0,1):
        for n in (-1,0,1):
            if m == 0 and n == 0:
                continue
            if l[i+m][j+n] == "#":
                c += 1
    return c

def oneIter(l):
    l2 = [['.']*(sizeX+2) for i in range(sizeY+2)]
    for i in range(1,sizeX+1):
        for j in range(1, sizeY+1):
            c = countNeigh(l, i, j)
            if l[i][j] == "." and c == 3:
                l2[i][j] = "#"
            elif l[i][j] == "#" and (c == 2 or c == 3):
                l2[i][j] = "#"
            else:
                l2[i][j] = "."
            if i in (1, sizeX) and j in (1, sizeY):
                l2[i][j] = "#"
    return l2

def countLights(l):
    c = 0
    for i in range(1, sizeY+1):
        for j in range(1, sizeX+1):
            if l[i][j] == "#":
                c += 1
    return c

def step(l, x):
    print("Initial: ")
    printScreen(l)
    for i in range(x):
        l = oneIter(l)
        print("Step ", i+1)
        printScreen(l)
        print("Lights on: ", countLights(l))

l = readInput()
step(l, 100)