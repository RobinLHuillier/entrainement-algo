def getVal(s,val):
    if not s or len(s) == 0:
        return 1
    if s[0] in "1234567890":
        return int(s)
    if s not in val:
        return s
    return val[s]

def work(val, op):
    res = 0

    if "AND" in op:
        v1, v2 = op.split("AND")
        v1 = getVal(v1,val)
        v2 = getVal(v2,val)
        res = v1&v2

    elif "OR" in op:
        v1, v2 = op.split("OR")
        v1 = getVal(v1,val)
        v2 = getVal(v2,val)
        res = v1|v2

    elif "LSHIFT" in op:
        v1, v2 = op.split("LSHIFT")
        v1 = getVal(v1,val)
        v2 = getVal(v2,val)
        res = (v1<<v2)

    elif "RSHIFT" in op:
        v1, v2 = op.split("RSHIFT")
        v1 = getVal(v1,val)
        v2 = getVal(v2,val)
        res = (v1>>v2)

    elif "NOT" in op:
        v1, v2 = op.split("NOT")
        v2 = getVal(v2,val)
        res = (~v2)&0xffff

    else:
        res = getVal(op, val)
    
    val[rec] = res

val = {}
operations = {}
read = True
inp = 0

while True:
    i = input()
    if len(i) == 0:
        read = False
        break
    op, rec = i.split('->')
    rec = rec.strip(' \n\r')
    operations[rec] = op

def modif(lis, val, operations):
    v1,v2 = lis
    v1 = v1.strip()
    v2 = v2.strip()
    v1 = getVal(v1,val)
    v2 = getVal(v2,val)
    if str(v1)[0] not in "0123456789":
        v1 = calculate(val,v1,operations)
    if str(v2)[0] not in "0123456789":
        v2 = calculate(val,v2,operations)
    return v1, v2

def calculate(val, name, operations):
    # print(name)
    # print(operations[name])

    if name in val:
        return val[name]
    op = operations[name]

    res = 0
    if "NOT" in op:
        v1,v2 = modif(op.split("NOT"), val, operations)
        res = (~v2)&0xffff
    elif "LSHIFT" in op:
        v1,v2 = modif(op.split("LSHIFT"), val, operations)
        res = (v1<<v2)
    elif "RSHIFT" in op:
        v1,v2 = modif(op.split("RSHIFT"), val, operations)
        res = (v1>>v2)
    elif "AND" in op:
        v1,v2 = modif(op.split("AND"), val, operations)
        res = (v1&v2)
    elif "OR" in op:
        v1,v2 = modif(op.split("OR"), val, operations)
        res = (v1|v2)
    else:
        v1,v2 = modif([op, " "], val, operations)
        res = v1
    
    val[name] = res
    return res


print("part 1:", calculate(val, 'a', operations))

b = val['a']

val = {}
val['b'] = b

print("part 2:", calculate(val, 'a', operations))