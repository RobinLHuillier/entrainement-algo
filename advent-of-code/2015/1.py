c = 0
d = 0
for i in input():
    d += 1
    if i == ")":
        c -= 1
    if i == "(":
        c += 1
    if c == -1:
        print("PART 2:", d)
        exit(0)
print("PART 1:",c)