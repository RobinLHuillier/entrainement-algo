ing = []

while True:
    i = input()
    if len(i) == 0:
        break
    cap, d, f, t, cal = map(int,i.split())
    ing.append((cap,d,f,t,cal))

def score(cookie, ing):
    tot = 1
    for i in range(5):
        j = 0
        for k in range(len(cookie)):
            j += ing[k][i]*cookie[k]
        if j < 0:
            return 0
        if i < 4:
            tot *= j
        elif j != 500:
            return 0
    return tot

def brute(ing, limit=100, cookie=[0]*len(ing), curr=0, value=0, quantity=0):
    if quantity == limit or curr >= len(cookie):
        return cookie, value
    
    maxi = 0
    cCook = []

    for i in range(1, limit-quantity+1):
        cook = cookie[::]
        cook[curr] = i
        cook, val = brute(ing, limit, cook, curr+1, score(cook, ing), quantity+i)
        if val > maxi:
            maxi = val
            cCook = cook
    
    return cCook, maxi

cookie, value = brute(ing, 100)
print(cookie, value)