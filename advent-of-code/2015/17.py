cont = []
FILL = 150

while True:
    i = input().strip()
    if len(i) == 0:
        break
    cont.append(int(i))

def backtrack(cont,val,container):
    if val == 0:
        return 1
    if len(cont) == 0 or container == 0:
        return 0

    c = cont[0]
    cnt = cont[1:]
    if c <= val:
        return backtrack(cnt,val-c,container-1) + backtrack(cnt,val,container)
    else:
        return backtrack(cnt,val,container)

print("Part 1:", backtrack(cont, FILL, len(cont)))

def backMin(chose, cont, val):
    # print(chose, cont)
    if val == 0:
        # print(chose)
        return chose
    if len(cont) == 0:
        return [0]*50

    c = cont[0]
    cnt = cont[1:]
    l = [0]*50
    if c <= val:
        l = backMin(chose[::]+[c],cnt,val-c)
    l2 = backMin(chose[::],cnt,val)
    if len(l2) < len(l):
        return l2
    return l

cnt = backMin([], cont, FILL)

print("Minimum set:", len(cnt), cnt)
print("Part 2:", backtrack(cont, FILL, len(cnt)))