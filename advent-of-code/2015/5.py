alpha = "abcdefghijklmnopqrstuvwxyz"

nice = 0

while True:
    i = input()
    if i == '\n' or len(i) == 0:
        break
    i = i.strip('\n\r ')
    print(i)

    double = False
    for j in range(len(i)-1):
        if i.count(i[j:j+2]) > 1:
            double = True
            break
    if not double:
        continue
    rep = False
    for j in range(len(i)-2):
        if i[j] == i[j+2]:
            rep = True
            break
    if not rep:
        continue
    nice += 1


    """
    vow = 0
    for v in "aeiou":
        if v in i:
            vow += i.count(v)
    if vow < 3:
        print("no vow")
        continue
    double = False
    for a in alpha:
        if a+a in i:
            double = True
            break
    if not double:
        print("no double")
        continue
    cont = False
    for j in ["ab", "cd", "pq", "xy"]:
        if j in i:
            cont = True
            break
    if cont:
        print("cont")
        continue
    nice += 1
    """

    
# print("Part 1:", nice)
print("Part 2:", nice)