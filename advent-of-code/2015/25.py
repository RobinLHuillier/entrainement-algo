topleft = 20151125
maxi = 6500
ROW = 2981
COL = 3075

def calculateNext(prec):
    MULT_CONST = 252533
    DIV_CONST = 33554393

    mult = prec * MULT_CONST
    div = mult // DIV_CONST
    rest = mult - (div * DIV_CONST)

    return rest

def showGrid(grid):
    for g in grid[1:]:
        for num in g[1:]:
            s = str(num)
            while len(s) < 10:
                s = ' ' + s
            print(s, end='')
        print()

def getNum(grid, line, col):
    return grid[line][col]

grid = [[0]*(maxi+1) for _ in range(maxi+1)]
line = 1
col = 1
prec = topleft
count = 0
total = maxi * maxi / 2

while line != 1 or col != maxi:
    if line == 1 and col == 1:
        grid[line][col] = topleft
    else:
        next = calculateNext(prec)
        grid[line][col] = next
        prec = next
    if line == 1:
        line = col + 1
        col = 1
    else:
        line -= 1
        col += 1
    count += 1
    if count % 100000 == 0:
        print(f"Processed {100*count/total}%")


print(getNum(grid, ROW, COL))