rules = dict()

while True:
    i = input()
    if len(i) == 0:
        break
    H, C = i.split(' => ')
    if H not in rules:
        rules[H] = [C]
    else:
        rules[H].append(C)
    
mol = input()
allPos = dict()

for h in rules.keys():
    indices = [idx for idx in range(len(mol)) if mol.startswith(h, idx)]
    for c in rules[h]:
        # print(c, indices, mol)
        for i in indices:
            ns = mol[:i] + c + mol[i+len(h):]
            # print("hey: ", h, c, mol[:i], c, mol[i+len(h):])
            allPos[ns] = 1

print(allPos)
print(len(allPos.keys()))