lights = [[0]*1000 for i in range(1000)]

while True:
    i = input()
    if len(i) == 0:
        break
    res = i.split()
    c1 = ""
    c2 = res[-1]
    tog = False
    ton = True
    if i[1] == "u":
        if i[6] == "f":
            ton = False
        c1 = res[2]
    else:
        tog = True
        c1 = res[1]
    x1, y1 = map(int, c1.split(','))
    x2, y2 = map(int, c2.split(','))
    
    print(i, tog, ton)


    for i in range(x1,x2+1):
        for j in range(y1,y2+1):
            if tog:
                lights[i][j] += 2
            elif ton:
                lights[i][j] += 1
            else:
                lights[i][j] = max(lights[i][j]-1,0)

c = 0
for i in lights:
    c += sum(i)

print("Part 2:",c)