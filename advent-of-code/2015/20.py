from math import sqrt

def sumMult(i):
    s = set()
    s.add(i)
    s.add(1)
    for j in range(2, int(sqrt(i+1))+1):
        if i%j == 0:
            s.add(j)
            s.add(i//j)
    return 10*sum(s)

g = 34000000

# i = 1
# res = sumMult(i)
# while res < g:
#     if i % 10000 == 0:
#         print(i, res)
#     i += 1
#     res = sumMult(i)

# print(f"Trouvé: {i}")

def sumMult2(i):
    s = set()
    s.add(i)
    for j in range(1, int(sqrt(i+1))+1):
        if i%j == 0:
            a = j
            b = i//j
            if a <= 50:
                s.add(b)
            if b <= 50:
                s.add(a)
    return 11*sum(s)

i = 1
res = sumMult2(i)
maxi = res
while res < g:
    if i % 10000 == 0:
        print(i, res, maxi)
    i += 1
    res = sumMult2(i)
    maxi = max(maxi, res)

print(f"Trouvé: {i}")

