t = 0
u = 0
v = 0

while True:
    i = input()
    if len(i) == 0:
        break

    k = 0
    l = len(i)
    j = 0
    while j < len(i):
        k += 1
        if i[j] == "\\":
            if i[j+1] == "x":
                j += 4
            else:
                j += 2
        elif i[j] == "\"":
            j += 1
            k -= 1
        else:
            j += 1

    m = 2 + l
    if "\\" in i:
        m += i.count("\\")
    if "\"" in i:
        m += i.count("\"")

    print(i, k, l, m)
    t += k
    u += l
    v += m

print("Part 1:", t, u, u-t)
print("Part 2:", v, u, v-u)