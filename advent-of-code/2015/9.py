import heapq
from math import inf
def dijkstra(n, src, succ):
  dist = [inf] * n
  dist[src] = 0
  prev = [None] * n
  prev[src] = src
  queue = [(0, src)]
  while queue:
    u = heapq.heappop(queue)[1]
    for v, w in succ[u]:
      alt = dist[u] + w
      if dist[v] > alt:
        dist[v] = alt
        prev[v] = u
        heapq.heappush(queue, (dist[v], v))
  return dist, prev

def get_key(dico,val):
    for key, value in dico.items():
        if val == value:
            return key

succ = []
names = {}
n = 0
while True:
    i = input()
    if len(i) == 0:
        break
    villes, dist = i.split("=")
    v1, v2 = villes.split("to")
    v1 = v1.strip()
    v2 = v2.strip()
    dist = int(dist.strip())

    for v in [v1,v2]:
        if v not in names:
            names[v] = n
            n += 1
            succ.append([])   
         
    succ[names[v1]].append((names[v2], dist))
    succ[names[v2]].append((names[v1], dist))

def backtrack(succ, names, visited=[], curr=None, length=0):
    if len(visited) == len(names):
        return visited, length
    mini = 0
    vis = []
    if curr == None:
        for name in names.keys():
            visi, l = backtrack(succ, names, [names[name]], name, 0)
            if l > mini:
                vis = visi
                mini = l
    else:
        for v,d in succ[names[curr]]:
            if v in visited:
                continue
            visi, l = backtrack(succ, names, visited+[v], get_key(names,v), length+d)
            if l > mini:
                vis = visi
                mini = l
    return vis, mini

vis,length = backtrack(succ, names)
print(" -> ".join([get_key(names,val)for val in vis]) + " = " + str(length))