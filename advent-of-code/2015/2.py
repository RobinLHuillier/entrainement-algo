d = 0
r = 0

while True:
    i = input()
    if i == "\n" or len(i) == 0:
        break
    l, w, h = map(int, i.split("x"))
    dim = 2*l*w + 2*w*h + 2*h*l + min(min(w*l,h*l),h*w)
    rib = l*w*h + 2*min(min(w+l, h+l), h+w)
    r += rib
    d += dim
    print(dim,i,rib,min(min(w+l, h+l), h+w),l*w*h)

print("Part 1:", d)
print("Part 2:", r)