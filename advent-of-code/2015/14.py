SPEED = "speed"
TIME = "time"
REST = "rest"
POS = "pos"
TIMEOUT = "timeout"
POINTS = "points"

ree = {}
while True:
    i = input()
    if len(i) == 0:
        break
    l = i.split()
    name = l[0].strip()
    speed = int(l[3].strip())
    time = int(l[6].strip())
    rest = int(l[13].strip())
    # print(name, speed, time, rest)
    ree[name] = {SPEED:speed,TIME:time,REST:rest,POS:0,TIMEOUT:-time,POINTS:0}

def oneSec(ree):
    for r in ree.keys():
        if ree[r][TIMEOUT] < 0:
            ree[r][POS] += ree[r][SPEED]
            ree[r][TIMEOUT] += 1
            if ree[r][TIMEOUT] == 0:
                ree[r][TIMEOUT] = ree[r][REST]
        else:
            ree[r][TIMEOUT] -= 1
            if ree[r][TIMEOUT] == 0:
                ree[r][TIMEOUT] = -ree[r][TIME]
    maxi, re = winning(ree, POS)
    for r in ree.keys():
        if ree[r][POS] == maxi:
            ree[r][POINTS] += 1

def winning(ree, type):
    maxi = 0
    re = []
    for r in ree.keys():
        if ree[r][type] >= maxi and ree[r][type] >= 0:
            maxi = ree[r][type]
            re = r
    return maxi, re

def elapse(ree, time):
    for i in range(time):
        oneSec(ree)

elapse(ree, 2503)
print("Part 1:", winning(ree, POS)[0])
print("Part 2:", winning(ree, POINTS)[0])