hints = {"children": 3, "cats": 7, "samoyeds": 2, "pomeranians": 3, "akitas": 0, "vizslas": 0, "goldfish": 5, "trees": 3, "cars": 2, "perfumes": 1}

while True:
    i = input()
    if len(i) == 0:
        break
    i += ","
    i = i.split()
    num = int(i[1][:-1])

    found = True
    for j in range(3):
        name = i[(j+1)*2][:-1]
        nb = int(i[(j+1)*2 +1][:-1])
        print(num, name, nb)
        if name in "catstrees":
            if hints[name] >= nb:
                found = False
                break
        elif name in "pomeraniansgoldfish":
            if hints[name] <= nb:
                found = False
                break
        else:
            if hints[name] != nb:
                found = False
                break

    if found:
        print("Part 2:",num, i)
        break