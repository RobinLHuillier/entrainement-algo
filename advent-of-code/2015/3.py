x1 = 0
y1 = 0
x2 = 0
y2 = 0
myTurn = False
visited = {}
visited[(0,0)] = 1

path = input().strip('\n\r ')

for p in path:
    if myTurn:
        if p == "^":
            y1 += 1
        if p == "v":
            y1 -= 1
        if p == "<":
            x1 -= 1
        if p == ">":
            x1 += 1
        if (x1,y1) not in visited:
            visited[(x1,y1)] = 1
        else:
            visited[(x1,y1)] += 1
    else:
        if p == "^":
            y2 += 1
        if p == "v":
            y2 -= 1
        if p == "<":
            x2 -= 1
        if p == ">":
            x2 += 1
        if (x2,y2) not in visited:
            visited[(x2,y2)] = 1
        else:
            visited[(x2,y2)] += 1
    myTurn = not myTurn

print("PART 1", len(visited.keys()))