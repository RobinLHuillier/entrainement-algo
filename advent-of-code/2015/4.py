import hashlib

key = "yzbqklnj"

i = 0
while True:
    i += 1
    res = hashlib.md5((key+str(i)).encode())
    if res.hexdigest()[:6] == "000000":
        break

print("Part 1:",i)