insts = [a.strip('\n')for a in open("23").readlines()]

reg = dict()
reg["a"] = 1
reg["b"] = 0

pc = 0

while pc < len(insts):
    inst = insts[pc]
    print(pc, inst, reg["a"], reg["b"])
    if inst[:3] == "hlf":
        reg[inst[4]] //= 2
    elif inst[:3] == "tpl":
        reg[inst[4]] *= 3
    elif inst[:3] == "inc":
        reg[inst[4]] += 1
    elif inst[:3] == "jmp":
        pc += int(inst.split()[1])-1
    elif inst[:3] == "jie" and reg[inst[4]]%2 == 0:
        pc += int(inst.split(',')[1])-1
    elif inst[:3] == "jio" and reg[inst[4]] == 1:
        pc += int(inst.split(',')[1])-1
    pc += 1

print("FIN", reg["a"], reg["b"])