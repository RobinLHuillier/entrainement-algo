xmin = 94
xmax = 151
ymin = -156
ymax = -103

# c'est un all X sans prendre en compte Y
def searchOnlyX(xmin, xmax, x=0):
    calc = x*(x+1)/2
    if xmin > calc:
        return searchOnlyX(xmin, xmax, x+1)
    if calc > xmax:
        return []
    return [x] + searchOnlyX(xmin,xmax,x+1)
print(searchOnlyX(xmin,xmax))
#print(searchOnlyX(xmin, xmax)) 
def searchY(ymin, ymax, y=0, deniv=0):
    calc = y*(y+1)/2
    return calc
#print(searchY(ymin, ymax, 155))
def isItIn(tx,ty,posx,posy,xmin,xmax,ymin,ymax):
    posx += tx
    posy += ty
    if tx > 0 :
        tx -= 1
    ty -= 1
    if posx > xmax or posy < ymin:
        return False
    if posx >= xmin and posy <= ymax:
        return True
    return isItIn(tx,ty,posx,posy,xmin,xmax,ymin,ymax)
#print(isItIn(6,6,0,0,xmin,xmax,ymin,ymax))
maxY = -(ymin+1)
minY = ymin
maxX = xmax
minX = min(searchOnlyX(xmin,xmax))
pos = set()
for i in range(minX,maxX+1):
    for j in range(minY,maxY+1):
        if isItIn(i,j,0,0,xmin,xmax,ymin,ymax):
            pos.add((i,j))
print(len(pos))