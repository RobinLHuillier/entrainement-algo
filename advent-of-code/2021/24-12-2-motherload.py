from monad import launchAutoProgram
from time import time

def importMonad():
    prog = [f.split('\n')[0] for f in open("24-monad")]
    listeProgs = []
    currProg = []
    for l in prog:
        if l == "inp w":
            if len(currProg) > 0 :
                listeProgs.append(currProg[:])
            currProg = [l]
        else:
            currProg.append(l)
    if len(currProg) > 0:
        listeProgs.append(currProg[:])
    return listeProgs
def createProgram(program, progName, file):
    nbInput = 0
    for p in program:
        if p[:3] == "inp":
            nbInput += 1
    inp = ""
    listeInput = []
    for i in range(nbInput):
        if len(inp) > 0:
            inp += ","
        inp += "abcdefghijklmnop"[i]
        listeInput.append("abcdefghijklmnop"[i])
    inp += ",w,x,y,z"
    curInput = 0
    with open(file+".py", 'a') as file:
        file.write("def " + progName + "(" + inp + "):\n")
        lines = []
        for p in program:
            instruction = p[:3]
            variables = [x for x in p[4:].split()]
            line = ""
            if instruction == "inp":
                line = variables[0] + "=" + listeInput[curInput]
                curInput += 1
            if instruction == "add":
                line = variables[0] + "+=" + variables[1]
            if instruction == "mul":
                line = variables[0] + "*=" + variables[1]
            if instruction == "div":
                if variables[1] in "wxyz":
                    line = "if " + variables[1] + "==0:\n       return None\n    "
                line += variables[0] + "=" + variables[0] + "//" + variables[1]
            if instruction == "mod":
                if variables[0] in "wxyz":
                    line = "if " + variables[0] + "<0"
                if variables[1] in "wxyz":    
                    line += " or " + variables[1] + "<=0"
                line += ":\n       return None\n    "
                line += variables[0] + "=" + variables[0] + "%" + variables[1]
            if instruction == "eql":
                line = variables[0] + "=int(" + variables[0] + "==" + variables[1] + ")"
            if len(line) > 0:
                lines.append("    "+line+"\n")
        for i in range(len(lines)-1):
            l1 = lines[i]
            l2 = lines[i+1]
            if l1[5:8] == "*=0" and l2[5:7] =="+=": # remise à zéro d'une variable
                l1 = l1[:5] + "=" + l2[7:]
                lines[i] = l1
                lines[i+1] = ""
            elif l1[-5:-3] == "==" and l2[-5:-3] == "==": # repérer un not
                v1 = l1[4]
                v2 = l1[10]
                v3 = l1[13]
                v4 = l2[4]
                v5 = l2[10]
                v6 = l2[13]
                if v1 == v2 and v2 == v4 and v4 == v5 and v6 == '0':
                    l1 = l1[:11] + "!" + l1[12:]
                    lines[i] = l1
                    lines[i+1] = ""
            elif l1[-4:] == "//1\n": # repérer les divisions par 1
                lines[i] = ""
        for l in lines:
            if len(l) > 0:
                file.write(l)
        file.write("    return [w,x,y,z]\n")
def writeAllProgram(progs):
    fileName = "monad"
    with open(fileName+".py","w") as file:
        file.write("# PROGRAMMES AUTO-GENERES\n")
    for i in range(len(progs)):
        createProgram(progs[i],"monad"+str(i),fileName)
    with open(fileName+".py","a") as file:
        file.write("def launchAutoProgram(num,inp,w,x,y,z):\n")
        for i in range(len(progs)):
            file.write("    if num==" + str(i) + ":\n        return monad" + str(i) + "(inp,w,x,y,z)\n")

## RELANCER REECRIT LES PROGS
progs = importMonad()
writeAllProgram(progs)

magicX = [[4,9],1,8,1,1,7,6,1,1,8,1,1,4,[1,6]]

def hashState(state):
    return ".".join(map(str,state))
def unhashState(state):
    return [int(x)for x in state.split('.')]
def generateAllStates(progNum, previousStates,onlyLast=False):
    t = time()
    print("generating all states for prog n°", progNum)
    k = len(previousStates.keys())
    pc = k//10
    if pc == 0: pc = 1
    print(k, "previous states")
    dico = {}
    cpt = 0
    for prevState in previousStates:
        prevNum = previousStates[prevState][0]
        cpt += 1
        if cpt%pc == 0:
            print(10*cpt//pc,"%")
        state = unhashState(prevState)
        for i in range(1+8*(onlyLast),10):
            newState = launchAutoProgram(progNum, i, state[0], state[1], state[2], state[3])
            # print(newState)
            if newState is not None:
                stateHash = hashState(newState)
                if stateHash not in dico or dico[stateHash][2] > prevNum:
                    dico[stateHash] = (i, prevState, prevNum)
                # print(dico[stateHash],stateHash)
    print("finished in", str(int(100*(time()-t))/100) + " secondes")
    return dico

chiffres = [{}for i in range(14)]

def writeAllThis(i):
    first = True
    with open("monadResult"+str(i)+".txt","w") as file:
        for c in chiffres[i].keys():
            num, prevState = chiffres[i][c]
            if first:
                first = False
            else:
                file.write("\n")
            file.write(c+":"+str(num)+"/"+prevState)
def readAllThis(i):
    with open("monadResult"+str(i)+".txt","r") as file:
        line = file.readline()
        while line:
            l = line.split('\n')[0]
            hashState, other = l.split(':')
            num, prevState = other.split('/')
            chiffres[i][hashState] = [num,prevState]
            line = file.readline()


t2 = time()

#### POUR LIRE L'INPUT S'IL EST ENREGISTRE AUPARAVANT
# for i in range(6):
#     readAllThis(i)
#     print(i,len(chiffres[i].keys()))

### RELANCER ECRASERAIT LES FICHIERS ATTENTION
for i in range(14):
    chiffres[i] = generateAllStates(i,chiffres[i-1] if i>0 else {"0.0.0.0":[1]})
    # writeAllThis(i)
    print(len(chiffres[i].keys()))
print(time()-t2)

possibles = []

def recreatePossibles(curStr, pos, newHash):
    if pos == -1:
        possibles.append(curStr)
        return
    num, prevState, prevNum = chiffres[pos][newHash]
    # print(curStr, num, prevState)
    recreatePossibles(str(num)+curStr, pos-1, prevState)

for c in chiffres[13].keys():
    a = c.split('.')
    if int(a[-1]) == 0:
        num, prevState, prevNum = chiffres[13][c]
        recreatePossibles(str(num), 12, prevState)

print("possibles")
for p in possibles:
    print(p)
