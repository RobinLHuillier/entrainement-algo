from copy import deepcopy

def loadInput():
    lines = [f.split('\n')[0]for f in open("25")]
    seaFloor = []
    for l in lines:
        seaFloor.append([c for c in l])
    return seaFloor

def affiche(mat):
    for i in range(len(mat[0])):
        print("-",end="")
    print("")
    for m in mat:
        for x in m:
            print(x,end="")
        print("")
    for i in range(len(mat[0])):
        print("-",end="")
    print("")

def step(mat):
    for c in ">v":
        newMat = deepcopy(mat)
        if c == ">":
            x,y = 0,1
        else:
            x,y = 1,0
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                if mat[i][j]==c:
                    nX = i+x
                    nY = j+y
                    if nX == len(mat):
                        nX = 0
                    if nY == len(mat[0]):
                        nY = 0
                    if mat[nX][nY] == ".":
                        newMat[nX][nY] = c
                        newMat[i][j] = "."
        mat = newMat
    return mat

def stepUntilStop(mat):
    nb = 0
    while True:
        nb += 1
        if nb%100 == 0:
            print("step",nb)
        mat2 = step(mat)
        if mat2 == mat:
            return (nb, mat2)
        mat = mat2

seaFloor = loadInput()
affiche(seaFloor)
nb,seaFloor=stepUntilStop(seaFloor)
affiche(seaFloor)
print(nb)