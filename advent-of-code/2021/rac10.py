lines=[f+" "for f in open("10")]
corres={"[":"]","(":")","<":">","{":"}"}
valeurs={"(":1,"[":2,"{":3,"<":4,")":3,"]":57,"}":1197,">":25137}
somme=0
scores=[]
for l in lines:
    stack = []
    for c in l:
        if c in "[({<":
            stack.append(c)
        else:
            prec = stack.pop(-1)
            if corres[prec] != c:
                if c in "])}>":
                    somme += valeurs[c]
                    break
                else:
                    score = 0
                    stack.append(prec)
                    while len(stack) > 0:
                        prec = stack.pop(-1)
                        score *= 5
                        score += valeurs[prec]
                    scores.append(score)
                    break
print(somme==392139,sorted(scores)[int((len(scores))/2)]==4001832844)

#1119