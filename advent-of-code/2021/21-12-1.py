p1Start = 3
p2Start = 7

def naif(pos1,pos2,pt1=0,pt2=0,die=1):
    pos1 = (pos1+3*die+3)%10
    pt1 += pos1 +1
    die += 3
    if pt1 >= 1000:
        return pt2*(die-1)
    pos2 = (pos2+3*die+3)%10
    pt2 += pos2 +1
    die += 3
    if pt2 >= 1000:
        return pt1*(die-1)
    return naif(pos1,pos2,pt1,pt2,die)

print("Q1")
print(naif(p1Start-1,p2Start-1))

moves = [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)] # (outcome, nbOfOutcome)
wins = [0,0]  # (p1,p2)

scores1 = [[i,0,[0 for j in range(10)]]for i in range(21)] # pour score 4 : sc, nb, [pos 0,1,...,9 contenant nb]
scores2 = [[i,0,[0 for j in range(10)]]for i in range(21)]

def advanceScore(score):
    win = 0
    newScore1 = [[i,0,[0 for j in range(10)]]for i in range(21)]
    for m in moves:
        out, nbOut = m
        for s in score:
            pt, nbPt, pos = s
            for i in range(10):
                p = pos[i]
                if p>0:
                    newPos = i + 1 + out
                    if newPos > 10:
                        newPos -= 10
                    nb = nbOut * p
                    sc = pt + newPos
                    if sc >= 21:
                        win += nb
                    else:
                        newScore1[sc][1] += nb
                        newScore1[sc][2][newPos-1] += nb
    return (win, newScore1)

def calc(s1,s2):
    print(sum(x[1]for x in s1),sum(x[1]for x in s2))
    res = advanceScore(s1)
    wins[0] += res[0] * sum(x[1]for x in s2)
    s1 = res[1]
    if sum(x[1]for x in s1)==0:
        return
    res = advanceScore(s2)
    wins[1] += res[0] * sum(x[1]for x in s1)
    s2 = res[1]
    if sum(x[1]for x in s2)==0:
        return
    calc(s1,s2)

scores1[0][2][p1Start-1] = 1
scores1[0][1] = 1
scores2[0][2][p2Start-1] = 1
scores2[0][1] = 1
calc(scores1,scores2)
print(max(wins))