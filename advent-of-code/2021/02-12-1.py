depth = 0
forward = 0
aim = 0
with open("2") as file:
    for line in file:
        mots = line.split()
        if mots[0] == "forward":
            forward += int(mots[1])
            depth += aim*int(mots[1])
        elif mots[0] == "down":
            aim += int(mots[1])
        elif mots[0] == "up":
            aim -= int(mots[1])
print("depth:", depth)
print("forward:", forward)
print("aim:", aim)
print("mul:", depth*forward)

#raccourci
l=map(lambda s:(s[0],int(s[1])),[l.split()for l in open("2")])
print()

#Johann
# Partie 1
#print(sum(x*(d=='forward') for d, x in entries) * sum(x*(d == 'down')-x*(d == 'up') for d, x in entries))

# Partie 2
#282
entries = [(line.split()[0], int(line.split()[1])) for line in open("2")]
print(sum(x for d, x in entries if d == 'forward') * sum(entries[i][1] * sum(x if d == 'down' else -x if d == 'up' else 0 for d, x in entries[:i]) for i in range(len(entries)) if entries[i][0] == 'forward'))
#254
f='forward'
e=[(l[0],(1-2*(l[0]=='up'))*int(l[1]))for l in[l.split()for l in open("2")]]
print(  sum(x*(d==f) for d, x in e) * 
        sum(e[i][1] * (e[i][0]==f) *
            sum(x*(d!=f)for d, x in e[:i])
                for i in range(len(e))))
#201
f='forward'
e=[(int(l[1])*(l[0]==f),(1-2*(l[0]=='up'))*int(l[1])*(l[0]!=f))for l in[l.split()for l in open("2")]]
print(  sum(x[0]for x in e)*
        sum(e[i][0]*
            sum(x[1]for x in e[:i])for i in range(len(e))))

#182
print((c:=0,d:=0,g:=0,[(c:=c+x[1],d:=d+x[0],g:=g+x[0]*c,d*g)for x in[(int(l[1])*(l[0]=='forward'),(1-2*(l[0]=='up'))*int(l[1])*(l[0]!='forward'))for l in[l.split()for l in open("2")]]][-1][3])[3])