from copy import deepcopy
import sys

matDepart = [[[1,8],[2,8]],[[1,4],[1,6]],[[2,2],[2,4]],[[1,2],[2,6]]]
### partie 1: 
# input : [[[1,8],[2,8]],[[1,4],[1,6]],[[2,2],[2,4]],[[1,2],[2,6]]]
# exemple : [[[2,2],[2,8]],[[1,2],[1,6]],[[1,4],[2,6]],[[2,4],[1,8]]]

def destinationFinale(mat, num, pos=None):
    i = 2
    j = 2*(num+1)
    if [i,j] in mat[num]:
        if pos == [i,j] or pos == [i-1,j]:
            return "yesDaddy"
        if [i-1,j] in mat[num]:
            return None
        return [i-1,j]
    return [i,j]

def coutDeplacement(num, depart, arrivee):
    if depart[0] != 0 and arrivee[0] != 0 and depart[1] != arrivee[1]:  # on fait un ^ > v
        return (depart[0] + arrivee[0] + abs(depart[1]-arrivee[1]))*10**num
    return (abs(depart[0]-arrivee[0]) + abs(depart[1]-arrivee[1]))*10**num

def creerListeChemin(depart, arrivee):
    chemin = []
    if depart[0] > 0:
        for i in range(depart[0],0,-1):
            chemin.append([i,depart[1]])
    step = 2*(arrivee[1]>depart[1])-1
    for i in range(depart[1],arrivee[1]+step,step):
        chemin.append([0,i])
    if arrivee[1] > 0:
        for i in range(1,arrivee[0]+1):
            chemin.append([i,arrivee[1]])
    if len(chemin) > 0 and [depart[0],depart[1]] in chemin: # on retire la case depart, pas la peine
        chemin.remove([depart[0],depart[1]])
    return chemin

def deplacementPossible(mat, depart, arrivee, chemin=[]):
    if len(chemin) == 0:
        chemin = creerListeChemin(depart, arrivee)
    for c in chemin:
        for m in mat:
            if c in m:
                return False
    return True

def caseNonOccupee(mat, pos):
    for m in mat:
        if pos in m:
            return False
    return True

def listeDeplacementPossible(mat, num, depart):
    listeDep = []
    finale = destinationFinale(mat,num)
    if deplacementPossible(mat, depart, finale):
        listeDep.append(finale)
    elif depart[0] == 0: # on ne pouvait aller qu'à la dest finale
        return []
    if depart[0] != 0:
        # regarder case au dessus en 0:
        chemin = creerListeChemin(depart, [0,depart[1]])
        if not deplacementPossible(mat,depart,[0,depart[1]],chemin):
            return [] # on peut même pas sortir d'ici
        # regarder jusqu'où on peut aller à gauche
        i = depart[1]-1
        arretPotentiel = [0,i]
        while caseNonOccupee(mat, arretPotentiel) and i >= 0:
            listeDep.append(arretPotentiel)
            i -= 2
            if i==-1:
                i=0
            arretPotentiel=[0,i]
        # regarder jusqu'où on peut aller à droite
        i = depart[1]+1
        arretPotentiel = [0,i]
        while caseNonOccupee(mat, arretPotentiel) and i < 11:
            listeDep.append(arretPotentiel)
            i += 2
            if i==11:
                i=10
            arretPotentiel=[0,i]
    return listeDep

def deplacer(mat,num,depart,arrivee):
    mat2 = deepcopy(mat)
    cout = coutDeplacement(num,depart,arrivee)
    for i in range(2):
        if mat[num][i] == depart:
            mat2[num][i] = arrivee[:]
    return (mat2, cout)

coutGlobal = sys.maxsize

def backtrack(mat, cout=0, dep=[]):
    global coutGlobal
    if cout > coutGlobal:
        #print("dépasse l'heuristique")
        return sys.maxsize
    deplacable = []
    for num in range(4):
        for pos in mat[num]:
            finale = destinationFinale(mat,num,pos)
            if finale != "yesDaddy":
                if deplacementPossible(mat,pos,finale):
                    newMat, cost = deplacer(mat,num,pos,finale)
                    return backtrack(newMat, cout+cost, dep+[[num,cost,pos,finale]])
                deplacable.append([num, pos])
    if len(deplacable) == 0:
        #print("terminé, cout: " + str(cout))
        if cout < coutGlobal:
            coutGlobal = cout
            print("nouveau cout min trouvé: " + str(cout))
            print(mat)
            print(dep)
        return cout
    coutMin = sys.maxsize
    for d in deplacable:
        num, pos = d
        # print("---------")
        # print(pos, num)
        # print(destinationFinale(mat,num,pos))
        if destinationFinale(mat,num,pos) != "yesDaddy":
            listeDep = listeDeplacementPossible(mat,num,pos)
            # print(listeDep)
            if len(listeDep) > 0:
                for l in listeDep:
                    newMat, cost = deplacer(mat,num,pos,l)
                    coutMin = min(coutMin, backtrack(newMat, cout+cost, dep+[[num,cost,pos,l]]))
    return coutMin

print(backtrack(matDepart))