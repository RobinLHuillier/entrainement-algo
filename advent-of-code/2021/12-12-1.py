lines = [f.split('-') for f in [f.split('\n')[0] for f in open("12")]]
dicoPath = {}
dicoVisited = {}
for l in lines:
    a,b = l
    if a not in dicoPath:
        dicoPath[a] = []
    if b not in dicoPath:
        dicoPath[b] = []
    dicoPath[a].append(b)
    dicoPath[b].append(a)
    dicoVisited[a]=False
    dicoVisited[b]=False

def isBigCave(cave):
    return cave==cave.upper()

cpt = 0
allPath = []

def constructAllPath(cur, visited, doubleVisit=False):
    global cpt,allPath
    if cur in visited and not isBigCave(cur) and (doubleVisit or cur=="start"):
        return None
    if cur in visited and not isBigCave(cur):
        doubleVisit=True
    visited.append(cur)
    if cur == "end":
        cpt += 1
        if cpt%1000 == 0: # or cpt>119700:
            print(cpt)
        #print("end")
        #print(visited)
        allPath += [visited]
        return None
    for path in dicoPath[cur]:
        constructAllPath(path,visited[::],doubleVisit)

constructAllPath("start",[])

print("FLATTENING")
#print(res)
flat_list = []
def flatten_list(list_of_lists):
    for item in list_of_lists:
        if type(item) == list:
            flatten_list(item)
        else:
            flat_list.append(item)
    
    return flat_list
#flatten_list(res)
#print(flat_list)

print("BUILDING PATHS")
def buildPath(listPath):
    k = len(listPath)//100
    cpt = 0
    paths = []
    curPath = []
    while len(listPath)>0:
        if cpt%k == 0:
            print(str(cpt/k),"%")
        k += 1
        cur = listPath.pop(0)
        curPath.append(cur)
        if cur == "end":
            paths.append(list(curPath.copy()))
            curPath = []
    return paths
#allPath = buildPath(flat_list)


print("--Q1 nb chemin:")
print(len(allPath))
print("----------------")