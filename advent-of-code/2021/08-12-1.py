from itertools import permutations

#contient le codage des chiffres de 0 à 9:
#indice 0 = codage du chiffre 0
#[1,0,0,0,0,0,1] = les cadrans "a" et "g" sont allumés, les autres éteints
dico = [[1,1,1,0,1,1,1],[0,0,1,0,0,1,0],[1,0,1,1,1,0,1],[1,0,1,1,0,1,1],[0,1,1,1,0,1,0],[1,1,0,1,0,1,1],[1,1,0,1,1,1,1], [1,0,1,0,0,1,0],[1,1,1,1,1,1,1],[1,1,1,1,0,1,1]]

#prend en paramètre une liste de strings ["abf", "degc"...] et un booléen pour trier ou non
#renvoie la liste des codes correspondant aux strings
# ex: ["abd"] -> [[1,1,0,1,0,0,0]]
#si sort = true, trie la liste de string par longueur croissante
#(ça nous sera utile pour accélérer la recherche des permutations, afin d'éliminer des candidats rapidement)
def miseEnTab(listeStr, sort=False):
    if sort:
        listeStr = sorted(listeStr, key=len)
    tab = []
    alphabet = "abcdefg"
    for string in listeStr:
        code = [0]*7
        for lettre in string:
            code[alphabet.find(lettre)] = 1
        tab.append(code)
    return tab

#prend en paramètre le code d'un nombre et une permutation
#renvoie le code transformé selon la permutation
#ex: nombre = [1,0,1,0], permutation = [1,3,0,2]
#    renvoie [0,1,1,0]   (la permutation donne l'ordre des positions du code)
def transformerPermutation(nombre, permutation):
    resultat = [0]*7
    for i in range(7):
        if nombre[i] == 1:
            resultat[permutation[i]] = 1
    return resultat

#prend en paramètre une liste de 4 codes, et une permutation
#renvoie un nombre entre 0 et 9999
#transforme la liste de codes par la permutation, puis cherche le nombre correspondant dans le dictionnaire
def calculFourDigit(listDigits, permutation):
    digits = []
    for nombre in listDigits:
        nbTransforme = transformerPermutation(nombre, permutation)
        for i in range(10):
            if dico[i] == nbTransforme:
                digits.append(i)
                break
    return digits[0]*1000 + digits[1]*100 + digits[2]*10 + digits[3]

#on stocke les inputs dans indices et fourDigits, réorganisés et recodés
indices = []
fourDigits = []
for lines in open("8"):
    line = lines.split('|')
    indices.append(miseEnTab(line[0].split(), True))
    fourDigits.append(miseEnTab(line[1].split()))

#pour chaque ligne d'indices, on crée une liste de permutation
#  on en cherche une qui transforme les indices en nombre existant dans le dictionnaire
#  une fois trouvée, on calcule le nombre affiché (fourDigits), et on l'ajoute à la somme
somme = 0
for i in range(len(indices)):
    print("calcul de la ligne " + str(i) + "...")
    listPermutations = list(permutations([0,1,2,3,4,5,6]))
    for permutation in listPermutations:
        possible = True
        for nombre in indices[i]:
            nbTransforme = transformerPermutation(nombre,permutation)
            if nbTransforme not in dico:
                possible = False
                break
        if possible:
            fourDigit = calculFourDigit(fourDigits[i],permutation)
            somme += fourDigit
            print("trouvé: " + str(fourDigit) + " , somme : " + str(somme))
            break