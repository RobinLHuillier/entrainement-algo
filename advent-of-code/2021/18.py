import math
nombres = [f.split('\n')[0] for f in open("18")]

nums = "0123456789"

def readInput(inp):
    if len(inp)==1:
        return int(inp)
    nbOuvrant = 1
    nbFermant = 0
    i = 0
    mil = -1
    while nbOuvrant != nbFermant:
        i += 1
        if inp[i] == "[":
            nbOuvrant +=1
        if inp[i] == "]":
            nbFermant +=1
        if inp[i] == "," and nbOuvrant-1 == nbFermant:
            mil = i
    return [readInput(inp[1:mil]),readInput(inp[mil+1:i])]

def getCurrentContentEnd(inp,deb):
    i = deb+1
    nbOuvrant = 1
    nbFermant = 0
    while nbOuvrant != nbFermant:
        if inp[i] == "[":
            nbOuvrant +=1
        if inp[i] == "]":
            nbFermant +=1
        i += 1
    return i

def getPosFourNest(inp):
    nbOuvrant = 0
    nbFermant = 0
    for i in range(len(inp)):
        if inp[i] == "[":
            nbOuvrant +=1
        if inp[i] == "]":
            nbFermant +=1
        if nbOuvrant - nbFermant > 4 and inp[i] in "0123456789":
            ind = i-1
            while inp[i] != "]":
                if inp[i] == "[":
                    ind = i
                i += 1
            return ind
    return None

def getNumberToTheLeft(inp,fin):
    while fin > 0:
        if inp[fin] in nums:
            if inp[fin-1] in nums:
                return fin-1
            return fin
        fin -= 1
    return None

def getNumberToTheRight(inp,deb):
    while deb < len(inp):
        if inp[deb] in nums:
            return deb
        deb += 1
    return None

def getLeftNumber(inp,deb):
    while inp[deb] not in nums:
        deb += 1
    if inp[deb+1] in nums:
        return int(inp[deb:deb+2])
    return int(inp[deb:deb+1])

def getRightNumber(inp,deb):
    while inp[deb] != ",":
        deb += 1
    deb += 1
    if inp[deb+1] in nums:
        return int(inp[deb:deb+2])
    return int(inp[deb:deb+1])

def getCurrentNumber(inp,deb):
    if inp[deb+1] in nums:
        return int(inp[deb:deb+2])
    return int(inp[deb:deb+1])

def getPosToSplit(inp):
    for i in range(len(inp)):
        if inp[i] in nums and inp[i+1] in nums:
            return i
    return None

def explode(inp):
    debExplode = getPosFourNest(inp)
    finExplode = getCurrentContentEnd(inp, debExplode)
    b = getRightNumber(inp, debExplode)
    a = getLeftNumber(inp, debExplode)
    posLeft = getNumberToTheLeft(inp, debExplode)
    posRight = getNumberToTheRight(inp, finExplode)
    if posLeft is None:
        debut = inp[:debExplode] + "0"
    else:
        debut = inp[:posLeft]
        x = getCurrentNumber(inp,posLeft)
        debut += str(x+a)
        if x < 10:
            debut += inp[posLeft+1:debExplode]
        else:
            debut += inp[posLeft+2:debExplode]
        if debut[-1] == "[":
            debut += "0"
    if posRight is None:
        fin = "0" + inp[finExplode:]
    else:
        fin = inp[finExplode:posRight]
        x = getCurrentNumber(inp,posRight)
        fin += str(x+b)
        if x < 10:
            fin += inp[posRight+1:]
        else:
            fin += inp[posRight+2:]
        if fin[0] == "]":
            fin = "0" + fin
    return debut + fin

def split(inp):
    x = getPosToSplit(inp)
    n = getCurrentNumber(inp,x)
    a = n//2
    b = int(math.ceil(n/2))
    newTab = "[" + str(a) + "," + str(b) + "]"
    return inp[:x] + newTab + inp[x+2:]

def addition(inp1, inp2):
    return "[" + inp1 + "," + inp2 + "]"

def magnitude(inp):
    if not isinstance(inp,list):
        return inp
    return 3*(magnitude(inp[0])) + 2*(magnitude(inp[1]))

def reduction(inp):
    while getPosFourNest(inp) is not None or getPosToSplit(inp) is not None:
        if getPosFourNest(inp) is not None:
            inp = explode(inp)
        else:
            inp = split(inp)
    return inp

## Partie 1

print("Question 1:")
for i in range(len(nombres)):
    if i == 0:
        testStr = nombres[0]
    else:
        testStr = addition(testStr, nombres[i])
    testStr = reduction(testStr)
print(magnitude(readInput(testStr)))

## Partie 2

print("Question 2:")
maxi = 0
for i in nombres:
    for j in nombres:
        if i != j:
            mag = magnitude(readInput(reduction(addition(i,j))))
            if mag > maxi:
                maxi = mag
print(maxi)