from time import time

t = time()

def genererPermuts():
    perm = []
    permut = [(1,2,3),(3,1,2),(2,3,1),(-1,3,2),(-3,2,1),(-2,1,3)]
    for p in permut:
        perm.append(p)
        x,y,z = p
        perm.append((x,z,-y))
        perm.append((x,-y,-z))
        perm.append((x,-z,y))
    return perm

permut = genererPermuts()

lines = [f.split('\n')[0]for f in open("19")]
scanners = []
for i in range(len(lines)):
    if len(lines[i]) == 0:
        scanners.append(scan)
    elif lines[i][1] == "-":
        scan = []
    else:
        x,y,z = [int(j)for j in lines[i].split(',')]
        scan.append((x,y,z))

def generateAllPermut(probe):
    perm = []
    for p in permut:
        cons = []
        for x in p:
            if x >= 0:
                cons.append(probe[x-1])
            else:
                cons.append(-probe[(-x)-1])
        perm.append(cons)
    return perm

def expandScannersPermut():
    for s in range(len(scanners)):
        for i in range(len(scanners[s])):
            scanners[s][i] = generateAllPermut(scanners[s][i])

expandScannersPermut()

def checkFacingTwoScanners(s1,s2,f1,f2):
    probes1 = [x[f1] for x in scanners[s1]]
    probes2 = [x[f2] for x in scanners[s2]]
    distances = []
    countDistances = []
    for p1 in probes1:
        x,y,z = p1
        for p2 in probes2:
            a,b,c = p2
            dist = (x-a,y-b,z-c)
            if dist not in distances:
                distances.append(dist)
                countDistances.append(1)
            else:
                i = distances.index(dist)
                countDistances[i] += 1
                if countDistances[i] >= 12:
                    return distances[i]
    return None

# coupler le tout
correctFacing = [0 for i in range(len(scanners))]
dist = [None for i in range(len(scanners))]
dist[0] = (0,0,0)
paire = [0 for i in range(len(scanners))]
paire[0] = 1
probes = [(p[0][0],p[0][1],p[0][2]) for p in scanners[0]]

while sum(paire) != len(scanners):
    for scanToCompare in range(len(scanners)):
        if paire[scanToCompare] == 0:
            print(len(probes),scanToCompare, sum(paire))
            for s in range(len(scanners)):
                if paire[s] == 1:
                    for i in range(24):
                        res = checkFacingTwoScanners(s,scanToCompare,correctFacing[s],i)
                        if res is not None:
                            dist[scanToCompare] = res
                            correctFacing[scanToCompare] = i
                            break
                    if dist[scanToCompare] is not None:
                        x,y,z = dist[scanToCompare]
                        a,b,c = dist[s]
                        dist[scanToCompare] = (x+a,y+b,z+c)
                        paire[scanToCompare] = 1
                        print("scanner " + str(scanToCompare) + " lié au scanner " + str(s))
                        print("orientation: " + str(permut[correctFacing[scanToCompare]]) + "  / position absolue: " + str(dist[scanToCompare]))
                        # compter les probes
                        s1 = scanToCompare
                        f1 = correctFacing[s1]
                        d1 = dist[s1]
                        for p in scanners[s1]:
                            x,y,z = p[f1]
                            x += d1[0]
                            y += d1[1]
                            z += d1[2]
                            if (x,y,z) not in probes:
                                probes.append((x,y,z))
                        break

print("nombre de probes:", len(probes))
print(time()-t)

def manhattan(p1, p2):
    a,b,c = p1
    x,y,z = p2
    return abs(a-x)+abs(b-y)+abs(c-z)

maxDist = 0
for d1 in dist:
    for d2 in dist:
        m = manhattan(d1,d2)
        if m > maxDist:
            maxDist = m
print(maxDist)

# ex = [f.split('\n')[0]for f in open("19-3")]
# probeEx = [f.split(',')for f in ex]
# probeEx = [(int(f[0]),int(f[1]),int(f[2]))for f in probeEx]

# erreur = []
# for p in probeEx:
#     if p not in probes:
#         print("erreur: ",p)
#         erreur.append(p)


# scanner 1 : pairé avec scanner 16. Orientation : -z, x, -y. Position absolue : (-186, -2456, -2380).
# scanner 2 : pairé avec scanner 1. Orientation : -x, -z, -y. Position absolue : (-139, -3642, -2490).
# scanner 3 : pairé avec scanner 13. Orientation : -y, -x, -z. Position absolue : (-116, 2406, -2569).
# scanner 4 : pairé avec scanner 18. Orientation : x, z, -y. Position absolue : (1173, 1146, -2550).
# scanner 5 : pairé avec scanner 0. Orientation : -y, x, z. Position absolue : (-108, 1136, -57).
# scanner 6 : pairé avec scanner 27. Orientation : x, -y, -z. Position absolue : (-2464, 3583, -2543).
# scanner 7 : pairé avec scanner 4. Orientation : -x, y, -z. Position absolue : (2300, 1212, -2377).
# scanner 8 : pairé avec scanner 30. Orientation : z, x, y. Position absolue : (1098, 3559, -3605).
# scanner 9 : pairé avec scanner 35. Orientation : -x, -y, z. Position absolue : (-1351, 4756, -3766).
# scanner 10 : pairé avec scanner 3. Orientation : -z, -y, -x. Position absolue : (-67, 2257, -3688).
# scanner 11 : pairé avec scanner 0. Orientation : z, -x, -y. Position absolue : (-104, -60, -1191).
# scanner 12 : pairé avec scanner 18. Orientation : y, z, x. Position absolue : (1094, -1335, -2464).
# scanner 13 : pairé avec scanner 4. Orientation : y, x, -z. Position absolue : (1128, 2365, -2443).
# scanner 14 : pairé avec scanner 3. Orientation : -x, z, y. Position absolue : (-128, 2387, -1294).
# scanner 15 : pairé avec scanner 3. Orientation : -z, -x, y. Position absolue : (-1331, 2414, -2418).
# scanner 16 : pairé avec scanner 12. Orientation : y, -z, -x. Position absolue : (1113, -2504, -2563).
# scanner 17 : pairé avec scanner 12. Orientation : y, -x, z. Position absolue : (1047, -1190, -3745).
# scanner 18 : pairé avec scanner 20. Orientation : x, -z, y. Position absolue : (1099, -114, -2497).
# scanner 19 : pairé avec scanner 32. Orientation : -z, y, x. Position absolue : (-2556, 4, -2493).
# scanner 20 : pairé avec scanner 11. Orientation : -y, z, -x. Position absolue : (-28, -99, -2459).
# scanner 21 : pairé avec scanner 29. Orientation : -y, -z, x. Position absolue : (-3713, 5845, -3627).
# scanner 22 : pairé avec scanner 4. Orientation : z, y, -x. Position absolue : (1210, 1159, -1260).
# scanner 23 : pairé avec scanner 19. Orientation : z, -y, x. Position absolue : (-3740, -8, -2546).
# scanner 24 : pairé avec scanner 19. Orientation : z, y, -x. Position absolue : (-2562, -1288, -2553).
# scanner 25 : pairé avec scanner 9. Orientation : x, z, -y. Position absolue : (-2525, 4655, -3628).
# scanner 26 : pairé avec scanner 7. Orientation : -y, -x, -z. Position absolue : (2233, 2293, -2383).
# scanner 27 : pairé avec scanner 15. Orientation : -x, z, y. Position absolue : (-1290, 3504, -2383).
# scanner 28 : pairé avec scanner 20. Orientation : y, -x, z. Position absolue : (-55, 1183, -2527).
# scanner 29 : pairé avec scanner 25. Orientation : -z, y, x. Position absolue : (-2389, 5879, -3631).
# scanner 30 : pairé avec scanner 13. Orientation : x, z, -y. Position absolue : (1043, 2368, -3606).
# scanner 31 : pairé avec scanner 5. Orientation : -z, -y, -x. Position absolue : (-101, 2413, -146).
# scanner 32 : pairé avec scanner 20. Orientation : x, z, -y. Position absolue : (-1219, -79, -2541).
# scanner 33 : pairé avec scanner 20. Orientation : -z, -x, y. Position absolue : (-158, -1274, -2455).
# scanner 34 : pairé avec scanner 16. Orientation : -x, y, -z. Position absolue : (1199, -3727, -2439).
# scanner 35 : pairé avec scanner 27. Orientation : z, x, y. Position absolue : (-1191, 3565, -3682).

# result 1 : 447
# result 2 : 15672

# temps passé :  669.5727827548981 s
