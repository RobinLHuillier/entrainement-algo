lines = [f.split('\n')[0] for f in open("22")]
reboot = []
for f in lines :
    cmd,coor = f.split()
    x,y,z=coor.split(',')
    x1,x2 = map(int,x[2:].split('..'))
    y1,y2 = map(int,y[2:].split('..'))
    z1,z2 = map(int,z[2:].split('..'))
    reboot.append([+(cmd=="on"),[[x1,y1,z1],[x2,y2,z2]]])

cubes = [[[0]*101 for i in range(101)] for j in range(101)]

for c in reboot:
    cmd = c[0]
    x1,y1,z1 = c[1][0]
    x2,y2,z2 = c[1][1]
    depasse = False
    for g in [x1,y1,z1,x2,y2,z2]:
        if g < -50 or g > 50:
            depasse = True
    if not depasse:
        for a in range(x1,x2+1):
            for b in range(y1,y2+1):
                for c in range(z1,z2+1):
                    cubes[a+50][b+50][c+50] = cmd
nb = 0
for a in range(101):
    for b in range(101):
        for c in range(101):
            nb += cubes[a][b][c]
print("Q1")
print(nb)
print("---------")
saveNb = nb

def splitMyShitUp(ev1,ev2,av1,av2):
    if ev1 <= av1 and ev2 >= av2:
        return [[av1,av2]]
    if ev1 <= av1:
        return [[av1,ev2],[ev2+1,av2]]
    elif ev2 >= av2:
        return [[av1,ev1-1],[ev1,av2]]
    else:
        return [[av1,ev1-1],[ev1,ev2],[ev2+1,av2]]

def eteintMoi(ev1,ev2,av1,av2,verbose=False):
    if verbose:
        print(ev1,ev2,av1,av2)
    if ev1 <= av1 and ev2 >= av2:
        return [av1,av2]
    if ev1 <= av1:
        return [av1,ev2]
    elif ev2 >= av2:
        return [ev1,av2]
    else:
        return [ev1,ev2]

def eteindre(coorEteinte,coorAllumee):
    ex1,ey1,ez1 = coorEteinte[0]
    ex2,ey2,ez2 = coorEteinte[1]
    ax1,ay1,az1 = coorAllumee[0]
    ax2,ay2,az2 = coorAllumee[1]
    if  ex2 < ax1 or ex1 > ax2 or \
        ey2 < ay1 or ey1 > ay2 or \
        ez2 < az1 or ez1 > az2:
        return [coorAllumee]
    newCoor = []
    coorX = splitMyShitUp(ex1,ex2,ax1,ax2)
    coorY = splitMyShitUp(ey1,ey2,ay1,ay2)
    coorZ = splitMyShitUp(ez1,ez2,az1,az2)
    x = eteintMoi(ex1,ex2,ax1,ax2)
    y = eteintMoi(ey1,ey2,ay1,ay2)
    z = eteintMoi(ez1,ez2,az1,az2)
    for c1 in coorX:
        for c2 in coorY:
            for c3 in coorZ:
                if x[0] != c1[0] or x[1] != c1[1] or y[0] != c2[0] or y[1] != c2[1] or z[0] != c3[0] or z[1] != c3[1]:
                    newCoor.append([[c1[0],c2[0],c3[0]],[c1[1],c2[1],c3[1]]])
    return newCoor

def countOn(lst):
    nb = 0
    for l in lst:
        x1,y1,z1 = l[0]
        x2,y2,z2 = l[1]
        dx = abs(x2-x1)+1
        dy = abs(y2-y1)+1
        dz = abs(z2-z1)+1
        nb += dx*dy*dz
    return nb

listOn = []

for r in reboot:
    # print("--------")
    # print(r)
    # print(listOn)
    # print("--")
    cmd = r[0]
    coor = r[1]
    newOn = []
    if r[0] == 0:
        for l in listOn:
            newOn += eteindre(coor,l)
            listOn = newOn
    else:
        newOn = [coor]
        for l in listOn:
            newNewOn = []
            for c in newOn:
                newNewOn += eteindre(l,c)
            newOn = newNewOn
        listOn += newOn
    #     print(newOn)
    # print(listOn)

count = countOn(listOn)
print(count)
print(count == saveNb)