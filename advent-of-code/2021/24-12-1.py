from time import time

WXYZ = [0,0,0,0]

def resetWXYZ():
    global WXYZ
    WXYZ = [0,0,0,0]
def launchProgram(program, startInput, verbose=False):
    global WXYZ
    if verbose:
        print("inputs:", startInput)
        print("état initial des variables",WXYZ)
    error = None
    currInput = 0
    for line in program:
        if verbose:
            print(line)
        instruction = line[:3]
        variables = [x if x in "wxyz" else int(x)for x in line[4:].split()]
        variables[0] = "wxyz".index(variables[0])
        if instruction == "inp":
            WXYZ[variables[0]] = startInput[currInput]
            currInput += 1
        if instruction == "add":
            WXYZ[variables[0]] += (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1])
        if instruction == "mul":
            WXYZ[variables[0]] *= (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1])
        if instruction == "div":
            if (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1]) == 0:
                error = "DIVISION PAR 0"
                break
            WXYZ[variables[0]] = WXYZ[variables[0]] // (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1])
        if instruction == "mod":
            if WXYZ[variables[0]] < 0 or (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1]) <= 0:
                error = "MODULO IMPOSSIBLE"
                break
            WXYZ[variables[0]] = WXYZ[variables[0]] % (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1])
        if instruction == "eql":
            if WXYZ[variables[0]] == (WXYZ["wxyz".index(variables[1])] if not isinstance(variables[1],int) else variables[1]):
                WXYZ[variables[0]] = 1
            else:
                WXYZ[variables[0]] = 0
        if verbose:
            print("état des variables",WXYZ)
    if verbose:
        if error is not None:
            print(error)
        else:
            print("TERMINE")
    return error
def importMonad():
    prog = [f.split('\n')[0] for f in open("24-monad")]
    listeProgs = []
    currProg = []
    for l in prog:
        if l == "inp w":
            if len(currProg) > 0 :
                listeProgs.append(currProg[:])
            currProg = [l]
        else:
            currProg.append(l)
    if len(currProg) > 0:
        listeProgs.append(currProg[:])
    return listeProgs
def launchAllMonad(monad, chiffres, combien=14, verbose=False):
    resetWXYZ()
    error = None
    for i in range(combien):
        error = launchProgram(monad[i], [chiffres[i]], verbose)
        if error is not None:
            print(error)
            break
    if error is None and WXYZ[3] == 0:
        print("OUI")
        return True
    elif verbose:
        print("NON") 
    return False
def launchMonad(monad, progNum, stateWXYZ, inp):
    global WXYZ
    WXYZ = stateWXYZ
    error = launchProgram(monad[progNum], [inp])
    if error is None:
        return WXYZ[:]
    return None
def hashState(state):
    return ".".join(map(str,state))
def unhashState(state):
    return [int(x)for x in state.split('.')]
def generateAllStates(monad, progNum, previousStates,onlyLast=False):
    global WXYZ
    t = time()
    print("generating all states for prog n°", progNum)
    k = len(previousStates.keys())
    pc = k//100
    if pc == 0: pc = 1
    print(k, "previous states")
    dico = {}
    cpt = 0
    for prevState in previousStates:
        cpt += 1
        if cpt%pc == 0:
            print(cpt//pc,"%")
        state = unhashState(prevState)
        for i in range(1+8*(onlyLast),10):
            newState = launchMonad(monad, progNum, state, i)
            # print(newState)
            if newState is not None:
                stateHash = hashState(newState)
                if stateHash not in dico or dico[stateHash][0] < i:
                    dico[stateHash] = (i, prevState)
                # print(dico[stateHash],stateHash)
    print("finished in", str(int(100*(time()-t))/100) + " secondes")
    return dico

monad = importMonad()
chiffres = [{}for i in range(14)]

t2 = time()
for i in range(14):
    chiffres[i] = generateAllStates(monad,i,chiffres[i-1] if i>0 else {"0.0.0.0":1}, i<5)
    print(len(chiffres[i].keys()))
print(time()-t2)
for c in chiffres[13].keys():
    a = c.split('.')
    if int(a[-1]) == 0:
        print(c)

possibles = []
for c in chiffres[13].keys():
    a = c.split('.')
    if int(a[-1]) == 0:
        possibles.append(c, chiffres[13][c])


# t = time()
# print(str(int(100*(time()-t))/100) + " secondes")