import itertools
d=[[1,1,1,0,1,1,1],[0,0,1,0,0,1,0],[1,0,1,1,1,0,1],[1,0,1,1,0,1,1],[0,1,1,1,0,1,0],[1,1,0,1,0,1,1],[1,1,0,1,1,1,1], [1,0,1,0,0,1,0],[1,1,1,1,1,1,1],[1,1,1,1,0,1,1]]
r=range(7)
m=lambda l:[[+(i in list(map(lambda x:"abcdefg".find(x),s)))for i in r]for s in l]
t=lambda n,p:[n[p[i]]for i in r]
print(sum([sum([sum([10**(3-i)*d.index(t(j[1][i],p))for i in range(4)])if all([(t(n,p)in d)for n in j[0]])else 0 for p in list(itertools.permutations(r))])for j in[(m(sorted(l[0].split(),key=len)),m(l[1].split()))for l in[f.split('|')for f in open("8")]]]))

#571