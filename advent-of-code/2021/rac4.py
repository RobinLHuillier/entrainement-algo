nb = -1
boards = []
x = 0
y = 0
for l in open("4"):
    if nb == -1:
        nb = [int(i) for i in l.split(',')]
        x = 0
        y = 0
        boards.append([])
    else:
        if y==5:
            x += 1
            boards.append([])
            y = 0
        if len(l) > 2:
            boards[x]+=[[int(i) for i in l.split()]]
            y += 1

def winBoard(b):
    return any([all([b[i][j]==-1 for i in range(5)])or all([b[j][i]==-1 for i in range(5)])for j in range(5)])

def posNb(n,b):
    for i in range(5):
        for j in range(5):
            if b[i][j] == n:
                return (i,j)
    return (-1,-1)

def calculatePoints(b):
    som = 0
    for i in range(5):
        for j in range(5):
            if b[i][j] != -1:
                som += b[i][j]
    return som

gagne = -1
lastNb = -1
found = False
newBoard = boards.copy()

for n in nb:
    if found:
        break
    boards = newBoard.copy()
    for i in range(len(boards)):
        pos = posNb(n,boards[i])
        if pos != (-1,-1):
            boards[i][pos[0]][pos[1]] = -1
        if winBoard(boards[i]):
            if len(boards) > 1:
                newBoard.remove(boards[i])
            else:
                gagne = calculatePoints(boards[i])
                lastNb = n
                found = True
                break

print(gagne*lastNb==20774)

#1404
#1591