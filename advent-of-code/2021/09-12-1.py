mat = []
for f in open("9"):
    l = f.split()[0]
    line = [1000] + [int(i) for i in l] + [1000]
    mat.append(line)
mat = [[1000]*len(mat[0])] + mat + [[1000]*len(mat[0])]

def sizeBasin(i,j):
    cur = mat[i][j]
    
    print(i,j, cur)
    size = 0
    mat[i][j] = 9
    if cur < 9:
        size += 1
    if mat[i+1][j] < 9 and mat[i+1][j] > cur:
        size += sizeBasin(i+1,j)
    if mat[i-1][j] < 9 and mat[i-1][j] > cur:
        size += sizeBasin(i-1,j)
    if mat[i][j+1] < 9 and mat[i][j+1] > cur:
        size += sizeBasin(i,j+1)
    if mat[i][j-1] < 9 and mat[i][j-1] > cur:
        size += sizeBasin(i,j-1)
    print(size)
    return size

basins = []
basinFound = False
#find low points
for i in range(1,len(mat)-1):
    for j in range(1,len(mat[0])-1):
        cur = mat[i][j]
        if cur < mat[i-1][j] and cur < mat[i+1][j] and cur < mat[i][j-1] and cur < mat[i][j+1]:
            if not basinFound:
                s = sizeBasin(i, j) #get the size of the basin
                basins.append((s, i, j))
                #basinFound = True

basins = sorted(basins, key=lambda x:x[0])[::-1]
print(basins[:3])
print(112*111*106)