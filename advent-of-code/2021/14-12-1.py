lines = [f.split('\n')[0]for f in open("14")]
poly = lines[0]
rules = {}
letters = set()
for l in poly:
    letters.add(l)
for i in range(2,len(lines)):
    depart, arrivee = lines[i].split(' -> ')
    rules[depart] = arrivee
    letters.add(arrivee)

dicoTemplate = {}
for k in rules.keys():
    dicoTemplate[k] = 0

countPairs = dicoTemplate.copy()
for i in range(len(poly)-1):
    pair = poly[i:i+2]
    countPairs[pair] += 1

def step(dic):
    nextDic = dicoTemplate.copy()
    for pair in countPairs.keys():
        nb = countPairs[pair]
        newLetter = rules[pair]
        newPair1 = pair[0] + newLetter
        newPair2 = newLetter + pair[1]
        nextDic[newPair1] += nb
        nextDic[newPair2] += nb
    return nextDic

def countMinMax(dic):
    count = {}
    for l in letters:
        count[l] = 0
    for pair in dic.keys():
        l1 = pair[0]
        l2 = pair[1]
        nb = dic[pair]
        count[l1] += nb
        count[l2] += nb
    mini = 1e40
    maxi = 0
    # on divise le compte par deux (chacune est comptée deux fois)
    for l in letters:
        count[l] = count[l]//2
    # on ajoute 1 pour première et dernière lettre
    count[poly[0]] += 1
    count[poly[-1]] += 1

    for l in letters:
        cpt = count[l]
        if cpt < mini:
            mini = cpt
        if cpt > maxi:
            maxi = cpt
    print(maxi, mini)
    return maxi - mini

for i in range(40):
    countPairs = step(countPairs)
print(countPairs, poly)
print(countMinMax(countPairs))



# def insert(s):
#     newPoly = ""
#     for i in range(len(poly)-1):
#         pair = poly[i:i+2]
#         newPoly += poly[i] + rules[pair]
#     newPoly += poly[-1]
#     return newPoly

# for i in range(40):
#     print(i,len(poly))
#     poly = insert(poly)

# mini = 1e9
# maxi = 0
# for l in letters:
#     cpt = poly.count(l)
#     if cpt < mini:
#         mini = cpt
#     if cpt > maxi:
#         maxi = cpt

# print(maxi-mini)
# impossible d'y aller en naïf, commence à prendre des temps de plusieurs secondes à partir de l'étape 20
# et ça va aller en empirant

