from copy import deepcopy
import sys
from time import time

x=15000
sys.setrecursionlimit(x)

matDepart = [[[1,8],[2,8]],[[1,4],[1,6]],[[2,2],[2,4]],[[1,2],[2,6]]]
### partie 1
# input : [[[1,8],[2,8]],[[1,4],[1,6]],[[2,2],[2,4]],[[1,2],[2,6]]]
# exemple : [[[2,2],[2,8]],[[1,2],[1,6]],[[1,4],[2,6]],[[2,4],[1,8]]]
### partie 2
# input : [[[1,8],[4,8],[3,6],[2,8]],[[1,4],[1,6],[3,4],[2,6]],[[4,2],[4,4],[2,4],[3,8]],[[1,2],[4,6],[2,2],[3,2]]]
# exemple : [[[4,2],[4,8],[3,6],[2,8]],[[1,2],[1,6],[3,4],[2,6]],[[1,4],[4,6],[2,4],[3,8]],[[4,4],[1,8],[2,2],[3,2]]]

profMax = 2
coutGlobal = 47000

t=time()

def destinationFinale(mat, num, pos=None):
    i = profMax
    j = 2*(num+1)
    while i > 0:
        if [i,j] in mat[num]:
            if pos == [i,j]:
                return "yesDaddy"
            i -= 1
        else:
            return [i,j]
    return None

def coutDeplacement(num, depart, arrivee):
    if depart[0] != 0 and arrivee[0] != 0 and depart[1] != arrivee[1]:  # on fait un ^ > v
        return (depart[0] + arrivee[0] + abs(depart[1]-arrivee[1]))*10**num
    return (abs(depart[0]-arrivee[0]) + abs(depart[1]-arrivee[1]))*10**num

def creerListeChemin(depart, arrivee):
    chemin = []
    if depart[0] > 0:
        for i in range(depart[0],0,-1):
            chemin.append([i,depart[1]])
    step = 2*(arrivee[1]>depart[1])-1
    for i in range(depart[1],arrivee[1]+step,step):
        chemin.append([0,i])
    if arrivee[1] > 0:
        for i in range(1,arrivee[0]+1):
            chemin.append([i,arrivee[1]])
    if len(chemin) > 0 and [depart[0],depart[1]] in chemin: # on retire la case depart, pas la peine
        chemin.remove([depart[0],depart[1]])
    return chemin

def deplacementPossible(mat, depart, arrivee, chemin=[]):
    if len(chemin) == 0:
        chemin = creerListeChemin(depart, arrivee)
    for c in chemin:
        for m in mat:
            if c in m:
                return False
    return True

def caseNonOccupee(mat, pos):
    for m in mat:
        if pos in m:
            return False
    return True

def listeDeplacementPossible(mat, num, depart):
    listeDep = []
    finale = destinationFinale(mat,num)
    if deplacementPossible(mat, depart, finale):
        listeDep.append(finale)
    elif depart[0] == 0: # on ne pouvait aller qu'à la dest finale
        return []
    if depart[0] != 0:
        # regarder case au dessus en 0:
        chemin = creerListeChemin(depart, [0,depart[1]])
        if not deplacementPossible(mat,depart,[0,depart[1]],chemin):
            return [] # on peut même pas sortir d'ici
        # regarder jusqu'où on peut aller à gauche
        i = depart[1]-1
        arretPotentiel = [0,i]
        while caseNonOccupee(mat, arretPotentiel) and i >= 0:
            listeDep.append(arretPotentiel)
            i -= 2
            if i==-1:
                i=0
            arretPotentiel=[0,i]
        # regarder jusqu'où on peut aller à droite
        i = depart[1]+1
        arretPotentiel = [0,i]
        while caseNonOccupee(mat, arretPotentiel) and i < 11:
            listeDep.append(arretPotentiel)
            i += 2
            if i==11:
                i=10
            arretPotentiel=[0,i]
    return listeDep

def deplacer(mat,num,depart,arrivee):
    mat2 = deepcopy(mat)
    cout = coutDeplacement(num,depart,arrivee)
    for i in range(2):
        if mat[num][i] == depart:
            mat2[num][i] = arrivee[:]
    return (mat2, cout)

def hashMaMatrice(mat):
    res = ""
    for m in mat:
        sortedM = sorted(m,key=lambda x:[x[0],x[1]])
        for x in sortedM:
            for y in x:
                if y == 10:
                    res+="a"
                else:
                    res+=str(y)
    return res

def unHashMaMatrice(hash):
    mat = []
    for i in range(4):
        miniHash = hash[profMax*2*i:profMax*2*(i+1)]
        letter = []
        for i in range(profMax):
            singleHash = miniHash[i*2:(i+1)*2]
            pos = []
            for y in singleHash:
                if y == "a":
                    pos.append(10)
                else:
                    pos.append(int(y))
            letter.append(pos)
        mat.append(letter)
    return mat

def iterate(mat, cout=0):
    global coutGlobal
    nextCandidatesMat = [hashMaMatrice(mat)]
    nextCandidatesCout = [cout]
    cpt = 0
    while len(nextCandidatesMat) > 0:
        newNextMat = []
        newNextCout = []
        cpt += 1
        k = len(nextCandidatesMat)
        print("itération " + str(cpt) + ": " + str(k) + " candidats, min actuel: " + str(coutGlobal))
        if k > 50000:
            pc = k//100
            #pc = 0
        else:
            pc = 0
        cpt2 = 0
        for n in range(len(nextCandidatesMat)):
            if pc != 0:
                cpt2 += 1
                if cpt2%pc == 0:
                    print(str(cpt2//pc) + "%")
            mat = unHashMaMatrice(nextCandidatesMat[n])
            cout = nextCandidatesCout[n]
            if cout > coutGlobal:
                # print("dépasse l'heuristique")
                continue
            deplacable = []
            arretMaintenant = False
            currentEstimation = 0
            for num in range(3,-1,-1):
                for pos in mat[num]:
                    finale = destinationFinale(mat,num,pos)
                    if finale != "yesDaddy":
                        if num == 3:
                            currentEstimation += coutDeplacement(num,pos,finale)
                        if deplacementPossible(mat,pos,finale):
                            newMat, cost = deplacer(mat,num,pos,finale)
                            newNextMat.append(hashMaMatrice(newMat))
                            newNextCout.append(cout+cost)
                            arretMaintenant = True
                            break
                        deplacable.append([num, pos])
                if arretMaintenant:
                    break
            if arretMaintenant:
                continue
            if cout+currentEstimation > coutGlobal: # seconde heuristique
               continue
            if len(deplacable) == 0:
                #print("terminé, cout: " + str(cout))
                if cout < coutGlobal:
                    coutGlobal = cout
                    print("nouveau cout min trouvé: " + str(cout))
                continue
            for d in deplacable:
                num, pos = d
                # print("---------")
                # print(pos, num)
                # print(destinationFinale(mat,num,pos))
                if destinationFinale(mat,num,pos) != "yesDaddy":
                    listeDep = listeDeplacementPossible(mat,num,pos)
                    # print(listeDep)
                    if len(listeDep) > 0:
                        for l in listeDep:
                            newMat, cost = deplacer(mat,num,pos,l)
                            newNextMat.append(hashMaMatrice(newMat))
                            newNextCout.append(cout+cost)
        nextCandidatesMat = newNextMat
        nextCandidatesCout = newNextCout

iterate(matDepart)
print(time()-t)

