# PROGRAMMES AUTO-GENERES
def monad0(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=13
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=8
    y*=x
    z+=y
    return [w,x,y,z]
def monad1(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=12
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=16
    y*=x
    z+=y
    return [w,x,y,z]
def monad2(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=10
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=4
    y*=x
    z+=y
    return [w,x,y,z]
def monad3(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-11
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=1
    y*=x
    z+=y
    return [w,x,y,z]
def monad4(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=14
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=13
    y*=x
    z+=y
    return [w,x,y,z]
def monad5(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=13
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=5
    y*=x
    z+=y
    return [w,x,y,z]
def monad6(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=12
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=0
    y*=x
    z+=y
    return [w,x,y,z]
def monad7(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-5
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=10
    y*=x
    z+=y
    return [w,x,y,z]
def monad8(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    x+=10
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=7
    y*=x
    z+=y
    return [w,x,y,z]
def monad9(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=0
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=2
    y*=x
    z+=y
    return [w,x,y,z]
def monad10(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-11
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=13
    y*=x
    z+=y
    return [w,x,y,z]
def monad11(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-13
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=15
    y*=x
    z+=y
    return [w,x,y,z]
def monad12(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-13
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=14
    y*=x
    z+=y
    return [w,x,y,z]
def monad13(a,w,x,y,z):
    w=a
    x=z
    if x<0:
       return None
    x=x%26
    z=z//26
    x+=-11
    x=int(x!=w)
    y=25
    y*=x
    y+=1
    z*=y
    y=w
    y+=9
    y*=x
    z+=y
    return [w,x,y,z]
def launchAutoProgram(num,inp,w,x,y,z):
    if num==0:
        return monad0(inp,w,x,y,z)
    if num==1:
        return monad1(inp,w,x,y,z)
    if num==2:
        return monad2(inp,w,x,y,z)
    if num==3:
        return monad3(inp,w,x,y,z)
    if num==4:
        return monad4(inp,w,x,y,z)
    if num==5:
        return monad5(inp,w,x,y,z)
    if num==6:
        return monad6(inp,w,x,y,z)
    if num==7:
        return monad7(inp,w,x,y,z)
    if num==8:
        return monad8(inp,w,x,y,z)
    if num==9:
        return monad9(inp,w,x,y,z)
    if num==10:
        return monad10(inp,w,x,y,z)
    if num==11:
        return monad11(inp,w,x,y,z)
    if num==12:
        return monad12(inp,w,x,y,z)
    if num==13:
        return monad13(inp,w,x,y,z)
