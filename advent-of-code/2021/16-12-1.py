f = [f.split()[0]for f in open("16")][0]
decode = {"0":"0000","1":"0001","2":"0010","3":"0011","4":"0100","5":"0101","6":"0110","7":"0111","8":"1000","9":"1001","A":"1010","B":"1011","C":"1100","D":"1101","E":"1110","F":"1111"}
binString = ""
for c in f:
    binString += decode[c]

def strToNum(str):
    num = 0
    for c in str:
        num = 2*num + int(c)
    return num

def readLiteral(str, head):
    numStr = ""
    readMe = True
    while readMe:
        if str[head] == "0":
            readMe = False
        numStr += str[head+1:head+5]
        head+=5
    return (strToNum(numStr),head)

def readPacket(str, head):  # -> version, newHead, num
    if head+6 >= len(str): #ajouts de bits pour compléter
        return None
    version = strToNum(str[head:head+3])
    id = strToNum(str[head+3:head+6])
    if id == 4:
        #litteral value
        num,newHead = readLiteral(str, head+6)
        return (version, newHead, num)
    else:
        #operator
        res = []
        lengthId = int(str[head+6])
        if lengthId == 0:
            lengthBits = strToNum(str[head+7:head+22])
            end = head+22+lengthBits
            newHead = head+22
            while newHead < end:
                newRes = readPacket(str, newHead)
                res.append(newRes)
                newHead = newRes[1]
        else:
            numPackets = strToNum(str[head+7:head+18])
            newHead = head+18
            for _ in range(numPackets):
                newRes = readPacket(str, newHead)
                res.append(newRes)
                newHead = newRes[1]
        v = version
        nums = []
        for r in res:
            v += r[0]
            nums.append(r[2])
        nb = 0
        if id == 0:
            nb = sum(nums)
        elif id == 1:
            nb = 1
            for n in nums:
                nb *= n
        elif id == 2:
            nb = min(nums)
        elif id == 3:
            nb = max(nums)
        elif id == 5:
            nb = 1 if nums[0] > nums[1] else 0
        elif id == 6:
            nb = 1 if nums[0] < nums[1] else 0
        elif id == 7:
            nb = 1 if nums[0] == nums[1] else 0
        return (v, newHead, nb)


print(readPacket(binString,0)[2])