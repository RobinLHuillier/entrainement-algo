lines = [f[:-1].split('-')for f in open("12")]
dicoPath = {k:[]for k in[i for s in lines for i in s]}
# dicoPath contient les routes possible à partir d'un point
# ex: dicoPath["start"] == ["A","b"]
#     à partir de "start" on peut aller soit à "A" soit à "b"
for line in lines:
    a,b = line
    dicoPath[a].append(b)
    dicoPath[b].append(a)

def isBigCave(cave):
    return cave==cave.upper()

allPath = []
def constructAllPath(current, pathVisited, doubleVisit=False):
    global allPath
    # si on a déjà visité la cave et que c'est une petite, on autorise une seule revisite
    if current in pathVisited and not isBigCave(current):
        if doubleVisit or current == "start":
            # mauvais chemin on s'arrête là
            return
        doubleVisit=True

    pathVisited.append(current)
    # si on arrive au bout, on l'ajoute à la liste des chemins possible et on arrête
    if current == "end":
        allPath += [pathVisited]
        return

    # on construit tous les chemins possible à partir de la position courante
    for nextCave in dicoPath[current]:
        constructAllPath(nextCave, pathVisited[::], doubleVisit)

constructAllPath("start",[])
print(len(allPath))

#119760