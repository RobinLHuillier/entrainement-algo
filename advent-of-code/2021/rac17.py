a,b,c,d=94,151,-156,-103
f=lambda a,b,x=0:[]if x*(x+1)/2>b else f(a,b,x+1)if x*(x+1)/2<a else[x]+f(a,b,x+1)
r,s,t,u=-(c+1),c,b,min(f(a,b))
g=lambda v,w,x,y,a,b,c,d:0 if x+v>b or y+w<c else 1 if x+v>=a and y+w<=d else g(v-(v>0),w-1,x+v,y+w,a,b,c,d)
print(len(set([(i,j)if g(i,j,0,0,a,b,c,d)else 0 for i in range(u,t+1)for j in range(s,r+1)]))-1)

# 5059
# 348