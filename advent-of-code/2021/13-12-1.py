lines = [f.split('\n')[0] for f in open("13")]
coor = []
folds = []
timeToFold = False
for l in lines:
    if len(l)==0:
        timeToFold = True
    elif timeToFold:
        var = l[11]
        nb = int(l[13:])
        folds.append((var, nb))
    else:
        x,y = l.split(',')
        coor.append((int(x),int(y)))

def retraitDoublon(l):
    l2 = []
    for x in l:
        if x not in l2:
            l2.append(x)
    return l2

def foldPaper(coor,fold):
    var,nb = fold
    newCoor = []
    for c in coor:
        x,y = c
        if (var == 'x' and x < nb) or (var == 'y' and y < nb):
            newCoor.append((x,y))
        elif var == 'x' and x > nb:
            x = nb - abs(x-nb)
            if x >= 0:
                newCoor.append((x,y))
        elif var == 'y' and y > nb:
            y = nb - abs(y-nb)
            if y >= 0:
                newCoor.append((x,y))
    newCoor = retraitDoublon(newCoor)
    return newCoor

print(len(coor))
for f in folds:
    coor = foldPaper(coor, f)
print(len(coor))
mat = [[0]*60 for i in range(10)]
for c in coor:
    x,y = c
    mat[y][x] = 1
lines = []
for i in range(10):
    string = ""
    for j in range(60):
        if mat[i][j]==0:
            string += "_"
        else:
            string += "O"
    lines.append(string)
for s in lines:
    print(s)