from time import time

t = time()

def genererPermuts():
    perm = []
    permut = [(1,2,3),(3,1,2),(2,3,1),(-1,3,2),(-3,2,1),(-2,1,3)]
    for p in permut:
        perm.append(p)
        x,y,z = p
        perm.append((x,z,-y))
        perm.append((x,-y,-z))
        perm.append((x,-z,y))
    return perm

permut = genererPermuts()

lines = [f.split('\n')[0]for f in open("19")]
scanners = []
for i in range(len(lines)):
    if len(lines[i]) == 0:
        scanners.append(scan)
    elif lines[i][1] == "-":
        scan = []
    else:
        x,y,z = [int(j)for j in lines[i].split(',')]
        scan.append((x,y,z))

def generateAllPermut(probe):
    perm = []
    for p in permut:
        cons = []
        for x in p:
            if x >= 0:
                cons.append(probe[x-1])
            else:
                cons.append(-probe[(-x)-1])
        perm.append(cons)
    return perm

def expandScannersPermut():
    for s in range(len(scanners)):
        for i in range(len(scanners[s])):
            scanners[s][i] = generateAllPermut(scanners[s][i])

expandScannersPermut()

def checkFacingTwoScanners(s,f,lprobes):
    probes1 = [x[f] for x in scanners[s]]
    distances = []
    countDistances = []
    for p1 in lprobes:
        x,y,z = p1
        for p2 in probes1:
            a,b,c = p2
            dist = (x-a,y-b,z-c)
            if dist not in distances:
                distances.append(dist)
                countDistances.append(1)
            else:
                i = distances.index(dist)
                countDistances[i] += 1
                if countDistances[i] >= 12:
                    return distances[i]
    return None

# coupler le tout
correctFacing = [0 for i in range(len(scanners))]
dist = [None for i in range(len(scanners))]
dist[0] = (0,0,0)
paire = [0 for i in range(len(scanners))]
paire[0] = 1
probes = [(p[0][0],p[0][1],p[0][2]) for p in scanners[0]]

while sum(paire) != len(scanners):
    for scanToCompare in range(len(scanners)):
        if paire[scanToCompare] == 0:
            for i in range(24):
                res = checkFacingTwoScanners(scanToCompare,i,probes)
                if res is not None:
                    dist[scanToCompare] = res
                    correctFacing[scanToCompare] = i
                    break
            if dist[scanToCompare] is not None:
                x,y,z = dist[scanToCompare]
                paire[scanToCompare] = 1
                print("scanner " + str(scanToCompare))
                print("orientation: " + str(permut[correctFacing[scanToCompare]]) + "  / position absolue: " + str(dist[scanToCompare]))
                print("nombres de beacons trouvés:" + str(len(probes)))
                # compter les probes
                s1 = scanToCompare
                f1 = correctFacing[s1]
                d1 = dist[s1]
                for p in scanners[s1]:
                    x,y,z = p[f1]
                    x += d1[0]
                    y += d1[1]
                    z += d1[2]
                    if (x,y,z) not in probes:
                        probes.append((x,y,z))
                break

print("nombre de probes:", len(probes))
print(time()-t)

def manhattan(p1, p2):
    a,b,c = p1
    x,y,z = p2
    return abs(a-x)+abs(b-y)+abs(c-z)

maxDist = 0
for d1 in dist:
    for d2 in dist:
        m = manhattan(d1,d2)
        if m > maxDist:
            maxDist = m
print(maxDist)

# 67s
# 430 beacons
# 11860 dist max