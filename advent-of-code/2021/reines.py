"""
#474
import time
def a(Q):print('\n'.join(['_ '*j+'x '+'_ '*(len(Q)-j-1)for j in Q]))
def b(Q,r,j):return not(any(Q[i]in[j,j-r+i,j+r-i]for i in range(r)))
def p(Q,r=0):
 if r==len(Q):return Q
 for i in range(len(Q)):
  if b(Q,r,i)and(f:=p(Q[:r]+[i]+Q[r+1:],r+1)):return f
 return None
def c(n):return p([-1]*n)
s,e,n=0,0,2
while e-s<5:
 s=time.time()
 Q=c(n)
 e=time.time()
 if Q is not None: 
  a(Q)
  print(f"N={n},time elapsed:{e-s}s")
 else:print(n,Q)
 n+=1
"""

#443
import time
def a(Q):return'\n'.join(['_ '*j+'x '+'_ '*(len(Q)-j-1)for j in Q])
def b(Q,r,j):return not(any(Q[i]in[j,j-r+i,j+r-i]for i in range(r)))
def p(Q,r=0,i=0):return Q if r==(k:=len(Q))else None if k==i else f if b(Q,r,i)and(f:=p(Q[:r]+[i]+Q[r+1:],r+1,0))else p(Q,r,i+1)
def c(n):return p([-1]*n)
s,e,n,t=0,0,2,time.time
while e-s<5:print(a(Q),f"N={n},time elapsed:{e-s}s")if(s:=t())and(Q:=c((n:=n+1)-1))and(e:=t())else print(n,Q)


"""
 if r==(k:=len(Q)):return Q
 if i==k:return None
 return f if b(Q,r,i)and(f:=p(Q[:r]+[i]+Q[r+1:],r+1,i))else p(Q,r,i+1)
"""