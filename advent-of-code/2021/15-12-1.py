import copy
from math import sqrt
import bisect

lines = [f.split()[0]for f in open("15")]
mat = []
for l in lines:
    line = []
    for c in l:
        line.append(int(c))
    mat.append(line)

# A star de ce que j'en comprends:
# on part de 0,0
# puis on lance l'algo avec deux liste: vu et aVoir
# on trie aVoir pour avoir le cumulé le plus bas en preums
# on pop chacun des aVoir: si c'est le point d'arrivée, on arrête et on renvoie le cumulé
# sinon on calcule le cumulé pour chaque voisin; 
#     si voisin existe déjà dans aVoir avec un cumulé plus élevé, on supprime on remplace par notre nouveau voisin
#     on ajoute le aVOir à la liste vu


# transforme la matrice de base selon les règles de l'ex 2
def expandMat(mat):
	#expand la ligne * 5	
	newMat = []	
	k = len(mat)
	for i in range(k):
		line = []    	
		for j in range(5):
			line+=[k+j for k in mat[i]]
		newMat.append(line)
	for i in range(len(newMat)):
		for j in range(len(newMat[0])):
			newMat[i][j] = ((newMat[i][j]-1)%9)+1
	for i in range(1,5):
		for j in range(k):
			newMat.append([k+i for k in newMat[j%k]])
	for i in range(len(newMat)):
		for j in range(len(newMat[0])):
			newMat[i][j] = ((newMat[i][j]-1)%9)+1
	return newMat

# renvoie les voisins d'une cellule (en prenant en compte les bords)
def voisins(mat,pos):
    n = len(mat)
    m = len(mat[0])
    x,y = pos
    vois = []
    if x > 0:
        vois.append((x-1,y))
    if y > 0:
        vois.append((x,y-1))
    if x < n-1:
        vois.append((x+1,y))
    if y < m-1:
        vois.append((x,y+1))
    return vois

def distance(a,b):
	i,j = a
	x,y = b
	return abs(i-x) + abs(j-y)

# ajouter une pondération pour accélérer A* (heuristique)
# ça l'accélère pas vraiment j'ai l'impression, mais au moins c'est rigolo
def distanceMat():
	newMat=[[0]*len(mat) for i in range(len(mat[0]))]
	arrivee = (len(mat), len(mat[0]))
	for i in range(len(mat)):
		for j in range(len(mat[0])):
			newMat[i][j] = distance((i,j),arrivee)
	return newMat

# beware the beast
def aStar(matriceModifiee, matriceDepart, matriceMarquee, matMarquee2, depart, objectif, dist=False, matriceDistance=[]):
    vus = []
    aVoir = []
    aVoir.append(depart)
    N = len(matriceDepart[0])
    M = len(matriceDepart)
    total = N*M
    pourcent = int(total/100)
    k = 0
    while len(aVoir) > 0:
        k+=1
        if k%pourcent == 0:
            print(k//pourcent,"%", len(vus), len(aVoir))
        aVoir = set(aVoir)
        if dist:
            aVoir = sorted(aVoir, key=lambda x:matriceModifiee[x[0]][x[1]]+matriceDistance[x[0]][x[1]])
        else:
            aVoir = sorted(aVoir, key=lambda x:matriceModifiee[x[0]][x[1]])
        obs = aVoir.pop(0)
        x,y = obs
        matMarquee2[x][y]=0
        if obs == objectif:
            return matriceModifiee[x][y]
        cost = matriceModifiee[x][y]
        for v in voisins(matriceModifiee,obs):
            i,j = v
            cout = cost + matriceDepart[i][j]
            if not(matMarquee[i][j]==1 or (matMarquee2[i][j]==1 and cout >= matriceModifiee[i][j])):
                matriceModifiee[i][j] = cout
                if matMarquee2[i][j]==0:
                    aVoir.append(v)
                    matMarquee2[i][j]=1
        matMarquee[x][y] = 1
    return None

mat[0][0]=0

matMarquee = [[0]*len(mat[0]) for i in range(len(mat))]
matMarquee2 = [[0]*len(mat[0]) for i in range(len(mat))]
secMat = copy.deepcopy(mat)
res = aStar(secMat, mat, matMarquee, matMarquee2, (0,0), (len(mat)-1, len(mat[0])-1))
#print(res)
#for i in mat:
#    print(i)
#print("arrivee")
#for i in secMat:
#    print(i)

mat[0][0]=1
mat = expandMat(mat)
mat[0][0]=0
secMat = copy.deepcopy(mat)
matMarquee = [[0]*len(mat[0]) for i in range(len(mat))]
matMarquee2 = [[0]*len(mat[0]) for i in range(len(mat))]
distMat = distanceMat()
res = aStar(secMat, mat, matMarquee, matMarquee2, (0,0), (len(mat)-1, len(mat[0])-1), True, distMat)
#print(res)
for i in mat:
    print(i)
print("arrivee")
for i in secMat:
    print(i)