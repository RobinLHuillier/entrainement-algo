a=[f.split('\n')[0]for f in open("20")]
t=lambda x:''.join([str(int(c=='#'))for c in x])
b=t(a[0])
c=[t(f)for f in a[2:]]

def e(c,b,p):
    m=len(c)
    n=len(c[0])
    for i in range(m):
        c[i]=p*2+c[i]+p*2
    c=[p*(n+4)]*2+c+[p*(n+4)]*2
    y=['.'*(n+2)for _ in range(m+2)]
    for i in range(1,m+3):
        for j in range(1,n+3):
            y[i-1]=y[i-1][:j-1]+b[int(c[i-1][j-1:j+2]+c[i][j-1:j+2]+c[i+1][j-1:j+2],2)]+y[i-1][j:]
    return y

for i in range(50):
    c=e(c,b,str(i%2))
print(sum([i.count('1')for i in c]))

# 19743
# 557