#données -> matrice
lines = [f for f in open("11")]
mat = []
for l in lines:
    mat.append([int(c)for c in l.split()[0]])
#calcul des flash
nbFlash = 0
simultane = False
step = 0
while not simultane:
    step += 1
    #print("-----\nSTEP: ",step)
    #for i in range(10):
    #    print(mat[i])
    flash = []
    for i in range(10):
        for j in range(10):
            mat[i][j] += 1
            if mat[i][j] > 9:
                flash.append((i,j))
                nbFlash += 1
    while len(flash)>0:
        i,j = flash.pop(0)
        if i > 0:
            mat[i-1][j] += 1
            if mat[i-1][j] == 10:
                flash.append((i-1,j))
                nbFlash += 1
        if i < 9:
            mat[i+1][j] += 1
            if mat[i+1][j] == 10:
                flash.append((i+1,j))
                nbFlash += 1
        if j > 0:
            mat[i][j-1] += 1
            if mat[i][j-1] == 10:
                flash.append((i,j-1))
                nbFlash += 1
        if j < 9:
            mat[i][j+1] += 1
            if mat[i][j+1] == 10:
                flash.append((i,j+1))
                nbFlash += 1
        if i > 0 and j > 0:
            mat[i-1][j-1] += 1
            if mat[i-1][j-1] == 10:
                flash.append((i-1,j-1))
                nbFlash += 1
        if i > 0 and j < 9:
            mat[i-1][j+1] += 1
            if mat[i-1][j+1] == 10:
                flash.append((i-1,j+1))
                nbFlash += 1
        if i < 9 and j > 0:
            mat[i+1][j-1] += 1
            if mat[i+1][j-1] == 10:
                flash.append((i+1,j-1))
                nbFlash += 1
        if i < 9 and j < 9:
            mat[i+1][j+1] += 1
            if mat[i+1][j+1] == 10:
                flash.append((i+1,j+1))
                nbFlash += 1
    simul = True
    for i in range(10):
        for j in range(10):
            if mat[i][j] > 9:
                mat[i][j] = 0
            else:
                simul = False
    if simul:
        simultane = True

print("nb step:",step)