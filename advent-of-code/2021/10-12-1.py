lines = [f+" " for f in open("10")]
corres = {"[":"]","(":")","<":">","{":"}"}
valeurs = {"(":1, "[":2, "{":3, "<":4} 
valSomme = {")":3, "]":57, "}":1197, ">":25137}
somme = 0
scores = []
cpt = -1
for l in lines:
    cpt += 1
    stack = []
    for c in l:
        if c in "[({<":
            stack.append(c)
        else:
            prec = stack.pop(-1)
            if corres[prec] != c:
                if c in "])}>":
                    if c == ")":
                        somme += 3
                    elif c=="]":
                        somme += 57
                    elif c=="}":
                        somme += 1197
                    elif c==">":
                        somme += 25137
                    break
                else:
                    score = 0
                    stack.append(prec)
                    print("line:", cpt, " / stack:", stack)
                    while len(stack) > 0:
                        prec = stack.pop(-1)
                        score *= 5
                        score += valeurs[prec]
                    scores.append(score)
                    print("score:", score)
                    break
scores = sorted(scores)
print(scores)
print("somme:",somme == 392139)
print("score:",scores[int((len(scores))/2)] == 4001832844)
print(len(scores), int((len(scores))/2))